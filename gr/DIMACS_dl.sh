#!/bin/sh


#
# This script will automatically download graphs from the DIMACS 9
# challenge as provided in http://www.dis.uniroma1.it/~challenge9
#

#
# These files are directly compatible with CUCH. However, to reduce
# overhead during extensive testing, it is recommended to transform
# them into .el format using CUCH with the appropriate command options
# beforehand (detailed description of command options provided in
# Instructions.txt)
#

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.USA.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.USA.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.CTR.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.CTR.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.W.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.W.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.E.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.E.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.LKS.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.LKS.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.CAL.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.CAL.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.NE.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.NE.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.NW.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.NW.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.FLA.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.FLA.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.COL.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.COL.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.BAY.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.BAY.gr.gz

wget http://users.diag.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.NY.gr.gz
wget http://users.diag.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.NY.gr.gz

