# CUCH: A CUDA Contraction Hierarchies / PHAST Implementation

Copyright (c) 2020 Louisiana State University

## Description

CUCH is a Linux CUDA (for NVidia GPUs) package for computing the
contraction hierarchy of a graph and for performing single-source,
shortest-path (SSSP) queries on the contracted graph. SSSP queries on
a contracted graph are much faster than queries on the original
graph. For example, on an NVidia Tesla V100 GPU a LonestarGPU SSSP
query of the DIMACS USA travel-time graph takes 287 ms, but a CUCH
query of the contracted USA graph takes only 5.3 ms. CUCH takes 1.57 s
to contract the graph and so time is saved for 6 or more queries:
0.287 * 6 = 1.722 s with LonestarGPU and 1.57 + 0.0053 * 6 = 1.602 s
with CUCH.

The contraction performed by CUCH is an adaptation of [Contraction
Hierarchies developed by Geisberger, Sanders, Schultes, and
Delling](http://dl.acm.org/citation.cfm?id=1788888.1788912). The SSSP
query is an adaptation of [PHAST developed by Delling, Goldberg,
Nowatzyk, and Werneck](http://dx.doi.org/10.1109/IPDPS.2011.89).

CUCH was developed by Roozbeh Karimi with contributions from David M.
Koppelman (koppel@ece.lsu.edu) and Chris J. Michael. For a description
of an initial version see [Roozbeh Karimi, David M. Koppelman, and
Chris J. Michael, "GPU road network graph contraction and SSSP query".
ICS 19](https://doi.org/10.1145/3330345.3330368). Those publishing
work based on CUCH are asked to cite the ICS 19 paper. BibTeX entries
for the paper and related work can be found in etc/cuch.bib.

The code here includes extensions and improvements to the original
CUCH as well as other tools associated with CUCH such as a simple high
performance CPU Dijkstra query, APSP queries, integration with the
Boost library SSSP query, integration with NVgraph library SSSP query,
and the ability to transform graphs prepared by the original CH code
by Geisberger et al. for use in CUCH.


## Prerequisites

The code has been tested on Red Hat Enterprise Linux 7 and 8, and
Fedora 32 systems equipped with NVidia GPUs from Maxwell (CC 5.x) up
to Turing (CC 7.5) GPUs running CUDA 10.1 to CUDA 11. It will likely
run on other Linux distributions that meet the prerequisites after
making minor changes.

At least the following are needed:

 - A Linux system with gcc 6.4 or later set up for code development.
 - CUDA Toolkit 10.1 or later.
 - Boost Libraries version .66 or later.
 
This list is not complete. Until more volunteers can be found to help
with CUCH packaging, users are expected to be able to identify missing
packages by examining error messages during the build.


## Configuration and Building

CUCH expects a CUDA installation at /usr/local/cuda. To use a
different CUDA location set environment variable CUDAPATH. CUCH by
default will build code for the GPU attached to the build host. To
build for a different GPU or for multiple GPUs set the variable
GPU_CCS in Makefile to the Compute Capabilities of those GPUs. (See
the Makefile comments for more details.)

To build CUCH run make in the CUCH src directory. For example:

    cd CUCHROOT/src
    make
    
The makefile will create an executable named "cuch" which is used for
running CUCH. To quickly test CUCH within the src directory use the
command

    ./cuch -a grph_cu_CH -f ../gr/LA.el -o LA --K-dy=0 --cu-c=0 --query-w=0 -s 70803
    
This will contract the DIMACS Louisiana graph (included with CUCH) and
query source node 70803. Sample output is shown below:

    ** CUCH: A CUDA Contraction Hierarchies / PHAST Implementation
    ** Copyright (c) 2020 Louisiana State University

    Using GPU 0: GeForce RTX 2080 SUPER
    Reading graph from ../gr/LA.el.bz2
    Graph has 413574 nodes, 988458 edges, mean deg 2.39.
    Starting graph contraction...completed in 77.872 ms, edge growth 0.919.
    SSSP querying node 70803 (level 2), distance and predecessor.
    Query complete, 2.554 ms query + 1.223 ms result to host.
    Writing performance data to LA.cu_stats
    CUCH exiting normally.


## Input Graphs

CUCH comes with the DIMACS Louisiana graph. Script
CUCHROOT/gr/DIMACS_dl.sh is provided to download additional graphs.
This script will download a large number of graphs, so one might want
to edit the script to download only the graphs that are needed.


## Running CUCH 

Basic instructions along with description of options along with some
examples as well as description of some file formats are provided here
to facilitate use of this toolkit.

For a simple demonstration of CUCH use the provided script file
src/CUCH_demo.sh (this demo requires internet connection in order to
download a test graph from DIMACS 9 library.)

CUCH toolkit is controlled by command line arguments. The following
lists these along with some examples:

  -a <app>
    Required argument, determines the function performed by the
    toolkit. A list of valid <app> is listed here.

      grph_cu_CH
        CUCH preprocessing and optional CUCH (PHAST) SSSP query.

      grph_cu_CH_prep
        CUCH preprocessing.

      grph_cu_CH_query
        CUCH (PHAST) SSSP query using an existing preprocessed graph
        file

      grph_gch_2_cuch
        Reads file containing CH prepared by Geisberger code and
        prepares CUCH file for it (determines levels, performs APSP on
        top <1024 nodes)

      dij_st
        CPU Dijkstra SSSP (uses binary heap)

      dij_boost
        CPU BOOST Dijkstra SSSP

      dij_nvgraph
        CUDA NVGRAPH SSSP

      grph_cu_APSP
        blocked cuda APSP grph_st_APSP CPU blocked APSP

      grph_st_APSP_dij
        CPU APSP using multiple Dijkstra calls

      grph_cu_APSP_bench
        Benchmarks blocked APSP on current system for different sizes
        and writes benchmark results to disk (takes no other
        arguments)

      grph_gr_2_st
        Transform .gr files (DIMACS) to .el file (CUCH edge list)

      grph_st_cleanup
        Find and remove duplicate/self edges

      grph_st_scale

        Prints edge weight stats (min, mean, max) and scales down all
        edge values by the factor selected by user

      grph_st_stats
        Scans a graph and prints certain basic stats about it

      path_evaluate
        Tests correctness of a query SSSP path by comparing the
        distance and path (parents) with results from a Dijkstra
        search, takes into account valid alternate routes path_compare
        Compares two SSSP query results, checking for distance and
        path (parents)

      path_APSP_compare
        Compares two APSP query results, checking for distance and
        path (parents)


  -f <file>
    Primary input file. Depending on <app> either a preprocessed graph
    or a simple graph. For simple graphs, supports both .el and .gr
    files as well as certain compressed format copies of them.

    Examples:
      '-f ./gr/USA-road-d.USA.gr.gz'
      gzip archive of a .gr input graph

      '-f USA_d_clean.el.bz2'
      bzip2 archive of a .el input graph

      '-f USA_d_CUCH.bin'
      Binary file containing a preprocessed CUCH graph 

      '-f USA_d_CUCH.cu_CHD'
      Text file containing a preprocessed CUCH graph 


  -b <file>
    Secondary input file. Used for path_evaluate, path_compare,
    path_APSP_compare Either a .path (SSSP result) or .APSP (APSP
    result) file


  -s <src>[,<src>]*
    Source nodes for SSSP query and verification.
      Sources are used for queries with -a grph_cu_CH and -a grph_cu_CH_query.
      Sources are used for verification with -v1 or -v2 and
       -a grph_cu_CH_prep: contracted graph verified.
       -a grph_cu_CH: contracted graph and query results verified.
       -a grph_cu_CH_query: query results verified.
    If <src> is a non-negative integer, it specifies a particular node id.
    If <src> is of the form n<int> and <int> is a non-negative integer,
      generate <int> randomly chosen sources uniformly distributed over
      node ids. Random numbers are generated using the C++11 library
      std::mt19937 random number engine.
    If <src> is of the form s0 seed the random number generator using
      the C+11 library std::random_device. Each run will use different
      sources in typical cases (e.g., more than one node in graph).
    If <src> is of the form s<int> and <int> is a positive integer,
      seed the random number generator with <int>.
    If no s<int> argument is present then s0 is assumed.
    If <src> is of the form s<int1>n<int2> or n<int2>s<int1>
      treat it as s<int1>,n<int2>.

    Examples:
     '-s 5,11,s1n20'
       Set two fixed-source nodes, 5 and 11, along with 20 randomly
       selected nodes using seed 1.

     '-s 5,11,n20'
       Set two fixed source nodes, 5 and 11, along with 20 randomly
       selected nodes (each run will have a new random set).

     ./cuch -a grph_cu_CH -f gr/LA_clean.el -s 100
       Contract, then perform SSSP query on source node 100

     ./cuch -a grph_cu_CH -f gr/LA_clean.el -s 3,14,15
       Contract, then perform SSSP query on source nodes 3, 14, and 15.

     ./cuch -a grph_cu_CH_prep -v1 -f gr/LA_clean.el -s 3,14,15
       Contract and verify contracted graph by performing CPU
       queries of nodes 3, 14, and 15.

     ./cuch -a grph_cu_CH -f gr/LA_clean.el -s 777,888,s1n20
       Contract, then perform SSSP query on source nodes 777 and 888
       as well as 20 randomly chosen nodes using seed 1. The same
       nodes will be chosen each time.

     ./cuch -a grph_cu_CH -f gr/LA_clean.el -s 777,888,n20
       Contract, then perform SSSP query on source nodes 777 and 888
       as well as 20 randomly chosen nodes using. A different
       20 nodes will be chosen each time (unless the graph is small).


  -o <file>
    Output file name base. Will be used to construct file names
    for output by appending an extension such as .el.bz2 or .cu_stats.

    Examples:
      '... -a grph_st_cleanup ... -o USA_d_clean'
      Writes the file USA_clean.el.bz2, a bzip2 archive containing a
      cleaned up graph

      '... -a grph_cu_CH_prep ... -o USA_d_CUCH --bin-dump 1'
      Writes files USA_d_CUCH.bin and USA_d_CUCH.cu_stats, containing
      a binary preprocessed graph and collected stats associated with
      the CUCH preprocessing respectively


  --verbose <int>
    CUCH specific option. Controls whether and how much tuning
    information is printed. If the --verify option is not present,
    --verbose also specifies whether verification is performed.
    0: [default] Print only information useful to an end user. Don't
       verify.
    1: Print some tuning information. Verify once. (--verify 1)
    2: Print a large amount of tuning information, such as
       characteristics of the graph at each iteration of the contraction
       process, but print it after contraction so that timing is not
       effected by print overhead. Verify multiple steps (--verify 2)
    3: Same as 2, but print information immediately and verify
       at each contraction iteration. Verify each iteration (--verify 3).
    
  --verify <int>
    Used for grph_cu_CH (contract and query) and grph_cu_CH_prep
    (contract only) apps. Controls whether contraction and query are
    verified. A query is verified by comparing the result of a CUCH
    SSSP query against an SSSP query performed by a reliable CPU
    algorithm. Contraction is verified by performing self-consistency
    checks and trial SSSP queries on the graph during and after
    contraction. Verification takes much longer than the operation
    being verified, and so should only be used for testing CUCH. The
    argument specifies the verification level as follows:
      0:  Don't verify.
      1:  Verify as late as possible (after each query or for non-query
          runs, after the contracted graph is complete).
      2:  Verify raw contracted graph, final contracted graph,
          and each query result.
      3:  Verify at each iteration in the contraction process,
          then the raw and final graph, and each query.
      -1: [default] Set based on verbosity level.
    Use 0 when not testing. Use 1 to check whether there are any
    errors. Use 2 or 3 to narrow down the location of the error.

  --gpu-require-nmps <int>
    CUCH specific option. On systems with multiple GPUs select the GPU
    with matching number of MPs.  Default, 0, will select Any GPU

  --gpu-require-idx <int>
    CUCH specific option. On systems with multiple GPUs select GPU
    based on index. Default, -1, will select any GPU

  --cu-chd-write <bool>
    CUCH specific option. grph-cu-CH-prep writes preprocessed graph
    file onto disk. Default, true

  --luby-dynamic-iter <bool>
    CUCH specific option. Number of Luby's iteration is selected
    dynamically to improve selection fraction. Default, true

  --luby-deterministic <bool>
    CUCH specific option. Luby's algorithm uses deterministic orders,
    also fixes number of Luby iterations to 3 (overrides
    luby-dynamic-iter). Default, false

  --elist-sort <bool>
    CUCH specific option. Sort edge list of each node based on
    neighbor index for both forward and backward graphs for easier
    debugging. Default, false

  --wps-oracle <double>
    CUCH specific option. Use a CPU Dijkstra search for WPS during
    shortcut (CPU times not counted).  Default, 0, disabled. 0.5,
    unlimited search. <N>, N hop limited search. -0.5, unlimited
    search but results not applied, shows histogram for stats
    collection. <-N>, N hop limited search but results not applied,
    shows histogram for stats collection

  --cull-ideal-hops <int>
    CUCH specific option. Cull longcut edges identified by CPU hop
    limited Dijkstra search. Default, 0, do nothing. 1, perfect search
    without hop limit. <N> larger than 1, use N-hop limited search for
    culling. -1, perfect search, but only collect stats, preprocessing
    continues similar to 0

  --cull-threshold-tpn <int>
    CUCH specific option. Add a threshold for minimum tpn (ceil lg max
    deg) to allow culling, works only if culling is enable. Default, 0

  --contract-one-hop <bool>
    CUCH specific option. If true, Use 1 hop WPS for
    contract. Default, false, use 2 hop for contract

  --longcuts-cull <int>
    CUCH specific option. 0, no culling. Default, 1, cull in score
    WPS. 2, cull in contract WPS

  --update-half <double>
    CUCH specific option. 0.0, update/extract both forward and
    backward graphs. Default, 1.0, update and extract only the forward
    graph, and build backward graph from it at each iteration

  --C-dynamic <int>
    CUCH specific option. Default, 0, use a constant C. 1, optimize
    for query speed. 2, optimize for preprocessing speed.

  --C-set <float>
    CUCH specific option. When --C-dynamic is 0, select value of C
    between 0.0 and 1.0. Default value 0.3, default selected if out of
    range value selected.

  --luby-dynamic-iter <bool>
    CUCH specific option. False, 2 iterations of Luby's MIS at each
    iteration. Default, True, selects number of Luby iterations
    dynamically to improve contraction fraction.

  --K-bm-autogen <bool>
    CUCH specific option. False, use the existing APSP benchmark
    results (on a new system uses collected benchmark results from a
    P100). Default, True, looks for existing APSP results, if a new
    system, collects APSP benchmark results and uses them.

  --K-dynamic <int>
    CUCH specific option. Default, 0, use a constant K. 1, optimize
    for query speed. 2, optimize for preprocessing speed.

  --K-set <int>
    CUCH specific option. When --K-dynamic is 0, select value of C
    between 64 and 3328. Default value 1024. default selected if out
    of range value selected.

  --wps-hash-loop-after-collision <bool>
    CUCH specific option. Default, False, treat hash collisions during
    WPS as no found WP. True, resolve collisions by looping over the
    F_B neighbors to find potential WPs (can improve edge growth at
    cost of slower preprocessing)
    
  --query-distance-only <bool>
    If false (default), SSSP queries compute the distance from the source
    and the predecessor node on a shortest path from the source. If true,
    only the distance is computed.

  --bin-dump <bool>

    CUCH prep specific option. Default, True, writes preprocessed CUCH
    graph as a binary file. False, writes result as text file (larger
    file, slower, for debugging).


## File Formats

<FILE>.el Input graph file, stores the adjacency list as text:

  Line 0: 'V,E'
    where V is number of nodes and E is number of edges

  Line 1: 'i,d'
    i is node index and d is number of edges going out from node i

  Lines 2 to d+1: 'j:u,w'
    j is edge number within the adjacency list of node i (e.g. line 2
    would be 0). u is node index of the destination of the edge. w is
    the weight of the edge

  remainder of the file follows similarly for each node and its
  adjacency list (Last line would be line V+E)



<FILE>.path Path file, stores SSSP or P2P query results as text:

  Line 0: 'V, S, D'
    V is number of nodes, S is source node's index and D is
    destination node's index (for P2P only)

  Lines 1 to V: 'p:i,w'
    p is parent index, i is node index (always equal to Line number
    minus one), w is the shortest distance to node i which goes
    through node p.
    p is set as '-' if: node i is unreachable (graph is disjoint), not
    reached by the time query is settled (in case of P2P) or the
    source node



<FILE>.APSP APSP path file, stores APSP results as text:

  Line 0: 'V'
    V is number of nodes.

  Lines 1 to V*V: 'p:i->j,w'
    p is parent index, i is source node index, j is destination node
    index, w is the shortest distance from node i to j which goes
    through node p.
    p is set as '-' if: node j is unreachable from i (graph is
    disjoint) or if j is the source node (i==j)



<FILE>.cu_CHD CUCH graph file, stores preprocessed graph as text. It
    includes metadata related to inner workings of CUCH data
    structures and query and can be very large. Certain values might
    not be correct depending on stats collection settings etc. (use
    only if you know what you're doing!):

  Line 0: 'V'
    V is number of nodes

  Line 1: 'D'
    D is maximum degree of the graph

  Line 2: 'T'    
    Number of threads allocated per node

  Line 3: 'O'
    O is the size (number of nodes) is the overlay graph

  Line 4: 'R'
    R is the maximum rank of nodes

  Line 5: 'WU'
    WU is maximum weight in the upward graph

  Line 6: 'WB'
    WB is maximum weight in the downward graph

  Line 7: 'EUF'
    EUF is length of upward forward graph's edge list

  Line 8: 'EUB'
    EUB is length of upward backward graph's edge list

  Line 9: 'EDB'
    EDB is length of downward backward graph's edge list

  Line 10: 'i,r,l,idx,idxi,duf,euf,dub,eub,ddb,edb,puf,pub,pdb'

    i is node index in the re-ordered CUCH graph. r is rank of the
    node. l is the level of the node (in graphs prepared by CUCH
    l==r-1). idx is node index of i in the original (input graph) node
    ordering. idx_i is node index of node i from original node
    ordering in the re-ordered CUCH graph (inverse of idx). duf, dub
    and ddb are number of edges for node i in the upward forward,
    upward backward and downward backward graphs respectively. puf,
    pub and pdb are the edge index indicating start of edge group
    belonging to node i in the edge lists of the upward forward,
    upward backward and downward backward graphs respectively.  euf,
    eub and edb are number of nodes in the corresponding edge group
    for the upward forward, upward backward and downward backward
    graphs respectively. If i is the p'th node of level l and l is
    smaller than NNIEG_OVER_ALLOC_CUT_OFF (set to 100), euf, eub and
    edb correspond to the p'th edge group, if level l has less than p
    edge groups, its value is ignored. If l is larger than
    NNIEG_OVER_ALLOC_CUT_OFF, for each level NNIEG_OVER_ALLOC_RATIO
    (set to 2) times as many edge groups can exist, for example if
    level 100 has 5 nodes starting from node 5000, euf for first edge
    group of level 101 would be located at i=5010 rather than i=5005.
    In levels higher than NNIEG_OVER_ALLOC_CUT_OFF, the following line
    will contain the remaining euf, eub, edb values.

  Line 11*: 'euf,eub,edb'
    If l>=NNIEG_OVER_ALLOC (read description above). In such case the
    following lines would be pushed further down accordingly

  Lines 11 to 10+duf: 'j,u,i'
    j is edge number within the adjacency list of node i in the upward
    forward graph. u is node index of the destination of this edge.

  Lines 11+duf to 10+duf+dub: 'j,u,i,w,m'
    j is edge number within the adjacency list of node i in the upward
    backward graph. u is node index of the destination of this edge. w
    is the weight of the edge. m is the midpoint of this edge (if a
    shortcut), otherwise, it is set as inf. Note that in CUCH, for
    shortcuts consisting of more than two edges, midpoint corresponds
    to the node closest to the end node of the shortcut, for example
    for the shortcut corresponding to path abcd, midpoint would be c
    even if b has a higher rank than c and was contracted last.

  Lines 11+duf+dub to 10+duf+dub+ddb: 'j,u,i,w,m'
    similar as above for the downward backward graph

  The above pattern continues for all V nodes. The following stores
  the graph corresponding to the uncontracted overlay nodes. (assume
  next line is line A)
  
  Line A: 'OLGO,WO,EO'
    O is number of nodes in overlay graph. WO is maximum weight in the
    overlay graph. EO is size of the edge list of the overlay
    graph. Note that this line always starts with the letters 'OLG',
    for example 'OLG500,30,3000'

  Line A+1: 'i,d,p'
    i is node index in the overlay graph. d is the degree of node i in
    overlay graph. p is index of first edge of node i in the overlay
    graph.

  Lines A+2 to A+1+d: 'j,u,w,m'
    j is edge number within the adjacency list of node i in the upward
    backward graph. u is node index of the destination of this edge. w
    is the weight of the edge. m is the midpoint of this edge (if a
    shortcut), otherwise, it is set as inf. Note that in CUCH, for
    shortcuts consisting of more than two edges, midpoint corresponds
    to the node closest to the end node of the shortcut, for example
    for the shortcut corresponding to path abcd, midpoint would be c
    even if b has a higher rank than c and was contracted last.

  The above pattern continues for all O nodes. The following stored
  the results for the APSP of the overlay graph. (assume next line is
  line B)
  
  Line B: 'OLPk'
    k is size of the overlay APSP (overlay size rounded up by
    64). note that this line always starts with the letters 'OLP', for
    example 'OLP1024'

  Lines B+1 to B+V*V: 'p:i->j,w'
    p is parent index, i is source node index, j is destination node
    index, w is the shortest distance from node i to j which goes
    through node p.
    p is set as '-' if: node j is unreachable from i (graph is
    disjoint) or if j is the source node (i==j)


