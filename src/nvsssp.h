#ifndef __NVSSSP_H__
#define __NVSSSP_H__

#include "main.h"
#include "graphs.h"

void nv_sssp_run(path_t *path, graph_st *graph_in, uint32_t src);


#endif
