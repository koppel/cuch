#ifndef __DIJ_H__
#define __DIJ_H__

#include "graphs.h"
#include "main.h"

typedef struct dij_stats
{
  size_t num_updated_edges;
  size_t num_relaxed_edges;
  size_t num_settled_nodes;
  long int time_wall_us;
}dij_stats_t;

dij_stats_t dij_st_ref_dest(graph_st_t *graph, nodeid_t src, nodeid_t dest, path_t *result);
dij_stats_t dij_bi_st_ref_dest(graph_st_t *graph_f, graph_st_t *graph_b, nodeid_t src, nodeid_t dest, path_t *result);
void dij_stats_init(dij_stats_t *ds);
void dij_stats_dump(FILE *fp, dij_stats_t ds);

typedef struct timeval timeval_t;

int timeval_subtract (timeval_t *result, timeval_t *x, timeval_t *y);
 
#endif
