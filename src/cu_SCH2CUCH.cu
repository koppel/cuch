/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>

#include "util/cuda-gpuinfo.h"

#include "cu_SCH2CUCH.h"
#include "cu_CH.cuh"

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <assert.h>
#include <cstdint>
#include <vector>
#include <string.h>

#include <nppdefs.h>

// #define ST
// guarantees that different syncthreads() calls will not affect each other by checking line number
// allows for threads to skip a syncthreads() (only applies to the last syncthread in a kernel)
#ifdef ST

#define __syncthreads()                                                       \
  { sync_threads();                                                           \
    st_line = atomicExch(&sync_count_sh, __LINE__);                           \
    assert(st_line == 0 || st_line == __LINE__);                              \
    sync_threads();                                                           \
    sync_count_sh = 0; }

#endif
 
using namespace std;

struct ch_layout_header
{
  uint8_t magic[4];
  uint32_t version, n_nodes, n_original_edges, n_shortcut_edges;
};

struct ch_layout_original_edge
{
  uint32_t src, dst, wht, flags;
};
struct ch_layout_shortcut_edge
{
  uint32_t src, dst, wht, flags, mid;
};


int graph_GCH_get_from_file(graph_CH_bi_t **graph, FILE *f)
{

  if ( f == NULL ) return -1;

  const uint8_t ch_magic[4] = {0x43, 0x48, 0x0d, 0x0a};

  ch_layout_header header;
  fread(&header, sizeof(header), 1, f);
  for ( int i=0; i<4; i++ ) assert( header.magic[i] == ch_magic[i] );

  printf("Num nodes %u, orig edges %u, shc edges %u\n",
         header.n_nodes, header.n_original_edges, header.n_shortcut_edges);

  vector<uint32_t> ranks(header.n_nodes);
  fread(ranks.data(), sizeof(ranks[0]), header.n_nodes, f);

  vector<ch_layout_original_edge> edges_orig(header.n_original_edges);
  fread(edges_orig.data(), sizeof(edges_orig[0]), edges_orig.size(), f);

  vector<ch_layout_shortcut_edge> edges_shc(header.n_shortcut_edges);
  fread(edges_shc.data(), sizeof(edges_shc[0]), edges_shc.size(), f);

  uint32_t terminator;
  fread(&terminator, sizeof(terminator), 1, f);
  assert( terminator == 0x12345678 );

  
  graph_CH_bi_t *graph_out = (graph_CH_bi_t *) malloc(sizeof(graph_CH_bi_t));

  graph_out->num_nodes = header.n_nodes;
  graph_out->graph_f.num_nodes = graph_out->num_nodes;
  graph_out->graph_b.num_nodes = graph_out->num_nodes;

  graph_out->node_ranks = (uint32_t *) malloc(graph_out->num_nodes * sizeof(uint32_t));
  
  graph_out->graph_f.nodes = (node_CH_t *) malloc(graph_out->graph_f.num_nodes * sizeof(node_CH_t));
  graph_out->graph_b.nodes = (node_CH_t *) malloc(graph_out->graph_b.num_nodes * sizeof(node_CH_t));

  
  // determine nnbr
  uint32_t *nnbr_f = (uint32_t *) calloc(graph_out->num_nodes, sizeof(uint32_t));
  uint32_t *nnbr_b = (uint32_t *) calloc(graph_out->num_nodes, sizeof(uint32_t));
  
  const int F_EDGE_FWD = 1;
  const int F_EDGE_BWD = 2;
  const int F_EDGE_SHC = 4;
  auto is_fwd = [](uint32_t f) { return f & F_EDGE_FWD; };
  auto is_bwd = [](uint32_t f) { return f & F_EDGE_BWD; };
  auto is_shc = [](uint32_t f) { return f & F_EDGE_SHC; };
  vector<size_t> n_dir_orig(4), n_dir_shc(4);

  for(uint32_t i=0; i<header.n_original_edges; i++){
    uint32_t flag = edges_orig[i].flags;
    n_dir_orig[flag & 0x3]++;
    if(flag & (1<<0)){
      nnbr_f[edges_orig[i].src]++;
      nnbr_b[edges_orig[i].dst]++;
    }
    if(flag & (1<<1)){
      nnbr_f[edges_orig[i].dst]++;
      nnbr_b[edges_orig[i].src]++;
    }
  }
  for(uint32_t i=0; i<header.n_shortcut_edges; i++){
    uint32_t flag = edges_shc[i].flags;
    n_dir_shc[flag & 0x3]++;
    if(flag & (1<<0)){
      nnbr_f[edges_shc[i].src]++;
      nnbr_b[edges_shc[i].dst]++;
    }
    if(flag & (1<<1)){
      nnbr_f[edges_shc[i].dst]++;
      nnbr_b[edges_shc[i].src]++;
    }
  }

  // allocate space for edges, etc.
  for(uint32_t i=0; i<graph_out->num_nodes; i++){
    graph_out->node_ranks[i] = ranks[i];
    graph_out->graph_f.nodes[i].num_neighbors = 0;
    graph_out->graph_f.nodes[i].llen = nnbr_f[i];
    graph_out->graph_b.nodes[i].num_neighbors = 0;
    graph_out->graph_b.nodes[i].llen = nnbr_b[i];

    graph_out->graph_f.nodes[i].neighbors = (nodeid_t *) malloc(nnbr_f[i]*sizeof(nodeid_t));
    graph_out->graph_f.nodes[i].weights = (weight_t *) malloc(nnbr_f[i]*sizeof(weight_t));
    graph_out->graph_f.nodes[i].midpoint = (nodeid_t *) malloc(nnbr_f[i]*sizeof(nodeid_t));

    graph_out->graph_b.nodes[i].neighbors = (nodeid_t *) malloc(nnbr_b[i]*sizeof(nodeid_t));
    graph_out->graph_b.nodes[i].weights = (weight_t *) malloc(nnbr_b[i]*sizeof(weight_t));
    graph_out->graph_b.nodes[i].midpoint = (nodeid_t *) malloc(nnbr_b[i]*sizeof(nodeid_t));
  }

  // copy edges to their corresponding nodes
  for(uint32_t i=0; i<header.n_original_edges; i++){
    weight_t weight = edges_orig[i].wht;
    uint32_t flag = edges_orig[i].flags;
    if(flag > 3 )
      printf
        ("unexpected flag, %d, at edge i = %u, s = %u, d = %u, w = %u, "
         "f = %u\n",
         flag, i, edges_orig[i].src, edges_orig[i].dst,
         edges_orig[i].wht, edges_orig[i].flags);
    
    if ( is_fwd(flag) ) {
      nodeid_t nodeid = edges_orig[i].src;
      nodeid_t nborid = graph_out->graph_f.nodes[nodeid].num_neighbors;
      nodeid_t nbor = edges_orig[i].dst;
      graph_out->graph_f.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_f.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_f.nodes[nodeid].midpoint[nborid] = NODEID_NULL;
      graph_out->graph_f.nodes[nodeid].num_neighbors++;

      nodeid = edges_orig[i].dst;
      nborid = graph_out->graph_b.nodes[nodeid].num_neighbors;
      nbor = edges_orig[i].src;
      graph_out->graph_b.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_b.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_b.nodes[nodeid].midpoint[nborid] = NODEID_NULL;
      graph_out->graph_b.nodes[nodeid].num_neighbors++;
    }

    if ( is_bwd(flag) ) {
      nodeid_t nodeid = edges_orig[i].src;
      nodeid_t nborid = graph_out->graph_b.nodes[nodeid].num_neighbors;
      nodeid_t nbor = edges_orig[i].dst;
      graph_out->graph_b.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_b.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_b.nodes[nodeid].midpoint[nborid] = NODEID_NULL;
      graph_out->graph_b.nodes[nodeid].num_neighbors++;

      nodeid = edges_orig[i].dst;
      nborid = graph_out->graph_f.nodes[nodeid].num_neighbors;
      nbor = edges_orig[i].src;
      graph_out->graph_f.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_f.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_f.nodes[nodeid].midpoint[nborid] = NODEID_NULL;
      graph_out->graph_f.nodes[nodeid].num_neighbors++;
    }
  }
  // only way to detect added edges is to compare with original graph,
  // we'll assume all edges are added (does not affect correctness)
  for(uint32_t i=0; i<header.n_shortcut_edges; i++){
    weight_t weight = edges_shc[i].wht;
    nodeid_t via = edges_shc[i].mid;
    uint32_t flag = edges_shc[i].flags;
    if ( flag <= 4 || flag >= 8 )
      printf
        ("unexpected flag, %d, at shortcut i = %u, s = %u, d = %u, w = %u, "
         "f = %u\n",
         flag, i, edges_orig[i].src, edges_orig[i].dst,
         edges_orig[i].wht, edges_orig[i].flags);
    
    if ( is_fwd(flag) ) {
      nodeid_t nodeid = edges_shc[i].src;
      nodeid_t nborid = graph_out->graph_f.nodes[nodeid].num_neighbors;
      nodeid_t nbor = edges_shc[i].dst;
      graph_out->graph_f.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_f.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_f.nodes[nodeid].midpoint[nborid] =
        is_shc(flag) ? via : NODEID_NULL;
      graph_out->graph_f.nodes[nodeid].num_neighbors++;

      nodeid = edges_shc[i].dst;
      nborid = graph_out->graph_b.nodes[nodeid].num_neighbors;
      nbor = edges_shc[i].src;
      graph_out->graph_b.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_b.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_b.nodes[nodeid].midpoint[nborid] = (flag & (1<<2)) ? via : NODEID_NULL;;
      graph_out->graph_b.nodes[nodeid].num_neighbors++;
    }
    
    if ( is_bwd(flag) ) {
      nodeid_t nodeid = edges_shc[i].src;
      nodeid_t nborid = graph_out->graph_b.nodes[nodeid].num_neighbors;
      nodeid_t nbor = edges_shc[i].dst;
      graph_out->graph_b.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_b.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_b.nodes[nodeid].midpoint[nborid] = (flag & (1<<2)) ? via : NODEID_NULL;
      graph_out->graph_b.nodes[nodeid].num_neighbors++;

      nodeid = edges_shc[i].dst;
      nborid = graph_out->graph_f.nodes[nodeid].num_neighbors;
      nbor = edges_shc[i].src;
      graph_out->graph_f.nodes[nodeid].neighbors[nborid] = nbor;
      graph_out->graph_f.nodes[nodeid].weights[nborid] = weight;
      graph_out->graph_f.nodes[nodeid].midpoint[nborid] = (flag & (1<<2)) ? via : NODEID_NULL;;
      graph_out->graph_f.nodes[nodeid].num_neighbors++;
    }
  }

  // change midpoints to the last midpoint from a real edge (backwards parent)
  for(uint32_t i=0; i<graph_out->num_nodes; i++){
    for(uint32_t j=0; j<graph_out->graph_b.nodes[i].num_neighbors; j++){
      uint32_t endnode = graph_out->graph_b.nodes[i].neighbors[j];
      uint32_t midpoint = graph_out->graph_b.nodes[i].midpoint[j];
      int update_mid = -1;
      // recursively find closest midpoint to shortcut end node
      while(midpoint!=NODEID_NULL){
	// update midpoint each iteration
	update_mid++;
	graph_out->graph_b.nodes[i].midpoint[j] = midpoint;
	for(uint32_t k=0; k<graph_out->graph_b.nodes[i].num_neighbors; k++){
	  if(graph_out->graph_b.nodes[i].neighbors[k] == midpoint){
	    midpoint = graph_out->graph_b.nodes[i].midpoint[k];
	    break;
	  }
	}
      }
      // swap the midpoint in forwards graph as well using the already found midpoint
      if(update_mid > 0){
	for(uint32_t k=0; k<graph_out->graph_f.nodes[endnode].num_neighbors; k++){
	  if(graph_out->graph_f.nodes[endnode].neighbors[k] == i){
	    graph_out->graph_f.nodes[endnode].midpoint[k] = graph_out->graph_b.nodes[i].midpoint[j];
	    break;
	  }
	}
      }
    }
  }

  for ( uint32_t i=0; i<graph_out->num_nodes; i++ )
    for ( uint32_t j=0; j<graph_out->graph_b.nodes[i].num_neighbors; j++ )
      if ( graph_out->graph_b.nodes[i].midpoint[j] == NODEID_NULL )
        graph_out->graph_b.nodes[i].midpoint[j] =
          graph_out->graph_b.nodes[i].neighbors[j];

  *graph = graph_out;

  size_t n_edges_f = 0, n_edges_b = 0;
  for ( uint32_t i=0; i<graph_out->num_nodes; i++ )
    {
      node_CH_t& ndf = graph_out->graph_f.nodes[i];
      assert( ndf.num_neighbors == nnbr_f[i] );
      n_edges_f += ndf.num_neighbors;
      node_CH_t& ndb = graph_out->graph_b.nodes[i];
      assert( ndb.num_neighbors == nnbr_b[i] );
      n_edges_b += ndb.num_neighbors;
    }

  const size_t n_edges_seen = n_dir_orig[1] + n_dir_orig[2] + 2 * n_dir_orig[3]
    + n_dir_shc[1] + n_dir_shc[2] + 2 * n_dir_shc[3];
  assert( header.n_original_edges ==
          n_dir_orig[1] + n_dir_orig[2] + n_dir_orig[3] );
  assert( header.n_shortcut_edges ==
          n_dir_shc[1] + n_dir_shc[2] + n_dir_shc[3] );
  assert( n_edges_seen == n_edges_f );
  assert( n_edges_seen == n_edges_b );

  return 0;
}


// I think graphs from chris's code might have duplicate edges (not necessarily with same length)
void graph_SCH_remove_dups(graph_CH_bi_t *graph)
{

  uint32_t dup_count=0;

  for(uint32_t i=0; i<graph->num_nodes; i++){

    uint32_t nnbr = graph->graph_f.nodes[i].num_neighbors;
    for(uint32_t j=0; j<nnbr; j++){
      if(graph->graph_f.nodes[i].neighbors[j] != NODEID_NULL){
	for(uint32_t k=j+1; k<nnbr; k++){
	  if(graph->graph_f.nodes[i].neighbors[j] == graph->graph_f.nodes[i].neighbors[k]){
	    if(graph->graph_f.nodes[i].weights[j] > graph->graph_f.nodes[i].weights[k]){
	      graph->graph_f.nodes[i].neighbors[j] = graph->graph_f.nodes[i].neighbors[k];
	      graph->graph_f.nodes[i].weights[j] = graph->graph_f.nodes[i].weights[k];
	      graph->graph_f.nodes[i].midpoint[j] = graph->graph_f.nodes[i].midpoint[k];
	    }
	    graph->graph_f.nodes[i].neighbors[k] = NODEID_NULL;
	    graph->graph_f.nodes[i].num_neighbors--;
	    dup_count++;
	  }
	}
      }
    }
    while(nnbr!=graph->graph_f.nodes[i].num_neighbors){
      for(uint32_t j=0; j<nnbr; j++){
	if(graph->graph_f.nodes[i].neighbors[j] == NODEID_NULL){
	  for(uint32_t k=j+1; k<nnbr; k++){
	    graph->graph_f.nodes[i].neighbors[k-1] = graph->graph_f.nodes[i].neighbors[k];
	    graph->graph_f.nodes[i].weights[k-1] = graph->graph_f.nodes[i].weights[k];
	    graph->graph_f.nodes[i].midpoint[k-1] = graph->graph_f.nodes[i].midpoint[k];
	  }
	  graph->graph_f.nodes[i].neighbors[nnbr-1] = NODEID_NULL;
	  nnbr--;
	}
      }
    }


    nnbr = graph->graph_b.nodes[i].num_neighbors;
    for(uint32_t j=0; j<nnbr; j++){
      if(graph->graph_b.nodes[i].neighbors[j] != NODEID_NULL){
	for(uint32_t k=j+1; k<nnbr; k++){
	  if(graph->graph_b.nodes[i].neighbors[j] == graph->graph_b.nodes[i].neighbors[k]){
	    if(graph->graph_b.nodes[i].weights[j] > graph->graph_b.nodes[i].weights[k]){
	      graph->graph_b.nodes[i].neighbors[j] = graph->graph_b.nodes[i].neighbors[k];
	      graph->graph_b.nodes[i].weights[j] = graph->graph_b.nodes[i].weights[k];
	      graph->graph_b.nodes[i].midpoint[j] = graph->graph_b.nodes[i].midpoint[k];
	    }
	    graph->graph_b.nodes[i].neighbors[k] = NODEID_NULL;
	    graph->graph_b.nodes[i].num_neighbors--;
	  }
	}
      }
    }
    while(nnbr!=graph->graph_b.nodes[i].num_neighbors){
      for(uint32_t j=0; j<nnbr; j++){
	if(graph->graph_b.nodes[i].neighbors[j] == NODEID_NULL){
	  for(uint32_t k=j+1; k<nnbr; k++){
	    graph->graph_b.nodes[i].neighbors[k-1] = graph->graph_b.nodes[i].neighbors[k];
	    graph->graph_b.nodes[i].weights[k-1] = graph->graph_b.nodes[i].weights[k];
	    graph->graph_b.nodes[i].midpoint[k-1] = graph->graph_b.nodes[i].midpoint[k];
	  }
	  graph->graph_b.nodes[i].neighbors[nnbr-1] = NODEID_NULL;
	  nnbr--;
	}
      }
    }

  }

  printf("%u duplicate edges removed.\n\n", dup_count);

}


void SCH_assign_levels(graph_CH_bi_t *graph, uint32_t *node_levels)
{
  
  // prepare node access order by rank
  uint32_t *ranks = (uint32_t *) malloc(graph->num_nodes*sizeof(uint32_t));
  uint32_t *ranks_inv = (uint32_t *) malloc(graph->num_nodes*sizeof(uint32_t));
  for(uint32_t i=0; i<graph->num_nodes; i++){
    ranks[i] = graph->node_ranks[i];
    ranks_inv[i] = i;
    node_levels[i] = NODEID_NULL;
  }
  uint32_t *work = (uint32_t *) malloc(2*graph->num_nodes*sizeof(uint32_t));
  merge_sort_ref(ranks, ranks_inv, work, &work[graph->num_nodes], graph->num_nodes);
  free(work);
  free(ranks);


  for(uint32_t i=0; i<graph->num_nodes; i++){
    uint32_t current = ranks_inv[i];
    uint32_t lvl = 0;

    for ( auto& gd: { graph->graph_b, graph->graph_f } )
      for ( uint32_t j=0; j< gd.nodes[current].num_neighbors; j++ ){
        uint32_t nbor = gd.nodes[current].neighbors[j];
        if ( node_levels[nbor] != NODEID_NULL )
          set_max( lvl, node_levels[nbor] + 1 );
      }

    node_levels[current] = lvl;
  }

  // Make sure that every edge crosses a level and that for all (s,d)
  // in E, if d is at higher (lower) level than s, then d is higher
  // (lower) rank than s.
  //
  for ( nodeid_t nid = 0;  nid < graph->num_nodes; nid++ )
    {
      const uint32_t lev = node_levels[nid];
      const nodeid_t r = graph->node_ranks[nid];
      for ( auto& gd: { graph->graph_b, graph->graph_f } )
        for ( uint32_t j=0; j< gd.nodes[nid].num_neighbors; j++ )
          {
            uint32_t nbor = gd.nodes[nid].neighbors[j];
            assert( node_levels[nbor] != lev );
            assert( lev < node_levels[nbor] == r < graph->node_ranks[nbor] );
          }
    }

  free(ranks_inv);
}


void graph_SCH_2_CUCH_d(cu_graph_CH_UD_t **graph, graph_CH_bi_t *graph_in, uint32_t *node_levels)
{

  GPU_Info gpu_info;
  
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;

  nodeid_t APSP_size = 1024;
  
  /// sort node IDs by level

  uint32_t num_nodes = graph_in->num_nodes;

  // prepare node order by level
  uint32_t *lvls = (uint32_t *) malloc(num_nodes*sizeof(uint32_t));
  uint32_t *lvls_inv = (uint32_t *) malloc(num_nodes*sizeof(uint32_t));
  for(uint32_t i=0; i<num_nodes; i++){
    lvls[i] = node_levels[i];
    lvls_inv[i] = i;
  }
  uint32_t *work = (uint32_t *) malloc(2*num_nodes*sizeof(uint32_t));
  merge_sort_ref(lvls, lvls_inv, work, &work[num_nodes], num_nodes);
  free(work);

  /// determine overlay size
  uint32_t overlay_size = 0;  // Avoid warnings.
  uint32_t max_level = 0;     // Avoid warnings.
  for(uint32_t i = num_nodes - APSP_size - 1; i<num_nodes - 1; i++){
    if(lvls[i] != lvls[i+1]){
      overlay_size = num_nodes - 1 - i;
      max_level = lvls[i+1];
      break;
    }
  }

  printf("Max CH level: %u\n", lvls[num_nodes-1]);
  printf("Max CU_CH level: %u\n", max_level);
  
  
  /// determine number of edges and allocate UD graph on host
  uint32_t num_edges_u_f = 0;
  uint32_t num_edges_u_b = 0;
  uint32_t num_edges_d_b = 0;
  uint32_t num_edges = 0;
  uint32_t num_edges_OL = 0;
  for(uint32_t i=0; i<num_nodes; i++){
    uint32_t nlvl = node_levels[i];

    if(nlvl < max_level){
      for(uint32_t j=0; j<graph_in->graph_f.nodes[i].num_neighbors; j++){
	if(node_levels[graph_in->graph_f.nodes[i].neighbors[j]] > nlvl){
	  num_edges_u_f++;
	}
      }
    }

    for(uint32_t j=0; j<graph_in->graph_b.nodes[i].num_neighbors; j++){
      if(node_levels[graph_in->graph_b.nodes[i].neighbors[j]] < min(nlvl, max_level)){
	num_edges_u_b++;
      }
    }

    if(nlvl < max_level){
      for(uint32_t j=0; j<graph_in->graph_b.nodes[i].num_neighbors; j++){
	if(node_levels[graph_in->graph_b.nodes[i].neighbors[j]] > nlvl){
	  num_edges_d_b++;
	}
      }
    }
    
    if(nlvl >= max_level){
      for(uint32_t j=0; j<graph_in->graph_b.nodes[i].num_neighbors; j++){      
	if(node_levels[graph_in->graph_b.nodes[i].neighbors[j]] >= max_level){
	  num_edges_OL++;
	}
      }
    }    
  }

  printf("Num edges u_b: %u\n", num_edges_u_b);
  printf("Num edges d_b: %u\n", num_edges_d_b);
  printf("Num edges OL: %u\n", num_edges_OL);

  num_edges = max(max(num_edges_u_f, num_edges_u_b), num_edges_d_b);
  nodeid_t max_rank = max_level + 1;
  uint32_t edge_list_length =  num_edges * 1.1 + 1024*max_rank;// extra space for padding
  uint32_t overlay_edge_list_length = num_edges_OL + 64;
  
  cu_graph_CH_UD_t *graph_out = (cu_graph_CH_UD_t *) malloc(sizeof(cu_graph_CH_UD_t));
  graph_out->graph_u_f.num_nodes = num_nodes;
  graph_out->graph_u_b.num_nodes = num_nodes;
  graph_out->graph_d_b.num_nodes = num_nodes;
  graph_out->overlay_CH.num_nodes = overlay_size;
  graph_out->overlay_APSP_path.num_nodes = APSP_size;

  graph_out->max_rank = max_rank;
  graph_out->overlay_size = overlay_size;

  graph_out->graph_u_f.max_num_edges = edge_list_length;
  graph_out->graph_u_b.max_num_edges = edge_list_length;
  graph_out->graph_d_b.max_num_edges = edge_list_length;
  graph_out->overlay_CH.max_num_edges = overlay_edge_list_length;
  graph_out->graph_u_f.empty_pointer = num_edges;
  graph_out->graph_u_b.empty_pointer = num_edges;
  graph_out->graph_d_b.empty_pointer = num_edges;
  graph_out->overlay_CH.empty_pointer = num_edges_OL;
  
  graph_out->node_ranks = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->node_levels = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->node_idx = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->node_idx_inv = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  
  graph_out->graph_u_f.num_neighbors = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_f.NNIEG = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_f.pointer = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_f.neighbors = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_u_f.vertex_ID = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));

  graph_out->graph_u_b.num_neighbors = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.NNIEG = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.pointer = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.neighbors = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_u_b.vertex_ID = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_u_b.weights = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_u_b.midpoint = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  for(uint32_t i=0; i<edge_list_length; i++){
    graph_out->graph_u_b.weights[i] = NPP_MAX_32U;
  }
 
  graph_out->graph_d_b.num_neighbors = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.NNIEG = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.pointer = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.neighbors = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_d_b.vertex_ID = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_d_b.weights = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_d_b.midpoint = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  for(uint32_t i=0; i<edge_list_length; i++){
    graph_out->graph_d_b.weights[i] = NPP_MAX_32U;
  }

  graph_out->node_levels_pt = (nodeid_t *) calloc(max_level+2, sizeof(nodeid_t));

  graph_out->overlay_CH.num_neighbors = (nodeid_t *) malloc(overlay_size*sizeof(nodeid_t));
  graph_out->overlay_CH.pointer = (nodeid_t *) malloc(overlay_size*sizeof(nodeid_t));
  graph_out->overlay_CH.neighbors = (nodeid_t *) malloc(overlay_edge_list_length*sizeof(nodeid_t));
  graph_out->overlay_CH.weights = (nodeid_t *) malloc(overlay_edge_list_length*sizeof(nodeid_t));
  graph_out->overlay_CH.midpoint = (nodeid_t *) malloc(overlay_edge_list_length*sizeof(nodeid_t));

  graph_out->overlay_APSP_path.prev = (nodeid_t *)malloc(APSP_size * APSP_size * sizeof(nodeid_t));
  graph_out->overlay_APSP_path.dist = (nodeid_t *)malloc(APSP_size * APSP_size * sizeof(nodeid_t));

  graph_out->edge_groups_in_level_u_b = nullptr;
  graph_out->edge_groups_in_level_d_b = nullptr;
  
  

  /// copy edges to host struct

  graph_out->node_levels_pt[0] = 0;
  
  uint32_t npt_u_f = 0;
  uint32_t npt_u_b = 0;
  uint32_t npt_d_b = 0;
  uint32_t npt_OL = 0;

  for ( uint32_t i=0; i<num_nodes; i++ )
    graph_out->node_idx_inv[lvls_inv[i]] = i;

  for(uint32_t i=0; i<num_nodes; i++){
    uint32_t current = lvls_inv[i];
    uint32_t nlvl = lvls[i];

    graph_out->node_ranks[i] = graph_in->node_ranks[current];
    graph_out->node_levels[i] = min(nlvl, max_level);
    graph_out->node_idx[i] = current;

    uint32_t nnbr = 0;
    graph_out->graph_u_f.pointer[i] = npt_u_f;
    if(nlvl < max_level){
      for(uint32_t j=0; j<graph_in->graph_f.nodes[current].num_neighbors; j++){
	if(node_levels[graph_in->graph_f.nodes[current].neighbors[j]] > nlvl){
	  graph_out->graph_u_f.neighbors[npt_u_f + nnbr] = graph_in->graph_f.nodes[current].neighbors[j];
	  graph_out->graph_u_f.vertex_ID[npt_u_f + nnbr] = i;
	  nnbr++;
	}
      }
    }
    npt_u_f += nnbr;
    graph_out->graph_u_f.num_neighbors[i] = nnbr;

    nnbr = 0;
    graph_out->graph_u_b.pointer[i] = npt_u_b;
    for(uint32_t j=0; j<graph_in->graph_b.nodes[current].num_neighbors; j++){
      if(node_levels[graph_in->graph_b.nodes[current].neighbors[j]] < min(nlvl, max_level)){
	graph_out->graph_u_b.neighbors[npt_u_b + nnbr] =
          graph_out->node_idx_inv
          [graph_in->graph_b.nodes[current].neighbors[j]];
	graph_out->graph_u_b.weights[npt_u_b + nnbr] = graph_in->graph_b.nodes[current].weights[j];
	graph_out->graph_u_b.midpoint[npt_u_b + nnbr] = graph_in->graph_b.nodes[current].midpoint[j];
	graph_out->graph_u_b.vertex_ID[npt_u_b + nnbr] = i;
	nnbr++;
      }
    }
    npt_u_b += nnbr;
    graph_out->graph_u_b.num_neighbors[i] = nnbr;

    nnbr = 0;
    graph_out->graph_d_b.pointer[i] = npt_d_b;
    if(nlvl < max_level){
      for(uint32_t j=0; j<graph_in->graph_b.nodes[current].num_neighbors; j++){
	if(node_levels[graph_in->graph_b.nodes[current].neighbors[j]] > nlvl){
	  graph_out->graph_d_b.neighbors[npt_d_b + nnbr] = graph_in->graph_b.nodes[current].neighbors[j];
	  graph_out->graph_d_b.weights[npt_d_b + nnbr] = graph_in->graph_b.nodes[current].weights[j];
	  graph_out->graph_d_b.midpoint[npt_d_b + nnbr] = graph_in->graph_b.nodes[current].midpoint[j];
	  graph_out->graph_d_b.vertex_ID[npt_d_b + nnbr] = i;
	  nnbr++;
	}
      }
      assert( nnbr );
    }
    npt_d_b += nnbr;
    graph_out->graph_d_b.num_neighbors[i] = nnbr;

    if((i>= 1) && nlvl<=max_level ){
      if(nlvl != lvls[i-1]){
	graph_out->node_levels_pt[nlvl] = i;
      }
    }
    
    
    if(nlvl >= max_level){
      nnbr = 0;
      graph_out->overlay_CH.pointer[i - graph_out->node_levels_pt[max_level]] = npt_OL;
      for(uint32_t j=0; j<graph_in->graph_b.nodes[current].num_neighbors; j++){      
	if(node_levels[graph_in->graph_b.nodes[current].neighbors[j]] >= max_level){
	  graph_out->overlay_CH.neighbors[npt_OL + nnbr] = graph_in->graph_b.nodes[current].neighbors[j];
	  graph_out->overlay_CH.weights[npt_OL + nnbr] = graph_in->graph_b.nodes[current].weights[j];
	  graph_out->overlay_CH.midpoint[npt_OL + nnbr] = graph_in->graph_b.nodes[current].midpoint[j];
	  nnbr++;
	}
      }
      npt_OL += nnbr;
      graph_out->overlay_CH.num_neighbors[i - graph_out->node_levels_pt[max_level]] = nnbr;
    }    

  }

  // renumber overlay node ids (needs to be applied separately)
  for(uint32_t i=0; i<overlay_size; i++){
    uint32_t npt = graph_out->overlay_CH.pointer[i];
    uint32_t nnbr = graph_out->overlay_CH.num_neighbors[i];
    for(uint32_t j=0; j<nnbr; j++){
      graph_out->overlay_CH.neighbors[npt + j] = graph_out->node_idx_inv[graph_out->overlay_CH.neighbors[npt + j]] - (num_nodes - overlay_size);
    }
  }

  for(uint32_t i=0; i<APSP_size; i++){
    for(uint32_t j=0; j<APSP_size; j++){
      if(i==j){
	graph_out->overlay_APSP_path.prev[i*APSP_size + j] = i;
	graph_out->overlay_APSP_path.dist[i*APSP_size + j] = 0;
      } else {
	graph_out->overlay_APSP_path.prev[i*APSP_size + j] = NODEID_NULL;
	graph_out->overlay_APSP_path.dist[i*APSP_size + j] = NODEID_NULL;
      }
    }
  }
  for(uint32_t i=0; i<APSP_size; i++){
    if(i<overlay_size){
      uint32_t npt = graph_out->overlay_CH.pointer[i];
      uint32_t nnbr = graph_out->overlay_CH.num_neighbors[i];
      for(uint32_t j=0; j<nnbr; j++){
	graph_out->overlay_APSP_path.dist[graph_out->overlay_CH.neighbors[npt + j]*APSP_size + i] = graph_out->overlay_CH.weights[npt + j];
	graph_out->overlay_APSP_path.prev[graph_out->overlay_CH.neighbors[npt + j]*APSP_size + i] = graph_out->overlay_CH.neighbors[npt + j];
      }
    }
  }
  

  /// copy struct to device, renumber edges and perform APSP
  cu_graph_CH_UD_t *graph_UD_d;
  graph_cu_CH_UD_h2d(&graph_UD_d, graph_out);

  graph_cu_UD_free_host(graph_out);

  uint32_t block_dim = 1024;
  uint32_t grid_dim = 2 * nmps;
  cu_CH_UD_renum<<<grid_dim,block_dim>>>(graph_UD_d);
  CE( cudaDeviceSynchronize() );
  CE( cudaGetLastError() );

  cu_graph_CH_UD_t *graph_UD_h_apsp = (cu_graph_CH_UD_t *) malloc(sizeof(cu_graph_CH_UD_t));
  CE( cudaMemcpy(graph_UD_h_apsp, graph_UD_d, sizeof(cu_graph_CH_UD_t), cudaMemcpyDeviceToHost) );
  // make sure path is allocated correctly (multiple of 64)
  assert(graph_UD_h_apsp->overlay_APSP_path.num_nodes % 64 == 0);
  
  // calculate the blocked APSP
  uint32_t block_factor = graph_UD_h_apsp->overlay_APSP_path.num_nodes / 64;
  block_dim = APSP_size;
  dim3 grid_dim_3(block_factor, block_factor);
  for(uint32_t iter=0; iter<block_factor; iter++){

    // phase 0, update block iter,iter
    cu_APSP_blocked<<<grid_dim_3,block_dim>>>(graph_UD_h_apsp->overlay_APSP_path.dist, graph_UD_h_apsp->overlay_APSP_path.prev, graph_UD_h_apsp->overlay_APSP_path.num_nodes, iter, 0);
    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );

    // phase 1, update the remainder of column iter and row iter
    cu_APSP_blocked<<<grid_dim_3,block_dim>>>(graph_UD_h_apsp->overlay_APSP_path.dist, graph_UD_h_apsp->overlay_APSP_path.prev, graph_UD_h_apsp->overlay_APSP_path.num_nodes, iter, 1);
    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );

    // phase 2, update everything else
    cu_APSP_blocked<<<grid_dim_3,block_dim>>>(graph_UD_h_apsp->overlay_APSP_path.dist, graph_UD_h_apsp->overlay_APSP_path.prev, graph_UD_h_apsp->overlay_APSP_path.num_nodes, iter, 2);
    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );
  }

  /// compress UD graph
  cu_graph_CH_UD_t *graph_UD_d_compressed;
  graph_cu_CH_UD_init_d(&graph_UD_d_compressed, num_nodes + 10, edge_list_length, max_rank, APSP_size, overlay_edge_list_length, UDP_Compress);

  cu_CH_UD_compress_pre_launch(graph_UD_d_compressed, graph_UD_d);

  block_dim = 512;
  grid_dim = 3;
  cu_CH_UD_compress_0<<<grid_dim,block_dim>>>(graph_UD_d_compressed, graph_UD_d);
  CE( cudaDeviceSynchronize() );
  CE( cudaGetLastError() );
  
  block_dim = 1024;
  grid_dim = 2 * nmps;
  cu_CH_UD_compress_1<<<grid_dim,block_dim>>>(graph_UD_d_compressed, graph_UD_d);
  CE( cudaDeviceSynchronize() );
  CE( cudaGetLastError() );
  
  graph_cu_UD_free_device(graph_UD_d);

  *graph = graph_UD_d_compressed;

}

