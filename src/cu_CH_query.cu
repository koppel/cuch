/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>
#include <map>

#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"

#include <nppdefs.h>

// Workaround for a bug exposed by the DIMACS Texas graph.
constexpr bool texas_workaround = true;


// #define ST
// guarantees that different syncthreads() calls will not affect each other by checking line number
// allows for threads to skip a syncthreads() (only applies to the last syncthread in a kernel)
#ifdef ST

#define __syncthreads()                                                       \
  { sync_threads();                                                           \
    st_line = atomicExch(&sync_count_sh, __LINE__);                           \
    assert(st_line == 0 || st_line == __LINE__);                              \
    sync_threads();                                                           \
    sync_count_sh = 0; }

#endif

__constant__ cu_graph_CH_UD_t graph_UD_c;
__constant__ CH_Options chopts_c;
__constant__ path_t result_ud_c; // Result using up/down node numbering.
__constant__ path_t result_or_c; // Result using original node numbering.

__host__ void
cu_CH_query_pre_launch
(cu_graph_CH_UD_t *graph_UD_d, path_t *result_ud_d, path_t *result_or_d)
{
  cuda_sym_dev_to_dev(graph_UD_c,graph_UD_d);
  cuda_sym_host_to_dev(chopts_c,&chopts);
  cuda_sym_dev_to_dev(result_ud_c,result_ud_d);
  cuda_sym_dev_to_dev(result_or_c,result_or_d);
}

// handles up to the first 31 levels in upwards DAG (marks and updates reachable nodes from src)
__global__ void __launch_bounds__(query_u_0_block_dim,1)
cu_CH_query_u_0
(uint32_t src, int lvl_start, int lvl_stop, uint32_t *node_tags)
{
#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  // debug used to suppress assert in places where it would use many registers.
  constexpr bool debug = false;

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  const bool fprev = !chopts_c.query_distance_only; // Find Predecessor

  constexpr int block_dim = query_u_0_block_dim;
  assert( block_dim == blockDim.x );

  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int wp_num = threadIdx.x >> wp_lg;
  const int n_wps = block_dim >> wp_lg;
  const int lane = threadIdx.x & ( wp_sz - 1 );

  const int n_nt = 32; // Number of node tag elements.

  if(blockIdx.x==0){

    const uint32_t lid = threadIdx.x >> 4;
    const uint32_t tnid = threadIdx.x - (lid<<4);

    // we'll calculate the path using UD node ids and once finished we'll run a sweep of the path
    // to move to input space
    uint32_t src_UD = graph_UD->node_idx_inv[src];

    uint32_t bit_n;
    uint32_t bit_mask;
    
    uint32_t byte_f;
    uint32_t bit_f;
    uint32_t bit_mask_f;


    // use the tags/queue only up to lvl_stop levels which ideally corresponds to ~96% of the graph
    // also more nodes tagged in remainder of graph, so using a simple sweep is more efficient
    __shared__ uint32_t lvl_tag;
    __shared__ uint32_t lvl_u_pt[32];
    __shared__ uint32_t tag_u_pt[32];// points to beginning of each lvl in node_tags (round up to 32)
    __shared__ uint32_t node_tags_sh[block_dim];
    __shared__ uint32_t nnbr_b_sh[block_dim];
    __shared__ uint32_t pt_b_sh[block_dim];
    __shared__ uint32_t nnbr_f_sh[block_dim];
    __shared__ uint32_t pt_f_sh[block_dim];

    
    if(threadIdx.x==0){
      lvl_tag = 0;
    }
    if(threadIdx.x < 512){
      node_tags[threadIdx.x] = 0; // Needed?
    }
    if(threadIdx.x < 32){
      lvl_u_pt[threadIdx.x] = graph_UD->node_levels_pt[threadIdx.x];
      tag_u_pt[threadIdx.x] = 0;
    }
    // align tag_u_pt by 32 byte (alignment by 4 byte is necessary for correctness)
    /// RRRR prefix sum
    __syncwarp();
    const uint32_t nd_per_nt = 8 * sizeof(node_tags[0]);

    if(threadIdx.x==0){

      for ( uint32_t i=lvl_start; i<lvl_stop; i++)
	tag_u_pt[i+1] = tag_u_pt[i]
          +  rnd_up( lvl_u_pt[i+1] - lvl_u_pt[i], block_dim * nd_per_nt )
          / nd_per_nt;
    }

    assert( lvl_start > 0 );

    __syncthreads();

    if ( threadIdx.x >= lvl_start && threadIdx.x < lvl_stop )
      {
        const uint32_t n_nodes_lvl =
          lvl_u_pt[threadIdx.x+1] - lvl_u_pt[threadIdx.x];
        const uint32_t n_iter_lvl =
          tag_u_pt[threadIdx.x+1] - tag_u_pt[threadIdx.x];
        assert( n_nodes_lvl <= n_iter_lvl * block_dim );
      }

    // initiate the upward search by marking nodes/levels reachable from src
    // up to lvl_stop.
    if(lid == 0){
      const nodeid_t current = src_UD;
      const int current_nnbr = graph_UD->graph_u_f.num_neighbors[current];
      const nodeid_t current_pt = graph_UD->graph_u_f.pointer[current];
      const int num_round = (current_nnbr>>4) + 1;
      for(uint32_t i=0; i<num_round; i++){
	if(i*16 + tnid < current_nnbr){
	  const nodeid_t nbr =
            graph_UD->graph_u_f.neighbors[current_pt + i*16 + tnid];
	  // check if nbr is below lvl_stop
	  if ( nbr < lvl_u_pt[lvl_stop] ){
	    // locate the location of the corresponding node's flag within node_tags
	    const uint byte_n = nbr >> 5;
	    bit_n = nbr - (byte_n<<5);
	    bit_mask = 1<<bit_n;
	    if(byte_n < 32*16){
	      // if the nbr is one of the first 16384 nodes (first 16 iter), mark in shared
	      atomicOr(&node_tags_sh[byte_n], bit_mask);
	    }
	    // mark the corresponding level as reachable
	    for(uint32_t j=lvl_start; j<lvl_stop; j++){
	      if(nbr >= lvl_u_pt[j] && nbr < lvl_u_pt[j+1]){
		// tag lvl j as reachable
		atomicOr(&lvl_tag, 1<<j);
		const uint byte_n = (nbr - lvl_u_pt[j]) >> 5;
		bit_n = (nbr - lvl_u_pt[j]) - (byte_n<<5);
		bit_mask = 1<<bit_n;
		atomicOr(&node_tags[tag_u_pt[j] + byte_n], bit_mask);
	      }
	    }
	  }
	}
      }
    }

    // do the upwards pass (only reachable nodes up to lvl_stop)
    for(uint32_t lvl=lvl_start; lvl<lvl_stop; lvl++){

      __syncthreads();

      // check if any node in this level is reachable
      if(lvl_tag & (1<<lvl)){
        uint32_t lvl_tag_new = 0;

        const bool lvl_last = ! ( lvl_tag >> lvl );

	// determine how many iters until the next batch of node_tags
	// in current lvl

        const uint num_iter = ( tag_u_pt[lvl+1] - tag_u_pt[lvl] ) / block_dim;

	for(uint32_t i=0; i<num_iter; i++){

          const uint32_t nt_0 =
            node_tags[tag_u_pt[lvl] + i*block_dim + threadIdx.x];

          if ( __all_sync( ~0, !nt_0 ) ) continue;

          node_tags_sh[threadIdx.x] = nt_0;

          __syncwarp();

          const nodeid_t current_i =
            lvl_u_pt[lvl] + i * block_dim * n_nt + wp_num * n_nt * wp_sz + lane;

	  // note we have n_nt*block_dim worth of tags in shared.
	  for(uint32_t j=0; j<n_nt; j++){

	    // check tags and if any of the nodes in current warp are
	    // valid, load all nnbrs to shared also each of the 32
	    // threads load their dist and prev

            const uint byte_n = n_nt * wp_num + j;
            const uint32_t node_tag = node_tags_sh[byte_n];

            if ( !node_tag ) continue;

            const bool current_tagged = node_tag & 1 << lane;
            const nodeid_t current = current_i + j * wp_sz;
            const nodeid_t current_0 = current - tnid;

            __shared__ nodeid_t prev_l[block_dim];
            __shared__ uint32_t dist_l[block_dim];

            __syncwarp();

            prev_l[threadIdx.x] = NPP_MAX_32U;
            dist_l[threadIdx.x] = NPP_MAX_32U;

            if ( current_tagged && current < lvl_u_pt[lvl+1] ){

              nnbr_b_sh[threadIdx.x] =
                graph_UD->graph_u_b.num_neighbors[current];
              pt_b_sh[threadIdx.x] = graph_UD->graph_u_b.pointer[current];

              nnbr_f_sh[threadIdx.x] =
                lvl_last ? 0 : graph_UD->graph_u_f.num_neighbors[current];
              pt_f_sh[threadIdx.x] =
                lvl_last ? 0 : graph_UD->graph_u_f.pointer[current];

            } else {
              nnbr_b_sh[threadIdx.x] = 0;
              nnbr_f_sh[threadIdx.x] = 0;
            }

            __shared__ int max_deg[n_wps];
            if ( !lane ) max_deg[wp_num] = 0;
            __syncwarp();
            if ( current_tagged )
              {
                const int dm =
                  max(nnbr_f_sh[threadIdx.x],nnbr_b_sh[threadIdx.x]);
                atomicMax( &max_deg[wp_num], dm );
              }
            __syncwarp();
            const int num_round = ( max_deg[wp_num] + 15 ) >> 4;

	    for(uint32_t l=0; l<num_round; l++){

              const uint32_t amask_l = __activemask();

	      for(uint32_t k=0; k<16; k++){

                const int sidx_k = ( lid << 4 ) + k;

		// check if current node is marked
		bit_n = ((lid & 1)<<4) + k;
		bit_mask = 1<<bit_n;

                __syncwarp(amask_l);

                if ( ! ( node_tag & bit_mask ) ) continue;

                // make sure you're not out of bounds (past into next lvl)
                if ( current_0 + k >= lvl_u_pt[lvl+1] ) continue;

                const uint32_t amask = __activemask();

                // load the node into shared and from there to its corresponding thread
                const bool bwd_here = l*16 + tnid < nnbr_b_sh[sidx_k];

                const uint32_t nbr_b_eid = pt_b_sh[sidx_k] + l*16 + tnid;
                const nodeid_t nbr_b =
                  bwd_here ? graph_UD->graph_u_b.neighbors[nbr_b_eid] : 0;
                const nodeid_t wht_b =
                  bwd_here ? graph_UD->graph_u_b.weights[nbr_b_eid] : 0;
                const uint32_t dist_nbr = bwd_here ? result->weight[nbr_b] : 0;
                const uint32_t dist_maybe = dist_nbr + wht_b;

                // relax the edge (with care regarding inf edges)
                const bool relax_try = bwd_here && dist_nbr != NPP_MAX_32U;
                if ( relax_try )
                  {
                    assert( !debug ||
                            dist_maybe >= dist_nbr && dist_maybe >= wht_b );
                    atomicMin( &dist_l[sidx_k], dist_maybe );
                  }

                __syncwarp(amask);

                if ( relax_try && dist_l[sidx_k] == dist_maybe )
                  prev_l[sidx_k] = nbr_b_eid;

                // handle the u_f flag and tag of now reachable nodes in the next levels (<15)
                if ( (l*16 + tnid) < nnbr_f_sh[sidx_k] ){
                  const nodeid_t nbr_f =
                    graph_UD->graph_u_f.neighbors[pt_f_sh[sidx_k] + l*16+tnid];
                  // check if nbr is below lvl_stop and if so mark it and its lvl
                  for(uint32_t p=lvl+1; p<lvl_stop; p++){
                    if ( nbr_f >= lvl_u_pt[p] && nbr_f < lvl_u_pt[p+1] ){
                      // tag lvl p as reachable
                      lvl_tag_new |= 1 << p;
                      byte_f = (nbr_f - lvl_u_pt[p]) >> 5;
                      bit_f = (nbr_f - lvl_u_pt[p]) - (byte_f<<5);
                      bit_mask_f = 1<<bit_f;
                      atomicOr(&node_tags[tag_u_pt[p] + byte_f], bit_mask_f);
                      break;
                    }
                  }
                }
	      }
              __syncwarp(amask_l);
	    }

            __syncwarp();

            const uint32_t dl = dist_l[threadIdx.x];
	    if ( dl != NPP_MAX_32U ) {
	      result->weight[current] = dl;
	      if ( fprev )
                {
                  const nodeid_t pl = prev_l[threadIdx.x];
                  assert( !debug || pl != NPP_MAX_32U );
                  result->prev[current] = graph_UD_c.graph_u_b.midpoint[pl];
                }
	    }

	  }
	}

        if ( lvl_tag_new & ~lvl_tag ) atomicOr( &lvl_tag, lvl_tag_new );

      }
    }
  }
  
}

// switch to a standard linear sweep of all edges for remaining levels of upwards DAG (multi-block)
__global__ void 
__launch_bounds__(1024,1)
cu_CH_query_u_1(uint32_t src, int lvl)
{
  
#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  assert(blockDim.x == 1024);
  
  __shared__ uint32_t lvl_pt_edge[2];
  __shared__ uint32_t lvl_pt_node[2];
  __shared__ uint32_t nnieg_pt;

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  const bool fprev = !chopts_c.query_distance_only; // Find Predecessor

  const uint32_t max_nieg = 5*1024;

  /// RRR for now doing only one edge group per iter per block to figure out correctness etc.
  __shared__ uint32_t dist[max_nieg];// note that it's possible that nieg > 1024
  __shared__ uint32_t prev[1];
  __shared__ uint32_t current[1];
  __shared__ nodeid_t nid_0_s;

  /// RRRR dist has to be shared, but the following can be either registers or shared (balance later)
  uint32_t weight;
  uint32_t nbr;
  uint32_t nbr_eid;
  
  uint32_t num_nodes = graph_UD->graph_u_f.num_nodes;
  uint32_t num_levels = graph_UD->node_levels[num_nodes-1] + 1; // levels start from 0

  if(lvl == num_levels-1){
    // last lvl (overlay)
    if(threadIdx.x==0){
      lvl_pt_node[0] = graph_UD->node_levels_pt[lvl];
      lvl_pt_edge[0] = graph_UD->graph_u_b.pointer[lvl_pt_node[0]];
      lvl_pt_edge[1] = graph_UD->graph_u_b.empty_pointer;
      if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
	nnieg_pt = lvl_pt_node[0];
      } else {
	const uint32_t lvl_cut_off_pt = graph_UD->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
	nnieg_pt =  lvl_cut_off_pt + (lvl_pt_node[0] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
      }
    }
  } else {
    if(threadIdx.x < 2){
      lvl_pt_node[threadIdx.x] = graph_UD->node_levels_pt[lvl + threadIdx.x];
      lvl_pt_edge[threadIdx.x] = graph_UD->graph_u_b.pointer[lvl_pt_node[threadIdx.x]];
    }
    if(threadIdx.x==0){
      if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
	nnieg_pt = lvl_pt_node[0];
      } else {
	const uint32_t lvl_cut_off_pt = graph_UD->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
	nnieg_pt = lvl_cut_off_pt + (lvl_pt_node[0] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
      }
    }
  }

  __syncthreads();

  uint32_t edge_group_per_iter = gridDim.x;
  uint32_t edge_group_next_lvl = lvl_pt_edge[1] >> 10;

  for(uint32_t current_edge_group = (lvl_pt_edge[0] >> 10) + blockIdx.x; current_edge_group < edge_group_next_lvl; current_edge_group += edge_group_per_iter){
    
    const uint32_t nieg_current_raw = graph_UD->graph_u_b.NNIEG[nnieg_pt + current_edge_group - (lvl_pt_edge[0] >> 10)];

    const uint32_t nieg_current = nieg_current_raw & 0xffff;
    const uint32_t nieg_flag = nieg_current_raw & 0xf0000000;
    const uint32_t egc = (nieg_current_raw & 0xff0000) >> 16;

    __syncthreads();

    if(nieg_flag == 0x40000000){
      assert(nieg_current == 1 && egc == 0);// sanity check
      // current edge group is continuation of the edge list of a node with nnbr>1024
      // a different block is handling it, continue.
      // this is to maintain the simple static work distribution between blocks
      continue;
    }

    if(nieg_flag == 0x80000000){
      assert(nieg_current == 1 && egc);// sanity check
      // current edge group is the start of the edge list of a node with nnbr>1024
      // current block handles all corresponding edge groups of current node
      if(threadIdx.x==0){
	current[0] = graph_UD->graph_u_b.vertex_ID[(current_edge_group<<10)];
	dist[0] = result->weight[current[0]];
	if ( fprev ) prev[0] = result->prev[current[0]];
      }

      __syncthreads();
      const uint32_t current_0 = current[0];
      assert( current_0 < num_nodes );

      for(uint32_t i=0; i<egc; i++){
	nbr_eid = ((current_edge_group + i)<<10) + threadIdx.x;
	weight = graph_UD->graph_u_b.weights[nbr_eid];
	nbr = graph_UD->graph_u_b.neighbors[nbr_eid];

        bool dist_changed = false;
	uint32_t new_dist;
	if(weight != NPP_MAX_32U){
	  uint32_t dist_nbr = result->weight[nbr];
	  new_dist = dist_nbr + weight;
	  if(dist_nbr != NPP_MAX_32U){
	    uint32_t old_dist = atomicMin(&(dist[0]), new_dist);
            dist_changed = old_dist > new_dist;
	    // check for overflow
	    assert(new_dist >= weight && new_dist >= dist_nbr);
	  }
	}
	
	__syncthreads();
        if ( fprev && dist_changed && dist[0] == new_dist ) prev[0] = nbr_eid;
      }

      __syncthreads();
      if(threadIdx.x==0){
	result->weight[current_0] = dist[0];
	if ( fprev )
          result->prev[current_0] = graph_UD_c.graph_u_b.midpoint[prev[0]];
      }

    } else {
      const nodeid_t nid =
        graph_UD->graph_u_b.vertex_ID[(current_edge_group<<10) + threadIdx.x];
      if ( threadIdx.x == 0 ) nid_0_s = nid;
      nbr_eid = (current_edge_group<<10) + threadIdx.x;
      weight = graph_UD->graph_u_b.weights[nbr_eid];
      nbr = graph_UD->graph_u_b.neighbors[nbr_eid];

      __syncthreads();
      const uint32_t nid_0 = nid_0_s;
      assert( nid_0 < num_nodes );

      assert(nieg_current <= max_nieg); // max_nieg is limited by shared memory

      for(uint32_t i=threadIdx.x; i<nieg_current; i+=1024){
	dist[i] = result->weight[nid_0 + i];
      }

      __syncthreads();
      /// RRRR add overload checks
      bool dist_changed = false;
      uint32_t new_dist;
      if(weight != NPP_MAX_32U){
	uint32_t dist_nbr = result->weight[nbr];
	new_dist = dist_nbr + weight;
	if(dist_nbr != NPP_MAX_32U){
	  uint32_t old_dist = atomicMin(&(dist[ nid - nid_0]), new_dist);
          dist_changed = old_dist > new_dist;
	  // check for overflow
	  assert(new_dist >= weight && new_dist >= dist_nbr);
	}
      }
    
      __syncthreads();
      if ( dist_changed && dist[nid-nid_0] == new_dist )
        {
          result->weight[nid] = new_dist;
          if ( fprev )
            result->prev[nid] = graph_UD_c.graph_u_b.midpoint[nbr_eid];
        }
    }

  }
  
}


// upwards pass on all nodes in num_iter levels [lvl : lvl+num_iters) (num_iter<255)
__global__ void
__launch_bounds__(1024,1)
cu_CH_query_u_1_multi_iter(int lvl, int num_iters)
{
#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  assert(blockDim.x == 1024);
  assert(gridDim.x == 1);
  assert( num_iters <= query_multi_max_iters );

  __shared__ uint32_t lvl_pt_edge[query_multi_max_iters+1];
  __shared__ uint32_t lvl_pt_node[query_multi_max_iters+1];
  __shared__ uint32_t nnieg_pt[query_multi_max_iters];

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  const bool fprev = !chopts_c.query_distance_only; // Find Predecessor
  const uint32_t max_nieg = 5*1024;

  /// RRR for now doing only one edge group per iter per block to figure out correctness etc.
  __shared__ uint32_t dist[max_nieg];// note that it's possible that nieg > 1024
  __shared__ uint32_t prev[1];// note that it's possible that nieg > 1024
  __shared__ uint32_t current[1024];

  /// RRRR dist has to be shared, but the following can be either registers or shared (balance later)
  uint32_t weight;
  uint32_t nbr;
  uint32_t nbr_eid;
  
  uint32_t num_nodes = graph_UD->graph_u_f.num_nodes;
  uint32_t num_levels = graph_UD->node_levels[num_nodes-1] + 1; // levels start from 0

  // the OL level should be handled by the multi-block query kernel on the upwards DAG
  assert(lvl+num_iters-1 <= num_levels-2);
  if(threadIdx.x<num_iters+1){
    lvl_pt_node[threadIdx.x] = graph_UD->node_levels_pt[lvl + threadIdx.x];
    lvl_pt_edge[threadIdx.x] = graph_UD->graph_u_b.pointer[lvl_pt_node[threadIdx.x]];
  }
  if(threadIdx.x<num_iters){
    if(lvl+threadIdx.x < NNIEG_OVER_ALLOC_CUT_OFF){
      nnieg_pt[threadIdx.x] = lvl_pt_node[threadIdx.x];
    } else {
      const uint32_t lvl_cut_off_pt = graph_UD->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
      nnieg_pt[threadIdx.x] =  lvl_cut_off_pt + (lvl_pt_node[threadIdx.x] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
    }
  }

  __syncthreads();

  for(int iter = 0 ; iter<num_iters; iter++){

    uint32_t edge_group_next_lvl = lvl_pt_edge[iter+1] >> 10;
    
    for(uint32_t current_edge_group = (lvl_pt_edge[iter] >> 10); current_edge_group < edge_group_next_lvl; current_edge_group++){

      __syncthreads();
      
      const uint32_t nieg_current_raw = graph_UD->graph_u_b.NNIEG[nnieg_pt[iter] + current_edge_group - (lvl_pt_edge[iter] >> 10)];
      
      const uint32_t nieg_current = nieg_current_raw & 0xffff;
      const uint32_t nieg_flag = nieg_current_raw & 0xf0000000;
      const uint32_t egc = (nieg_current_raw & 0xff0000) >> 16;

      if(nieg_flag == 0x40000000){
	assert(nieg_current == 1 && egc == 0);// sanity check
	// current edge group is continuation of the edge list of a node with nnbr>1024
	// previous iter already handled it, continue.
	// this is to maintain the simple dist/path update
	continue;
      }

      if(nieg_flag == 0x80000000){
	assert(nieg_current == 1 && egc);// sanity check
	// current edge group is the start of the edge list of a node with nnbr>1024
	// current block handles all corresponding edge groups of current node
	if(threadIdx.x==0){
	  current[0] = graph_UD->graph_u_b.vertex_ID[(current_edge_group<<10)];
	  dist[0] = result->weight[current[0]];
	  if ( fprev ) prev[0] = result->prev[current[0]];
	}
	
	__syncthreads();
	const uint32_t current_0 = current[0];
        assert( current_0 < num_nodes );
	
	for(uint32_t i=0; i<egc; i++){
	  nbr_eid = ((current_edge_group + i)<<10) + threadIdx.x;
	  weight = graph_UD->graph_u_b.weights[nbr_eid];
	  nbr = graph_UD->graph_u_b.neighbors[nbr_eid];
	  
          bool dist_changed = false;
	  uint32_t new_dist;
	  if(weight != NPP_MAX_32U){
	    uint32_t dist_nbr = result->weight[nbr];
	    new_dist = dist_nbr + weight;
	    if(dist_nbr != NPP_MAX_32U){
	      uint32_t old_dist = atomicMin(&(dist[0]), new_dist);
              dist_changed = old_dist > new_dist;
	      // check for overflow
	      assert(new_dist >= weight && new_dist >= dist_nbr);
	    }
	  }
	  
	  __syncthreads();
          if ( fprev && dist_changed && dist[0] == new_dist ) prev[0] = nbr_eid;
	}
	
	__syncthreads();
	if(threadIdx.x==0){
	  result->weight[current_0] = dist[0];
	  if ( fprev )
            result->prev[current_0] = graph_UD_c.graph_u_b.midpoint[prev[0]];
	}
	
      } else {
	current[threadIdx.x] = graph_UD->graph_u_b.vertex_ID[(current_edge_group<<10) + threadIdx.x];
	nbr_eid = (current_edge_group<<10) + threadIdx.x;
	weight = graph_UD->graph_u_b.weights[nbr_eid];
	nbr = graph_UD->graph_u_b.neighbors[nbr_eid];
	
	__syncthreads();
	uint32_t current_0 = current[0];
        assert( current_0 < num_nodes );
	
	assert(nieg_current <= max_nieg); // max_nieg is limited by shared memory
	
	for(uint32_t i=threadIdx.x; i<nieg_current; i+=1024){
	  dist[i] = result->weight[current_0 + i];
	}
	
	__syncthreads();
	/// RRRR add overload checks
        bool dist_changed = false;
	uint32_t new_dist;
	if(weight != NPP_MAX_32U){
	  uint32_t dist_nbr = result->weight[nbr];
	  new_dist = dist_nbr + weight;
	  if(dist_nbr != NPP_MAX_32U){
	    uint32_t old_dist = atomicMin(&(dist[current[threadIdx.x] - current_0]), new_dist);
            dist_changed = old_dist > new_dist;
	    // check for overflow
	    assert(new_dist >= weight && new_dist >= dist_nbr);
	  }
	}

        __syncthreads();
	
        if ( dist_changed && dist[current[threadIdx.x]-current_0] == new_dist )
          {
            result->weight[current[threadIdx.x]] = new_dist;
            if ( fprev )
              result->prev[current[threadIdx.x]] =
                graph_UD_c.graph_u_b.midpoint[nbr_eid];
          }
      }

    }
  }
  
}


// update the overlay nodes using the APSP results
__global__ void
__launch_bounds__(1024,1)
cu_CH_query_OL()
{

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  const bool fprev = !chopts_c.query_distance_only; // Find Predecessor

  const uint32_t block_dim = 1024;
  assert(blockDim.x==block_dim);

  const int npt = (K_MAX + block_dim - 1)/block_dim;
  const int i_iters = K_MAX / (APSP_SH_DIM / OL_BLSZ);
  const int q_iters = APSP_SH_DIM / block_dim;

  __shared__ uint32_t apsp_sh[APSP_SH_DIM];
  __shared__ uint32_t dist_sh[K_MAX];

  
  uint32_t nbr[npt];
  uint32_t weight[npt];

  __shared__ uint32_t lvl_u_pt[2];

  uint32_t lid = threadIdx.x>>4;
  uint32_t tnid = threadIdx.x - (lid<<4);
  
  uint32_t num_nodes = graph_UD->graph_u_f.num_nodes;
  uint32_t num_levels = graph_UD->node_levels[num_nodes-1] + 1; // levels start from 0

  if(threadIdx.x==0){
    lvl_u_pt[0] = graph_UD->node_levels_pt[num_levels - 1];
    lvl_u_pt[1] = num_nodes;
  }

  __syncthreads();

  const uint32_t APSP_dim = graph_UD->overlay_APSP_path.num_nodes;

  // each block handles OL_BLSZ(=16) of the K OL nodes (hence K/16 blocks)
  assert(gridDim.x==APSP_dim/OL_BLSZ);


  for(uint32_t p=0; p<npt; p++){
    if(lvl_u_pt[0] + p*block_dim + threadIdx.x < lvl_u_pt[1]){
      dist_sh[p*block_dim + threadIdx.x] = result->weight[lvl_u_pt[0] + p*block_dim + threadIdx.x];
      // technically only the first 16 threads need to do this
      weight[p] = dist_sh[p*block_dim + threadIdx.x];
      nbr[p] = NPP_MAX_32U;
    } else if (p*block_dim + threadIdx.x < K_MAX){
      // the above condition is needed to allow APSP_SH_DIM that's not a multiple of block_dim
      dist_sh[p*block_dim + threadIdx.x] = NPP_MAX_32U;
    }
  }

  // note, APSP path is s->d
  // we're loading OL_BLSZ(=16) columns by 128 rows at each i iter
  
  // Not sure why, but this syncthreads forces reconvergence of the
  // if/else in the loop above, whereas the syncthreads in the loop
  // right below does not.
  __syncthreads();
  for(uint32_t i=0; i<i_iters; i++){
    // need to sync since threads were cooperating to load APSP into shared
    __syncthreads();
    // each iteration of j loading 16 columns by 64 rows
    for(uint32_t q=0; q<q_iters; q++){
      const uint32_t rid = i*128 + q*64 + lid;
      const uint32_t cid = blockIdx.x*OL_BLSZ + tnid;
      // query main ensures validity of cid when determining grid_dim, rid has to be checked here
      if(rid < APSP_dim){
	apsp_sh[q*block_dim + threadIdx.x] = graph_UD->overlay_APSP_path.dist[rid*APSP_dim + cid];
      }
    }
    // need to sync since threads were cooperating to load APSP into shared
    __syncthreads();
    // note that only 16 nodes per block, so only the corresponding lid handles the distance relaxation
    for(uint32_t p=0; p<npt; p++){
      // rid is tied to blockIdx.x, no need to have extra checks here for cid
      if(lid + p*(block_dim/OL_BLSZ) == blockIdx.x){
        auto loop = [&](bool do_assert)
          {
            for(uint32_t j=0; j<128; j++){
              // relax the edge (with care regarding inf edges)
              if ( weight[p] > dist_sh[i*128 + j] + apsp_sh[j*16 + tnid]
                   && dist_sh[i*128 + j] != NPP_MAX_32U
                   && apsp_sh[j*16 + tnid] != NPP_MAX_32U ) {
                weight[p] = dist_sh[i*128 + j] + apsp_sh[j*16 + tnid];
                // check for overflow
                assert( !do_assert
                        || weight[p] >= dist_sh[i*128 + j]
                        && weight[p] >= apsp_sh[j*16 + tnid] );
                nbr[p] = i*128 + j;
              }
            }
          };
        if ( chopts_c.asserts_costly_skip ) loop(false); else loop(true);
      }
    }
  }
  // update the results (only the corresponding lid)
  for(uint32_t p=0; p<npt; p++){
    // rid is tied to blockIdx.x, no need to have extra checks here for cid
    if(lid + p*(block_dim/OL_BLSZ) == blockIdx.x){
      if( (lvl_u_pt[0] + p*block_dim + threadIdx.x < lvl_u_pt[1]) && (nbr[p] != NPP_MAX_32U) ){
	result->weight[lvl_u_pt[0] + p*block_dim + threadIdx.x] = weight[p];
        if ( fprev )
          result->prev[lvl_u_pt[0] + p*block_dim + threadIdx.x] = (lvl_u_pt[0] + graph_UD->overlay_APSP_path.prev[nbr[p] * APSP_dim + p*block_dim + threadIdx.x]) | 0xC0000000; // highest 2 bit == 11 means OL graph 
      }
    }
  }

}

// downwards pass on all nodes in lvl
__global__ void
__launch_bounds__(1024,1)
cu_CH_query_d(int lvl)
{

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  assert(blockDim.x == 1024);
  
  __shared__ uint32_t lvl_pt_edge[2];
  __shared__ uint32_t lvl_pt_node[2];
  __shared__ uint32_t nnieg_pt;

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  const bool fprev = !chopts_c.query_distance_only; // Find Predecessor

  const uint32_t max_nieg = 5*1024;

  /// RRR for now doing only one edge group per iter per block to figure out correctness etc.
  __shared__ uint32_t dist[max_nieg];// note that it's possible that nieg > 1024
  __shared__ uint32_t prev[1];
  __shared__ nodeid_t nid_0_s;

  /// RRRR dist has to be shared, but the following can be either registers or shared (balance later)
  uint32_t weight;
  uint32_t nbr;
  uint32_t nbr_eid;
  
  uint32_t num_nodes = graph_UD->graph_u_f.num_nodes;
  uint32_t num_levels = graph_UD->node_levels[num_nodes-1] + 1; // levels start from 0
    
  if(lvl == num_levels-1){
    // last lvl (overlay)
    if(threadIdx.x==0){
      lvl_pt_node[0] = graph_UD->node_levels_pt[lvl];
      lvl_pt_edge[0] = graph_UD->graph_d_b.pointer[lvl_pt_node[0]];
      lvl_pt_edge[1] = graph_UD->graph_d_b.empty_pointer;
      if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
	nnieg_pt = lvl_pt_node[0];
      } else {
	const uint32_t lvl_cut_off_pt = graph_UD->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
	nnieg_pt =  lvl_cut_off_pt + (lvl_pt_node[0] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
      }
    }
  } else {
    if(threadIdx.x < 2){
      lvl_pt_node[threadIdx.x] = graph_UD->node_levels_pt[lvl + threadIdx.x];
      lvl_pt_edge[threadIdx.x] = graph_UD->graph_d_b.pointer[lvl_pt_node[threadIdx.x]];
    }
    if(threadIdx.x==0){
      if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
	nnieg_pt = lvl_pt_node[0];
      } else {
	const uint32_t lvl_cut_off_pt = graph_UD->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
	nnieg_pt =  lvl_cut_off_pt + (lvl_pt_node[0] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
      }
    }
  }

  __syncthreads();
  
  uint32_t edge_group_per_iter = gridDim.x;
  uint32_t edge_group_next_lvl = lvl_pt_edge[1] >> 10;

  for(uint32_t current_edge_group = (lvl_pt_edge[0] >> 10) + blockIdx.x; current_edge_group < edge_group_next_lvl; current_edge_group += edge_group_per_iter){
    
    const uint32_t nieg_current_raw = graph_UD->graph_d_b.NNIEG[nnieg_pt + current_edge_group - (lvl_pt_edge[0] >> 10)];

    const uint32_t nieg_current = nieg_current_raw & 0xffff;
    const uint32_t nieg_flag = nieg_current_raw & 0xf0000000;
    const uint32_t egc = (nieg_current_raw & 0xff0000) >> 16;

    if(nieg_flag == 0x40000000){
      assert(nieg_current == 1 && egc == 0);// sanity check
      // current edge group is continuation of the edge list of a node with nnbr>1024
      // a different block is handling it, continue.
      // this is to maintain the simple static work distribution between blocks
      continue;
    }


    if(nieg_flag == 0x80000000){
      assert(nieg_current == 1 && egc);// sanity check
      // current edge group is the start of the edge list of a node with nnbr>1024
      // current block handles all corresponding edge groups of current node
      if(threadIdx.x==0){
	nid_0_s = graph_UD_c.graph_d_b.vertex_ID[(current_edge_group<<10)];
	dist[0] = result->weight[nid_0_s];
        prev[0] = NPP_MAX_32U;
      }

      __syncthreads();
      const uint32_t current_0 = nid_0_s;
      assert( current_0 < num_nodes );

      for(uint32_t i=0; i<egc; i++){
	nbr_eid = ((current_edge_group + i)<<10) + threadIdx.x;
	weight = graph_UD->graph_d_b.weights[nbr_eid];
	nbr = graph_UD->graph_d_b.neighbors[nbr_eid];

        bool dist_changed = false;
	uint32_t new_dist;
	if(weight != NPP_MAX_32U){
	  uint32_t dist_nbr = result->weight[nbr];
	  new_dist = dist_nbr + weight;
	  if(dist_nbr != NPP_MAX_32U){
	    uint32_t old_dist = atomicMin(&(dist[0]), new_dist);
            dist_changed = old_dist > new_dist;
	    // check for overflow
	    assert(new_dist >= weight && new_dist >= dist_nbr);
	  }
	}
	
	__syncthreads();
        if ( fprev && dist_changed && dist[0] == new_dist ) prev[0] = nbr_eid;
	__syncthreads();
      }

      __syncthreads();
      if(threadIdx.x==0){
	result->weight[current_0] = dist[0];
        if ( prev[0] != NPP_MAX_32U )
          result->prev[current_0] = graph_UD_c.graph_d_b.midpoint[prev[0]];
        assert(false);  // Thank you for finding a testcase.
      }

    } else {
      const nodeid_t nid =
        graph_UD->graph_d_b.vertex_ID[(current_edge_group<<10) + threadIdx.x];
      nbr_eid = (current_edge_group<<10) + threadIdx.x;
      weight = graph_UD->graph_d_b.weights[nbr_eid];
      nbr = graph_UD->graph_d_b.neighbors[nbr_eid];
      if ( !threadIdx.x ) nid_0_s = nid;

      __syncthreads();
      const nodeid_t nid_0 = nid_0_s;
      // Bug workaround. 26 December 2019, 17:44:28 CST
      if ( texas_workaround && nid_0 == NPP_MAX_32U ) continue;

      assert( nid_0 < num_nodes );

      assert(nieg_current <= max_nieg); // max_nieg is limited by shared memory

      for(uint32_t i=threadIdx.x; i<nieg_current; i+=1024){
	dist[i] = result->weight[nid_0 + i];
      }

      __syncthreads();
      /// RRRR add overload checks
      bool dist_changed = false;
      uint32_t new_dist;
      if(weight != NPP_MAX_32U){
	uint32_t dist_nbr = result->weight[nbr];
	new_dist = dist_nbr + weight;
	if(dist_nbr != NPP_MAX_32U){
	  uint32_t old_dist = atomicMin( &dist[ nid - nid_0 ], new_dist );
          dist_changed = old_dist > new_dist;
	  // check for overflow
	  assert(new_dist >= weight && new_dist >= dist_nbr);
	}
      }
    
      __syncthreads();
      if ( dist_changed && dist[nid-nid_0] == new_dist )
        {
          result->weight[nid] = new_dist;
          if ( fprev )
            result->prev[nid] = graph_UD_c.graph_d_b.midpoint[nbr_eid];
        }
    }
    
  }
  
}


// downwards pass on all nodes in num_iter levels [lvl : lvl+num_iters) (num_iter<255)
__global__ void
__launch_bounds__(1024,1)
cu_CH_query_d_multi_iter(int lvl, int num_iters)
{

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  const bool fprev = !chopts_c.query_distance_only; // Find Predecessor

  assert(blockDim.x == 1024);
  assert(gridDim.x == 1);

  assert( num_iters <= query_multi_max_iters );

  __shared__ uint32_t lvl_pt_edge[query_multi_max_iters+1];
  __shared__ uint32_t lvl_pt_node[query_multi_max_iters+1];
  __shared__ uint32_t nnieg_pt[query_multi_max_iters];

  const uint32_t max_nieg = 5*1024;

  /// RRR for now doing only one edge group per iter per block to figure out correctness etc.
  __shared__ uint32_t dist[max_nieg];// note that it's possible that nieg > 1024
  __shared__ uint32_t prev[1];
  __shared__ nodeid_t nid_0_s;

  /// RRRR dist has to be shared, but the following can be either registers or shared (balance later)
  uint32_t weight;
  uint32_t nbr;
  uint32_t nbr_eid;
  
  uint32_t num_nodes = graph_UD->graph_u_f.num_nodes;
  uint32_t num_levels = graph_UD->node_levels[num_nodes-1] + 1; // levels start from 0

  // the OL level should be zero on the downwards DAG
  assert(lvl+num_iters-1 <= num_levels-2);
  if(threadIdx.x<num_iters+1){
    lvl_pt_node[threadIdx.x] = graph_UD->node_levels_pt[lvl + threadIdx.x];
    lvl_pt_edge[threadIdx.x] = graph_UD->graph_d_b.pointer[lvl_pt_node[threadIdx.x]];
  }
  if(threadIdx.x<num_iters){
    if(lvl+threadIdx.x < NNIEG_OVER_ALLOC_CUT_OFF){
      nnieg_pt[threadIdx.x] = lvl_pt_node[threadIdx.x];
    } else {
      const uint32_t lvl_cut_off_pt = graph_UD->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
      nnieg_pt[threadIdx.x] =  lvl_cut_off_pt + (lvl_pt_node[threadIdx.x] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
    }
  }

  __syncthreads();

  for(int iter = num_iters-1; iter>=0; iter--){

    uint32_t edge_group_next_lvl = lvl_pt_edge[iter+1] >> 10;
    
    for(uint32_t current_edge_group = (lvl_pt_edge[iter] >> 10); current_edge_group < edge_group_next_lvl; current_edge_group++){
      
      const uint32_t nieg_current_raw = graph_UD->graph_d_b.NNIEG[nnieg_pt[iter] + current_edge_group - (lvl_pt_edge[iter] >> 10)];
      
      const uint32_t nieg_current = nieg_current_raw & 0xffff;
      const uint32_t nieg_flag = nieg_current_raw & 0xf0000000;
      const uint32_t egc = (nieg_current_raw & 0xff0000) >> 16;

      __syncthreads();

      if(nieg_flag == 0x40000000){
	assert(nieg_current == 1 && egc == 0);// sanity check
	// current edge group is continuation of the edge list of a node with nnbr>1024
	// previous iter already handled it, continue.
	// this is to maintain the simple dist/path update
	continue;
      }

      if(nieg_flag == 0x80000000){
	assert(nieg_current == 1 && egc);// sanity check
	// current edge group is the start of the edge list of a node with nnbr>1024
	// current block handles all corresponding edge groups of current node
	if(threadIdx.x==0){
	  nid_0_s = graph_UD->graph_d_b.vertex_ID[(current_edge_group<<10)];
	  dist[0] = result->weight[nid_0_s];
          prev[0] = NPP_MAX_32U;
	}
	
	__syncthreads();
	const uint32_t current_0 = nid_0_s;
        assert( current_0 < num_nodes );
	
	for(uint32_t i=0; i<egc; i++){
	  nbr_eid = ((current_edge_group + i)<<10) + threadIdx.x;
	  weight = graph_UD->graph_d_b.weights[nbr_eid];
	  nbr = graph_UD->graph_d_b.neighbors[nbr_eid];
	  
          bool dist_changed = false;
	  uint32_t new_dist;
	  if(weight != NPP_MAX_32U){
	    uint32_t dist_nbr = result->weight[nbr];
	    new_dist = dist_nbr + weight;
	    if(dist_nbr != NPP_MAX_32U){
	      uint32_t old_dist = atomicMin(&(dist[0]), new_dist);
              dist_changed = old_dist > new_dist;
	      // check for overflow
	      assert(new_dist >= weight && new_dist >= dist_nbr);
	    }
	  }
	  
	  __syncthreads();
          if ( fprev && dist_changed && dist[0] == new_dist ) prev[0] = nbr_eid;
	  __syncthreads();
	}
	
	__syncthreads();
	if(threadIdx.x==0){
	  result->weight[current_0] = dist[0];
	  if ( prev[0] != NPP_MAX_32U )
            result->prev[current_0] = graph_UD_c.graph_d_b.midpoint[prev[0]];
	}
	
      } else {
        const nodeid_t nid =
          graph_UD->graph_d_b.vertex_ID[(current_edge_group<<10) + threadIdx.x];
	nbr_eid = (current_edge_group<<10) + threadIdx.x;
	weight = graph_UD->graph_d_b.weights[nbr_eid];
	nbr = graph_UD->graph_d_b.neighbors[nbr_eid];
        if ( !threadIdx.x ) nid_0_s = nid;

	__syncthreads();
	const nodeid_t nid_0 = nid_0_s;
        // Bug workaround. 26 December 2019, 17:44:28 CST
        if ( texas_workaround && nid_0 == NPP_MAX_32U ) continue;

        assert( nid_0 < num_nodes );

	assert(nieg_current <= max_nieg); // max_nieg is limited by shared memory
    
	for(uint32_t i=threadIdx.x; i<nieg_current; i+=1024){
	  dist[i] = result->weight[nid_0 + i];
	}

	__syncthreads();
	/// RRRR add overload checks
        bool dist_changed = false;
	uint32_t new_dist;
	if(weight != NPP_MAX_32U){
	  uint32_t dist_nbr = result->weight[nbr];
	  new_dist = dist_nbr + weight;
	  if(dist_nbr != NPP_MAX_32U){
	    uint32_t old_dist = atomicMin(&(dist[ nid - nid_0]), new_dist);
            dist_changed = old_dist > new_dist;
	    // check for overflow
	    assert(new_dist >= weight && new_dist >= dist_nbr);
	  }
	}
    
        __syncthreads();
        if ( dist_changed && dist[nid-nid_0] == new_dist )
          {
            result->weight[nid] = new_dist;
            if ( fprev )
              result->prev[nid] = graph_UD_c.graph_d_b.midpoint[nbr_eid];
          }
      }
    
    }
  }
  
}


// unpack OL, this kernel can be eliminated if a midpoint matrix of size K*K is allocated
// such a matrix would eliminate the need for graph_OL, so the memory cost would likely be similar
// RRRR if this kernel's time is significant enough I should do this
__global__ void
__launch_bounds__(1024,1)
cu_CH_path_unpack_OL()
{
  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  path_t* const result = &result_ud_c;
  
  assert(blockDim.x == 1024);

  __shared__ volatile uint32_t current_nnbr[32];
  __shared__ volatile uint32_t current_pt[32];
  // used for broadcasting prev (since the broadcaster is undetermined, can't used shuffle)
  __shared__ volatile uint32_t prev_sh[32];
  /// this is purely a safety variable and can be removed
  __shared__ volatile uint32_t edge_found[32];
  
  const nodeid_t num_nodes = graph_UD->graph_u_f.num_nodes;

  nodeid_t lid = threadIdx.x >> 5; // warp ID
  uint32_t tnid = threadIdx.x - (lid<<5); // thread's id within corresponding warp

  // pointer to the beginning of overlay nodes
  const uint32_t overlay_pt = graph_UD->node_levels_pt[graph_UD->node_levels[num_nodes - 1]];

  uint32_t current = overlay_pt + (blockIdx.x << 5) + lid;
  uint32_t prev;

  while(current < num_nodes){

    prev = result->prev[current];
    __syncwarp();
    if(tnid == 0){
      prev_sh[lid] = prev;
    }
    __syncwarp();
    if(prev != NPP_MAX_32U){

      // only interested if OL.
      if((prev & 0xC0000000) == 0xC0000000 ){
	uint32_t prev_OL = (prev & 0x3FFFFFFF) - overlay_pt;
	uint32_t current_OL = current - overlay_pt;

	// unpack OL edge
	if(tnid == 0){
	  edge_found[lid] = 0;
	}
	if(tnid == 0){
	  current_nnbr[lid] = graph_UD->overlay_CH.num_neighbors[current_OL];
	  current_pt[lid] = graph_UD->overlay_CH.pointer[current_OL];
	}
        __syncwarp();
	uint32_t nbr = tnid;
	while(nbr < current_nnbr[lid]){
	  uint32_t nbr_id = graph_UD->overlay_CH.neighbors[current_pt[lid] + nbr];
	  if(nbr_id == prev_OL){

            uint32_t midpoint =
              graph_UD->overlay_CH.midpoint[current_pt[lid] + nbr];
	    assert( midpoint != NODEID_NULL );
	    edge_found[lid] = 1;
            prev = midpoint;
            prev_sh[lid] = prev;
          }

	  nbr += 32;
	}
        __syncwarp();
	// make sure that the edge was actually found
	if(tnid == 0){
	  assert(edge_found[lid] == 1);
	}
	// broadcast prev through shared
	prev = prev_sh[lid];

      }
    }

    // thread zero of each warp renumbers and writes the unpacked prev
    if(tnid == 0){
      result->prev[current] = prev;
    }
      
    current += gridDim.x * 32;
  }

}


// renumbers the path result of query to correspond to the input node_id space.
// for simplicity in shared allocation, we'll only call this with 1024 blocks (32 warps)
__global__ void
__launch_bounds__(1024,1)
cu_CH_path_unpack_renum()
{
  constexpr uint32_t block_dim = 1024;
  assert(blockDim.x == block_dim);

  const cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;
  const path_t* const result_in = &result_ud_c;

  const uint32_t tid = blockIdx.x * block_dim + threadIdx.x;
  const uint32_t num_thd = gridDim.x * block_dim;

  const nodeid_t num_nodes = graph_UD->graph_u_f.num_nodes;

  if ( chopts_c.query_distance_only )
    {
      for ( nodeid_t nid_or = tid; nid_or < num_nodes; nid_or += num_thd )
        result_or_c.weight[nid_or] =
          result_ud_c.weight[graph_UD_c.node_idx_inv[nid_or]];
      return;
    }

  constexpr bool gather = true;

  for ( nodeid_t nid = tid;  nid < num_nodes;  nid += num_thd ) {

    const nodeid_t current_origin = gather ? nid : graph_UD_c.node_idx[nid];
    const nodeid_t current = gather ? graph_UD_c.node_idx_inv[nid] : nid;

    const nodeid_t dist = result_ud_c.weight[current];
    uint32_t prev = result_in->prev[current];

    assert( prev == NPP_MAX_32U || ( prev & 0xC0000000 ) == 0 );

    // Write the output results.
    result_or_c.dist_prev[current_origin] = { dist, prev };
  }

}

