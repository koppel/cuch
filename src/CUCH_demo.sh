#!/bin/sh

## CUCH - CUDA Graph Contraction and Query
## Copyright (c) 2020 Louisiana State University
##


#
# This script will automatically download the CAL_t graphs from the
# DIMACS 9 challenge as provided in
# http://www.dis.uniroma1.it/~challenge9. This graph is next
# preprocessed using CUCH. A SSSP query from a random node is then
# performed using this preprocessed graph. Finally the result is
# tested for correctness.
#

GRAPHDIR=../gr
DIMACSURL=http://users.diag.uniroma1.it/challenge9/data/USA-road-t

# Download the input graph CAL_t and save it in graph directory.

GRAPHNAME=USA-road-t.CAL.gr.gz
GRAPHPATH=$GRAPHDIR/$GRAPHNAME

if [ -a $GRAPHPATH ]; then
 echo Using graph found at $GRAPHPATH
else
  wget -P $GRAPHDIR/ $DIMACSURL/$GRAPHNAME
fi

# CUCH preprocessing with default options
./cuch -a grph_cu_CH_prep -f $GRAPHPATH -o CAL_t_CUCH

# CUCH SSSP query from a random source node with default options
./cuch -a grph_cu_CH_query -f CAL_t_CUCH.bin -s n1 -o CAL_t_rand

# CUCH preprocessing with default options
./cuch -a path_evaluate -f $GRAPHPATH -b CAL_t_rand.path


