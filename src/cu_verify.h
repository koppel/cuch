/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef CU_VERIFY_H
#define CU_VERIFY_H

#include <unordered_map>
#include <vector>
#include "graphs.h"



typedef weight_t Weight_Sum;

class SSSP_Node {
public:
  nodeid_t idx;
  int heap_idx; // Index into heap used to implement a priority queue.
  Weight_Sum dist;
  int dist_hops;
  int ns_serial;
  // Max hops of all nodes visited at time dist updated.
  int relax_search_hops_max;
  bool from_cc; // Distance was updated by the contraction candidate.
};

template<typename T>
class Array_Heap {
public:
  Array_Heap(size_t n):array(n),heap(n+1),ns_serial(0),tail(0)
  { serial = 0; init(array); }
  Array_Heap():ns_serial(0),tail(0){  }

  void init(size_t n)
  {
    if ( array.size() == n ) return;
    array.resize(n);
    heap.resize(n+1);
    init(array);
  }

  void init(std::vector<T>& arrayp)
  {
    heap[0] = NULL;
    for ( auto& nd: arrayp )
      {
        const size_t g_idx = &nd - arrayp.data();
        nd.idx = g_idx;
        nd.ns_serial = 0;
        heap_assign( g_idx+1, &nd );
      }
  }

  operator std::vector<T>&() { return array; }
  T& operator [](int idx) { return array[idx]; }
  bool visited (int idx) const { return array[idx].ns_serial == ns_serial; }

  int heap_reset() { return heap_reset(ns_serial+1); }
  int heap_reset(int next_ns_serial)
    {
      assert( next_ns_serial > ns_serial );
      ns_serial = next_ns_serial;
      tail = 1;
      return ns_serial;
    }

private:
  std::vector<T> array;
  std::vector<T*> heap;
  int ns_serial, tail, serial;

  void heap_assign(int idx, T* nd){ heap[ nd->heap_idx=idx ] = nd; }
  void heap_swap(int idx1, T* nd2)
  {
    T* const nd1 = heap[idx1];
    const int idx2 = nd2->heap_idx;
    heap_assign( idx1, nd2 );
    heap_assign( idx2, nd1 );
  }

public:
  void priority_set(int array_idx, Weight_Sum dist_new, int hops_new)
  { priority_set(&array[array_idx], dist_new, hops_new); }
  void priority_set(T *nd, Weight_Sum dist_new, int hops_new)
  {
    // One day this will work for priority reductions too.
    assert( nd->ns_serial != ns_serial || nd->dist >= dist_new );
    nd->dist = dist_new;
    nd->dist_hops = hops_new;

    if ( nd->ns_serial != ns_serial )
      {
        assert( nd->heap_idx >= tail );
        heap_swap(tail,nd);
        nd->ns_serial = ns_serial;
        tail++;
      }

    int idx = nd->heap_idx;
    assert( idx < tail );

    while ( idx > 1 )
      {
        const int p_idx = idx >> 1;
        T* const p_nd = heap[p_idx];
        if ( p_nd->dist <= dist_new ) break;
        heap_assign(idx,p_nd);
        idx = p_idx;
      }
    if ( idx != nd->heap_idx ) heap_assign(idx,nd);
    audit();
  }

  T* heap_pop()
  {
    assert( tail > 0 && tail < heap.size() );
    if ( tail == 1 ) return NULL;
    T* const rv = heap[1];
    tail--;
    if ( tail == 1 ) return rv;

    T* const nd = heap[tail];
    const Weight_Sum dist = nd->dist;
    heap_assign(tail,rv);

    int p_idx = 1;

    while ( true )
      {
        const int l_idx = p_idx << 1;
        if ( l_idx >= tail ) break;
        T* const l_nd = heap[l_idx];
        if ( l_idx + 1 == tail )
          {
            if ( dist > l_nd->dist ){ heap_assign(p_idx,l_nd); p_idx = l_idx; }
            break;
          }
        T* const r_nd = heap[l_idx+1];
        const bool l_smaller = l_nd->dist <= r_nd->dist;
        const int s_idx = l_smaller ? l_idx : l_idx + 1;
        T* const s_nd = l_smaller ? l_nd : r_nd;
        if ( dist <= s_nd->dist ) break;
        heap_assign(p_idx,s_nd);
        p_idx = s_idx;
      }
    heap_assign(p_idx,nd);
    audit();
    return rv;
  }
  size_t heap_size() { return tail - 1; }
  void audit()
    {
      const bool no_audit = true;
      if ( no_audit ) return;
      assert( heap[0] == NULL );
      std::vector<int> ch(heap.size());
      for ( uint i=1; i<heap.size(); i++ )
        {
          const int idx = heap[i]->idx;
          assert( heap[i]->heap_idx == i );
          assert( idx >= 0 && idx < heap.size() - 1 );
          assert( ch[idx]++ == 0 );
          const int l_idx = i << 1;
          assert( l_idx >= tail || heap[i]->dist <= heap[l_idx]->dist );
          assert( l_idx+1 >= tail || heap[i]->dist <= heap[l_idx+1]->dist );
        }
      serial++;
    }
};

class cu_Contract_Query_State;

class Contract_Dist_Verify {
public:
  Contract_Dist_Verify()
    :host_graphs_ours(true),
     g_orig_h(nullptr),g_curr_h(nullptr),g_UD_h(nullptr){};
  void init(cu_graph_CH_bi_t *graph_d);
  void init_h(cu_graph_CH_bi_t *graph_h, cu_graph_CH_UD_t *graph_UD_h);
  void init_h(cu_graph_CH_bi_t *graph_h);
  void init_h(cu_graph_CH_UD_t *graph_UD_h);
  ~Contract_Dist_Verify();
  void verify_self_consistency(cu_graph_CH_bi_t *g_d, bool check_fwd = true);
  void verify
  (cu_Contract_Query_State *cq_state,
   cu_graph_CH_bi_t *curr_d, cu_graph_CH_UD_t *UD_d);
  std::string verify
  (cu_Contract_Query_State *cq_state, cu_graph_CH_UD_t *UD_final_d);
  void verify(cu_Contract_Query_State *cq_state, graph_CH_bi_t *graph);
  void verify(path_t *result);

  int n_queries_per_iter_set(int n);
private:
  void init();
  std::vector<SSSP_Node>& sssp
  ( Array_Heap<SSSP_Node>& sssp_nodes, cu_graph_CH_bi_t *g_h, nodeid_t src );
  nodeid_t nnodes_or;
  Array_Heap<SSSP_Node> h_sssp_orig;
  bool host_graphs_ours;
  cu_graph_CH_bi_t *g_orig_h, *g_curr_h;
  cu_graph_CH_UD_t *g_UD_h;
  std::unordered_map<Ege,nodeid_t> or_edge_weights;
  int n_queries_per_iter;
  size_t n_zero_wht; // Number of zero-weight edges.
  bool is_not_dir;  // For each (s,d) in E there's a (d,s) with same weight.
  bool is_wht_dir; // For each (s,d) in E there's a (d,s), but weights differ.
  bool is_topol_dir; // There are some (s,d) in E without a (d,s) in E.
};

#endif
