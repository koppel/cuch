/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __MAIN_H__
#define __MAIN_H__

#define __STDC_LIMIT_MACROS
#define __STDC_CONSTANT_MACROS
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <time.h>
#include <string>
#include <boost/filesystem.hpp>
typedef boost::filesystem::path fspath;

/*
 * Primitives
 */

#ifdef NODEID_64
#define nodeid_t uint64_t
#define NODEID_NULL UINT64_MAX
#define FNODEID PRIu64
#define SNODEID SCNu64
#else
#define nodeid_t uint32_t
#define NODEID_NULL UINT32_MAX
#define FNODEID PRIu32
#define SNODEID SCNu32
#endif

#ifdef WEIGHT_64
#define weight_t uint64_t
#define WEIGHT_INF UINT64_MAX
#define FWEIGHT PRIu64
#define SWEIGHT SCNu64
#else
#define weight_t uint32_t
#define WEIGHT_INF UINT32_MAX
#define FWEIGHT PRIu32
#define SWEIGHT SCNu32
#endif

#if defined NBOR_64
#define nnbor_t uint64_t
#define FNBOR PRIu64
#define SNBOR SCNu64
#elif defined NBOR_32
#define nnbor_t uint32_t
#define FNBOR PRIu32
#define SNBOR SCNu32
#else
#define nnbor_t uint16_t
#define FNBOR PRIu16
#define SNBOR SCNu16
#endif

#if UINT_MAX == UINT32_MAX
#define GRAPH_CL_SIZET uint
#else
#define GRAPH_CL_SIZET ulong
#endif

#define PFVA(fmt,func) va_list ap; va_start(ap,fmt); func(fmt,ap); va_end(ap);
class Print_Filter {
public:
  Print_Filter():inited(false),pr_tune(false),pr_loop(false){}

  void user(const char* fmt ...) __attribute__ ((format(printf,2,3)))
  {
    va_list ap;
    va_start(ap, fmt);
    vprintf(fmt,ap);
    va_end(ap);
  }
  void warn(const char* fmt ...) __attribute__ ((format(printf,2,3)))
  {
    va_list ap;
    va_start(ap, fmt);
    vprintf(fmt,ap);
    va_end(ap);
  }
  void fatal_user(const char* fmt ...) __attribute__ ((format(printf,2,3)))
  {
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr,fmt,ap);
    va_end(ap);
    exit(1);
  }

  // Note: Functions below can be called by wrappers.
  void tune(const char* fmt, va_list ap)
  {
    assert( inited );
    if ( pr_tune ) vprintf(fmt,ap);
  }
  void tune(const char* fmt ...) __attribute__ ((format(printf,2,3)))
  { PFVA(fmt,tune); }

  void loop(const char* fmt, va_list ap)
  {
    assert( inited );
    if ( pr_loop ) vprintf(fmt,ap);
  }
  void loop(const char* fmt ...) __attribute__ ((format(printf,2,3)))
  { PFVA(fmt,loop); }

  void tune_loop_set(bool t, bool l)
    {
      inited = true;
      pr_tune = t;
      pr_loop = l;
    }

private:
  bool inited, pr_tune, pr_loop;
};

extern Print_Filter pr;

void pr_tune(const char* fmt ...) __attribute__ ((format(printf,1,2)));
inline void pr_tune(const char* fmt ...){ PFVA(fmt,pr.tune); }

void pr_loop(const char* fmt ...) __attribute__ ((format(printf,1,2)));
inline void pr_loop(const char* fmt ...) { PFVA(fmt,pr.loop); }
#undef PVFA

// Open a stream for reading given a path.  Use bzcat or zcat if
// compressed version found.
//
class File_Open_Read {
public:
  File_Open_Read(const std::string pathp);
  ~File_Open_Read();
  explicit operator bool () { return f != NULL; }
  operator FILE* () { return f; }
  bool ext_match(const char* ext);
  std::string path_opened_get(){ return path_opened; }

private:
  FILE *f;
  bool is_pipe;
  std::string path_opened;
  std::string path_sans_co; // Path without compression suffix. (E.g., bz2)
};


/*
 * error handling
 */

void fatal(const char *msg, ...) __attribute__ ((format(printf,1,2)));

// Assert Always
#define ASSERTA(cond)                                                          \
{                                                                              \
   if( !(cond) ) { fatal("Assertion Failure at %s:%d.\n", __FILE__, __LINE__); \
                   exit(1);                                                   }\
}

// If false, command-line option must be false.
constexpr bool static_wps_hash_loop_after_collision = false;

struct CH_Options {
  int gpu_require_nmps; // Used to choose among several GPUs. If 0, any.
  int gpu_require_idx;  // Use GPU based in index. If -1, any.
  bool cu_chd_write;
  bool query_write;     // If 0, don't write query file.
  bool luby_deterministic;
  bool elist_sort;
  double wps_oracle; // See code setting wps_oracle_use, etc.
  int cull_ideal_hops;
  int cull_threshold_tpn;
  float contract_one_hop;
  int longcuts_cull; // 0, don't; 1, in score; 2, in contract.
  int C_dynamic;// 2:preprocessing, 1: query, 0: off (default value of 0.3 if invalid C_set)
  float C_set;// User selected static C, ignored if out of range or C_dynamic!=0

  // Coefficients used to compute a node's score.
  float score_edge_diff;
  float score_max_deg_nbr;
  float score_mean_weight;
  float score_max_weight;
  float score_deg;
  // If true, score uses mean edge weight of original graph;
  // if false, use the mean edge weight of the current overlay.
  bool score_g_wht_0;
  int score_precision; // Number of bits used in radix sort for selection.

  bool luby_dynamic_iter;
  bool K_bm_autogen; // If true, generate host-specific BM if not found.
  int K_dynamic;// 2:preprocessing, 1:query, 0:off (default value of 1024 if invalid K_set)
  int K_set;// User selected static K, ignored if out of range or K_dynamic!=0
  bool wps_hash_loop_after_collision;
  bool bin_dump;// write CUCH preprocessed files to disk as binary file.

  int verify;

  // If true, query provides distance but not previous node on path from source.
  bool query_distance_only;

  // Number of levels to treat sparsely on upward pass of query.
  int query_n_sparse_levels;

  // The items above are set from command-line options or their defaults.
  // The items below are set dynamically.
  bool cull_score, cull_contract;
  bool wps_oracle_use;      // Use oracle's shortcuts.
  bool wps_oracle_perform;  // Show results of WPS but GPU uses its own.
  int wps_oracle_hop_limit;
  bool asserts_costly_skip;  // Don't evaluate costly assertion conditions.

  // Members below are set based on verbosity level (AOTW).
  bool print_defer; // Otherwise print immediately.
  bool print_tuning_in_loops;  // Can generate thousands of lines per run.
  bool print_tuning;

  bool verify_in_loops;
  bool verify_contr_raw;
  bool verify_contr_final;
  bool verify_query;
};

extern CH_Options chopts;
extern int verbosity;
extern fspath cuch_exe_dir;

#endif

