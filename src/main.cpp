/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <cstdio>
#include <getopt.h>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cstdarg>
#include <time.h>
#include <assert.h>
#include <string>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

#include "main.h"
#include "graphs.h"
#include "heap.h"
#include "dij.h"
#include "APSP.h"
#include "nvsssp.h"

#include "cu_CH.h"
#include "cu_main.h"
#include "cu_SCH2CUCH.h"

#include "cu_util_timing.h"

int verbosity = 0;

Print_Filter pr;

CH_Options chopts;
fspath cuch_exe_dir;

using namespace std;

#define NDIGITS(a) (((a)==0) ? 1 : floor(log10((a)<0?-a:a)) + 1)

void fatal(const char *msg, ...)
{
  va_list ap;

  va_start(ap, msg);
  vfprintf(stderr, msg, ap);
  va_end(ap);
  
  exit(0);
}

static bool
ends_with(const string s, const string suffix)
{
  string ext = fspath(s).extension().generic_string();
  if ( ext.length() != suffix.length() ) return false;
  for ( size_t i = 0; i < suffix.length(); i++ )
    if ( tolower(ext[i]) != tolower(suffix[i]) ) return false;
  return true;
}

static bool
path_can_read(const string path, const char *err_msg = NULL)
{
  const int fd = open( path.c_str(), O_RDONLY);
  if ( fd == -1 )
    {
      if ( err_msg )
        pr.fatal_user
          ("%s %s: %s\n", err_msg, path.c_str(), strerror(errno) );
      return false;
    }
  close(fd);
  return true;
}


// Open a stream for reading given a path.  Use bzcat if
// path or path.bz2 is compressed.
//
File_Open_Read::File_Open_Read(const string pathp)
{
  f = NULL;
  const string co_bz2 = ".bz2", co_gz = ".gz", co_none = "";
  string path = pathp; // Suffix may be added.

  for ( auto co: { co_bz2, co_gz } )
    {
      const string path_try = pathp + co;
      if ( path_can_read(path_try) ) { path = path_try; break; }
    }

  string co_found = "";
  for ( auto co: { co_bz2, co_gz } )
    if ( ends_with(path,co) ) { co_found = co; break; }

  path_sans_co = path.substr(0,path.size()-co_found.size());

  if ( co_found == co_bz2 || co_found == co_gz )
    {
      if ( !path_can_read(path,"Could not open file ") ) exit(1);
      string bz_cmd = ( co_found == co_bz2 ? "bzcat " : "zcat " ) + path;
      path_opened = path;
      f = popen(bz_cmd.c_str(), "r");
      is_pipe = true;
      if ( !f ) pr.fatal_user("Could not pipe using %s: %s\n",
                              bz_cmd.c_str(), strerror(errno));
      return;
    }

  f = fopen(path.c_str(),"r");
  path_opened = path;
  is_pipe = false;
  if ( !f )
    pr.fatal_user("Could open file %s: %s\n", path.c_str(), strerror(errno));
}

File_Open_Read::~File_Open_Read()
{
  if ( !f ) return;
  if ( is_pipe ) pclose(f); else fclose(f);
}

bool
File_Open_Read::ext_match(const char* suffix)
{
  return ends_with(path_sans_co,string(".")+suffix);
}


class File_Open_Write_BZ2 {
public:
  File_Open_Write_BZ2(const string pathp)
  {
    f = NULL;
    const bool is_bz2 = ends_with(pathp,".bz2");
    const string path_bz2 = pathp + ".bz2";
    const string path = is_bz2 ? pathp : pathp + ".bz2";

    string bz_cmd = "bzip2 --stdout --compress > " + path;
    f = popen(bz_cmd.c_str(), "w");
    if ( !f ) pr.fatal_user("Could not pipe for output using %s: %s\n",
                            bz_cmd.c_str(), strerror(errno));
  }

  ~File_Open_Write_BZ2()
    {
      if ( !f ) return;
      pclose(f);
    }

  explicit operator bool () { return f != NULL; }
  operator FILE* () { return f; }

private:
  FILE *f;
};


class File_Open_Write {
public:
  File_Open_Write(const string path)
  {
    f = fopen(path.c_str(), "w");
    if ( !f ) pr.fatal_user("Could not open %s for output: %s\n",
                            path.c_str(), strerror(errno));
  }

  ~File_Open_Write()
    {
      if ( !f ) return;
      fclose(f);
    }

  explicit operator bool () { return f != NULL; }
  operator FILE* () { return f; }

private:
  FILE *f;
};


Node_Arg::Node_Arg(char *str_in)
{
  n_rand_pending = n_rand = 0;
  rand_seed = 0;
  nnodes = 0;
  const char* emsg = "In -s argument";
  while ( char* token = strsep(&str_in,",") )
    {
      switch ( token[0] ) {
      case 0: srcs.push_back( NODEID_NULL ); break;
      case 'n': case 's':
        while ( char d = token[0] )
          {
            const size_t val = strtoul(++token,&token,0);
            switch ( d ) {
            case 'n':
              if ( n_rand )
                pr.fatal_user
                  ("%s 'n' (# rand srcs) appears a second time.",emsg);
              n_rand_pending = n_rand = val;
              break;
            case 's':
              if ( rand_seed )
                pr.fatal_user("%s 's' (seed) appears a second time.",emsg);
              rand_seed = val;
              break;
            default: pr.fatal_user("%s, unexpected character %c.",emsg,d);
            }}
        break;
      default:
        srcs.push_back( strtoul(token,&token,0) );
        if ( token[0] )
          pr.fatal_user("%s, unexpected character %c.",emsg,token[0]);
        break;
      }
    }
}

void
Node_Arg::nnodes_set(nodeid_t n)
{
  assert( nnodes == 0 || nnodes == n );
  nnodes = n;
  if ( !n_rand_pending ) return;
  random_device rd;
  mt19937 gen( rand_seed ? rand_seed : rd() );
  uniform_int_distribution<nodeid_t> rdist(0,nnodes-1);
  for ( ; n_rand_pending; n_rand_pending-- )
    srcs.push_back( rdist(gen) );
}

static size_t get_nodeid_inputs(char *str_in, nodeid_t **values)
{
  char *token;
  const char delim[2] = ",";
  size_t step = 5;
  size_t max_count;
  size_t count = 0;
  nodeid_t *out_values;

  max_count = step;
  out_values = (nodeid_t *) malloc(max_count * sizeof(nodeid_t));
  token = strsep(&str_in, delim);

  while ( token != NULL ){
    if ( count >= max_count ){
      max_count += step;
      out_values = (nodeid_t *) realloc(out_values, max_count * sizeof(nodeid_t));
    }
    if (strlen(token) > 0)
      out_values[count] = atoi(token);
    else
      out_values[count] = NODEID_NULL;
    count++;
    token = strsep(&str_in, delim);
  }

  *values = out_values;
  return count;
}

enum {
    //The first app listed is the default.
    APP_DIJ_ST=0,
    APP_DIJ_ST_BI,
    APP_DIJ_BOOST,
    APP_DIJ_NVGRAPH,
    APP_APSP_ST,
    APP_APSP_DIJ_ST,
    APP_CU_APSP_ST,
    APP_CU_APSP_BENCH,
    APP_GRPH_ST_INV,
    APP_GRPH_ST_STATS,
    APP_GRPH_ST_CLEANUP,
    APP_GRPH_ST_SCALE,
    APP_GRPH_GR_2_ST,
    APP_GRPH_GCH_2_CUCH,
    APP_CU_CH,
    APP_CU_CH_PREP,
    APP_CU_CH_QUERY,
    APP_PATH_COMPARE,
    APP_PATH_EVALUATE,
    APP_PATH_APSP_COMPARE,
    TEST_FUNCTION
};

class Opt_Specs {
public:
  Opt_Specs(vector<option> other_opts)
    :completed(false), long_opt_val_next(256)
  {
    opts = other_opts;
  }
  ~Opt_Specs(){ for ( auto s: oset_strs ) free(s); }
  bool completed;
  vector<option> opts;
  int long_opt_val_next;
  map<int,bool*> addr_opts_bool;
  map<int,int*> addr_opts_int;
  map<int,float*> addr_opts_float;
  map<int,double*> addr_opts_double;
  vector<char*> oset_strs;  // Storage to free after options read.

  int opt_spec_common(string vname)
  {
    for ( auto& c: vname ) if ( c == '_' ) c = '-';
    const int val = long_opt_val_next++;
    oset_strs.push_back( strdup(vname.c_str()) );
    opts.push_back( { oset_strs.back(), required_argument, 0, val } );
    return val;
  };
  void addr_set(int val, bool &v) { addr_opts_bool[val] = &v; }
  void addr_set(int val, int &v) { addr_opts_int[val] = &v; }
  void addr_set(int val, float &v) { addr_opts_float[val] = &v; }
  void addr_set(int val, double &v) { addr_opts_double[val] = &v; }
  template<typename T1, typename T2>
  int oset(T1& var, string vname, T2 def)
  {
    assert( !completed );
    const int val = opt_spec_common(vname);
    addr_set(val,var);
    var = def;
    return val;
  };
  bool* addr_bool_get(int c)
    { return addr_opts_bool.count(c) ? addr_opts_bool[c] : nullptr; }
  int* addr_int_get(int c)
    { return addr_opts_int.count(c) ? addr_opts_int[c] : nullptr; }
  float* addr_float_get(int c)
    { return addr_opts_float.count(c) ? addr_opts_float[c] : nullptr; }
  double* addr_double_get(int c)
    { return addr_opts_double.count(c) ? addr_opts_double[c] : nullptr; }
  operator option* ()
  {
    if ( !completed )
      {
        // Terminate the options structure array.
        opts.push_back( {0,0,0,0} );
        completed = true;
      }
    return opts.data();
  }

};


int main(int argc, char **argv)
{

  int c;
  int opt_idx = 0;
  char *app_name = NULL;
  char *fin_name = NULL;
  char *fin2_name = NULL;
  char *fout_name = NULL;
  string f_contr_name;
  char *src_in = NULL;
  char *dest_in = NULL;
  int nDigits;
  nodeid_t *dest_vals = NULL;
  size_t num_dest = 0;
  size_t app=-1;
  size_t i;

  // Get this executable's directory.
  fspath exe(argv[0]);
  cuch_exe_dir = boost::filesystem::canonical(exe).parent_path();

  if (argc == 1){
    // main called without any options
    printf("main called without any options\n");
  }

  Opt_Specs opt_specs
    ({{"application", required_argument, 0, 'a'},
      {"filein", required_argument, 0, 'f'},
      {"filein2", required_argument, 0, 'b'}, // backwards graph (dij_bi) / 2nd input file (compare)
      {"fileout", required_argument, 0, 'o'},
      {"source", required_argument, 0, 's'},
      {"destination", required_argument, 0, 'd'},
      {"verbose", required_argument, 0, 'v' }});

#define OSET(v,d) opt_specs.oset(chopts.v, #v, d)

  OSET(gpu_require_nmps, 0);
  OSET(gpu_require_idx, -1);
  OSET(cu_chd_write, true);
  OSET(query_write, true);
  OSET(wps_oracle, 0);
  OSET(cull_ideal_hops, 0);
  OSET(cull_threshold_tpn, 0);
  OSET(contract_one_hop, 0);
  OSET(luby_deterministic, false);
  OSET(luby_dynamic_iter,true);
  OSET(elist_sort, false);
  OSET(longcuts_cull, 1);
  OSET(C_dynamic, 0);
  OSET(C_set, 0.4);
  OSET(K_bm_autogen, true);
  OSET(K_dynamic, 2);
  OSET(K_set, 1024);

  OSET(score_edge_diff, 100);
  OSET(score_max_deg_nbr, -80);
  OSET(score_mean_weight, 0);
  OSET(score_max_weight, 0);
  OSET(score_deg, -5);
  OSET(score_g_wht_0, true);
  OSET(score_precision,16);

  OSET(wps_hash_loop_after_collision,false);
  OSET(bin_dump, true);
  OSET(query_distance_only, false );
  OSET(query_n_sparse_levels, 31);
  OSET(verify,-1);

  option* const opts = opt_specs;  // Don't call oset beyond this point.

  //Option parsing
  while (1){
    c = getopt_long(argc, argv, "a:b:c:f:o:s:v:d:", opts, &opt_idx);
    
    if ( c == -1 ) break;

    if ( bool* const ptr = opt_specs.addr_bool_get(c) )
      {
        int opt_int = atoi(optarg);
        if ( opt_int < 0 || opt_int > 1 )
          {
            fprintf
              (stderr,"Argument of %s must be 0 or 1.\n", opts[opt_idx].name);
            exit(1);
          }
        *ptr = opt_int;
        continue;
      }
    if ( int* const ptr = opt_specs.addr_int_get(c) )
      {
        int opt_val = atoi(optarg);
        *ptr = opt_val;
        continue;
      }
    if ( float* const ptr = opt_specs.addr_float_get(c) )
      {
        float opt_val = atof(optarg);
        *ptr = opt_val;
        continue;
      }
    if ( double* const ptr = opt_specs.addr_double_get(c) )
      {
        double opt_val = atof(optarg);
        *ptr = opt_val;
        continue;
      }
    
 switch ( c ) {
    case 'a':
      app_name = (char *) malloc(strlen(optarg)+1);
      app_name = strcpy(app_name, optarg);
      break;
    case 'c':
      f_contr_name = string(optarg);
      break;
    case 'f':
      fin_name = (char *) malloc(strlen(optarg)+1);
      fin_name = strcpy(fin_name, optarg);
      break;
    case 'b':
      fin2_name = (char *) malloc(strlen(optarg)+1);
      fin2_name = strcpy(fin2_name, optarg);
      break;
    case 'o':
      fout_name = (char *) malloc(strlen(optarg)+1);
      fout_name = strcpy(fout_name, optarg);
      break;
    case 's':
      src_in = (char *) malloc(strlen(optarg)+1);
      src_in = strcpy(src_in, optarg);
      break;
    case 'v':
      {
        verbosity = atoi(optarg);
        if ( verbosity < 0 || verbosity  > 3 )
          {
            pr.fatal_user("Argument of verbose must be 0, 1, 2, or 3.\n");
          }
      }
    case 'd':
      dest_in = (char *) malloc(strlen(optarg)+1);
      dest_in = strcpy(dest_in, optarg);
      break;
    case '?':
      // Unrecognized option. An error message should already have been printed.
      exit(1);
    default:
      abort();
    }
  }

  chopts.cull_score = chopts.longcuts_cull == 1;
  chopts.cull_contract = chopts.longcuts_cull == 2;
  assert( chopts.longcuts_cull >= 0 && chopts.longcuts_cull <= 2 );

  chopts.wps_oracle_use = chopts.wps_oracle > 0;
  chopts.wps_oracle_perform = chopts.wps_oracle != 0;
  const int hops = fabs(chopts.wps_oracle);
  chopts.wps_oracle_hop_limit = hops ?: 1<<30;

  if ( chopts.wps_hash_loop_after_collision &&
       !static_wps_hash_loop_after_collision )
    {
      fprintf
        (stderr,
         "Must set hard-coded variable static_wps_hash_loop_after_collision to true\n"
         " in order to set command-line option wps-hash-loop-after-collision to 1.\n");
      exit(1);
    }

  Node_Arg src_vals(src_in);
  const size_t num_src = src_vals;
  num_dest = get_nodeid_inputs(dest_in, &dest_vals);
  if ( num_src > 0 && num_dest == 0 ){
    num_dest = num_src;
    dest_vals = (nodeid_t *) malloc(num_dest * sizeof(nodeid_t));
    for ( i=0; i < num_dest; i++ )
      dest_vals[i] = NODEID_NULL;
  }


  /*
   * Application decode
   */
  if ( strcmp(app_name, "dij_st") == 0 ) app = APP_DIJ_ST;
  else if ( strcmp(app_name, "dij_st_bi") == 0 ) app = APP_DIJ_ST_BI;
  else if ( strcmp(app_name, "dij_boost") == 0 ) app = APP_DIJ_BOOST;
  else if ( strcmp(app_name, "dij_nvgraph") == 0 ) app = APP_DIJ_NVGRAPH;
  else if ( strcmp(app_name, "grph_st_APSP") == 0 ) app = APP_APSP_ST;
  else if ( strcmp(app_name, "grph_st_APSP_dij") == 0 ) app = APP_APSP_DIJ_ST;
  else if ( strcmp(app_name, "grph_cu_APSP") == 0 ) app = APP_CU_APSP_ST;
  else if ( strcmp(app_name, "grph_cu_APSP_bench") == 0 ) app = APP_CU_APSP_BENCH;
  else if ( strcmp(app_name, "grph_st_inv") == 0 ) app = APP_GRPH_ST_INV;
  else if ( strcmp(app_name, "grph_st_stats") == 0 ) app = APP_GRPH_ST_STATS;
  else if ( strcmp(app_name, "grph_st_cleanup") == 0 ) app = APP_GRPH_ST_CLEANUP;
  else if ( strcmp(app_name, "grph_st_scale") == 0 ) app = APP_GRPH_ST_SCALE;
  else if ( strcmp(app_name, "grph_gr_2_st") == 0 ) app = APP_GRPH_GR_2_ST;
  else if ( strcmp(app_name, "grph_gch_2_cuch") == 0 ) app = APP_GRPH_GCH_2_CUCH;
  else if ( strcmp(app_name, "grph_cu_CH") == 0 ) app = APP_CU_CH;
  else if ( strcmp(app_name, "grph_cu_CH_prep") == 0 ) app = APP_CU_CH_PREP;
  else if ( strcmp(app_name, "grph_cu_CH_query") == 0 ) app = APP_CU_CH_QUERY;
  else if ( strcmp(app_name, "path_compare") == 0 ) app = APP_PATH_COMPARE;
  else if ( strcmp(app_name, "path_evaluate") == 0 ) app = APP_PATH_EVALUATE;
  else if ( strcmp(app_name, "path_APSP_compare") == 0 ) app = APP_PATH_APSP_COMPARE;
  else if ( strcmp(app_name, "test") == 0 ) app = TEST_FUNCTION;

  const bool querying = app == APP_CU_CH && num_src;

  // Information shared by contract & query.
  cu_Contract_Query_State cq_state(src_vals);

  // Interpret Verbosity Level
  //
  chopts.print_defer = verbosity < 3;
  chopts.print_tuning_in_loops = verbosity >= 2;
  chopts.print_tuning = verbosity >= 1;
  pr.tune_loop_set(chopts.print_tuning,chopts.print_tuning_in_loops);

  // If unset, set verification level based on verbosity level.
  //
  if ( chopts.verify < 0 )
    chopts.verify =
      !chopts.print_defer ? 3 :
      chopts.print_tuning_in_loops ? 2 :
      chopts.print_tuning ? 1 : 0;

  // Interpret Verification Level
  //
  chopts.asserts_costly_skip = chopts.verify == 0;
  chopts.verify_in_loops = chopts.verify >= 3;
  chopts.verify_query = chopts.verify >= 1;
  chopts.verify_contr_raw = chopts.verify >= 2;
  chopts.verify_contr_final = chopts.verify >= ( querying ? 2 : 1 );

  // Create contracted graph file name in case it is needed.
  //
  const string contr_file_out_name_raw =
    ( f_contr_name.size() ? f_contr_name :
      string( fout_name ? fout_name : "graphtk" ) );
  const string contr_file_out_name =
    chopts.bin_dump ? contr_file_out_name_raw + ".bin"
    : (ends_with( contr_file_out_name_raw, ".cu_CHD" )
       ? contr_file_out_name_raw : contr_file_out_name_raw + ".cu_CHD");

  // Create stats file name in case it is needed.
  //
  const string stats_out_name =
    string( fout_name ? fout_name : "graphtk" )  + ".cu_stats";

  pr.user("%s",
R"xx(** CUCH: A CUDA Contraction Hierarchies / PHAST Implementation
** Copyright (c) 2020 Louisiana State University

)xx");

  if ( chopts.score_precision < 8 || chopts.score_precision > 32 )
    pr.fatal_user
      ("Option score-precision=%d out of range.  Can range from 8 to 32.\n",
       chopts.score_precision);

  switch(app){
  case(APP_DIJ_ST):
    {
      FILE *file_out;
      char *src_dest_str = NULL;
      graph_st_t *graph_st;
      path_t *result;
      dij_stats_t stats;
      dij_stats_t total_stats;

      dij_stats_init(&total_stats);

      if ( num_src == 0 )
	fatal("dij app requires a valid source node ID.  Use '-s <node id>'.\n");

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }
      src_vals.nnodes_set(graph_st->num_nodes);
      for ( i = 0; i < num_src; i++){
	path_init(&result, graph_st->num_nodes);
	stats = dij_st_ref_dest(graph_st, src_vals[i], dest_vals[i], result);

	if ( fout_name == NULL ){
	  file_out = stdout;
	}else{
	  char *file_out_name = (char *) malloc(strlen(fout_name)+8);
	  strcpy(file_out_name, fout_name);
	  if ( num_src > 1 ){
	    nDigits = NDIGITS(src_vals[i]);
	    if ( dest_vals[i] == NODEID_NULL){
	      src_dest_str = (char *) malloc(nDigits+6);
	      sprintf(src_dest_str, "_%d_all", src_vals[i]);
	    }else{
	      nDigits += NDIGITS(dest_vals[i]);
	      src_dest_str = (char *) malloc(nDigits+3);
	      sprintf(src_dest_str, "_%d_%d", src_vals[i], dest_vals[i]);
	    }
	    file_out_name = (char *) realloc(file_out_name, strlen(file_out_name) + strlen(src_dest_str) + 8);
	    strcat(file_out_name, src_dest_str);
	  }
	  strcat(file_out_name, ".path");
	  file_out = fopen(file_out_name, "w+");
	  free(file_out_name);
	}

	path_dump(file_out, result);
	path_free(result);

	if ( fout_name != NULL ){
	  fclose(file_out);
	  if ( src_dest_str != NULL )
	    free(src_dest_str);
	}

	dij_stats_dump(stderr, stats);
	total_stats.num_relaxed_edges += stats.num_relaxed_edges;
	total_stats.num_updated_edges += stats.num_updated_edges;
	total_stats.num_settled_nodes += stats.num_settled_nodes;
	total_stats.time_wall_us += stats.time_wall_us;
      }

      if ( num_src > 1){
	fprintf(stderr, "\nNumber of queries: %zu\n", num_src);
	dij_stats_dump(stderr, total_stats);
      }
      graph_st_free(graph_st);
    }
    break;
  case(APP_DIJ_BOOST):
    {
      FILE *file_out;
      char *src_dest_str = NULL;
      graph_st_t *graph_st;
      path_t *result;

      if ( num_src == 0 )
	fatal("dij app requires a valid source node ID.  Use '-s <node id>'.\n");

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      using namespace boost;

      typedef adjacency_list < listS, vecS, directedS,
			       no_property, property < edge_weight_t, uint32_t > > graph_t;
      typedef graph_traits < graph_t >::vertex_descriptor vertex_descriptor;
      typedef std::pair<uint32_t, uint32_t> Edge;
      
      const uint32_t num_nodes = graph_st->num_nodes;
      const uint32_t num_edges = graph_st->num_edges;
      Edge *edge_array = (Edge *) malloc(num_edges * sizeof(Edge));
      uint32_t *weights = (uint32_t *) malloc(num_edges * sizeof(uint32_t));
      uint32_t eid = 0;
      for(uint32_t i=0; i<num_nodes; i++){
	for(uint32_t j=0; j<graph_st->num_neighbors[i]; j++){
	  edge_array[eid] = Edge(i, graph_st->neighbors_pt[i][j]);
	  weights[eid] = graph_st->weights_pt[i][j];
	  eid++;
	}
      }

      graph_st_free(graph_st);
      graph_st = NULL;

      graph_t g(edge_array, edge_array + num_edges, weights, num_nodes);
      src_vals.nnodes_set(num_nodes);

      for ( i = 0; i < num_src; i++){

	path_init(&result, num_nodes);
	
	std::vector<vertex_descriptor> p(num_vertices(g));
	std::vector<uint32_t> d(num_vertices(g));
	vertex_descriptor s = vertex(src_vals[i], g);

	double t_start_s, t_end_s;

	t_start_s = time_wall_fp();

	dijkstra_shortest_paths(g, s, predecessor_map(&p[0]).distance_map(&d[0]));

	t_end_s = time_wall_fp();

	printf(" Time %7.3f ms\n", (t_end_s - t_start_s) * 1e+3);

	result->src = src_vals[i];
	result->dest = NODEID_NULL;
	graph_traits < graph_t >::vertex_iterator vi, vend;
	for (tie(vi, vend) = vertices(g); vi != vend; ++vi) {
	  result->weight[*vi] = d[*vi];
	  result->prev[*vi] = p[*vi];
	}

	if ( fout_name == NULL ){
	  file_out = stdout;
	}else{
	  char *file_out_name = (char *) malloc(strlen(fout_name)+8);
	  strcpy(file_out_name, fout_name);
	  if ( num_src > 1 ){
	    nDigits = NDIGITS(src_vals[i]);
	    if ( dest_vals[i] == NODEID_NULL){
	      src_dest_str = (char *) malloc(nDigits+6);
	      sprintf(src_dest_str, "_%d_all", src_vals[i]);
	    }else{
	      nDigits += NDIGITS(dest_vals[i]);
	      src_dest_str = (char *) malloc(nDigits+3);
	      sprintf(src_dest_str, "_%d_%d", src_vals[i], dest_vals[i]);
	    }
	    file_out_name = (char *) realloc(file_out_name, strlen(file_out_name) + strlen(src_dest_str) + 8);
	    strcat(file_out_name, src_dest_str);
	  }
	  strcat(file_out_name, ".path");
	  file_out = fopen(file_out_name, "w+");
	  free(file_out_name);
	}

	path_dump(file_out, result);
	path_free(result);

	if ( fout_name != NULL ){
	  fclose(file_out);
	  if ( src_dest_str != NULL )
	    free(src_dest_str);
	}

      }

      if ( num_src > 1){
	fprintf(stderr, "\nNumber of queries: %zu\n", num_src);
      }
    }
    break;
  case(APP_DIJ_NVGRAPH):
    {
      FILE *file_out;
      char *src_dest_str = NULL;
      graph_st_t *graph_st;
      path_t *result;

      if ( num_src == 0 )
	fatal("dij app requires a valid source node ID.  Use '-s <node id>'.\n");

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      const uint32_t num_nodes = graph_st->num_nodes;
      src_vals.nnodes_set(num_nodes);
      for ( i = 0; i < num_src; i++){

	path_init(&result, num_nodes);
	
	nv_sssp_run(result, graph_st, src_vals[i]);

	result->src = src_vals[i];
	result->dest = NODEID_NULL;

	if ( fout_name == NULL ){
	  file_out = stdout;
	}else{
	  char *file_out_name = (char *) malloc(strlen(fout_name)+8);
	  strcpy(file_out_name, fout_name);
	  if ( num_src > 1 ){
	    nDigits = NDIGITS(src_vals[i]);
	    if ( dest_vals[i] == NODEID_NULL){
	      src_dest_str = (char *) malloc(nDigits+6);
	      sprintf(src_dest_str, "_%d_all", src_vals[i]);
	    }else{
	      nDigits += NDIGITS(dest_vals[i]);
	      src_dest_str = (char *) malloc(nDigits+3);
	      sprintf(src_dest_str, "_%d_%d", src_vals[i], dest_vals[i]);
	    }
	    file_out_name = (char *) realloc(file_out_name, strlen(file_out_name) + strlen(src_dest_str) + 8);
	    strcat(file_out_name, src_dest_str);
	  }
	  strcat(file_out_name, ".path");
	  file_out = fopen(file_out_name, "w+");
	  free(file_out_name);
	}

	path_dump(file_out, result);
	path_free(result);

	if ( fout_name != NULL ){
	  fclose(file_out);
	  if ( src_dest_str != NULL )
	    free(src_dest_str);
	}

      }

      if ( num_src > 1){
	fprintf(stderr, "\nNumber of queries: %zu\n", num_src);
      }

      graph_st_free(graph_st);
      graph_st = NULL;

    }
    break;
  case(APP_APSP_ST):
    {
      FILE *file_out;
      char *file_out_name;
      graph_st_t *graph_st;
      APSP_path_t *result;

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      APSP_path_init(&result, graph_st->num_nodes);
      APSP_st(graph_st, result);

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".APSP");
      }
      file_out = fopen(file_out_name, "w+");
      APSP_path_dump(file_out, result);
      fclose(file_out);

      APSP_path_free(result);
      free(file_out_name);
      graph_st_free(graph_st);
    }
    break;
  case(APP_APSP_DIJ_ST):
    {
      FILE *file_out;
      char *file_out_name;
      graph_st_t *graph_st;
      APSP_path_t *result;

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      APSP_path_init(&result, graph_st->num_nodes);
      APSP_dij_st(graph_st, result);

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".APSP");
      }
      file_out = fopen(file_out_name, "w+");
      APSP_path_dump(file_out, result);
      fclose(file_out);

      APSP_path_free(result);
      free(file_out_name);
      graph_st_free(graph_st);
    }
    break;
  case(APP_CU_APSP_ST):
    {
      FILE *file_out;
      char *file_out_name;
      graph_st_t *graph_st;
      APSP_path_t *result;

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      APSP_path_init(&result, graph_st->num_nodes);
      // form the initial adjacancy matrix which is the zero'th iteration of FW
      for(size_t i=0; i<graph_st->num_nodes; i++){
	for(size_t j=0; j<graph_st->num_neighbors[i]; j++){
	  result->dist_pt[i][graph_st->neighbors_pt[i][j]] = graph_st->weights_pt[i][j];
	  result->prev_pt[i][graph_st->neighbors_pt[i][j]] = i;
	}
      }
      cu_APSP_main(result->dist, result->prev, result->num_nodes);

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".APSP");
      }
      file_out = fopen(file_out_name, "w+");
      APSP_path_dump(file_out, result);
      fclose(file_out);

      APSP_path_free(result);
      free(file_out_name);
      graph_st_free(graph_st);
    }
    break;
  case(APP_CU_APSP_BENCH):
    cu_cuda_init(); // Selects the GPU honoring any command-line arguments.
    cu_APSP_benchmark();
    break;
  case(APP_GRPH_ST_INV):
    {
      char *file_out_name;
      graph_st_t *graph_st_f;
      graph_st_t *graph_st_b;

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st_f, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st_f, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st_f);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      graph_st_get_backwards(&graph_st_b, graph_st_f);

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".el");
      }
      File_Open_Write_BZ2 fout(file_out_name);
      graph_st_norm_dump(fout, graph_st_b);

      free(file_out_name);
      graph_st_free(graph_st_f);
      graph_st_free(graph_st_b);
    }
    break;
  case(APP_GRPH_ST_CLEANUP):
    {
      char *file_out_name;
      graph_st_t *graph_st;
      int dups = 0;

      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      dups = graph_st_remove_duplicates_self_loops(graph_st);

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".el");
      }
      File_Open_Write_BZ2 fout(file_out_name);
      graph_st_norm_dump(fout, graph_st);

      printf("remove %d duplicate or self-loop edges\n", dups);

      free(file_out_name);
      graph_st_free(graph_st);
    }
    break;
  case(APP_GRPH_ST_STATS):
    {
      graph_st_t *graph_st;

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      graph_st_get_stats(graph_st);

      graph_st_free(graph_st);
    }
    break;
  case(APP_DIJ_ST_BI):
    {
      FILE *file_out;
      char *src_dest_str = NULL;
      graph_st_t *graph_st;
      graph_st_t *graph_st_b;
      path_t *result;
      dij_stats_t stats;
      dij_stats_t total_stats;

      dij_stats_init(&total_stats);

      if ( num_src == 0 )
	fatal("dij_st_bi app requires a valid source node ID.  Use '-s <node id>'.\n");
      if ( num_dest == 0 )
	fatal("dij_st_bi app requires a valid destination node ID.  Use '-d <node id>'.\n");

      bool need_clean = false;
      File_Open_Read fin1(fin_name);
      if ( fin1.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin1);
      } else if ( fin1.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin1);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      need_clean = false;
      File_Open_Read fin2(fin_name);
      if ( fin2.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st_b, fin2);
      } else if ( fin2.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st_b, fin2);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st_b);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }
      src_vals.nnodes_set(graph_st->num_nodes);
      for ( i = 0; i < num_src; i++){
	path_init(&result, graph_st->num_nodes);
	stats = dij_bi_st_ref_dest(graph_st, graph_st_b, src_vals[i], dest_vals[i], result);

	if ( fout_name == NULL ){
	  file_out = stdout;
	}else{
	  char *file_out_name = (char *) malloc(strlen(fout_name)+8);
	  strcpy(file_out_name, fout_name);
	  if ( num_src > 1 ){
	    nDigits = NDIGITS(src_vals[i]);
	    if ( dest_vals[i] == NODEID_NULL){
	      src_dest_str = (char *) malloc(nDigits+6);
	      sprintf(src_dest_str, "_%d_all", src_vals[i]);
	    }else{
	      nDigits += NDIGITS(dest_vals[i]);
	      src_dest_str = (char *) malloc(nDigits+3);
	      sprintf(src_dest_str, "_%d_%d", src_vals[i], dest_vals[i]);
	    }
	    file_out_name = (char *) realloc(file_out_name, strlen(file_out_name) + strlen(src_dest_str) + 8);
	    strcat(file_out_name, src_dest_str);
	  }
	  strcat(file_out_name, ".path");
	  file_out = fopen(file_out_name, "w+");
          free(file_out_name);
	}

	path_dump(file_out, result);
	path_free(result);

	if ( fout_name != NULL ){
	  fclose(file_out);
	  if ( src_dest_str != NULL )
	    free(src_dest_str);
	}

	dij_stats_dump(stderr, stats);
	total_stats.num_relaxed_edges += stats.num_relaxed_edges;
	total_stats.num_updated_edges += stats.num_updated_edges;
	total_stats.num_settled_nodes += stats.num_settled_nodes;
	total_stats.time_wall_us += stats.time_wall_us;
      }

      if ( num_src > 1){
	fprintf(stderr, "\nNumber of queries: %zu\n", num_src);
	dij_stats_dump(stderr, total_stats);
      }
      graph_st_free(graph_st);
      graph_st_free(graph_st_b);
    }
    break;
  case APP_CU_CH: case APP_CU_CH_PREP:
    {
      P_Timer pt("APP_CU_CH_PREP");
      const bool no_query = num_src == 0 || app == APP_CU_CH_PREP;

      graph_CH_bi_t *graph_CH;
      cu_graph_CH_bi_t*& graph_cu_CH_h = cq_state.graph_cu_CH_h;
      cu_graph_CH_bi_t *graph_cu_CH_d;

      cu_cuda_init();
      TRACE_MEM_USAGE();

      File_Open_Read fin(fin_name);
      graph_CH_get_from_file(&graph_CH, fin);

      const bool need_apsp_bm = chopts.K_dynamic == 2;
      const bool bm_ok1 = !need_apsp_bm || apsp_bench_info.load();
      if ( !bm_ok1 && chopts.K_bm_autogen )
        {
          pr.user("Generating host-specific APSP benchmark data.\n");
          cu_APSP_benchmark();
        }
      const bool bm_ok = bm_ok1 || apsp_bench_info.load();
      if ( !bm_ok )
        pr.fatal_user
          (R"~~(

Could not find suitable APSP benchmark data required by --K-dynamic==2. 
To generate this data run with "-a grph_cu_APSP_bench" on this host or
run with --K-bm-autogen=1 to automatically generate (and keep) benchmark data.
)~~");

      TRACE_MEM_USAGE();

      const double dur_graph_read = pt.dur_s("Graph Read");
      
      graph_CH_2_cu_CH(&graph_cu_CH_h, graph_CH);
      const size_t n_edges = graph_cu_CH_h->graph_f.num_edges_exact;
      assert( n_edges == graph_CH->graph_f.num_edges );

      TRACE_MEM_USAGE();

      graph_cu_CH_bi_h2d(&graph_cu_CH_d, graph_cu_CH_h);

      TRACE_MEM_USAGE();

      src_vals.nnodes_set(graph_CH->num_nodes);

      const double dur_graph_to_gpu = pt.dur_s("Graph to GPU");

      /// an arbitrary overestimate used for allocation (will have to add contingency in case not large enough)
      const nodeid_t max_rank = 900;

      // setting max_num_iter to the value above, this hopefully
      // accommodates graphs with over 500 million nodes
      cu_CH_stats_t* const stats = &cq_state.stats;
      cu_CH_stats_init(stats, graph_CH, max_rank, stats_out_name);
      stats->in_graph_path = fin_name;
      stats->env_apsp_benchmark_path = apsp_bench_info.bm_path_get();
      
      size_t elist_padded_sz = 0;
      for ( size_t i=0; i<graph_CH->num_nodes; i++ )
        elist_padded_sz +=
          rnd_up( max(1,cu_graph_ud_alloc_mult/2)
                  + graph_cu_CH_h->graph_f.num_neighbors[i],
                  cu_graph_ud_alloc_mult );
      const size_t elist_sz_ud = elist_padded_sz * 1.1;

      TRACE_MEM_USAGE();

      cu_graph_CH_UD_t*& graph_UD_d = cq_state.graph_UD_d;

      graph_cu_CH_UD_init_d
        ( &graph_UD_d, graph_CH->num_nodes, elist_sz_ud,
          max_rank, 0, K_MAX * K_MAX, UDP_Contract);

      TRACE_MEM_USAGE();

      const double dur_init_d = pt.dur_s("init_d");

      cu_CH_contract_main(&graph_cu_CH_d, cq_state);

      TRACE_MEM_USAGE();

      const double dur_cu_CH_contract = pt.dur_s("cu_CH_contract_main");
      cu_graph_CH_UD_t*& graph_UD_h = cq_state.graph_UD_h;

      if ( !no_query || chopts.cu_chd_write )
        graph_cu_CH_UD_d2h(&graph_UD_h, graph_UD_d);
      TRACE_MEM_USAGE();
      const double dur_d2h = pt.dur_s("d2h");
      graph_CH_bi_free(graph_CH);
      if ( no_query )
        {
          graph_cu_CH_bi_free_host(graph_cu_CH_h);
          graph_cu_UD_free_device(graph_UD_d);
        }
      TRACE_MEM_USAGE();
      graph_cu_CH_bi_free_device();
      TRACE_MEM_USAGE();

      const double dur_free = pt.dur_s("free");

      if ( chopts.cu_chd_write )
        {
          File_Open_Write fout(contr_file_out_name);
          pr.user
            ("Writing contracted graph to %s\n", contr_file_out_name.c_str());
	  if(chopts.bin_dump){
	    graph_UD_bin_dump(fout, graph_UD_h);
	  } else {
	    graph_UD_dump(fout, graph_UD_h);
	  }
        }

      const double dur_ud_dump = pt.dur_s("graph_UD_dump");
      const double dur_all = pt.dur_total_s();

      // graph_cu_UD_free_host(graph_UD_h);// RRRR add this function


      stats->partition_app_text = pt.text;
      if ( no_query )
        cu_CH_stats_dump(stats);

      TRACE_MEM_USAGE();

      if ( no_query )
        cu_cuda_memcheck_at_end();

      pr.tune("Time/s:(a, r,t,i, M, t,f,d) "
              "%6.3f  %5.3f %5.3f %5.3f  %6.3f  %5.3f %5.3f %5.3f\n",
              dur_all, dur_graph_read, dur_graph_to_gpu, dur_init_d,
              dur_cu_CH_contract, dur_d2h, dur_free, dur_ud_dump);
      if ( no_query ) break;
    }

    [[fallthrough]];

  case(APP_CU_CH_QUERY):
    {
      P_Timer pt("APP_CU_CH_QUERY");
      FILE *file_out;
      char *file_out_name;
      char *src_str = NULL;
      const bool verify_orig = fin2_name && chopts.verify;

      cu_cuda_init(); // Selects the GPU honoring any command-line arguments.

      if ( num_src == 0 )
	pr.fatal_user
          ("CH_query app requires a valid source node ID.  Use '-s <node id>'.\n");

      cu_graph_CH_UD_t*& graph_UD_d = cq_state.graph_UD_d;
      cu_graph_CH_UD_t*& graph_UD_h = cq_state.graph_UD_h;

      if ( !graph_UD_d )
        {
          File_Open_Read fin(fin_name);
          pr.user("Reading contracted graph from %s\n",
                  fin.path_opened_get().c_str());
          // determine file type (binary or text)
          uint32_t bin_read;
          ASSERTA(fscanf(fin, "CHFF%u\n", &bin_read)==1);
          if(bin_read){
            ASSERTA(graph_UD_bin_get_from_file(&graph_UD_h, fin) == 0);
          } else {
            ASSERTA(graph_UD_get_from_file(&graph_UD_h, fin) == 0);
          }

          pt.dur_s("Graph Read");
          graph_cu_CH_UD_h2d(&graph_UD_d, graph_UD_h);
          pt.dur_s("Graph to GPU");

          cu_CH_stats_init(&cq_state.stats, graph_UD_h, stats_out_name);
          cq_state.stats.in_graph_path = fin_name;
          cq_state.stats.num_iterations =
            graph_UD_h->node_levels[graph_UD_h->graph_u_f.num_nodes-1];

          if ( verify_orig )
            {
              // Read original graph for checking.
              File_Open_Read forig(fin2_name);
              graph_CH_bi_t *graph_CH;
              graph_CH_get_from_file(&graph_CH, forig);
              pr.user("Reading original graph from %s\n",
                      forig.path_opened_get().c_str());
              graph_CH_2_cu_CH(&cq_state.graph_cu_CH_h, graph_CH);
              graph_CH_bi_free(graph_CH);
              cq_state.contract_dist_verify.init_h
                ( cq_state.graph_cu_CH_h, graph_UD_h );
            }
          pt.dur_s("Graph Original Read");
        }

      graph_UD_finish(graph_UD_h);
      const uint n_levels =
        graph_UD_h->node_levels[graph_UD_h->graph_u_f.num_nodes-1];
      auto egil_cpy = [&](auto& s, auto g)
        { s.resize(n_levels+1);  memcpy(s.data(),g,s.size()*sizeof(g[0])); };
      egil_cpy( cq_state.stats.edge_groups_in_level_u_b,
                graph_UD_h->edge_groups_in_level_u_b );
      egil_cpy( cq_state.stats.edge_groups_in_level_d_b,
                graph_UD_h->edge_groups_in_level_d_b );

      double dur_query = 0;
      double dur_dump = 0, dur_verify = 0;
      const nodeid_t num_nodes = graph_UD_h->graph_u_f.num_nodes;
      src_vals.nnodes_set(num_nodes);

      if ( verify_orig )
        {
          cq_state.contract_dist_verify.n_queries_per_iter_set
            ( cq_state.srcs.num_args() );
          if ( true || chopts.verify_contr_final )
            {
              string verify_msg =
                cq_state.contract_dist_verify.verify(&cq_state,graph_UD_d);
              pr.user("%s",verify_msg.c_str());
            }
        }

      for ( auto src: src_vals ) {

        TRACE_MEM_USAGE();

	cu_CH_query_main(src, cq_state);

        dur_query += pt.dur_s("cu_CH_query_main");

        if ( chopts.verify_query )
          cq_state.contract_dist_verify.verify(&cq_state.result_h);

        dur_verify += pt.dur_s("verify");

	if ( chopts.query_write && fout_name ) {
	  file_out_name = (char *) malloc(strlen(fout_name)+8);
	  strcpy(file_out_name, fout_name);
	  if ( num_src > 1 ){
	    nDigits = NDIGITS(src);
	    src_str = (char *) malloc(nDigits+6);
	    sprintf(src_str, "_%d_all", src);
	    file_out_name = (char *) realloc(file_out_name, strlen(file_out_name) + strlen(src_str) + 8);
	    strcat(file_out_name, src_str);
	  }
	  strcat(file_out_name, ".path");
          pr.user("Writing query results to %s\n",file_out_name);
	  file_out = fopen(file_out_name, "w+");
          path_dump(file_out, &cq_state.result_h);
	  fclose(file_out);
	  free(file_out_name);
	  if ( src_str != NULL )
	    free(src_str);
        }

        dur_dump += pt.dur_s("path dump");
      }

      TRACE_MEM_USAGE();

      path_free_h(cq_state.result_h);
      path_free_d(cq_state.result_0_dh, cq_state.result_0_d);
      path_free_d(cq_state.result_1_dh, cq_state.result_1_d);
      graph_cu_UD_free_device(graph_UD_d);
      graph_cu_CH_bi_free_host(cq_state.graph_cu_CH_h);

      TRACE_MEM_USAGE();

      pt.dur_s("free");
      pt.dur_total_s();
      // graph_cu_UD_free_host(graph_UD_h);// RRRR add this function

      pr.tune("Query Timing:\n%s\n", pt.text.c_str());

      if ( cq_state.stats.stats_file_name.size() )
        cu_CH_stats_dump(&cq_state.stats);

      TRACE_MEM_USAGE();

      cu_cuda_memcheck_at_end();
    }
    break;
  case(APP_PATH_COMPARE):
    {
      FILE *file_in;
      FILE *file_in_2;

      path_t *result_0;
      path_t *result_1;

      file_in = fopen(fin_name, "r");
      if ( file_in == NULL ) fatal("File %s not found.\n", fin_name);
      path_get_from_file(&result_0, file_in);
      fclose(file_in);

      file_in_2 = fopen(fin2_name, "r");
      if ( file_in_2 == NULL ) fatal("File %s not found.\n", fin2_name);
      path_get_from_file(&result_1, file_in_2);
      fclose(file_in_2);
      
      assert(result_0->num_nodes == result_1->num_nodes);
      if(result_0->src != result_1->src){
	printf("WARNING! paths starting from different sources!\n");
      }
      if(result_0->dest != result_1->dest){
	printf("WARNING! paths end at different destinations!\n");
      }
      
      uint32_t count = 0;
      for(uint32_t i=0; i<result_0->num_nodes; i++){
	if(result_0->weight[i] != result_1->weight[i]){
	  // printf("failed at i = %u\n", i);
	  count++;
	}
      }
      printf("failed (dist) in %u nodes out of %u\n", count, result_0->num_nodes);
      count = 0;
      for(uint32_t i=0; i<result_0->num_nodes; i++){
	if(result_0->prev[i] != result_1->prev[i]){
	  // printf("failed (prev) at i = %u\n", i);
	  count++;
	}
      }
      printf("failed (prev) in %u nodes out of %u\n", count, result_0->num_nodes);
      
      path_free(result_0);
      path_free(result_1);
      
    }
    break;
  case(APP_PATH_EVALUATE):
    {
      // alternative to path compare, uses CPU dijkstra, compares path and checks alternate paths
      FILE *file_in_2;

      path_t *result_0;
      path_t *result_1;

      File_Open_Read fin(fin_name);
      graph_st_t* graph_st = graph_st_get_from_file(fin);

      file_in_2 = fopen(fin2_name, "r");
      if ( file_in_2 == NULL ) pr.fatal_user("File %s not found.\n", fin2_name);
      pr.user("Reading SSSP results from %s\n", fin2_name);
      path_get_from_file(&result_1, file_in_2);
      fclose(file_in_2);

      if(graph_st->num_nodes != result_1->num_nodes){
	pr.fatal_user("Size of graph and SSSP results don't match!\n");
      }

      path_init(&result_0, graph_st->num_nodes);
      dij_st_ref_dest(graph_st, result_1->src, NODEID_NULL, result_0);

      uint32_t count = 0;
      for(uint32_t i=0; i<result_0->num_nodes; i++){
	if(result_0->weight[i] != result_1->weight[i]){
	  // printf("failed at i = %u\n", i);
	  count++;
	}
      }
      if ( count )
        pr.user("Wrong distance in %u nodes out of %u\n",
                count, result_0->num_nodes);
      else
        pr.user("All distances correct.\n");
      count = 0;
      for(uint32_t i=0; i<result_0->num_nodes; i++){
	if(result_0->prev[i] != result_1->prev[i]){
	  // check whether it's a valid alternate path
	  uint32_t prev = result_1->prev[i];
	  uint32_t dist = result_1->weight[i];
	  uint32_t nbrid = NODEID_NULL;
	  uint32_t weight = 0;
	  for(uint32_t j=0; j<graph_st->num_neighbors[result_1->prev[i]]; j++){
	    if(i == graph_st->neighbors_pt[prev][j]){
	      nbrid = j;
	      weight = graph_st->weights_pt[prev][j];
	    }
	  }
	  if(nbrid == NODEID_NULL || (result_1->weight[prev] + weight != dist) ){
	    // printf("failed (prev) at i = %u\n", i);
	    count++;
	  }
	}
      }
      if ( count )
        pr.user("Incorrect predecessor (prev) in %u nodes out of %u\n",
                count, result_0->num_nodes);
      else
        pr.user("All predecessors (previous nodes) correct.\n");
      
      path_free(result_0);
      path_free(result_1);
      
      graph_st_free(graph_st);

    }
    break;
  case(APP_PATH_APSP_COMPARE):
    {
      FILE *file_in;
      FILE *file_in_2;
      /// later perhaps add a third input for original graph to allow validating alternative paths

      APSP_path_t *result_0;
      APSP_path_t *result_1;

      file_in = fopen(fin_name, "r");
      if ( file_in == NULL ) fatal("File %s not found.\n", fin_name);
      APSP_path_get_from_file(&result_0, file_in);
      fclose(file_in);

      file_in_2 = fopen(fin2_name, "r");
      if ( file_in_2 == NULL ) fatal("File %s not found.\n", fin2_name);
      APSP_path_get_from_file(&result_1, file_in_2);
      fclose(file_in_2);
      
      assert(result_0->num_nodes == result_1->num_nodes);
      
      uint32_t count = 0;
      for(uint32_t i=0; i<result_0->num_nodes; i++){
	for(uint32_t j=0; j<result_0->num_nodes; j++){
	  if(result_0->dist_pt[i][j] != result_1->dist_pt[i][j]){
	    // printf("failed at i = %u, j = %u\n", i, j);
	    count++;
	  }
	}
      }
      printf("failed (dist) in %u paths out of %u\n", count, result_0->num_nodes*result_0->num_nodes);
      count = 0;
      for(uint32_t i=0; i<result_0->num_nodes; i++){
	for(uint32_t j=0; j<result_0->num_nodes; j++){
	  if(result_0->prev_pt[i][j] != result_1->prev_pt[i][j]){
	    // printf("failed (prev) at i = %u, j = %u\n", i, j);
	    count++;
	  }
	}
      }
      printf("failed (prev) in %u paths out of %u\n", count, result_0->num_nodes*result_0->num_nodes);
      
      APSP_path_free(result_0);
      APSP_path_free(result_1);
      
    }
    break;
  case(APP_GRPH_ST_SCALE):
    {
      // prints out min/mean/max edge weights and scales graph edge weights down (only scales down)
      char *file_out_name;
      graph_st_t *graph_st;

      bool need_clean = false;
      File_Open_Read fin(fin_name);
      if ( fin.ext_match("el") ){
	need_clean = graph_st_get_norm_from_file(&graph_st, fin);
      } else if ( fin.ext_match("gr") ) {
	need_clean = graph_st_get_gr_from_file(&graph_st, fin);
      } else {
	fatal("Unrecognized file type \n");
      }

      if( need_clean ){
	int dups = graph_st_remove_duplicates_self_loops(graph_st);
	printf("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
      }

      weight_t max_weight = 0;
      weight_t min_weight = WEIGHT_INF;
      double mean_weight = 0;
      for(uint32_t i=0; i<graph_st->num_edges; i++){
	mean_weight += (double)graph_st->weights[i] / (double)graph_st->num_edges;
	if(graph_st->weights[i] > max_weight){
	  max_weight = graph_st->weights[i];
	}
	if(graph_st->weights[i] < min_weight){
	  min_weight = graph_st->weights[i];
	}
      }
      printf("min_weight = %u\nmax_weight = %u\nmean_weight = %f\nmax_allowed_weight = %u\n", min_weight, max_weight, mean_weight, WEIGHT_INF);

      printf("enter scale_down value (integer scale_down only)\n");
      uint32_t scale_constant;
      scanf("%d", &scale_constant);
      printf("scale_down value set to :%d\n", scale_constant);
      for(uint32_t i=0; i<graph_st->num_edges; i++){
	graph_st->weights[i] = graph_st->weights[i] / scale_constant;
      }

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".el");
      }
      File_Open_Write_BZ2 fout(file_out_name);
      graph_st_norm_dump(fout, graph_st);

      free(file_out_name);
      graph_st_free(graph_st);

    }
    break;
  case(APP_GRPH_GCH_2_CUCH):
    {
      FILE *file_in;
      const bool verify_orig = fin2_name && chopts.verify;
      graph_CH_bi_t *graph_CH;
      cu_graph_CH_UD_t *graph_UD_h;
      cu_graph_CH_UD_t *graph_UD_d;
      uint32_t *node_levels;

      cu_cuda_init(); // Selects the GPU honoring any command-line arguments.

      file_in = fopen(fin_name, "r");
      if ( file_in == NULL ) fatal("File %s not found.\n", fin_name);
      graph_GCH_get_from_file(&graph_CH, file_in);
      fclose(file_in);

      if ( verify_orig )
        {
          // Read original graph for checking.
          File_Open_Read forig(fin2_name);
          graph_CH_bi_t *graph_orig;
          graph_CH_get_from_file(&graph_orig, forig);
          graph_CH_2_cu_CH(&cq_state.graph_cu_CH_h, graph_orig);
          graph_CH_bi_free(graph_orig);
          cq_state.contract_dist_verify.init_h( cq_state.graph_cu_CH_h );
          cq_state.contract_dist_verify.verify(&cq_state,graph_CH);
        }

      // Geisberger graphs should not have duplicates, this kernel is just a precaution
      graph_SCH_remove_dups(graph_CH);

      // uncomment next line to activate .el dump of the contracted graph
      // #define DUMP_EL
#ifdef DUMP_EL

      graph_st_t *graph_st;

      graph_CH_bi_2_st(&graph_st, graph_CH);

      const char* fel_name = "graphtk_GCH.el";
      File_Open_Write_BZ2 fel(fel_name);
      graph_st_norm_dump(fel, graph_st);

      graph_st_free(graph_st);

#endif

      node_levels = (uint32_t *) malloc(graph_CH->num_nodes*sizeof(uint32_t));
      
      SCH_assign_levels(graph_CH, node_levels);
      
      graph_SCH_2_CUCH_d(&graph_UD_d, graph_CH, node_levels);

      graph_cu_CH_UD_d2h(&graph_UD_h, graph_UD_d);

      if ( verify_orig )
        {
          cq_state.contract_dist_verify.init_h
            ( cq_state.graph_cu_CH_h, graph_UD_h );
          string verify_msg =
            cq_state.contract_dist_verify.verify(&cq_state,graph_UD_d);
          pr.user("%s",verify_msg.c_str());
        }

      if ( chopts.cu_chd_write )
        {
          File_Open_Write fout(contr_file_out_name);
          pr.user
            ("Writing converted graph to %s\n", contr_file_out_name.c_str());
	  if ( chopts.bin_dump ) graph_UD_bin_dump(fout, graph_UD_h);
          else                   graph_UD_dump(fout, graph_UD_h);
        }
      
      graph_CH_bi_free(graph_CH);
      graph_cu_UD_free_device(graph_UD_d);
      // graph_cu_UD_free_host(graph_UD_h);// RRRR add this function
      
    }
    break;
  case(APP_GRPH_GR_2_ST):
    {
      // prints out num_nodes and num_edges and writes the input GR (DIMACS9) graph as EL file.
      FILE *file_in;
      char *file_out_name;
      graph_st_t *graph_st;

      file_in = fopen(fin_name, "r");
      if ( file_in == NULL ) fatal("File %s not found.\n", fin_name);
      graph_st_get_gr_from_file(&graph_st, file_in);
      fclose(file_in);

      printf("num_nodes = %u\nnum_edges = %u\n", graph_st->num_nodes, graph_st->num_edges);
      

      if ( fout_name == NULL ){
	  file_out_name = strdup("graphtk.el");
      }else{
	file_out_name = (char *) malloc(strlen(fout_name)+8);
	strcpy(file_out_name, fout_name);
	strcat(file_out_name, ".el");
      }
      File_Open_Write_BZ2 fout(file_out_name);
      graph_st_norm_dump(fout, graph_st);

      free(file_out_name);
      graph_st_free(graph_st);
      
    }
    break;
  case(TEST_FUNCTION):
    {
      
    }
    break;
  default:
    fatal("Unrecognized app name, \"%s\"\n",app_name);
    break;
  }

  free(dest_vals);
  free(src_in);
  free(dest_in);
  free(fin_name);
  free(fout_name);

  pr.user("CUCH exiting normally.\n");
 
  return 0;

}
