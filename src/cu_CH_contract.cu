/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>

#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"

#include <nppdefs.h>

const bool __attribute__((unused)) debug_sm_idx = false;

// Collect information on how witness paths were found, such
// as being found via a 1-hop search using a hash table.
static const bool stats_wps_want = false;  // Slows down execution.


// #define ST
// guarantees that different syncthreads() calls will not affect each other by checking line number
// allows for threads to skip a syncthreads() (only applies to the last syncthread in a kernel)
#ifdef ST

#define __syncthreads()                                                       \
  { sync_threads();                                                           \
    st_line = atomicExch(&sync_count_sh, __LINE__);                           \
    assert(st_line == 0 || st_line == __LINE__);                              \
    sync_threads();                                                           \
    sync_count_sh = 0; }

#endif

__constant__ CH_Options chopts_c;

__host__ void
cu_CH_contract_nodes_pre_launch(cu_graph_CH_bi_t *graph_bi_d)
{
  cuda_sym_host_to_dev(chopts_c,&chopts);
}

template <uint32_t tpn, int bfb_rnds> __global__ void
__launch_bounds__(contract_nodes_tpn_to_block_size(tpn))
cu_CH_contract_nodes
(cu_graph_CH_bi_t graph_c, nodeid_t *node_list, nodeid_t list_length,
 cu_shc_CH_t *shc_list, cu_CH_query_stats *qstats_a, nodeid_t *d_DB_est)
{
  const cu_graph_CH_bi_t* const graph = &graph_c;
  const float opt_one_hop = chopts_c.contract_one_hop;

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const bool opt_longcuts_cull =
    chopts_c.cull_contract && int(tpn) >= chopts_c.cull_threshold_tpn;
  const bool opt_longcuts = opt_longcuts_watch || opt_longcuts_cull;
  const bool opt_loop_after_collision =
    static_wps_hash_loop_after_collision
    && chopts_c.wps_hash_loop_after_collision;

  // If true longcuts eliminated from fwd graph too. Doesn't work well.
  const bool opt_longcuts_fwd = false;

  const int TPN = 1 << tpn;
  const uint32_t tnid_msk = TPN - 1;
  const int block_dim = contract_nodes_tpn_to_block_size(tpn);
  assert( block_dim == blockDim.x );

  constexpr int wp_lg = 5;
  constexpr int wp_sz = 1 << wp_lg;
  const int lane = threadIdx.x % wp_sz;
  const int wp_num = threadIdx.x >> wp_lg;
  constexpr int n_wps = block_dim >> wp_lg;

  constexpr int grp_lg = 3;
  constexpr int grp_sz = 1 << grp_lg;
  constexpr int grp_mk = grp_sz - 1;

  // Size of backward-forward buffer.
  constexpr int bfb_sz = bfb_rnds * grp_sz;

#ifdef NDEBUG
  const bool stats_want = false;
#else
  const bool stats_want = true;
#endif
  const bool stats_check = false;
  const bool stats_blk = stats_want && qstats_a;
  const auto qstats = &qstats_a[blockIdx.x];
  const bool stats = stats_blk && threadIdx.x == 0;
  const bool stats_wps = stats_blk && stats_wps_want;
  __shared__ int64_t tbuf, tcomp1, tcomp2, dbuf, dcomp1, dcomp2, *stats_current;
  __shared__ int64_t tfinish, dfinish;

  auto stats_continue =
    [&](int64_t& expect, int64_t& set, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
        assert( &expect == stats_current ); stats_current = &set; }
      set = clock64();
      accum += set - expect;
    };

  auto stats_start =
    [&](int64_t& set)
    {
      if ( !stats ) return;
      if ( stats_check ) { assert( !stats_current ); stats_current = &set; }
      set = clock64();
    };

  auto stats_stop =
    [&](int64_t& expect, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
         assert( &expect == stats_current ); stats_current = NULL; }
      accum += clock64() - expect;
    };

  __shared__ int n_ij, n_2hop, n_fpath, n_1hshc, n_2hshc, n_fph1, n_fph2;

  if ( stats )
    {
      qstats->qstart = clock64();
      dbuf = dcomp1 = dcomp2 = dfinish = 0;
      if ( stats_check ) stats_current = NULL;
      n_ij = 0; n_2hop = 0; n_fpath = 0; n_1hshc = 0; n_2hshc = 0; n_fph1 = 0; n_fph2 = 0;
    }

  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * block_dim;
  // compute num_threads
  const int num_thd = block_dim * gridDim.x;
  // number of vertices handled at each iteration by each block
  const int vertex_per_iter = block_dim >> tpn;

  const nodeid_t lid = vertex_per_iter > 1 ? threadIdx.x >> tpn : 0;

  // thread's neighbor id, effectively tid%(1<<tpn)
  const uint32_t tnid = vertex_per_iter > 1 ? threadIdx.x % TPN : threadIdx.x;

  assert(tpn == graph->thread_per_node);
  
  // increment value in each iteration
  const int vertex_inc = num_thd >> tpn;
  // calculate num_iters to avoid using a while loop to avoid syncthreads imbalance
  const nodeid_t num_iters = div_up(list_length, vertex_inc);

  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)
  
  const int n_selts = ((5*(block_dim>>tpn)) + ((2*grp_sz) + 7)*block_dim);
  __shared__ uint32_t shm[n_selts];

  int make_f_pos = 0;
  auto make_f = [&](uint n) {
      const int pos = make_f_pos;
      make_f_pos += n;
      return [=](uint idx) -> uint32_t&
        { assert( !debug_sm_idx || idx < n ); return shm[pos+idx]; };  };

  auto SH_CURRENT_ID = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_F = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_B = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_F = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_B = make_f(vertex_per_iter);

  auto SH_FORWARD_NBR = make_f(block_dim);
  auto SH_FORWARD_WEIGHT = make_f(block_dim);
  auto SH_FORWARD_MID = make_f(block_dim);
  auto SH_BACKWARD_NBR = make_f(block_dim);
  auto SH_BACKWARD_WEIGHT = make_f(block_dim);

  auto SH_BACKWARD_POINTER_F = make_f(block_dim);
  auto SH_BACKWARD_NNBR_F = make_f(block_dim);
  auto SH_BACKWARD_FORWARD_NBR = make_f(grp_sz*block_dim);
  auto SH_BACKWARD_FORWARD_WEIGHT = make_f(grp_sz*block_dim);
  // recycling space from backwards_forwards nodes/edges
  make_f_pos -= ( 2 + 2*grp_sz ) * block_dim;
  auto SH_FORWARD_POINTER_B = make_f(block_dim);
  auto SH_FORWARD_NNBR_B = make_f(block_dim);

  auto SH_FORWARD_BACKWARD_NBR = make_f(block_dim);
  auto SH_FORWARD_BACKWARD_WEIGHT = make_f(block_dim);

  // Leaves 14-8 = 6 free.
  // hash size per thread.
  const int hash_size_pt = tpn < 5 ? 2 : tpn < 6 ? 4 : 8;
  auto SH_NID_TO_FWD_BWD_TIDX = make_f( block_dim * hash_size_pt );

  assert( n_selts >= make_f_pos );

  uint32_t* const fwd_hash_base = &SH_NID_TO_FWD_BWD_TIDX(0);
  const int hash_word_per_node = hash_size_pt << tpn;
  const int hash_byte_per_node = hash_word_per_node * 4;
  uint32_t* const fwd_hash_lw = fwd_hash_base + lid * hash_word_per_node;
  uint8_t* const fwd_hash_lb = (uint8_t*) fwd_hash_lw;

  auto nid_to_fwd_hash_get =
    [&](nodeid_t nid) -> uint8_t&
    { return fwd_hash_lb[nid%hash_byte_per_node]; };

  auto hash_clear =
    [&]() { for ( int c = 0;  c < hash_size_pt;  c++ )
              fwd_hash_lw[tnid + (c<<tpn)] = uint32_t(-1); };

  // local storage (registers, we hope)
  nodeid_t lweight[bfb_sz];
  nodeid_t lnbr[bfb_sz];

  const uint32_t iter_per_node_r = div_up(TPN,bfb_sz);
  const uint32_t iter_per_node_q = TPN;

  // maximum length of the shc_flag (based on tpn)
  const uint32_t flag_len = div_up(TPN,32);
  uint32_t lshc[flag_len]; // an array of binary flags for each shc coming out of this node

  nodeid_t current_pos = tid>>tpn; // current thread group's position within node_list
  // local_ID, thread group's working set id within current block (needed for shared mem accesses)
  // starting current_pos per block (current_pos of thread 0)
  nodeid_t current_pos_0 = (blockIdx.x * block_dim) >> tpn;

  if ( stats_blk ) sync_threads();
  stats_start(tbuf);  // Also set at end of main loop body.

  nodeid_t num_longcuts = 0, num_longcuts_cut = 0;

  uint32_t deg_sum_DB_est_l = 0;

  for(uint32_t iter = 0; iter < num_iters; iter++){
    // load current iteration's node IDs and the corresponding pointer/nnbr/node status into shared
    if ( threadIdx.x < vertex_per_iter )
      {
        if ( current_pos_0+threadIdx.x < list_length )
          {
            const nodeid_t nid = node_list[current_pos_0 + threadIdx.x];
            SH_CURRENT_ID( threadIdx.x ) = nid;
            SH_CURRENT_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[nid];
            SH_CURRENT_POINTER_B( threadIdx.x ) = graph->graph_b.pointer[nid];
            SH_CURRENT_NNBR_F(threadIdx.x) = graph->graph_f.num_neighbors[nid];
            SH_CURRENT_NNBR_B(threadIdx.x) = graph->graph_b.num_neighbors[nid];
          }
        else
          {
            SH_CURRENT_NNBR_F( threadIdx.x ) = 0;
            SH_CURRENT_NNBR_B( threadIdx.x ) = 0;
          }
      }

    // sync needed here because the first warps are cooperating in loading the entire block's data
    __syncthreads();

    const bool participant = vertex_per_iter == 1 || current_pos < list_length;

    SH_BACKWARD_NBR( threadIdx.x ) = nodeid_invalid;
    SH_FORWARD_NBR( threadIdx.x ) = nodeid_invalid;

    if( participant ){

      // load the current iteration's forward vertices into shared along with their state (rank)
      if(tnid < SH_CURRENT_NNBR_F( lid )){
        const int feidx = SH_CURRENT_POINTER_F( lid ) + tnid;
	SH_FORWARD_NBR( threadIdx.x ) = graph->graph_f.neighbors[feidx];
	SH_FORWARD_WEIGHT( threadIdx.x ) = graph->graph_f.weights[feidx];
	const nodeid_t mid_or_inv = graph->graph_f.midpoint[feidx];
	SH_FORWARD_MID( threadIdx.x ) = graph->graph_b.midpoint[mid_or_inv];
      }
      // load the current iteration's backward vertices into shared (check already contracted)
      if(tnid < SH_CURRENT_NNBR_B( lid )){
        const nodeid_t beidx = SH_CURRENT_POINTER_B( lid ) + tnid;
	SH_BACKWARD_NBR( threadIdx.x ) = graph->graph_b.neighbors[beidx];
	SH_BACKWARD_WEIGHT( threadIdx.x ) = graph->graph_b.weights[beidx];
      }
    }

    const nodeid_t lnid = SH_BACKWARD_NBR( threadIdx.x );
    const bool bwd_nbr_here = !nodeid_is_special(lnid);
    const bool fwd_nbr_here = !nodeid_is_special(SH_FORWARD_NBR(threadIdx.x));

    assert( tnid < SH_CURRENT_NNBR_B(lid) || !bwd_nbr_here );

      // load incoming vertices' forward edges into register space (using shared to speed things up)
      // load pointer/nnbr/status for the incoming neighboring nodes

    if ( bwd_nbr_here )
      {
        SH_BACKWARD_POINTER_F(threadIdx.x) = graph->graph_f.pointer[lnid];
        SH_BACKWARD_NNBR_F(threadIdx.x) = graph->graph_f.num_neighbors[lnid];
      }
    else
      {
        SH_BACKWARD_NNBR_F( threadIdx.x ) = 0;
      }

    // so long as vertices are handled by only one warp no need for this syncthread

    const int lnnbr = SH_BACKWARD_NNBR_F( threadIdx.x );
    const int lnptr = SH_BACKWARD_POINTER_F( threadIdx.x );


    // Determine maximum degree of bwd_fwd neighbors, will be used
    // to limit 2-hop search.

    __shared__ int max_bwd_deg_f_wps[n_wps];
    const int max_bwd_deg_f_wp = max_wp(lnnbr);
    if ( tpn > 5 )
      {
        if ( lane == 0 ) max_bwd_deg_f_wps[wp_num] = max_bwd_deg_f_wp;
        __syncthreads();
        if ( wp_num == 0 )
          max_bwd_deg_f_wps[0] =
            max_wp(lane < n_wps ? max_bwd_deg_f_wps[lane] : 0);
        __syncthreads();
      }

    const int max_bwd_deg_f_nd =
      tpn > 5 ? max_bwd_deg_f_wps[0] : max_bwd_deg_f_wp;

    stats_continue(tbuf,tcomp1,dbuf);

    const int nnbr_f = SH_CURRENT_NNBR_F( lid );
    const int nnbr_b = SH_CURRENT_NNBR_B( lid );
    // only tid==0 needs to do this, not worth a branch for a single add instruction!
    // NOTE that if it's using local storage instead of register, branch might be worth
    deg_sum_DB_est_l += nnbr_b;

    const bool do_one_hop = opt_one_hop == 1
      || opt_one_hop > 1 && max_bwd_deg_f_nd
      + ( tpn < 5 ? TPN/4 : nnbr_f * ( opt_one_hop - 1 ) ) < TPN;

    // Forward neighbors block.
    __shared__ uint32_t fwd_nbrs_bl[n_wps];
    const uint32_t fwd_nbrs_wp = __ballot_sync(~0,fwd_nbr_here);
    if ( tpn > wp_lg && lane == 0 ) fwd_nbrs_bl[wp_num] = fwd_nbrs_wp;

    if ( participant ) {

      if ( tpn <= wp_lg )
        {
          const int tpnw = tpn <= wp_lg ? tpn : wp_lg;
          const int nd_per_wp = 1 << wp_lg - tpnw;
          const uint32_t msk = tpn < 5 ? ( 1 << ( tpn<5 ? TPN : 1 ) ) - 1 : ~0;
          const uint32_t shc = fwd_nbrs_wp >> ( lid % nd_per_wp << tpn ) & msk;
          lshc[0] = shc;
        }
      else
        {
          __syncthreads();
          for ( int i=0; i<n_wps; i++ ) lshc[i] = fwd_nbrs_bl[i];
        }

      bool linear_scan = TPN <= 16; // Can be modified below.
      if ( !linear_scan )
        {
	  hash_clear();
          if ( tpn > 5 ) __syncthreads();
          if ( fwd_nbr_here )
            nid_to_fwd_hash_get(SH_FORWARD_NBR(threadIdx.x)) = tnid;
          if ( tpn > 5 ) __syncthreads();
          const uint8_t fwd_nbr_eidx = nid_to_fwd_hash_get(lnid);
          const int fwd_nbr_sidx = ( lid << tpn ) + fwd_nbr_eidx;
          const bool hit =
            fwd_nbr_eidx < nnbr_f && SH_FORWARD_NBR( fwd_nbr_sidx ) == lnid;
          if ( hit )
            lshc[ fwd_nbr_eidx >> 5 ] &= ~ ( 1 << ( fwd_nbr_eidx & 0x1f ) );
          linear_scan = !hit && fwd_nbr_eidx != 0xff;
        }

      // unmark shortcuts to self
      if ( linear_scan )
        for ( int i=0; i<TPN; i++ ) {
          if ( i >= nnbr_f ) break;
          const int e_fwd_sidx = i + ( lid << tpn );
          const nodeid_t fwd_nbr = SH_FORWARD_NBR( e_fwd_sidx );
          const uint32_t fwd_e_num_msk = ~ ( 1 << ( i & 0x1f ) );
          if ( lnid == fwd_nbr ) lshc[i>>5] &= fwd_e_num_msk;
        }
    }

    if ( stats_blk ) __syncthreads();
    stats_stop(tcomp1,dcomp1);

    // we only have bfb_sz backward-forward neighbor per node at any
    // given time (shared size limitations)
    for(uint32_t r=0; r<iter_per_node_r; r++){

      const int bwd_fwd_e_num_0 = bfb_sz * r;
      if ( bwd_fwd_e_num_0 >= max_bwd_deg_f_nd ) break;

      // we're re-using shared space for loading/storing b-f and f-b edges, so must reload ptr/nnbr
      // note, no benefit for checking nnbr to do this only for valid threads since no global loads

      SH_BACKWARD_POINTER_F( threadIdx.x ) = lnptr;
      SH_BACKWARD_NNBR_F( threadIdx.x ) = lnnbr;
	// so long as vertices are handled by only one warp no need for this syncthread
      if(tpn>5){
        __syncthreads();
      }

      stats_start(tbuf);
	
      const int unid = threadIdx.x & grp_mk; // I forgot what "un" was for.
      const int unt0 = threadIdx.x & ~grp_mk;
      const bool have_work = bwd_fwd_e_num_0 < lnnbr;
      const uint32_t wp_work = __ballot_sync(~0,have_work);

      if ( wp_work && !do_one_hop ) {

        const int nnbr_b = SH_CURRENT_NNBR_B( lid );
        const int my_bwd_nnbr_f = SH_BACKWARD_NNBR_F( threadIdx.x );
        const nodeid_t my_bwd_fwd_ptr_f = SH_BACKWARD_POINTER_F( threadIdx.x );

#pragma unroll
        for ( int g = 0; g < bfb_rnds; g++ )
          {
            const int bwd_fwd_e_num = bfb_sz * r + g * grp_sz + unid;
            for ( int m = 0;  m < grp_sz;  m++ )
              {
                const int bwd_sidx = unt0 + m;
                const int bwd_e_num = bwd_sidx & tnid_msk;
                __syncwarp();
                const int bwd_nnbr_f = __shfl_sync(~0,my_bwd_nnbr_f,m,grp_sz);
                const nodeid_t bwd_fwd_ptr_f =
                  __shfl_sync( ~0, my_bwd_fwd_ptr_f, m, grp_sz );
                if ( bwd_e_num >= nnbr_b ) continue;
                if ( bwd_fwd_e_num >= bwd_nnbr_f ) continue;
                assert( participant );
                const int bwd_fwd_e_gidx = bwd_fwd_ptr_f + bwd_fwd_e_num;
                const int bwd_fwd_e_sidx = m * block_dim + threadIdx.x;
                SH_BACKWARD_FORWARD_NBR( bwd_fwd_e_sidx )
                  = graph->graph_f.neighbors[ bwd_fwd_e_gidx ];
                SH_BACKWARD_FORWARD_WEIGHT( bwd_fwd_e_sidx )
                  = graph->graph_f.weights[ bwd_fwd_e_gidx ];
              }
            for ( int m = 0;  m < grp_sz;  m++ )
              {
                const int j = g * grp_sz + m;
                const int bwd_fwd_e_sidx = unid * block_dim + unt0 + m;
                lnbr[j] = SH_BACKWARD_FORWARD_NBR( bwd_fwd_e_sidx );
                lweight[j] = SH_BACKWARD_FORWARD_WEIGHT( bwd_fwd_e_sidx );
              }
          }
      }

      if ( tpn > 5 ) __syncthreads();
      stats_stop(tbuf,dbuf);

      // similarly only bfb_sz forward-backward neighbor per node
      // resident on shared at any given time
      for(uint32_t q=0; q<iter_per_node_q; q++){

        const int fwd_eidx = q;
        if ( fwd_eidx >= SH_CURRENT_NNBR_F( lid ) ) break;

	const int tidx_e0 = threadIdx.x & ~( TPN-1 );
	const int fwd_sidx = tidx_e0 + fwd_eidx;

        if ( nodeid_is_special(SH_FORWARD_NBR(fwd_sidx)) ) continue;

        stats_start(tbuf);

	// note, for forward-backward edges we'll load full nodes, but
	// only fbb_sz at a time, this way we're matching the shared mem
	// usage for loading backward-forward edges to be precise,
	// we're loading neighbors fbb_sz*q through fbb_sz*(q+1)

	// load outgoing vertices' backward edges into shared memory space
	// load pointer/nnbr/status for the outgoing neighboring nodes
	if ( tnid == fwd_eidx )
          {
            if ( participant ){
              SH_FORWARD_POINTER_B( threadIdx.x ) =
                graph->graph_b.pointer[SH_FORWARD_NBR( threadIdx.x )];
              SH_FORWARD_NNBR_B( threadIdx.x ) =
                graph->graph_b.num_neighbors[SH_FORWARD_NBR( threadIdx.x )];
            } else {
              SH_FORWARD_NNBR_B( threadIdx.x ) = 0;
            } }

	// so long as vertices are handled by only one warp no need for this syncthread
	if(tpn>5){
	  __syncthreads();
	}

        const bool bwd_e_here = bwd_nbr_here;

        //
        // Check next fbb_sz forward nodes for witness paths.
        //

	const uint32_t fwd_e_num_msk = ~ ( 1 << ( fwd_eidx & 0x1f ) );

        // Local memory access unless q unrolled.
        const int shc_sft = 5;
        assert( shc_sft >= 0 );
        auto lshc_grp = lshc[q>>shc_sft];
        const auto lshc_grp_no_shc = lshc_grp & fwd_e_num_msk;
        const bool needed_wp = lshc_grp & ~fwd_e_num_msk;
        bool need_wp = needed_wp;

	const uint8_t eidx_null = uint8_t(-1);
	
	if ( tpn > 5 ) __syncthreads();

	// Reset hash table.
	hash_clear();
	  
	if ( tpn > 5 || stats_blk ) __syncthreads();

	const uint32_t fwd_bwd_nnbr = SH_FORWARD_NNBR_B( fwd_sidx );
	const uint32_t fwd_nnbr = SH_CURRENT_NNBR_F( lid );

	// Load all bwd edges of node fwd_eidx = q.
	//
	if ( tnid < fwd_bwd_nnbr && participant  ) {
	  
	  const int fwd_bwd_e_gidx = SH_FORWARD_POINTER_B( fwd_sidx ) + tnid;

	  const nodeid_t fwd_bwd_nbr =
	    graph->graph_b.neighbors[fwd_bwd_e_gidx];

	  SH_FORWARD_BACKWARD_NBR( threadIdx.x ) = fwd_bwd_nbr;

          if ( !nodeid_is_special(fwd_bwd_nbr) )
            {
              SH_FORWARD_BACKWARD_WEIGHT( threadIdx.x ) =
                graph->graph_b.weights[fwd_bwd_e_gidx];

              // Hash table maps: node id -> edge number of node fwd_eidx.
              nid_to_fwd_hash_get( fwd_bwd_nbr ) = tnid;
            }
	}

	if ( tpn > 5 || stats_blk ) __syncthreads();

	stats_continue(tbuf,tcomp1,dbuf);
	
	// calculate the shortcut and do the 1-hop and 2-hop search.
	const nodeid_t bwd_wht = SH_BACKWARD_WEIGHT( threadIdx.x );
	const nodeid_t lshc_w = bwd_wht + SH_FORWARD_WEIGHT( fwd_sidx );

        // When non-zero, the weight of the edge connecting this
        // thread's backward node to the current forward node.
        nodeid_t edge_direct_w = 0;
        int edge_direct_bwd_eidx = -1;
        int edge_direct_fwd_j = -1;
        bool need_longcut = opt_longcuts;
        bool found_longcut = false;
	
	// Try 1-hop search using hash table.
	//
	if ( ( r == 0 || opt_longcuts ) && bwd_e_here )
	  {
	    const auto eidx = nid_to_fwd_hash_get(lnid);
	    const int tidx = tidx_e0 + eidx;
	    if ( eidx < fwd_bwd_nnbr
		 && lnid == SH_FORWARD_BACKWARD_NBR( tidx ) )
              {
                if ( opt_longcuts )
                  {
                    edge_direct_w = SH_FORWARD_BACKWARD_WEIGHT( tidx );
                    edge_direct_bwd_eidx = eidx;
                  }
                if ( need_wp && lshc_w >= SH_FORWARD_BACKWARD_WEIGHT( tidx ) )
                  {
                    need_wp = false;
                    if ( stats_wps ) atomicAdd(&n_1hshc, 1);
                  }
              }
	  }
	
	uint32_t ths = 0; // Iterative two-hop search bitmask.
	const nodeid_t fwd_nbr = SH_FORWARD_NBR( fwd_sidx );
	
	// If we still need to, do 1- and 2-hop searches.
	//
	if ( !do_one_hop && bwd_e_here && ( need_wp || edge_direct_w ) )
#pragma unroll
	  for(uint32_t j=0; j<bfb_sz; j++){
	    
	    if ( stats_wps ) atomicAdd(&n_ij, 1);
	    if ( bfb_sz*r + j >= lnnbr ) break;
	    if ( lweight[j] > lshc_w ) continue;
            if ( nodeid_is_special(lnbr[j]) ) continue;

	    // Find any paths missed by the 1-hop search that was
	    // performed using hash table, see code above.
	    if ( lnbr[j] == fwd_nbr && lweight[j] <= lshc_w )
	      {
                // Compiler bug workaround?
                //   Problem: If condition above always true with -G flag.
                //   Built on Wed_Apr_24_19:10:27_PDT_2019
                //   Cuda compilation tools, release 10.1, V10.1.168
                assert( lnbr[j] == fwd_nbr );

                if ( need_wp )
                  {
                    need_wp = false;
                    if ( stats_wps ) atomicAdd(&n_1hshc, 1);
                    if ( stats_wps ) atomicAdd(&n_fph1, 1);
                    ths = 0;
                  }
                if ( opt_longcuts && opt_longcuts_fwd )
                  {
                    assert( edge_direct_w == 0 || edge_direct_w == lweight[j] );
                    edge_direct_w = lweight[j];
                    edge_direct_fwd_j = j;
                  }
                if ( !edge_direct_w ) break;
	      }

	    // Do 2-hop search using hash table.
	    //
	    auto eidx = nid_to_fwd_hash_get(lnbr[j]);
	    
	    // Hash table miss, no intermediate node.
	    if ( eidx == eidx_null ) continue;
	    
	    const int tidx = tidx_e0 + eidx;
	    if ( eidx < fwd_bwd_nnbr
		 && SH_FORWARD_BACKWARD_NBR( tidx ) == lnbr[j] )
	      {
		// Hash table hit.

                const nodeid_t w_path =
                  lweight[j] + SH_FORWARD_BACKWARD_WEIGHT( tidx );

		// for two hop search, the comparison should not
		// include equality
		if ( w_path >= lshc_w ) continue;

                if ( need_wp )
                  {
                    need_wp = false;
                    if ( stats_wps ) atomicAdd(&n_2hshc, 1);
                    ths = 0;
                  }
                if ( need_longcut && w_path < edge_direct_w )
                  {
                    need_longcut = false;
                    found_longcut = true;
                  }
                if ( need_longcut || need_wp ) continue; else break;
	      }
	    
	    // Hash table miss. Store j value for iterative 2-hop search.
	    ths |= 1 << j;
	  }

          if ( found_longcut )
            {
              num_longcuts++;
              if ( opt_longcuts_cull )
                {
                  const nodeid_t fwd_e_gidx =
                    lnptr + edge_direct_fwd_j + bfb_sz * r;
                  const nodeid_t bwd_e_gidx =
                    SH_FORWARD_POINTER_B(fwd_sidx) + edge_direct_bwd_eidx;
                  const bool found_fwd = edge_direct_fwd_j >= 0;
                  const bool found_bwd = edge_direct_bwd_eidx >= 0;
                  const bool found_both = found_fwd && found_bwd;
                  if ( found_both && opt_longcuts_fwd )
                    {
                      const nodeid_t dst_prev =
                        atomicExch
                        ( &graph_c.graph_f.neighbors[fwd_e_gidx],
                          nodeid_invalid );
                      const nodeid_t edge_dst = SH_FORWARD_NBR(fwd_sidx);
                      assert( dst_prev == nodeid_invalid
                              || dst_prev == edge_dst );
                      if ( dst_prev != nodeid_invalid )
                        num_longcuts_cut++;
                    }
                  if ( found_bwd || found_both )
                    {
                      const nodeid_t src_prev =
                        atomicExch
                        ( &graph_c.graph_b.neighbors[bwd_e_gidx],
                          nodeid_invalid );
                      assert( src_prev == nodeid_invalid
                              || src_prev == lnid );
                      if ( src_prev != nodeid_invalid )
                        num_longcuts_cut++;
                    }
                }
            }
	
	if ( stats_blk ){__syncthreads();}
	stats_continue(tcomp1,tcomp2,dcomp1);
	
	// Perform iterative 2-hop searches.
	//
	while ( opt_loop_after_collision && ths )
	  {
	    // Use find-first-set function to find 1 bits in ths.
	    const int j = __ffs(ths) - 1;
	    ths ^= ((typeof ths)1) << j;
	    
	    nodeid_t lnbr_j = 0;
	    nodeid_t lweight_j = 0;
	    
	    // Retrieve j'th elements without scaring the compiler
	    // into using local memory for lnbr and lweight.
#define T4(b)   if ( bfb_sz > 8 && j&8 ){ T3(b+8) } else { T3(b) }
#define T3(b)   if ( j&4 ) { T2(b+4) } else { T2(b) }
#define T2(b)   if ( j&2 ) { T1(b+2) } else { T1(b) }
#define T1(b)   if ( j&1 ) { T0(b+1); } else { T0(b); }
#define T0(jj)  { lnbr_j = lnbr[jj]; lweight_j = lweight[jj]; }
	    if ( bfb_sz > 16 && j&16 ){ T4(16) } else { T4(0) }
#undef T4
#undef T3
#undef T2
#undef T1
#undef T0
	    
	    if ( stats_wps ) atomicAdd(&n_2hop, 1);
	    bool fpath = false;
	    
	    for(uint32_t k=0; k<TPN; k++){
	      if( k >= fwd_bwd_nnbr) break;
	      
	      const int fwd_bwd_sidx = k + (lid<<tpn);
	      const nodeid_t fwd_bwd_nbr =
		SH_FORWARD_BACKWARD_NBR( fwd_bwd_sidx );
	      const bool found_path = lnbr_j == fwd_bwd_nbr;
	      
	      fpath |= found_path;
	      
	      // for two hop search, the comparison should not
	      // include equality
	      
	      if ( found_path &&
		   lweight_j + SH_FORWARD_BACKWARD_WEIGHT( fwd_bwd_sidx )
		   < lshc_w )
		{
                  need_wp = false;
		  ths = 0;
		  if ( stats_wps ) atomicAdd(&n_2hshc, 1);
		  if ( stats_wps ) atomicAdd(&n_fph2, 1);
		  break;
		}
	    }
	    
	    if ( stats_wps && fpath ) atomicAdd(&n_fpath, 1);
	  }
	
	if ( stats_blk ){__syncthreads();}
	stats_stop(tcomp2,dcomp2);
	
        // Update lshc array.
        if ( needed_wp && !need_wp ) lshc[ q >> shc_sft ] = lshc_grp_no_shc;
	
	// all tpn threads must finish before loading the next set of 8 forward neighbors
	
        
      } // End of q loop

      if ( tpn > 5 ) __syncthreads();
    } // End of r loop

    stats_start(tfinish);

    // write back all confirmed shortcuts
    if ( current_pos < list_length && bwd_nbr_here ){

      const nodeid_t current = SH_CURRENT_ID( lid );

      for(uint32_t i=0; i<flag_len; i++){
	for(uint32_t j=0; j<32; j++){
	  if( ((i<<5) + j) < SH_CURRENT_NNBR_F( lid ) ){
	    if((lshc[i] & (1<<j)) != 0){
	      uint32_t mid_point;
	      if(SH_FORWARD_MID( (i<<5) + j + (lid<<tpn) ) != NODEID_NULL ){
		// if the second half of the shortcut is already a shortcut, use its midpoint
		mid_point = SH_FORWARD_MID( (i<<5) + j + (lid<<tpn) );
	      } else {
		// else current is the closest midpoint to the edge's end (midpoint uses the inputIDs
		mid_point = graph->node_ids[ current ];
	      }

              shc_t ei;
              ei.src_id = lnid;
              ei.dst_id = SH_FORWARD_NBR( (i<<5) + j + (lid<<tpn) );
              ei.wht = SH_BACKWARD_WEIGHT( threadIdx.x )
                + SH_FORWARD_WEIGHT( (i<<5) + j + (lid<<tpn) );
              ei.mid_oid = mid_point;

              const int shc_dbin = ei.dst_id % shc_list->n_segs;
	      uint32_t shc_dst_count_old =
                atomicAdd(&(shc_list->len_dst[shc_dbin]), 1);

	      assert(shc_dst_count_old < shc_list->max_len_per_seg);
	      shc_list->shc_dst
                [shc_list->max_len_per_seg * shc_dbin + shc_dst_count_old] = ei;

	    }
	  }
	}
      }
    }

    // setup next iteration
    current_pos += vertex_inc;
    current_pos_0 += vertex_inc;
    __syncthreads();

    stats_continue(tfinish,tbuf,dfinish);
  }

  if(tid == 0){
    d_DB_est[0] = deg_sum_DB_est_l / num_iters; // not worth using float, only a very rough estimate
  }

  if ( opt_longcuts )
    {
      atomicAdd( &graph_c.self_d->num_longcuts, num_longcuts );
      atomicAdd( &graph_c.self_d->num_longcuts_cut, num_longcuts_cut );
    }

  if ( !stats || threadIdx.x >= 32 ) return;

  if ( stats )
    {
      qstats->qend = clock64();
      qstats->dbuf = dbuf;
      qstats->dcomp1 = dcomp1;
      qstats->dcomp2 = dcomp2;
      qstats->dfinish = dfinish;
      qstats->n_ij = n_ij;
      qstats->n_2hop = n_2hop;
      qstats->n_fpath = n_fpath;
      qstats->n_1hshc = n_1hshc;
      qstats->n_2hshc = n_2hshc;
      qstats->n_fph1 = n_fph1;
      qstats->n_fph2 = n_fph2;
    }
}


template <uint32_t tpn, uint32_t blocked_blsz_co_lg, int bfb_rnds>
__global__ void __launch_bounds__(1 << blocked_blsz_co_lg)
cu_CH_contract_nodes_blocked
(cu_graph_CH_bi_t graph_c, nodeid_t *node_list, nodeid_t list_length,
 cu_shc_CH_t *shc_list, cu_CH_query_stats *qstats_a, nodeid_t *d_DB_est)
{
  const cu_graph_CH_bi_t* const graph = &graph_c;
  const float opt_one_hop = chopts_c.contract_one_hop;

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const bool opt_longcuts_cull =
    chopts_c.cull_contract && int(tpn) >= chopts_c.cull_threshold_tpn;
  const bool opt_longcuts = opt_longcuts_watch || opt_longcuts_cull;
  const bool opt_loop_after_collision =
    static_wps_hash_loop_after_collision
    && chopts_c.wps_hash_loop_after_collision;

  const int TPN = 1 << tpn;
  const int block_lg = blocked_blsz_co_lg;
  const int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int sq_block_per_node = TPN >> block_lg;
  const int block_per_node = sq_block_per_node*sq_block_per_node;

  constexpr int wp_lg = 5;
  constexpr int wp_sz = 1 << wp_lg;
  const int lane = threadIdx.x % wp_sz;
  const int wp_num = threadIdx.x >> wp_lg;
  constexpr int n_wps = block_dim >> wp_lg;

  const int grp_lg = 3;
  const int grp_sz = 1 << grp_lg;
  const int grp_mk = grp_sz - 1;

  // Size of backward-forward buffer.
  constexpr int bfb_sz = bfb_rnds * grp_sz;

#ifdef NDEBUG
  const bool stats_want = false;
#else
  const bool stats_want = true;
#endif
  const bool stats_check = false;
  const bool stats_blk = stats_want && qstats_a;
  const auto qstats = &qstats_a[blockIdx.x];
  const bool stats = stats_blk && threadIdx.x == 0;
  const bool stats_wps = stats_blk && stats_wps_want;
  __shared__ int64_t tbuf, tcomp1, tcomp2, dbuf, dcomp1, dcomp2, *stats_current;
  __shared__ int64_t tfinish, dfinish;

  auto stats_continue =
    [&](int64_t& expect, int64_t& set, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
        assert( &expect == stats_current ); stats_current = &set; }
      set = clock64();
      accum += set - expect;
    };

  auto stats_start =
    [&](int64_t& set)
    {
      if ( !stats ) return;
      if ( stats_check ) { assert( !stats_current ); stats_current = &set; }
      set = clock64();
    };

  auto stats_stop =
    [&](int64_t& expect, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
         assert( &expect == stats_current ); stats_current = NULL; }
      accum += clock64() - expect;
    };

  __shared__ int n_ij, n_2hop, n_fpath, n_1hshc, n_2hshc, n_fph1, n_fph2;

  if ( stats )
    {
      qstats->qstart = clock64();
      dbuf = dcomp1 = dcomp2 = dfinish = 0;
      if ( stats_check ) stats_current = NULL;
      n_ij = 0; n_2hop = 0; n_fpath = 0; n_1hshc = 0; n_2hshc = 0; n_fph1 = 0; n_fph2 = 0;
    }

  assert(tpn == graph->thread_per_node);
  
  // number of vertices handled at each iteration by each block
  const int vertex_per_iter = 1; // technically, 1/block_per_node.

  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)
  
  const int n_selts = ((5) + ((16) + 8)*block_dim);
  __shared__ uint32_t shm[n_selts];

  int make_f_pos = 0;
  auto make_f = [&](uint n) {
      const int pos = make_f_pos;
      make_f_pos += n;
      return [=](uint idx) -> uint32_t&
        { assert( !debug_sm_idx || idx < n ); return shm[pos+idx]; };  };

  auto SH_CURRENT_ID = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_F = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_B = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_F = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_B = make_f(vertex_per_iter);

  auto SH_FORWARD_NBR = make_f(block_dim);
  auto SH_FORWARD_WEIGHT = make_f(block_dim);
  auto SH_FORWARD_MID = make_f(block_dim);
  auto SH_BACKWARD_NBR = make_f(block_dim);
  auto SH_BACKWARD_WEIGHT = make_f(block_dim);

  auto SH_BACKWARD_POINTER_F = make_f(block_dim);
  auto SH_BACKWARD_NNBR_F = make_f(block_dim);
  auto SH_BACKWARD_FORWARD_NBR = make_f(grp_sz*block_dim);
  auto SH_BACKWARD_FORWARD_WEIGHT = make_f(grp_sz*block_dim);
  // recycling space from backwards_forwards nodes/edges
  make_f_pos -= ( 2 + 2*grp_sz ) * block_dim;
  auto SH_FORWARD_POINTER_B = make_f(block_dim);
  auto SH_FORWARD_NNBR_B = make_f(block_dim);

  auto SH_FORWARD_BACKWARD_NBR = make_f(block_dim);
  auto SH_FORWARD_BACKWARD_WEIGHT = make_f(block_dim);

  // Leaves 14-8 = 6 free.
  // hash size per thread.
  const int hash_size_pt = 8;
  auto SH_NID_TO_FWD_BWD_TIDX = make_f( block_dim * hash_size_pt );

  assert( n_selts >= make_f_pos );

  uint32_t* const fwd_hash_base = &SH_NID_TO_FWD_BWD_TIDX(0);
  const int hash_word_per_node = hash_size_pt << block_lg;
  const int hash_byte_per_node = hash_word_per_node * 4;
  uint32_t* const fwd_hash_lw = fwd_hash_base;
  uint8_t* const fwd_hash_lb = (uint8_t*) fwd_hash_lw;

  auto nid_to_fwd_hash_get =
    [&](nodeid_t nid) -> uint8_t&
    { return fwd_hash_lb[nid%hash_byte_per_node]; };

  const uint8_t eidx_null = 0xff;
  assert( block_dim <= 256 ); // Hash won't work if threadIdx.x > 255.

  auto hash_clear =
    [&]() { for ( int c = 0;  c < hash_size_pt;  c++ )
              fwd_hash_lw[threadIdx.x + (c<<block_lg)] = uint32_t(-1); };

  // local storage (registers, we hope)
  nodeid_t lweight[bfb_sz];
  nodeid_t lnbr[bfb_sz];

  const uint32_t iter_per_node_r = div_up(TPN,bfb_sz);
  const uint32_t iter_per_node_q = block_dim;
  const uint32_t iter_per_node_s = sq_block_per_node;

  // maximum length of the shc_flag (based on tpn)
  const uint32_t flag_len = div_up(block_dim,32);
  uint32_t lshc[flag_len]; // an array of binary flags for each shc coming out of this node

  if ( stats_blk ) sync_threads();
  stats_start(tbuf);  // Also set at end of main loop body.

  nodeid_t num_longcuts = 0, num_longcuts_cut = 0;

  const nodeid_t work_limit = list_length * block_per_node;

  uint32_t deg_sum_DB_est_l = 0;
  uint32_t num_iters = 0;

  while ( true ) {
    __shared__ nodeid_t work_item;
    if ( !threadIdx.x )
      work_item = atomicAdd(&graph_c.self_d->work_item_next,1);
    __syncthreads();
    if ( work_item >= work_limit ) break;

    const nodeid_t current_pos = work_item / block_per_node;
    const int lbid = work_item % block_per_node;
    const int lbid_b = lbid / sq_block_per_node;
    const int lbid_f = lbid % sq_block_per_node;

    // load current iteration's node IDs and the corresponding pointer/nnbr/node status into shared
    if ( threadIdx.x == 0 ){
      const nodeid_t nid = node_list[current_pos];
      SH_CURRENT_ID( threadIdx.x ) = nid;
      SH_CURRENT_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[nid];
      SH_CURRENT_POINTER_B( threadIdx.x ) = graph->graph_b.pointer[nid];
      SH_CURRENT_NNBR_F(threadIdx.x) = graph->graph_f.num_neighbors[nid];
      SH_CURRENT_NNBR_B(threadIdx.x) = graph->graph_b.num_neighbors[nid];
    }

    // sync needed here because the first warps are cooperating in loading the entire block's data
    __syncthreads();

    const int nnbr_f = SH_CURRENT_NNBR_F( 0 );
    const int nnbr_b = SH_CURRENT_NNBR_B( 0 );
    const int fwd_sidx_stop = nnbr_f - lbid_f * block_dim;
    const int bwd_sidx_stop = SH_CURRENT_NNBR_B( 0 ) - lbid_b * block_dim;
    const bool fwd_elt_here = threadIdx.x < fwd_sidx_stop;
    const bool bwd_elt_here = threadIdx.x < bwd_sidx_stop;
    // only tid==0 needs to do this, not worth a branch for a single add instruction!
    // NOTE that if it's using local storage instead of register, branch might be worth
    // in blocked due to work queue, it is possible for same node's nnbr to be counted multiple times
    // however it shouldn't be a problem since it's just a sampled estimate anyways
    deg_sum_DB_est_l += nnbr_b;
    num_iters++;

    if ( lbid_f*block_dim < nnbr_f && lbid_b*block_dim < SH_CURRENT_NNBR_B(0) ){
	
      // load the current iteration's forward vertices into shared along with their state (rank)

      const int feidx =
        SH_CURRENT_POINTER_F( 0 ) + lbid_f*block_dim + threadIdx.x;
      SH_FORWARD_NBR( threadIdx.x ) =
        fwd_elt_here ? graph->graph_f.neighbors[feidx] : nodeid_invalid;
      const bool fwd_e_here = !nodeid_is_special(SH_FORWARD_NBR(threadIdx.x));

      if ( fwd_e_here ) {
	SH_FORWARD_WEIGHT( threadIdx.x ) = graph->graph_f.weights[feidx];
	const nodeid_t mid_or_inv = graph->graph_f.midpoint[feidx];
	SH_FORWARD_MID( threadIdx.x ) = graph->graph_b.midpoint[mid_or_inv];
      }

      // load the current iteration's backward vertices into shared (check already contracted)

      const nodeid_t beidx =
        SH_CURRENT_POINTER_B( 0 ) + lbid_b*block_dim + threadIdx.x;
      const nodeid_t lnid = SH_BACKWARD_NBR( threadIdx.x ) =
        bwd_elt_here ? graph->graph_b.neighbors[beidx] : nodeid_invalid;
      const bool bwd_e_here = !nodeid_is_special(lnid);

      // no need to sync, threads working on their own data

      // load incoming vertices' forward edges into register space (using shared to speed things up)
      // load pointer/nnbr/status for the incoming neighboring nodes
      if ( bwd_e_here ) {
	SH_BACKWARD_WEIGHT( threadIdx.x ) = graph->graph_b.weights[beidx];
	SH_BACKWARD_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[lnid];
	SH_BACKWARD_NNBR_F(threadIdx.x) = graph->graph_f.num_neighbors[lnid];
      } else {
	SH_BACKWARD_NNBR_F( threadIdx.x ) = 0;
      }
    
      const int lnnbr = SH_BACKWARD_NNBR_F( threadIdx.x );
      const int lnptr = SH_BACKWARD_POINTER_F( threadIdx.x );

      const int max_bwd_deg_f_wp = max_wp(lnnbr);
      __shared__ int max_bwd_deg_f_wps[n_wps];
      if ( lane == 0 ) max_bwd_deg_f_wps[wp_num] = max_bwd_deg_f_wp;
      __syncthreads();
      if ( wp_num == 0 )
        max_bwd_deg_f_wps[0] =
          max_wp(lane < n_wps ? max_bwd_deg_f_wps[lane] : 0);
      __syncthreads();
      const int max_bwd_deg_f_nd = max_bwd_deg_f_wps[0];
    
      stats_continue(tbuf,tcomp1,dbuf);

      const bool do_one_hop = opt_one_hop == 1
        || opt_one_hop > 1 && max_bwd_deg_f_nd
        + ( tpn < 5 ? TPN/4 : nnbr_f * ( opt_one_hop - 1 ) ) < TPN;

      // Initialize lshc to the paths through
      // current starting at the backward neighbor assigned to this thread ..
      // .. and ending at forward neighbors in the current block (lbid_f) ..
      // .. except for any forward neighbor equal to this thread's
      // backward neighbor.


      // If true, use hash table to find whether lnid matches any fwd edge.
      const bool use_hash = true;
      
      {
        // Forward neighbors block.
        __shared__ uint32_t fwd_nbrs_bl[n_wps];
        const uint32_t fwd_nbrs_wp = __ballot_sync(~0,fwd_e_here);
        if ( lane == 0 ) fwd_nbrs_bl[wp_num] = fwd_nbrs_wp;
        __syncthreads();
        for ( int i=0; i<n_wps; i++ ) lshc[i] = fwd_nbrs_bl[i];
	
	bool linear_scan = !use_hash; // Can be modified below.
	if ( !linear_scan ){

          // Clear hash table entries to eidx_null (0xff).
	  hash_clear();

	  __syncthreads();

          // Write forward edges, except for forward edge assigned
          // to thread number eidx_null. 
          //
	  if ( ( block_dim < eidx_null || threadIdx.x < eidx_null )
               && fwd_e_here )
	    nid_to_fwd_hash_get(SH_FORWARD_NBR(threadIdx.x)) = threadIdx.x;
          //
          // Note that it would not be possible to detect a collision
          // if thread number eidx_null wrote the table after another
          // colliding thread. That's because a collision is concluded
          // when the node id's don't match and the element value is
          // other than eidx_null. If the element value is eidx_null
          // then the entry is concluded to be empty.
          //
          // For example, tid 250 carries node 0x9 and tid 255
          // (assuming eidx_null = 255) carries node 0x100009. Both
          // map to hash table entry number 9. The code above would
          // write a 250. A lookup for 0x9 would hit and a lookup for
          // 0x100009 would be a detected collision. If instead tid
          // 255 wrote the table after 250, a lookup for 0x100009
          // would hit, and a lookup for 0x9 would conclude that the
          // entry was empty.

	  __syncthreads();

          if ( bwd_e_here ) {
	    const uint8_t fwd_nbr_eidx = nid_to_fwd_hash_get(lnid);
	    const int fwd_nbr_sidx = fwd_nbr_eidx;
	    const bool hit =
	      ( block_dim >= 256 || fwd_nbr_eidx != eidx_null )
	      && SH_FORWARD_NBR( fwd_nbr_sidx ) == lnid;
	    if ( hit )
	      lshc[ fwd_nbr_eidx >> 5 ] &= ~ ( 1 << ( fwd_nbr_eidx & 0x1f ) );
            linear_scan = !hit && fwd_nbr_eidx != 0xff;
	  }
	}
	
	// Use a linear scan if a hash table collides or is not used.
	if ( linear_scan ){
	  for ( int i=0; i<block_dim; i++ ) {
	    if ( i + lbid_f*block_dim >= nnbr_f ) break;
	    const int e_fwd_sidx = i;
	    const nodeid_t fwd_nbr = SH_FORWARD_NBR( e_fwd_sidx );
	    const uint32_t fwd_e_num_msk = ~ ( 1 << ( i & 0x1f ) );
	    if ( lnid == fwd_nbr ) lshc[i>>5] &= fwd_e_num_msk;
	  }
	}
      }

      if ( stats_blk ) __syncthreads();
      stats_stop(tcomp1,dcomp1);

      // we only have bfb_sz backward-forward neighbor per node at any
      // given time (shared size limitations)
      for(uint32_t r=0; r<iter_per_node_r; r++){

        const int bwd_fwd_e_num_0 = bfb_sz * r;
        if ( bwd_fwd_e_num_0 >= max_bwd_deg_f_nd ) break;

	// we're re-using shared space for loading/storing b-f and f-b edges, so must reload ptr/nnbr
	// note, no benefit for checking nnbr to do this only for valid threads since no global loads

	SH_BACKWARD_POINTER_F( threadIdx.x ) = lnptr;
	SH_BACKWARD_NNBR_F( threadIdx.x ) = lnnbr;

	__syncthreads();

	stats_start(tbuf);
	
	const int unid = threadIdx.x & grp_mk; // I forgot what "un" was for.
	const int unt0 = threadIdx.x & ~grp_mk;
        const bool have_work = bwd_fwd_e_num_0 < lnnbr;
        const uint32_t wp_work = __ballot_sync(~0,have_work);

        if ( wp_work && !do_one_hop )
          {
            const int nnbr_b = SH_CURRENT_NNBR_B( 0 );
            const int my_bwd_nnbr_f = SH_BACKWARD_NNBR_F( threadIdx.x );
            const nodeid_t my_bwd_fwd_ptr_f =
              SH_BACKWARD_POINTER_F( threadIdx.x );

#pragma unroll
            for ( int g = 0; g < bfb_rnds; g++ ) {
              const int bwd_fwd_e_num = bfb_sz * r + g * grp_sz + unid;
              for ( int m = 0;  m < grp_sz;  m++ ) {
                const int bwd_sidx = unt0 + m;
                const int bwd_e_num = bwd_sidx + lbid_b*block_dim;
                __syncwarp();
                const int bwd_nnbr_f = __shfl_sync(~0,my_bwd_nnbr_f,m,grp_sz);
                const nodeid_t bwd_fwd_ptr_f =
                  __shfl_sync( ~0, my_bwd_fwd_ptr_f, m, grp_sz );
                if ( bwd_e_num >= nnbr_b ) continue;
                if ( bwd_fwd_e_num >= bwd_nnbr_f ) continue;
                const int bwd_fwd_e_gidx = bwd_fwd_ptr_f + bwd_fwd_e_num;
                const int bwd_fwd_e_sidx = m * block_dim + threadIdx.x;
                SH_BACKWARD_FORWARD_NBR( bwd_fwd_e_sidx )
                  = graph->graph_f.neighbors[ bwd_fwd_e_gidx ];
                SH_BACKWARD_FORWARD_WEIGHT( bwd_fwd_e_sidx )
                  = graph->graph_f.weights[ bwd_fwd_e_gidx ];
              }
              for ( int m = 0;  m < grp_sz;  m++ ) {
                const int j = g * grp_sz + m;
                const int bwd_fwd_e_sidx = unid * block_dim + unt0 + m;
                lnbr[j] = SH_BACKWARD_FORWARD_NBR( bwd_fwd_e_sidx );
                lweight[j] = SH_BACKWARD_FORWARD_WEIGHT( bwd_fwd_e_sidx );
              }
            }
          }

	__syncthreads();
        stats_stop(tbuf,dbuf);

	// similarly only bfb_sz forward-backward neighbor per node
	// resident on shared at any given time
	for(uint32_t q=0; q<iter_per_node_q; q++){

	  const int fwd_eidx = q + lbid_f*block_dim;
	  if ( fwd_eidx >= SH_CURRENT_NNBR_F( 0 ) ) break;
          if ( nodeid_is_special( SH_FORWARD_NBR( q ) ) ) continue;

          stats_start(tbuf);

	  // note, for forward-backward edges we'll load full nodes, but
	  // only fbb_sz at a time, this way we're matching the shared mem
	  // usage for loading backward-forward edges to be precise,
	  // we're loading neighbors fbb_sz*q through fbb_sz*(q+1)

	  // load outgoing vertices' backward edges into shared memory space
	  // load pointer/nnbr/status for the outgoing neighboring nodes
	  if ( threadIdx.x == q ) {
	    SH_FORWARD_POINTER_B( threadIdx.x ) =
	      graph->graph_b.pointer[SH_FORWARD_NBR( threadIdx.x )];
	    SH_FORWARD_NNBR_B( threadIdx.x ) =
	      graph->graph_b.num_neighbors[SH_FORWARD_NBR( threadIdx.x )];
	  }

	  __syncthreads();

	  // Local memory access unless q unrolled.
	  const int shc_sft = 5;
	  assert( shc_sft >= 0 );
	  auto lshc_grp = lshc[q>>shc_sft];

	  const int fwd_sidx = q;
	  const uint32_t fwd_e_num_msk = ~ ( 1 << ( fwd_eidx & 0x1f ) );

          const auto lshc_grp_no_shc = lshc_grp & fwd_e_num_msk;
          const bool needed_wp = lshc_grp & ~fwd_e_num_msk;
          bool need_wp = needed_wp;
	
	  const uint32_t fwd_bwd_nnbr = SH_FORWARD_NNBR_B( fwd_sidx );
	  const uint32_t fwd_nnbr = SH_CURRENT_NNBR_F( 0 );
	
	  // calculate the shortcut and do the 1-hop and 2-hop search.
	  const nodeid_t bwd_wht = SH_BACKWARD_WEIGHT( threadIdx.x );
	  const nodeid_t lshc_w = bwd_wht + SH_FORWARD_WEIGHT( fwd_sidx );
	
          // When non-zero, the weight of the edge connecting this
          // thread's backward node to the current forward node.
          nodeid_t edge_direct_w = 0;
          int edge_direct_bwd_eidx = -1;
          nodeid_t path_wht_min = lshc_w; // At least there will be a shortcut.
          bool need_longcut = opt_longcuts;

	  for ( uint32_t s=0; s<iter_per_node_s; s++) {
	    __syncthreads();
	  
	    // Reset hash table.
	    hash_clear();
	  
	    __syncthreads();
	  
	    // Load all bwd edges of node fwd_eidx = fbb_sz * q + i.
	    //
	    if ( threadIdx.x + s*block_dim < fwd_bwd_nnbr ) {
	    
	      const int fwd_bwd_e_gidx = SH_FORWARD_POINTER_B( fwd_sidx ) + threadIdx.x + s*block_dim;
	    
	      const nodeid_t fwd_bwd_nbr =
		graph->graph_b.neighbors[fwd_bwd_e_gidx];
	    
	      SH_FORWARD_BACKWARD_NBR( threadIdx.x ) = fwd_bwd_nbr;
	      SH_FORWARD_BACKWARD_WEIGHT( threadIdx.x ) =
		graph->graph_b.weights[fwd_bwd_e_gidx];
	    
	      // Hash table maps: node id -> edge number of node fwd_eidx.
	      if ( block_dim < eidx_null || threadIdx.x < eidx_null )
		nid_to_fwd_hash_get( fwd_bwd_nbr ) = threadIdx.x;
	      // avoiding threadIdx.x==eidx_null, similar to before
	    } else {
	      SH_FORWARD_BACKWARD_NBR( threadIdx.x ) = nodeid_invalid;
	    }
	  
	    __syncthreads();
	  
	    stats_continue(tbuf,tcomp1,dbuf);

	    // Try 1-hop search using hash table.
	    //
	    if ( bwd_e_here && ( r == 0 && need_wp || opt_longcuts ) ) {
	      const auto eidx = nid_to_fwd_hash_get(lnid);
	      const int tidx = eidx;
	      if ( ( block_dim >= 256 || eidx != eidx_null )
		   && lnid == SH_FORWARD_BACKWARD_NBR( tidx ) )
                {
                  if ( opt_longcuts )
                    {
                      edge_direct_w = SH_FORWARD_BACKWARD_WEIGHT( tidx );
                      edge_direct_bwd_eidx = s * block_dim + eidx;
                    }
                  if ( need_wp && lshc_w >= SH_FORWARD_BACKWARD_WEIGHT( tidx ) )
                    {
                      need_wp = false;
                      if ( stats_wps ) atomicAdd(&n_1hshc, 1);
                    }
                }
	    }
	  
	    uint32_t ths = 0; // Iterative two-hop search bitmask.
	    const nodeid_t fwd_nbr = SH_FORWARD_NBR( fwd_sidx );

	    // If we still need to, do 1- and 2-hop searches.
	    //
            if ( !do_one_hop && bwd_e_here && ( need_wp || edge_direct_w ) ) {
#pragma unroll
	      for(uint32_t j=0; j<bfb_sz; j++){
	      
		if ( stats_wps ) atomicAdd(&n_ij, 1);
		if ( bfb_sz*r + j >= lnnbr ) break;
		if ( lweight[j] > lshc_w ) continue;
                if ( nodeid_is_special(lnbr[j]) ) continue;
	      
		// Find any paths missed by the 1-hop search that was
		// performed using hash table, see code above.
		if ( lnbr[j] == fwd_nbr && lweight[j] <= lshc_w ) {

                  if ( need_wp )
                    {
                      need_wp = false;
                      if ( stats_wps ) atomicAdd(&n_1hshc, 1);
                      if ( stats_wps ) atomicAdd(&n_fph1, 1);
                      ths = 0;
                    }
                  if ( opt_longcuts )
                    {
                      edge_direct_w = lweight[j];
                    }
                  if ( !opt_longcuts ) break;
		}
	      
		// Do 2-hop search using hash table.
		//
		auto eidx = nid_to_fwd_hash_get(lnbr[j]);
		const int tidx = eidx;
                const bool hit =
                  ( block_dim >= 256 || eidx != eidx_null )
                  && SH_FORWARD_BACKWARD_NBR( tidx ) == lnbr[j];
                const bool collision = !hit && eidx != eidx_null;

		if ( hit ) {
		  // Hash table hit.

                  const nodeid_t w_path =
                    lweight[j] + SH_FORWARD_BACKWARD_WEIGHT( tidx );

		  // for two hop search, the comparison should not
		  // include equality
		  if ( lshc_w <= w_path ) continue;
		
                  if ( need_wp )
                    {
                      need_wp = false;
                      if ( stats_wps ) atomicAdd(&n_2hshc, 1);
                      ths = 0;
                    }
                  set_min( path_wht_min, w_path );
                  if ( path_wht_min < edge_direct_w ) need_longcut = false;
                  if ( need_longcut || need_wp ) continue; else break;
		}
	      
		// Hash table miss. Store j value for iterative 2-hop search.
                if ( collision ) ths |= 1 << j;
	      }
	    }
	  
	    if ( stats_blk ){__syncthreads();}
	    stats_continue(tcomp1,tcomp2,dcomp1);
	  
	    // Perform iterative 2-hop searches.
	    //
	    while ( opt_loop_after_collision && ths ) {
	      // Use find-first-set function to find 1 bits in ths.
	      const int j = __ffs(ths) - 1;
	      ths ^= ((typeof ths)1) << j;
	    
	      nodeid_t lnbr_j = 0;
	      nodeid_t lweight_j = 0;
	    
	      // Retrieve j'th elements without scaring the compiler
	      // into using local memory for lnbr and lweight.
#define T4(b)   if ( bfb_sz > 8 && j&8 ){ T3(b+8) } else { T3(b) }
#define T3(b)   if ( j&4 ) { T2(b+4) } else { T2(b) }
#define T2(b)   if ( j&2 ) { T1(b+2) } else { T1(b) }
#define T1(b)   if ( j&1 ) { T0(b+1); } else { T0(b); }
#define T0(jj)  { lnbr_j = lnbr[jj]; lweight_j = lweight[jj]; }
	      if ( bfb_sz > 16 && j&16 ){ T4(16) } else { T4(0) }
#undef T4
#undef T3
#undef T2
#undef T1
#undef T0
	    
	      if ( stats_wps ) atomicAdd(&n_2hop, 1);
	      bool fpath = false;
	    
	      for(uint32_t k=0; k<block_dim; k++){
		if( k + s*block_dim >= fwd_bwd_nnbr) break;
	      
		const int fwd_bwd_sidx = k;
		const nodeid_t fwd_bwd_nbr =
		  SH_FORWARD_BACKWARD_NBR( fwd_bwd_sidx );
		const bool found_path = lnbr_j == fwd_bwd_nbr;
	      
		fpath |= found_path;
	      
		// for two hop search, the comparison should not
		// include equality
	      
		if ( found_path &&
		     lweight_j + SH_FORWARD_BACKWARD_WEIGHT( fwd_bwd_sidx )
		     < lshc_w ) {
                  need_wp = false;
		  ths = 0;
		  if ( stats_wps ) atomicAdd(&n_2hshc, 1);
		  if ( stats_wps ) atomicAdd(&n_fph2, 1);
		  break;
		}
	      }
	    
	      if ( stats_wps && fpath ) atomicAdd(&n_fpath, 1);
	    }
	  
	    if ( stats_blk ){__syncthreads();}

	    stats_continue(tcomp2,tbuf,dcomp2);
	  
	  }  // End of s loop.

          if ( opt_longcuts && path_wht_min < edge_direct_w )
            {
              num_longcuts++;
              if ( opt_longcuts_cull )
                {
                  const nodeid_t bwd_e_gidx =
                    SH_FORWARD_POINTER_B(fwd_sidx) + edge_direct_bwd_eidx;
                  const bool found_bwd = edge_direct_bwd_eidx >= 0;
                  if ( found_bwd )
                    {
                      const nodeid_t src_prev =
                        atomicExch
                        ( &graph_c.graph_b.neighbors[bwd_e_gidx],
                          nodeid_invalid );
                      assert( src_prev == nodeid_invalid
                              || src_prev == lnid );
                      if ( src_prev != nodeid_invalid )
                        num_longcuts_cut++;
                    }
                }
            }

          stats_stop(tbuf,dbuf);
	
	  // Update lshc array.
          if ( needed_wp && !need_wp ) lshc[ q >> shc_sft ] = lshc_grp_no_shc;
	
	  // all tpn threads must finish before loading the next set of 8 forward neighbors
	
	} // End of q loop
      
	__syncthreads();
      } // End of r loop
    
      stats_start(tfinish);

      // write back all confirmed shortcuts
      if ( current_pos < list_length && bwd_e_here ){

	const nodeid_t current = SH_CURRENT_ID( 0 );

	const int e_nnbr_f = SH_CURRENT_NNBR_F( 0 ) - lbid_f*block_dim > block_dim ?
	  block_dim : SH_CURRENT_NNBR_F( 0 ) - lbid_f*block_dim;
	for(uint32_t i=0; i<flag_len; i++){
	  for(uint32_t j=0; j<32; j++){
	    if( ((i<<5) + j) < e_nnbr_f ){
	      if((lshc[i] & (1<<j)) != 0){
		uint32_t mid_point;
		if(SH_FORWARD_MID( (i<<5) + j ) != NODEID_NULL ){
		  // if the second half of the shortcut is already a shortcut, use its midpoint
		  mid_point = SH_FORWARD_MID( (i<<5) + j );
		} else {
		  // else current is the closest midpoint to the edge's end (midpoint uses the inputIDs
		  mid_point = graph->node_ids[ current ];
		}

		shc_t ei;
		ei.src_id = lnid;
		ei.dst_id = SH_FORWARD_NBR( (i<<5) + j );
                assert( !nodeid_is_special(ei.src_id) );
                assert( !nodeid_is_special(ei.dst_id) );
		ei.wht = SH_BACKWARD_WEIGHT( threadIdx.x )
		  + SH_FORWARD_WEIGHT( (i<<5) + j );
		ei.mid_oid = mid_point;

		const int shc_dbin = ei.dst_id % shc_list->n_segs;
		uint32_t shc_dst_count_old =
                  atomicAdd(&(shc_list->len_dst[shc_dbin]), 1);

		assert(shc_dst_count_old < shc_list->max_len_per_seg);

                shc_list->shc_dst
                  [shc_list->max_len_per_seg * shc_dbin + shc_dst_count_old] = ei;

	      }
	    }
	  }
	}
      }

      __syncthreads();
      stats_continue(tfinish,tbuf,dfinish);

    } // end of out of range blocks condition

  }

  if(blockIdx.x==0 && threadIdx.x==0){
    d_DB_est[0] = deg_sum_DB_est_l / num_iters; // not worth using float, only a very rough estimate
  }

  if ( opt_longcuts )
    {
      atomicAdd( &graph_c.self_d->num_longcuts, num_longcuts );
      atomicAdd( &graph_c.self_d->num_longcuts_cut, num_longcuts_cut );
    }

  if ( !stats || threadIdx.x >= 32 ) return;

  if ( stats ) {
    qstats->qend = clock64();
    qstats->dbuf = dbuf;
    qstats->dcomp1 = dcomp1;
    qstats->dcomp2 = dcomp2;
    qstats->dfinish = dfinish;
    qstats->n_ij = n_ij;
    qstats->n_2hop = n_2hop;
    qstats->n_fpath = n_fpath;
    qstats->n_1hshc = n_1hshc;
    qstats->n_2hshc = n_2hshc;
    qstats->n_fph1 = n_fph1;
    qstats->n_fph2 = n_fph2;
  }

}


using namespace std;

map<int,LConfig>
configs_contract_get(GPU_Info& gpu_info)
{
  const bool tune = chopts.print_tuning_in_loops;
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;
  const int degree_sum_nblk = nmps * 32;

  map<int,vector<LConfig>> configs;

  auto occupancy_co =
    [&](int tpn, int block_lg, int bfb_sz, Func_K_Contract k_ptr)
    {
      LConfig config;
      config.inited = true;
      const bool blocked = block_lg < tpn;
      const double thd_per_tpn = ( 1 << block_lg ) / double( 1 << tpn );
      config.nd_per_blk_iter =
        blocked ? thd_per_tpn * thd_per_tpn : thd_per_tpn;
      config.tie_breaker = bfb_sz;
      config.shared_dim = 0;
      config.block_dim = 1 << block_lg;
      config.k_ptr = k_ptr;
      CE( cudaFuncGetAttributes(&config.cfa, k_ptr) );
      CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
          ( &config.blks_per_mp, k_ptr,
            config.block_dim, config.shared_dim) );
      /* For size of qstats_d. */
      assert(config.blks_per_mp<=degree_sum_nblk/nmps);
      config.grid_dim = config.blks_per_mp * nmps;
      config.nd_occ = config.grid_dim * config.nd_per_blk_iter;
      configs[tpn].push_back(config);
    };

# define OCC_CO_B(tpn,bfb) occupancy_co \
    (tpn,contract_nodes_tpn_to_block_lg(tpn),bfb,cu_CH_contract_nodes<tpn,bfb>);

# define OCC_CO1(tpn) OCC_CO_B(tpn,1);
# define OCC_CO2(tpn) OCC_CO_B(tpn,1); OCC_CO_B(tpn,2);
# define OCC_CO4(tpn) OCC_CO_B(tpn,1); OCC_CO_B(tpn,2); OCC_CO_B(tpn,4);

# define OCC_COBB(tpn,bfb) occupancy_co		    \
    (tpn,contract_nodes_tpn_to_block_size_blocked_lg, bfb,  \
     cu_CH_contract_nodes_blocked<tpn,contract_nodes_tpn_to_block_size_blocked_lg,bfb>);

# define OCC_COB4(tpn) OCC_COBB(tpn,1); OCC_COBB(tpn,2); OCC_COBB(tpn,4)

  OCC_CO1(3); OCC_CO2(4); OCC_CO4(5);
  OCC_CO4(6); OCC_CO4(7); OCC_CO4(8);

  OCC_COB4(9); OCC_COB4(10); OCC_COB4(11); OCC_COB4(12);

  // RRRR technically this can be defined infinitely, realistically it
  // should not be needed past 10 (degree of 1024 which corresponds to
  // a complete graph for current K) even if blocked version is used
  // starting from TPN==5, currently it is defined only till 4096

#undef OCC_CO_B
#undef OCC_CO1
#undef OCC_CO2
#undef OCC_CO4
#undef OCC_COBB
#undef OCC_COB4

  map<int,LConfig> configs_co;
  for ( auto& elt: configs )
    {
      const int tpn = elt.first;
      LConfig *c_best = nullptr;
      for ( auto& c: elt.second )
        if ( !c_best
             || c.blks_per_mp > c_best->blks_per_mp
             || c.blks_per_mp == c_best->blks_per_mp
             && c.tie_breaker > c_best->tie_breaker )
          c_best = &c;
      if ( tune )
        for ( auto& c: elt.second )
          printf
            ("Contr tpn %2d  bl sz %4d  bfb %d  bl/mp %2d  loc %3zu  %s\n",
             tpn, c.block_dim, c.tie_breaker,
             c.blks_per_mp, c.cfa.localSizeBytes,
             &c == c_best ? "*" : "");

      configs_co[tpn] = *c_best;
    }

  return configs_co;
}

map<int,vector<LConfig>>
configs_contract_get_all(GPU_Info& gpu_info)
{
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;
  const int degree_sum_nblk = nmps * 32;

  map<int,vector<LConfig>> configs;

  auto __attribute__((unused)) occupancy_co =
    [&](int tpn, int block_lg, int bfb_sz, Func_K_Contract k_ptr)
    {
      LConfig config;
      config.inited = true;
      config.nd_per_blk_iter = (1 << block_lg ) / double( 1 << tpn );
      config.tie_breaker = bfb_sz;
      config.shared_dim = 0;
      config.block_dim = 1 << block_lg;
      config.k_ptr = k_ptr;
      CE( cudaFuncGetAttributes(&config.cfa, k_ptr) );
      CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
          ( &config.blks_per_mp, k_ptr,
            config.block_dim, config.shared_dim) );
      /* For size of qstats_d. */
      assert(config.blks_per_mp<=degree_sum_nblk/nmps);
      config.grid_dim = config.blks_per_mp * nmps;
      configs[tpn].push_back(config);
    };

# define OCC_CO_B(tpn,bfb) occupancy_co \
    (tpn,contract_nodes_tpn_to_block_lg(tpn),bfb,cu_CH_contract_nodes<tpn,bfb>);

# define OCC_CO1(tpn) OCC_CO_B(tpn,1);
# define OCC_CO2(tpn) OCC_CO_B(tpn,1); OCC_CO_B(tpn,2);
# define OCC_CO4(tpn) OCC_CO_B(tpn,1); OCC_CO_B(tpn,2); OCC_CO_B(tpn,4);

# define OCC_COBB(tpn,blsz,bfb) occupancy_co		    \
    (tpn,blsz, bfb,  \
     cu_CH_contract_nodes_blocked<tpn,blsz,bfb>);

# define OCC_COB4(tpn, blsz) OCC_COBB(tpn,blsz,1); OCC_COBB(tpn,blsz,2); OCC_COBB(tpn,blsz,4)

# if config_dynamic

  OCC_CO1(3); OCC_CO2(4); OCC_CO4(5);
  OCC_CO4(6); OCC_CO4(7); OCC_CO4(8);

  OCC_COB4(7,6); OCC_COB4(8,6); OCC_COB4(9,6); OCC_COB4(10,6); OCC_COB4(11,6); OCC_COB4(12,6);
  OCC_COB4(8,7); OCC_COB4(9,7); OCC_COB4(10,7); OCC_COB4(11,7); OCC_COB4(12,7);
  OCC_COB4(9,8); OCC_COB4(10,8); OCC_COB4(11,8); OCC_COB4(12,8);

# endif

  // RRRR technically this can be defined infinitely, realistically it
  // should not be needed past 10 (degree of 1024 which corresponds to
  // a complete graph for current K) even if blocked version is used
  // starting from TPN==5, currently it is defined only till 4096

#undef OCC_CO_B
#undef OCC_CO1
#undef OCC_CO2
#undef OCC_CO4
#undef OCC_COBB
#undef OCC_COB4

  // bfb_size has too high an overhead, not worth using for min-maxing utilization
  map<int,vector<LConfig>> configs_co;
  for ( auto& elt: configs )
    {
      const int tpn = elt.first;
      LConfig *c_best = nullptr;
      for ( auto& c: elt.second )
        if ( !c_best
             || c.blks_per_mp > c_best->blks_per_mp
             || c.blks_per_mp == c_best->blks_per_mp
             && c.tie_breaker > c_best->tie_breaker )
          c_best = &c;

      configs_co[tpn].push_back(*c_best);
    }

  return configs_co;
}
