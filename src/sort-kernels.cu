/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef SORT_KERNELS
#define SORT_KERNELS

#include <string.h>
#include <assert.h>
#include <cuda-gpuinfo.h>
#include <cfloat>

using namespace std;

#include "sort.cuh"

enum Sort_Order { SO_Descending = 0, SO_Ascending = 1 };

__device__ inline uint32_t
sort_key_convert(uint32_t key_rawer, float order)
{
  const bool ascending = order == float(SO_Ascending);
  const uint32_t sign_mask = uint32_t(1) << 31;
  const uint32_t key_raw = ascending ? key_rawer : key_rawer ^ sign_mask;
  const bool neg = key_raw & sign_mask;
  return neg ? uint32_t(-1) ^ key_raw : key_raw | sign_mask;
}
__device__ inline uint32_t
sort_key_convert(uint32_t key_raw, uint32_t order)
{
  const bool ascending = order == uint32_t(SO_Ascending);
  return ascending ? key_raw : key_raw ^ uint32_t(-1);
}
__device__ inline uint32_t
sort_key_convert(uint32_t key_raw, int32_t order)
{
  assert( false ); // Now that we need it, lets implement it!
  return key_raw;
}

__constant__ Radix_Sort_GPU_Constants dapp;


#ifdef DEBUG_SORT
const int __attribute__((unused)) debug_sort = true;
#else
const int __attribute__((unused)) debug_sort = false;
#endif

template<int BLOCK_LG, int RADIX_LG>
struct Pass_1_Stuff
{
  uint32_t keys[elt_per_thread+(elt_per_thread<<BLOCK_LG)];
  int prefix[4 + (1<<BLOCK_LG)];
  int runend[1<<RADIX_LG];
  int thisto[1<<RADIX_LG];
  int ghisto[1<<RADIX_LG];
};

template <int BLOCK_LG, int RADIX_LG>
__device__ void
sort_block_1_bit_split
(int bit_low, int bit_count,
 Pass_1_Stuff<BLOCK_LG,RADIX_LG>& p1s);

template <int BLOCK_LG, int RADIX_LG>
__device__ void radix_sort_pass_1_tile
(int digit_pos, int tile_idx, bool first_iter,
 Pass_1_Stuff<BLOCK_LG,RADIX_LG>& p1s);

template <typename Key_Type, Sort_Order order, int BLOCK_LG, int RADIX_LG>
__global__ void  __launch_bounds__(1<<BLOCK_LG)
radix_sort_pass_1(int digit_pos, bool first_iter, bool last_iter)
{
  __shared__ Pass_1_Stuff<BLOCK_LG,RADIX_LG> p1s;
  int block_size = 1 << BLOCK_LG;
  int radix = 1 << RADIX_LG;
  int elt_per_tile = block_size * elt_per_thread;
  int tiles_per_array = div_ceil(dapp.array_size,elt_per_tile);
  int tiles_per_block = div_ceil(tiles_per_array,gridDim.x);
  int tile_start = tiles_per_block * blockIdx.x;
  int tile_stop = min( tiles_per_array, tile_start + tiles_per_block);

  const int dig_per_thd = BLOCK_LG >= RADIX_LG ? 1 : radix/block_size;
  const bool rad_participant = block_size <= radix || threadIdx.x < radix;
# define DIG(i) (threadIdx.x + (i) * block_size)

  if ( rad_participant )
    for ( int i = 0;  i < dig_per_thd;  i++ )
      p1s.ghisto[ DIG(i) ] = 0;

  for ( int tile_idx = tile_start; tile_idx < tile_stop; tile_idx++ )
    radix_sort_pass_1_tile<Key_Type,order,BLOCK_LG,RADIX_LG>
      (digit_pos,tile_idx,first_iter,p1s);

  if ( !rad_participant ) return;

  for ( int i = 0;  i < dig_per_thd;  i++ )
    {
      const int histo_idx = blockIdx.x * radix + DIG(i);
      dapp.sort_histo[ histo_idx ] = p1s.ghisto[ DIG(i) ];
    }
}

template <typename Key_Type, Sort_Order order, int BLOCK_LG, int RADIX_LG>
__device__ void
radix_sort_pass_1_tile
(int digit_pos, int tile_idx,
 bool first_iter, Pass_1_Stuff<BLOCK_LG,RADIX_LG>& p1s)
{
  int start_bit = digit_pos * RADIX_LG;
  int block_size = 1 << BLOCK_LG;
  int radix = 1 << RADIX_LG;
  const int digit_mask = radix - 1;
  int elt_per_tile = elt_per_thread * block_size;
  int idx_block_start = elt_per_tile * tile_idx;
  const int dig_per_thd = BLOCK_LG >= RADIX_LG ? 1 : radix/block_size;
  const bool rad_participant = block_size <= radix || threadIdx.x < radix;

  int idx_block_stop = idx_block_start + elt_per_tile;
  int idx_start = idx_block_start + threadIdx.x;

  Sort_Elt* const sort_src =
    first_iter ? dapp.sort_in :
    digit_pos & 1 ? dapp.sort_out : dapp.sort_out_b;

  // Load Element Keys
  //
  for ( int sidx = threadIdx.x, i = 0;
        i < elt_per_thread; i++, sidx += block_size )
    {
      Sort_Elt key_raw = sort_src[ idx_block_start + sidx ];
      Sort_Elt key = sort_key_convert(key_raw,Key_Type(order));
      p1s.keys[sidx] = ( sidx << 16 ) + ( ( key >> start_bit ) & digit_mask );
    }

  // Sort based upon current digit position
  //
  sort_block_1_bit_split<BLOCK_LG,RADIX_LG>(start_bit,RADIX_LG,p1s);

  // Write sorted elements to global memory and prepare for histogram.
  //
  for ( int idx = idx_start, sidx = threadIdx.x;
        idx < idx_block_stop; sidx += block_size, idx += block_size )
    {
      // Write element.
      //
      dapp.perm[idx] = p1s.keys[sidx] >> 16;

      // Extract digit and write to shared memory.
      //
      int digit = p1s.keys[sidx] & digit_mask;
      p1s.keys[sidx] = digit;
    }

  if ( threadIdx.x == 0 ) p1s.keys[elt_per_tile] = radix;

  // Initialize histogram for this tile to zero.
  //
  if ( rad_participant )
    for ( int i = 0;  i < dig_per_thd;  i++ )
      p1s.thisto[ DIG(i) ] = 0;

  __syncthreads();

  // Find highest index for each digit.
  //
  for ( int i = 0; i < elt_per_thread; i++ )
    {
      int sidx = threadIdx.x + i * block_size;
      int digit = p1s.keys[sidx];      // Our digit.
      int digit_1 = p1s.keys[sidx+1];  // Next guy's digit.

      // If "next guy's" digit is different then sidx is highest index
      // for digit.
      //
      if ( digit != digit_1 )
        p1s.runend[ digit ] = sidx;
    }

  __syncthreads();

  // Finish histogram by looking for smallest index for each digit.
  //
  for ( int i = 0; i < elt_per_thread; i++ )
    {
      int sidx = threadIdx.x + i * block_size;
      int digit = p1s.keys[sidx];                      // Our digit.
      int digit_0 = sidx > 0 ? int(p1s.keys[sidx-1]) : -1; // Previous guy's digit.
      if ( digit != digit_0 )
        {
          int run_end_sidx = p1s.runend[ digit ];
          int count = run_end_sidx - sidx + 1;
          p1s.ghisto[ digit ] += count;     // Histogram for block.
          p1s.thisto[ digit ] = count;      // Histogram for tile.
        }
    }

  __syncthreads();

  if ( !rad_participant ) return;

  for ( int i = 0;  i < dig_per_thd;  i++ )
    {
      const int thisto_idx = tile_idx * radix + DIG(i);
      dapp.sort_tile_histo[ thisto_idx ] = p1s.thisto[ DIG(i) ];
    }
}

template <int block_lg, int RADIX_LG>
__device__ void
sort_block_1_bit_split
(int bit_low, int bit_count, Pass_1_Stuff<block_lg,RADIX_LG>& p1s)
{
  const int block_size = 1 << block_lg;
  const int elt_per_tile = elt_per_thread * block_size;

  if ( threadIdx.x == 0 ) p1s.prefix[0] = 0;

  // Sort Elements From LSB to MSB.
  //
  for ( int bit_pos=0; bit_pos<bit_count; bit_pos++ )
    {
      const uint bit_mask = 1 << bit_pos;

      // Storage for thread's keys.
      //
      uint32_t keys[elt_per_thread];

      __syncthreads();

      // Initialize data for prefix sum of bit bit_pos, and make copy of key.
      //
      int my_ones_write = 0;

      const int wp_lg = 5;
      const int wp_sz = 1 << wp_lg;
      const int wp_mk = wp_sz - 1;
      const int lane = threadIdx.x & wp_mk;
      const int wp_idx = threadIdx.x >> wp_lg;
      const uint32_t msk = 0xffffffff;
      int my_pf_wp = 0;

      for ( int i = 0; i < elt_per_thread; i++ )
        {
          const int sidx = threadIdx.x * elt_per_thread + i;

          // Make a copy of key.
          //
          const Sort_Elt key = p1s.keys[ sidx ];
          keys[i] = key;
          const bool one = key & bit_mask;
          if ( one ) my_ones_write++;
        }

      my_pf_wp = my_ones_write;

      // Compute intra-warp prefix sum. (Sum within warp.)
      //
      for ( int tree_level = 0; tree_level < wp_lg; tree_level++ )
        {
          int dist = 1 << tree_level;
          uint neighbor_prefix = __shfl_up_sync(msk,my_pf_wp,dist);
          if ( dist <= lane ) my_pf_wp += neighbor_prefix;
        }

      // Write total number of 1's in warp to shared memory. This
      // will be used to compute prefix sum between warps.
      //
      if ( lane == wp_mk ) p1s.prefix[wp_idx+1] = my_pf_wp;

      __syncthreads();

      // Compute inter-warp prefix sum.  Only warp 0 does this.
      //
      if ( wp_idx == 0 )
        {
          uint wp_prefix = p1s.prefix[threadIdx.x+1];
          for ( int tree_level = 0; tree_level < block_lg - wp_lg;
                tree_level++ )
            {
              int dist = 1 << tree_level;
              uint neighbor_prefix = __shfl_up_sync(msk,wp_prefix,dist);
              if ( dist <= threadIdx.x ) wp_prefix += neighbor_prefix;
            }
          p1s.prefix[threadIdx.x+1] = wp_prefix;
        }
      __syncthreads();
      const uint wp_prefix = p1s.prefix[wp_idx];
      __syncthreads();

      // Combine inter-warp prefix (wp_prefix) with intra-warp prefix
      // (my_pf_wp) to get prefix sum within block.
      //
      p1s.prefix[threadIdx.x+1] = wp_prefix + my_pf_wp;

      // At this point p1s.prefix contains exclusive prefix of each group.

      __syncthreads();

      const int all_threads_num_ones = p1s.prefix[ block_size ];
      const int idx_one_tid_0 = elt_per_tile - all_threads_num_ones;
      const int smaller_tids_num_ones = p1s.prefix[ threadIdx.x ];

      int idx_zero_me = threadIdx.x * elt_per_thread - smaller_tids_num_ones;
      int idx_one_me = idx_one_tid_0 + smaller_tids_num_ones;

      for ( int i = 0;  i < elt_per_thread;  i++ )
        {
          const int key = keys[i];
          const int new_idx = key & bit_mask ? idx_one_me++ : idx_zero_me++;
          p1s.keys[ new_idx ] = keys[i];
        }

    }
  __syncthreads();
}


template <typename Key_Type, Sort_Order order, int BLOCK_LG, int RADIX_LG>
__global__ void   __launch_bounds__(1<<BLOCK_LG)
radix_sort_pass_2(int digit_pos, bool first_iter, bool last_iter)
{
  const int block_size = 1 << BLOCK_LG;
  const int elt_per_tile = elt_per_thread * block_size;
  int tiles_per_array = div_ceil(dapp.array_size,elt_per_tile);
  int tiles_per_block = div_ceil(tiles_per_array,gridDim.x);

  int tile_start = tiles_per_block * blockIdx.x;
  int tile_stop = min( tiles_per_array, tile_start + tiles_per_block );

  const int sort_radix = 1 << RADIX_LG;
  const int digit_mask = sort_radix - 1;
  const int start_bit = digit_pos * RADIX_LG;

  volatile __shared__ int g_prefix[ sort_radix + 1 ];

  if ( threadIdx.x == 0 ) g_prefix[ 0 ] = 0;

  const int warp_sz = 32;
  const int dig_per_thd = BLOCK_LG >= RADIX_LG ? 1 : sort_radix/block_size;
  const bool rad_participant =
    block_size <= sort_radix || threadIdx.x < sort_radix;
  const bool rad_attendee = rad_participant || sort_radix > warp_sz;
  volatile __shared__ int tile_offsets[ 2 * sort_radix ];

  if ( rad_attendee )
    {
      int overhead_bin_sum[dig_per_thd];
      int global_bin_sum[dig_per_thd];
      int global_bin_prefix[dig_per_thd];

      for ( auto& e: global_bin_sum ) e = 0;

      if ( rad_participant )
        {
          for ( int gh_idx = 0; gh_idx < gridDim.x; gh_idx++ )
            {
              for ( int i = 0;  i < dig_per_thd;  i++ )
                {
                  const int d = threadIdx.x + i * block_size;
                  const int gh_bin_idx = gh_idx * sort_radix + d;
                  if ( gh_idx == blockIdx.x )
                    overhead_bin_sum[i] = global_bin_sum[i];
                  global_bin_sum[i] += dapp.sort_histo[gh_bin_idx];
                }
            }

          //
          // Compute Global Prefix Sum
          //

          for ( int i = 0;  i < dig_per_thd;  i++ )
            {
              const int d = threadIdx.x + i * block_size;
              g_prefix[ 1 + d ] = global_bin_sum[i];
            }
          //
          // At this point g_prefix holds a global histogram.

          for ( int i=0; i<dig_per_thd; i++ )
            global_bin_prefix[i] = global_bin_sum[i];
        }

      for ( int lev=0; lev<RADIX_LG; lev++ )
        {
          const int dist = 1 << lev;
          int sum_0[dig_per_thd];

          if ( sort_radix > warp_sz ) __syncthreads();
          if ( rad_participant )
            for ( int i = 0;  i < dig_per_thd;  i++ )
              {
                const int d = threadIdx.x + i * block_size;
                sum_0[i] = dist <= d ? g_prefix[ 1 + d - dist ] : 0;
              }
          if ( sort_radix > warp_sz ) __syncthreads();
          if ( rad_participant )
            for ( int i = 0;  i < dig_per_thd;  i++ )
              {
                const int d = threadIdx.x + i * block_size;
                g_prefix[ 1 + d ] = global_bin_prefix[i] += sum_0[i];
              }
        }


      // Now, g_prefix holds a global prefix sum.
      //
      // E.g., g_prefix[3] is the location where the first key having
      // digit value 3 in the entire array is to be written. That key
      // is probably being handled by block 0.

      if ( sort_radix > warp_sz ) __syncthreads();
      if ( rad_participant )
        for ( int i = 0;  i < dig_per_thd;  i++ )
          {
            const int d = threadIdx.x + i * block_size;
            g_prefix[ d ] += overhead_bin_sum[i];
          }

      //
      // Now, g_prefix holds a prefix sum for this block.
      //
      // E.g., g_prefix[3] is the location where the first key having
      // digit value 3 in this block is to be written.

    }

  __syncthreads();

  if ( rad_participant )
    for ( int i=0;  i<dig_per_thd;  i++ )
      {
        const int d = threadIdx.x + i * block_size;
        tile_offsets[d] = 0;
      }

  __syncthreads();

  for ( int tile_idx = tile_start; tile_idx < tile_stop; tile_idx++ )
    {
      int counts[dig_per_thd];

      if ( rad_attendee )
        {
          int offsets[dig_per_thd];
          if ( rad_participant )
            for ( int i = 0;  i < dig_per_thd;  i++ )
              {
                const int d = threadIdx.x + i * block_size;
                const int bo_idx = tile_idx * sort_radix + d;
                const int to_idx = sort_radix + d;
                offsets[i] = counts[i] = tile_offsets[ to_idx ] =
                  dapp.sort_tile_histo[ bo_idx ];
              }

          for ( int lev=0; lev<RADIX_LG; lev++ )
            {
              const int dist = 1 << lev;
              if ( sort_radix > warp_sz ) __syncthreads();
              if ( rad_participant )
                for ( int i = 0;  i < dig_per_thd;  i++ )
                  {
                    const int d = threadIdx.x + i * block_size;
                    int to_idx = sort_radix + d;
                    offsets[i] += tile_offsets[ to_idx - dist ];
                  }
              if ( sort_radix > warp_sz ) __syncthreads();
              if ( rad_participant )
                for ( int i = 0;  i < dig_per_thd;  i++ )
                  {
                    const int d = threadIdx.x + i * block_size;
                    int to_idx = sort_radix + d;
                    tile_offsets[ to_idx ] = offsets[i];
                  }
            }
          if ( rad_participant )
            for ( int i = 0;  i < dig_per_thd;  i++ )
              {
                const int d = threadIdx.x + i * block_size;
                assert( counts[i] <= elt_per_tile );
                assert( tile_offsets[sort_radix + d - 1] <= elt_per_tile );
              }
        }

      __syncthreads();

      int idx_tile_start = tile_idx * elt_per_tile;

      __shared__ union { Sort_Elt k; Sort_Data d; } elts[elt_per_tile];

      Sort_Data* const data_rd =
        first_iter ? dapp.data_in :
        digit_pos & 1 ? dapp.data_out   : dapp.data_out_b;
      Sort_Elt* const key_rd =
        first_iter ? dapp.sort_in :
        digit_pos & 1 ? dapp.sort_out   : dapp.sort_out_b;
      Sort_Data* const data_wr =
        digit_pos & 1 ? dapp.data_out_b : dapp.data_out;
      Sort_Elt* const key_wr =
        digit_pos & 1 ? dapp.sort_out_b : dapp.sort_out;

      for ( int i=0; i<elt_per_thread; i++ )
        {
          int tile_elt_rank = threadIdx.x + i * block_size;
          elts[tile_elt_rank].k = key_rd[idx_tile_start + tile_elt_rank ];
        }

      __syncthreads();

      int digit_indices[elt_per_thread];
      int tperms[elt_per_thread];

      for ( int i=0; i<elt_per_thread; i++ )
        {
          int tile_elt_rank = threadIdx.x + i * block_size;
          int idx = idx_tile_start + tile_elt_rank;
          uint16_t p = dapp.perm[idx];
          tperms[i] = p;
          Sort_Elt key_raw = elts[ p ].k;
          Sort_Elt key = sort_key_convert(key_raw,Key_Type(order));
          uint digit = ( key >> start_bit ) & digit_mask;
          int tile_digit_rank = tile_offsets[ sort_radix + digit - 1 ];
          int key_digit_rank = tile_elt_rank - tile_digit_rank;
          assert( key_digit_rank >= 0 );
          int idx_digit_index = g_prefix[ digit ] + key_digit_rank;
          digit_indices[i] = idx_digit_index;

          if ( debug_sort && last_iter )
            key_wr[idx] = ( idx_digit_index << 12 ) + tile_digit_rank;
          else
            key_wr[idx_digit_index] = key_raw;
        }

      __syncthreads();

      for ( int i=0; i<elt_per_thread; i++ )
        {
          int tile_elt_rank = threadIdx.x + i * block_size;
          elts[tile_elt_rank].d = data_rd[idx_tile_start + tile_elt_rank ];
        }

      __syncthreads();

      for ( int i=0; i<elt_per_thread; i++ )
        {
          data_wr[digit_indices[i]] = elts[tperms[i]].d;
        }

      __syncthreads();

      if ( rad_participant )
        for ( int i = 0;  i < dig_per_thd;  i++ )
          {
            const int d = threadIdx.x + i * block_size;
            g_prefix[ d ] += counts[i];
          }

      //
      // Now, g_prefix holds a prefix sum for the next tile.
      //
      // E.g., g_prefix[3] is the location where the first key having
      // digit value 3 in the next tile is to be written.
    }
}

template <typename Key_Type, Sort_Order order>
__global__ void
sort_check(const Key_Type* const sort_out, Sort_Elt mask)
{
  const int nthreads = blockDim.x * gridDim.x;
  int tid = blockDim.x * blockIdx.x + threadIdx.x;

  const int a = order == SO_Descending;
  const int b = !a;

  auto get_masked = [&](int idx)
  {
    union { Key_Type k; Sort_Elt i; } u = { sort_out[idx] };
    u.i &= mask;
    return u.k;
  };

  for ( int idx = tid; idx < dapp.array_size - 1; idx += nthreads )
    { assert( get_masked(idx+a) <= get_masked(idx+b) ); }
}

__device__ inline float
sort_val_limit(float order)
{
  return order == float(SO_Descending) ? -FLT_MAX : FLT_MAX;
}
__device__ inline uint32_t
sort_val_limit(uint32_t order)
{
  return order == uint32_t(SO_Descending) ? 0 : uint32_t(-1);
}

__device__ inline uint32_t
sort_key_map(uint32_t order, uint32_t key)
{
  const uint32_t pivot = 0x92abf28e;  // openssl rand -hex 4
  assert( key != pivot ); // Hopefully I'll fix it. dmk 18 Aug 2018.
  const bool ascend = order == uint32_t(SO_Ascending);
  return ascend && key > pivot ? key - 1 :
    !ascend && key < pivot ? key + 1 : key;
}

__device__ inline uint32_t
sort_key_unmap(uint32_t order, uint32_t key)
{
  const uint32_t pivot = 0x92abf28e;  // openssl rand -hex 4
  assert( key != pivot );
  const bool ascend = order == uint32_t(SO_Ascending);
  return ascend && key >= pivot ? key + 1 :
    !ascend && key <= pivot ? key - 1 : key;
}

__device__ inline float
sort_key_map(float order, float key)
{
  assert( key != sort_val_limit(order) );  // To be fixed one day.
  return key;
}

__device__ inline float
sort_key_unmap(float order, float_t key)
{
  return key;
}

template <typename Key_Type, Sort_Order order,
          int BLOCK_LG, int elt_per_thread_lg>
__global__ void
sort_block_batcher
(Key_Type *keys_out, Sort_Data *data_out,
 Key_Type *keys_in, Sort_Data *data_in, int n_elts)
{
  const int elt_per_thread = 1 << elt_per_thread_lg;
  const int block_size = 1 << BLOCK_LG;
  const int elt_per_thread_half = elt_per_thread >> 1;
  const int elt_per_block = elt_per_thread * block_size;
  const int chunk_lg = elt_per_thread_lg + BLOCK_LG;
  const int idx_block_start = 0;
  __shared__ Key_Type keys[elt_per_block];
  __shared__ uint16_t perm[elt_per_block];
  uint32_t* const data = (uint32_t*) &keys[0];

  if ( BLOCK_LG < 6 && n_elts == 1 )
    {
      // Special case for performance tuning. AOTW the 1-elt case
      // takes more than half the time of the 38-elt case, so no point
      // in special handling for one-warp cases.
      if ( threadIdx.x ) return;
      keys_out[0] = keys_in[0];
      data_out[0] = data_in[0];
      return;
    }

  for ( int i = 0; i < elt_per_thread; i++ )
    {
      const int sidx = threadIdx.x + i * block_size;
      Key_Type key = sidx < n_elts
        ? sort_key_map(Key_Type(order),keys_in[ idx_block_start + sidx ])
        : sort_val_limit(Key_Type(order));
      keys[sidx] = key;
      perm[sidx] = sidx;
    }

  const bool participant = elt_per_thread_lg || threadIdx.x < block_size/2;
  const bool descending = order == SO_Descending;
  auto swap = [](uint16_t& a, uint16_t& b) { uint16_t t = a; a = b; b = t;};

  for ( int m_lg=0; m_lg<BLOCK_LG; m_lg++ )
    {
      int sort_dir_vector = 1 << m_lg;
      bool sort_dir = bool( threadIdx.x & sort_dir_vector ) ^ descending;
      for ( int bit = m_lg; bit >= 0; bit-- )
        {
          int bit_vector = 1 << bit;
          uint shift_mask = ~( bit_vector - 1 );
          int idx_0_t = threadIdx.x + ( threadIdx.x & shift_mask );
          int idx_1_t = idx_0_t + bit_vector;
          if ( BLOCK_LG > 5 ) __syncthreads(); else __syncwarp();
          if ( participant )
          for ( int i=0; i<elt_per_thread; i += 2 )
            {
              int idx_0 = idx_0_t + ( i << BLOCK_LG );
              int idx_1 = idx_1_t + ( i << BLOCK_LG );
              Key_Type key_0 = keys[idx_0];
              Key_Type key_1 = keys[idx_1];
              if ( (key_0 < key_1 ) == sort_dir )
                {
                  keys[idx_0] = key_1;  keys[idx_1] = key_0;
                  swap(perm[idx_0],perm[idx_1]);
                }
            }
        }
    }

  if ( BLOCK_LG > 5 ) __syncthreads();

  for ( int m_lg=BLOCK_LG; m_lg<chunk_lg; m_lg++ )
    {
      int sort_dir_vector = 1 << ( m_lg + 1 );
      for ( int bit = m_lg; bit >= 0; bit-- )
        {
          int bit_vector = 1 << bit;
          uint shift_mask = ~( bit_vector - 1 );
          if ( BLOCK_LG > 5 ) __syncthreads(); else __syncwarp();
          for ( int i=0; i<elt_per_thread_half; i++ )
            {
              int idx_ref = threadIdx.x + i * block_size;
              int idx_0 = idx_ref + ( idx_ref & shift_mask );
              int idx_1 = idx_0 + bit_vector;
              bool sort_dir = bool( idx_0 & sort_dir_vector ) ^ descending;
              Key_Type key_0 = keys[idx_0];
              Key_Type key_1 = keys[idx_1];
              if ( (key_0 < key_1 ) == sort_dir )
                {
                  keys[idx_0] = key_1;  keys[idx_1] = key_0;
                  swap(perm[idx_0],perm[idx_1]);
                }
            }
        }
    }

  if ( BLOCK_LG > 5 ) __syncthreads();

  for ( int sidx = threadIdx.x, i = 0;
        i < elt_per_thread; i++, sidx += block_size )
    if ( sidx < n_elts )
      keys_out[ idx_block_start + sidx ] =
        sort_key_unmap(Key_Type(order),keys[sidx]);
  if ( BLOCK_LG > 5 ) __syncthreads();
  for ( int sidx = threadIdx.x, i = 0;
        i < elt_per_thread; i++, sidx += block_size )
    if ( sidx < n_elts )
      data[sidx] = data_in[ idx_block_start + sidx ];
  if ( BLOCK_LG > 5 ) __syncthreads();
  for ( int sidx = threadIdx.x, i = 0;
        i < elt_per_thread; i++, sidx += block_size )
    if ( sidx < n_elts )
      data_out[ idx_block_start + sidx ] = data[perm[sidx]];
}


template<typename Key_Type = uint32_t, Sort_Order order = SO_Ascending,
         int block_lg = 10, int radix_lg = 8>
class Radix_Sort_Kernels
{
public:
  GPU_Info& gi;
  Radix_Sort_GPU_Constants dapp, rsc_shadow;
  int allocated_array_size;
  int grid_size;
  Sort_Elt *sort_out_b;
  Sort_Data *data_out_b;

  template<typename T1, typename T2>
  T1 rnd_up(T1 n, T2 m){ return m*((n+m-1)/m); }
  int fl1(uint32_t n) {return 8 * sizeof(n) - __builtin_clz(n);}

  Radix_Sort_Kernels(GPU_Info& gip, int key_bits):gi(gip)
  {
    assert( Key_Type(SO_Ascending) != Key_Type(SO_Descending) );
    allocated_array_size = 0;
    const int block_size = 1 << block_lg;
    const int num_mp = gi.cuda_prop.multiProcessorCount;
    const int wp_lg = 5;
    const int wp_per_mp_limit = 64;

    memset(&dapp,0,sizeof(dapp));
    memset(&rsc_shadow,0,sizeof(rsc_shadow));
    sort_out_b = NULL;
    data_out_b = NULL;

    Kernel_Info ki1 =
      gi.GET_INFO((radix_sort_pass_1<Key_Type,order,block_lg,radix_lg>));
    Kernel_Info ki2 =
      gi.GET_INFO((radix_sort_pass_2<Key_Type,order,block_lg,radix_lg>));

    // The maximum number of active blocks per MP for pass 1 and pass 2
    // kernels when launched with a block size of thd_per_block.
    //
    const int max_bl_per_mp_1 = ki1.get_max_active_blocks_per_mp(block_size);
    const int max_bl_per_mp_2 = ki2.get_max_active_blocks_per_mp(block_size);

    const int wp_per_bl_lg = block_lg - wp_lg;

    const int max_wp_per_mp_1 = max_bl_per_mp_1 << wp_per_bl_lg;
    const int max_wp_per_mp_2 = max_bl_per_mp_2 << wp_per_bl_lg;

    const int use_wp_per_mp_1 = min(max_wp_per_mp_1,wp_per_mp_limit);
    const int use_wp_per_mp_2 = min(max_wp_per_mp_2,wp_per_mp_limit);
    const int use_bl_per_mp_1 = max( 1, use_wp_per_mp_1 >> wp_per_bl_lg );
    const int use_bl_per_mp_2 = max( 1, use_wp_per_mp_2 >> wp_per_bl_lg );
    const int grid_size_1 = num_mp * use_bl_per_mp_1;
    const int grid_size_2 = num_mp * use_bl_per_mp_2;
    const int active_wp_1 =
      min(use_bl_per_mp_1,max_bl_per_mp_1) << wp_per_bl_lg;
    const int active_wp_2 =
      min(use_bl_per_mp_2,max_bl_per_mp_2) << wp_per_bl_lg;

    grid_size = max(grid_size_1,grid_size_2);

    const int sort_radix = 1 << radix_lg;

    dapp.radix_lg = radix_lg;
    dapp.radix = sort_radix;

    dapp.elt_per_tile = block_size * elt_per_thread;
    const int ndigits = div_ceil( key_bits, radix_lg );
    dapp.ndigits = ndigits;
    const bool is_fp = is_floating_point<Key_Type>::value;
    const int key_digits = div_ceil( 8 * sizeof(Key_Type), radix_lg );
    dapp.digit_first = is_fp ? key_digits - ndigits : 0;
    dapp.bhisto_array_size_elts = sort_radix * grid_size;
  }

  void n_elts_set(uint32_t array_size)
  {
    dapp.array_size = array_size;
    const int num_tiles = dapp.num_tiles =
      div_ceil( array_size, dapp.elt_per_tile );
    const int tiles_per_block = div_ceil(num_tiles,grid_size);
    dapp.thisto_array_size_elts = num_tiles << radix_lg;
  }

  ~Radix_Sort_Kernels()
    {
      free_internal();
    }

  void prepare_for_n_elts(int array_size)
  {
    dapp.array_size = array_size;
    const int num_tiles = dapp.num_tiles =
      div_ceil( array_size, dapp.elt_per_tile );
    const int tiles_per_block = div_ceil(num_tiles,grid_size);
    const int sort_radix = 1 << radix_lg;
    dapp.thisto_array_size_elts = sort_radix * num_tiles;
    alloc_internal();
  }
  void alloc_internal()
  {
    if ( allocated_array_size >= dapp.array_size ) return;
    allocated_array_size = rnd_up(dapp.array_size,dapp.elt_per_tile);
#define ALLOC_INTERNAL(ptr,n_elts)                                            \
    if ( ptr ) CE( cudaFree( ptr ) );                                         \
    CE( cudaMalloc( &ptr, n_elts * sizeof(ptr[0]) ) );

    ALLOC_INTERNAL( dapp.perm, allocated_array_size );
    ALLOC_INTERNAL( sort_out_b, allocated_array_size );
    ALLOC_INTERNAL( data_out_b, allocated_array_size );
    ALLOC_INTERNAL( dapp.sort_tile_histo, dapp.thisto_array_size_elts );
    ALLOC_INTERNAL( dapp.sort_histo, dapp.bhisto_array_size_elts );
#undef ALLOC_INTERNAL
  }

  void free_internal()
  {
    if ( !allocated_array_size ) return;
    allocated_array_size = 0;

#define FREE_INTERNAL(ptr) \
    if ( ptr ) { CE( cudaFree( ptr ) ); ptr = NULL; }
    FREE_INTERNAL( dapp.perm );
    FREE_INTERNAL( sort_out_b );
    FREE_INTERNAL( data_out_b );
    FREE_INTERNAL( dapp.sort_tile_histo );
    FREE_INTERNAL( dapp.sort_histo );
#undef FREE_INTERNAL
  }

  void sort
  (Key_Type *keys_out, Sort_Data *data_out,
   Key_Type *keys_in, Sort_Data *data_in, uint32_t n_elts )
  {
    const bool check_sort = false;

    // Use a single-block Batcher sort if the input is small enough.
    //
    const int clg_elts = fl1(n_elts); // Ceiling of log base 2 of n_elts.
    if ( clg_elts < 14 )
      {
        switch ( clg_elts ){

#define SBB(blg,elt_per_thd)                                                  \
          case blg + elt_per_thd:                                             \
            sort_block_batcher                                                \
              <Key_Type,order,blg,elt_per_thd> <<<1,1<<blg>>>                 \
              (keys_out,data_out,keys_in,data_in,n_elts);                     \
            break;

            SBB(10,3); SBB(10,2); SBB(10,1);
            SBB(9,1); SBB(8,1); SBB(7,1); SBB(6,1); SBB(5,1);
        default: SBB(5,0);
#undef SBB
        }
        return;
      }
    //
    // Otherwise, use a radix sort.

    prepare_for_n_elts(n_elts);
    dapp.sort_in = (Sort_Elt*)keys_in;
    dapp.data_in = data_in;

    const bool last_iter_b = ( dapp.digit_first + dapp.ndigits - 1 ) & 1;
    dapp.sort_out   =
       last_iter_b ? ((Sort_Elt*)sort_out_b) : ((Sort_Elt*)keys_out);
    dapp.sort_out_b =
      !last_iter_b ? ((Sort_Elt*)sort_out_b) : ((Sort_Elt*)keys_out);
    dapp.data_out   =  last_iter_b ? data_out_b : data_out;
    dapp.data_out_b = !last_iter_b ? data_out_b : data_out;

    const int excess = dapp.num_tiles * dapp.elt_per_tile - dapp.array_size;
    assert( excess >= 0 );
    const bool is_fp = Key_Type(0.1);
    if ( excess )
      CE( cudaMemset( &dapp.sort_in[dapp.array_size],
                      is_fp ? ( order == SO_Ascending ? 0x7f : 0xff )
                      : ( order == SO_Ascending ? 0xff : 0x00 ),
                      excess * sizeof(dapp.sort_in[0]) ) );

    CE( cudaMemcpyToSymbol(::dapp,&dapp,sizeof(dapp)) );

    for ( int i = 0; i < dapp.ndigits; i++ )
      {
        const int digit = dapp.digit_first + i;
        const bool last_iter = i + 1 == dapp.ndigits;
        const bool first_iter = i == 0;
        const int db = 1 << block_lg;
        radix_sort_pass_1<Key_Type,order,block_lg,radix_lg><<<grid_size,db>>>
          (digit,first_iter,last_iter);
        radix_sort_pass_2<Key_Type,order,block_lg,radix_lg><<<grid_size,db>>>
          (digit,first_iter,last_iter);
      }

    if ( check_sort )
      {
        const uint32_t mask = ~0 << dapp.digit_first * radix_lg;
        const int num_mp = gi.cuda_prop.multiProcessorCount;
        sort_check<Key_Type,order><<<num_mp,1024>>>(keys_out,mask);
        CE( cudaDeviceSynchronize() );
        CE( cudaGetLastError() );
      }
  }
};

#endif
