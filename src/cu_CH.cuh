/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __CU_CH_CUH__
#define __CU_CH_CUH__

#include "main.h"
#include "graphs.h"
#include "cu_CH.h"
#include "util/cuda-gpuinfo.h"
#include "cu_util_timing.cuh"
#include <map>
#include <array>

#if !defined(__CUDA_ARCH__)
const int mp_shared_size_bytes = 3 << 14;
#else
#if __CUDA_ARCH__ <= 350
const int mp_shared_size_bytes = 3 << 14;
#elif __CUDA_ARCH__ == 500
const int mp_shared_size_bytes = 4 << 14;
#elif __CUDA_ARCH__ == 520
const int mp_shared_size_bytes = 6 << 14;
#elif __CUDA_ARCH__ <= 600
const int mp_shared_size_bytes = 4 << 14;
#elif __CUDA_ARCH__ == 620
const int mp_shared_size_bytes = 4 << 14;
#else
// 6.1 and 7.0
const int mp_shared_size_bytes = 6 << 14;
#endif
#endif

#define config_dynamic 0

const int block_lg_overlay_1 = 9;
const int block_dim_overlay_1 = 1 << block_lg_overlay_1;

const int block_lg_update_1 = 8;
const int block_dim_update_1 = 1 << block_lg_update_1;

const int block_lg_APSP_prep = 10;
const int block_dim_APSP_prep = 1 << block_lg_APSP_prep;

constexpr uint query_multi_max_iters = 255;

constexpr uint query_u_0_block_dim = 1024;

/*
 * CUDA API Error-Checking Wrapper
 */

#ifndef CE
#define CE(call)							\
  {									\
    const cudaError_t rv = call;					\
    if ( rv != cudaSuccess )						\
      {									\
	printf("CUDA error %d, %s in line %d of file %s\n",rv,cudaGetErrorString(rv), __LINE__, __FILE__); \
	exit(1);							\
      }									\
  }
#endif

#ifdef __has_include
#  if __has_include(<bit>) && __cplusplus > 201703L
#    include <bit>
#    define INCLUDED_BIT
#  endif
#endif

#ifdef INCLUDED_BIT

template<class T> int fl1(T n) { return log2p1(n); }

#else

QUAL_HOST_DEVICE inline int __Clz(u_int32_t n)
{
#ifdef __CUDA_ARCH__
 return __clz(n);
#else
 return __builtin_clz(n);
#endif
}
QUAL_HOST_DEVICE inline int __Clzll(u_int64_t n)
{
#ifdef __CUDA_ARCH__
 return __clzll(n);
#else
 return __builtin_clzll(n);
#endif
}

// Find Last 1. Find minimum number of bits needed.
QUAL_HOST_DEVICE inline int fl1(u_int32_t n) __attribute__((unused));
QUAL_HOST_DEVICE inline int fl1(u_int32_t n) {return 8 * sizeof(n) - __Clz(n);}
QUAL_HOST_DEVICE inline int fl1(int32_t n) __attribute__((unused));
QUAL_HOST_DEVICE inline int fl1(int32_t n) {return fl1(u_int32_t(n));}
QUAL_HOST_DEVICE inline int fl1(u_int64_t n) __attribute__((unused));
QUAL_HOST_DEVICE inline int fl1(u_int64_t n) {return 8*sizeof(n) - __Clzll(n);}

// C++20 functions.
template<class T> __device__ __host__ T
log2p1(T x) { return fl1(x); }
template<class T> __device__ __host__ T
floor2(T x) { return x <= 0 ? 0 : T(1) << fl1(x) - 1; }
template<class T> __device__ __host__ T
ceil2(T x) { return x <= 0 ? 0 : T(1) << fl1(x-1); }

#endif


template<typename T>
__device__ __host__ inline bool
set_max(T& accum, T val)
{
  if ( val > accum ) { accum = val; return true; }
  return false;
}


template<typename T> T
cuda_dev_to_host(T* src)
{
  T dest;
  CE( cudaMemcpy(&dest, src, sizeof(dest), cudaMemcpyDeviceToHost) );
  return dest;
}

template<typename T> void
cuda_dev_to_host(T& dest, T* src) { dest = cuda_dev_to_host(src); }

template<typename T> void
cuda_dev_to_host(T* dest, T* src, size_t n_elts)
{ CE( cudaMemcpyAsync
      (dest, src, sizeof(src[0]) * n_elts, cudaMemcpyDeviceToHost) ); }

template<typename T> void
cuda_host_to_dev(T* dest, const T& src)
{ CE( cudaMemcpyAsync(dest, &src, sizeof(src), cudaMemcpyHostToDevice) ); }

template<typename T> void
cuda_host_to_dev(T* dest, T* src, size_t n_elts)
{ CE( cudaMemcpyAsync
      (dest, src, sizeof(src[0]) * n_elts, cudaMemcpyHostToDevice) ); }

template<typename T1, typename T2> void
cuda_host_to_dev(T1* dest, const T2& src)
{ cuda_host_to_dev(dest,T1(src)); }

#define cuda_sym_host_to_dev(dest, src) \
 CE( cudaMemcpyToSymbolAsync \
     (dest, src, sizeof(*src), 0, cudaMemcpyHostToDevice) )

#define cuda_sym_dev_to_dev(dest, src) \
 CE( cudaMemcpyToSymbolAsync \
     (dest, src, sizeof(*src), 0, cudaMemcpyDeviceToDevice) )


struct __builtin_align__(16) Score_Components {
  int deg, nbr_deg_max;
  nodeid_t wht_sum, wht_max;
};

typedef void (*Func_Kernel)();

typedef void (*Func_K_Contract)
(cu_graph_CH_bi_t graph_c, nodeid_t *node_list, nodeid_t list_length,
 cu_shc_CH_t *shc_list, cu_CH_query_stats *qstats_a, nodeid_t *d_DB_est);

typedef void (*Func_K_Score)
(cu_graph_CH_bi_t graph_c, float *scores,
 cu_CH_query_stats *qstats_a, Score_Components *score_components);

struct LConfig {
  LConfig(){inited = false;}
  int block_dim;
  int grid_dim;
  int shared_dim;
  int blks_per_mp;
  double nd_per_blk_iter;
  double nd_occ; // Number of nodes operated on by all resident threads.
  int tie_breaker; // Higher is better.
  bool inited;
  union { Func_K_Contract k_ptr; Func_K_Score k_sc; };
  cudaFuncAttributes cfa;
};

std::map<int,std::array<LConfig,2>>
configs_score_get(GPU_Info& gpu_info);

std::map<int,LConfig>
configs_contract_get(GPU_Info& gpu_info);

std::map<int,std::vector<LConfig>>
configs_contract_get_all(GPU_Info& gpu_info);


__host__ __device__ inline constexpr int
contract_nodes_tpn_to_block_lg(int t)
{ return t <= 5 ? 5 : t; }

__host__ __device__ inline constexpr int
contract_nodes_tpn_to_block_size(int t)
{ return 1 << contract_nodes_tpn_to_block_lg(t); }

// for blocked version fixed (max) blocks of 256 is assumed
// RRRR may change later for optimizations based on graph size etc.
#define score_nodes_tpn_to_block_size_blocked_lg ( 8 )
#define contract_nodes_tpn_to_block_size_blocked_lg ( 8 )

__device__ inline void sync_threads() { __syncthreads(); }

cu_graph_CH_bi_t& graph_cache_lookup(void* dev_addr);
cu_graph_CH_bi_t& graph_cache_lookup_struct_to_host(void* dev_addr);
void graph_cache_assert_dev_clean(void* dev_addr);
void graph_cache_set_dev_dirty(void *dev_addr);
void graph_cache_set_host_dirty(void *dev_addr);
void graph_cache_assume_dev_dirty(void *dev_addr);
void graph_cache_assume_all_clean(void *dev_addr);
nodeid_t graph_cache_n_edges_allocated_get(cu_graph_CH_bi_t *graph);


struct __builtin_align__(16) Extract_Block_Storage
{
  // It is very important that the compiler emits a vector load rather than
  // four loads when accessing an instance of this structure.
  nodeid_t n_nds_keep, n_fes_keep, n_bes_keep, padding;
};


void
cu_CH_score_nodes_pre_launch
(cu_graph_CH_bi_t *graph_bi_d, cu_graph_CH_UD *graph_UD_d);

__global__ void elist_populate(cu_graph_CH_bi_t graph);

template <uint32_t tpn> __global__ void
cu_CH_score_nodes
(cu_graph_CH_bi_t graph, float *node_scores, cu_CH_query_stats *qstats,
 Score_Components *score_components);

template <uint32_t tpn> __global__ void
cu_CH_score_nodes_blocked
(cu_graph_CH_bi_t graph, float *node_scores,
 cu_CH_query_stats *qstats, Score_Components *score_components);

__global__ void
cu_CH_score_nodes_blocked_2
(cu_graph_CH_bi_t graph_c,
 const Score_Components* __restrict__ score_components,
 float* __restrict__ node_scores );

// renumber node_scores to make sure that the contesting nodes have a linear order
__global__ void
cu_CH_linear_order
(nodeid_t *node_scores, nodeid_t *nid, cu_graph_CH_bi_t *graph);
// if enough shared memory is available. (note, this runs on a single block)
__global__ void cu_CH_gather_s_key
(nodeid_t *nnbr, nodeid_t *selected_list, cu_graph_CH_bi_t *graph);
__host__ void
cu_CH_select_nodes_pre_launch(cu_graph_CH_bi_t *graph_bi_d);

__host__ __device__ inline constexpr int
select_nodes_tpn_to_block_lg(int t)
{ return t <= 6 ? 6 : t <= 8 ? t : 8; }

// single iteration multiple blocks
template <uint32_t tpn> __global__ void
cu_CH_select_nodes_gl
(nodeid_t *selected_list,
 nodeid_t *iscore_in, nodeid_t *iscore_out, nodeid_t *nid);

void
cu_CH_contract_nodes_pre_launch(cu_graph_CH_bi_t *graph_bi_d);

void cu_CH_update_graph_pre_launch
(cu_graph_CH_UD_t *graph_UD,cu_graph_CH_bi_t *graph_bi_d);
__global__ void cu_CH_update_graph_01
( cu_graph_CH_bi_t *graph_d, cu_shc_CH_t shc_list );
template <uint32_t tpn> __global__ void cu_CH_update_graph_1(cu_graph_CH_UD_t *graph_UD, cu_graph_CH_bi_t *graph_d, nodeid_t *node_list, nodeid_t list_length, uint32_t rank);

void cu_CH_extract_pre_launch
(cu_graph_CH_bi_t *graph_bi_d);
__global__ void
cu_CH_extract_overlay_00
( cu_graph_CH_bi_t *graph_in, Extract_Block_Storage *g_ebs);
__global__ void
cu_CH_extract_overlay_01
(cu_graph_CH_bi_t *graph_out, cu_graph_CH_bi_t *graph_in,
 nodeid_t *node_id, nodeid_t *node_id_inv, Extract_Block_Storage *g_ebs);
template <uint32_t tpn> __global__ void
cu_CH_extract_overlay_1
(cu_graph_CH_bi_t graph_out, cu_graph_CH_bi_t graph_in,
 const nodeid_t* __restrict__ node_id,
 const nodeid_t* __restrict__ node_id_inv);

template<int tpn> __global__ void
cu_CH_invert_0(cu_graph_CH_bi_t graph);
__global__ void
cu_CH_invert_1(cu_graph_CH_bi_t graph);

__host__ void cu_graph_CH_UD_c_update(cu_graph_CH_UD_t *graph_UD_d);
__global__ void cu_CH_UD_edge_count(cu_graph_CH_UD_t *graph_UD_d);

void
cu_CH_ud_invert_launch
( GPU_Info& gpu_info, std::vector<cuda_event>& ev_array, cu_CH_stats_t *stats,
  cu_graph_CH_bi_t *ov_graph_d,
  cu_graph_CH_UD_t& ud_graph, cu_graph_CH_UD_t* ud_graph_d );

__global__ void
cu_CH_ud_invert_0
(cu_graph_CH_bi_t* ograph, cu_graph_CH_UD_t graph, cu_graph_CH_UD_t* graph_d);
__global__ void
cu_CH_ud_invert_1(cu_graph_CH_UD_t graph);
__global__ void
cu_CH_ud_invert_2(cu_graph_CH_UD_t graph, cu_graph_CH_UD_t *graph_d);
__global__ void
cu_CH_ud_invert_3(cu_graph_CH_UD_t graph);

// called after last iteration, before running APSP. adds the overlay graph into graph_UD both as the
// highest level/rank into the DAGs (in the downward DAG overlay nodes are leafs whereas in the upward
// DAG they form the roots, graph_b_removed contains the links); as well as in graph_CH form (used for
// unpacking shortcuts when forming paths). Also, initializes APSP path based on overlay
template <uint32_t tpn> __global__ void
cu_CH_APSP_prep(cu_graph_CH_UD_t *graph_UD, cu_graph_CH_bi_t *graph);

// this is the final kernel of CH which goes over the UD graphs and renumbers all nbrs to their idx
// in the UD space (not doing this will result in an extra level of pointer chasing during queries)
__global__ void cu_CH_UD_renum(cu_graph_CH_UD_t *graph_UD);

// comrpess the edge lists in a new graph_UD. similar to extract overlay, _0 determines pointers
// using prefix_sums and _1 does the actually copy of the edge list.
// RRRR it is possible to avoid having this kernel and have UD compressed during contraction, but for
// RRRR now we will keep it as is and do the compression at the end until the release version of code
// RRRR this will also allow us to test different versions of output UD graph and query kernels
// RRRR e.g. degree sorting
void cu_CH_UD_compress_pre_launch
(cu_graph_CH_UD_t *graph_UD_out, cu_graph_CH_UD_t *graph_UD_in);
__global__ void cu_CH_UD_compress_0(cu_graph_CH_UD_t *graph_UD_out, cu_graph_CH_UD_t *graph_UD_in);
__global__ void cu_CH_UD_compress_1(cu_graph_CH_UD_t *graph_UD_out, cu_graph_CH_UD_t *graph_UD_in);

// a modified implementation of the 3-phased blocked APSP proposed in katz[2008] and venkatraman[2003]
// blocking factor = 64, takes in (m*64)^2 adjacency array and prev array along with iteration
// and phase number. 0<iter<(m-1), and phase is either 0/1/2. it should always be called with 1024
// threads and m^2 blocks (2D, i.e. gridDim.x==m and gridDim.y==m). note that in phase 0 only one
// block (block [iter,iter] is active, in phase 1 only 2*m-2 blocks (blocks [iter,j] and [i,iter]
// where i!=iter and j!=iter) are active and in phase 2 only (m-1)^2 blocks (all blocks [i,j] where
// i!=iter and j!=iter) are active.
// to calculate APSP, caller must initialize prev and dist and form the iter loop from 0 to m where at
// each iteration the kernel is called 3 times (once for each phase)
// the shortest path distance from node i to j is stored at dist[i*num_nodes + j] and the parent of j
// in the corresponding path (nodeid k) is stored at prev[i*num_nodes + j]
template <bool verify=true> __global__ void
cu_APSP_blocked
(weight_t *dist, nodeid_t *prev, uint32_t num_nodes,
 uint32_t iter, uint32_t phase);

__global__ void array_fill_linear(uint32_t *array, uint32_t len);

__host__ void
cu_CH_query_pre_launch
(cu_graph_CH_UD_t *graph_UD_d, path_t *result_ud_d, path_t *result_or_d);
__global__ void cu_CH_query_u_0
(uint32_t src, int lvl_start, int lvl_stop, uint32_t *node_tags);
__global__ void cu_CH_query_u_1(uint32_t src, int lvl);
__global__ void cu_CH_query_u_1_multi_iter(int lvl, int num_iters);
__global__ void cu_CH_query_OL();
__global__ void cu_CH_query_d(int lvl);
__global__ void cu_CH_query_d_multi_iter(int lvl, int num_iters);
__global__ void cu_CH_path_unpack_OL();
__global__ void cu_CH_path_unpack_renum();


__global__ void
cu_degree_sum_get(cu_graph_CH_bi *graph, int *degree_sums);

// Set max_deg member in cu_graph_CH_t. Intended for performance
// tuning and debugging.
__global__ void max_degree_update(cu_graph_CH_bi_t* graph);
__global__ void max_degree_update(cu_graph_CH_UD_t* graph);

// Sort edge lists in destination order. Intended in part to
// obtain deterministic runs.
void tune_elist_sort(cu_graph_CH_bi_t *g_dev);



#endif
