/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//


 /// Show implementation of sorting on CUDA

#include <stdio.h>
#include <assert.h>
#include <nperf.h>
#include <vector>
#include <algorithm>
#include <functional>
#include <ptable.h>
#include <cuda-gpuinfo.h>
#include "cu_util_timing.cuh"

#include "sort.cuh"


__host__ __device__ inline int rnd_up(int n, int m){ return m*((n+m-1)/m); };

using namespace std;




static inline uint32_t
fp_to_unsigned(float fp)
{
  union { float f; uint32_t u; } v = { fp };
  const uint32_t sign_mask = uint32_t(1) << 31;
  const bool neg = v.u & sign_mask;
  return neg ? uint32_t(-1) ^ v.u : v.u | sign_mask;
}

#include "sort-kernels.cu"

__host__ void
kernels_get_attr(GPU_Info *gpu_info)
{
#define GETATTR(func) gpu_info->GET_INFO(func)

#define GAPAIR(block_lg,radix_lg)                                             \
  GETATTR((radix_sort_pass_1<uint32_t,SO_Ascending,block_lg,radix_lg>));      \
  GETATTR((radix_sort_pass_2<uint32_t,SO_Ascending,block_lg,radix_lg>));

#define GASET(radix_lg)                                                       \
  GAPAIR(5,radix_lg); GAPAIR(6,radix_lg); GAPAIR(7,radix_lg);                 \
  GAPAIR(8,radix_lg); GAPAIR(9,radix_lg); GAPAIR(10,radix_lg);

  GASET(4);
  GASET(5);
  GASET(6);
  GASET(8);
#undef GETATTR
#undef GASET
#undef GAPAIR
}


typedef void (*KPtr)(int,bool,bool);

// This routine executes on the CPU.
//
__host__ Kernel_Info*
sort_launch_pass_1
(GPU_Info *gpu_info, int dg, int db,
 int radix_lg, int digit_pos, bool first_iter, bool last_iter)
{

#define LAUNCH_RD(BLG,RD_LG) \
  case 1<<BLG: kfunc=radix_sort_pass_1<uint32_t,SO_Ascending,BLG,RD_LG>; break;

#define LAUNCH_BLKS(RD_LG)                                                    \
    case RD_LG: switch ( db ){                                                \
      LAUNCH_RD(5,RD_LG); LAUNCH_RD(6,RD_LG); LAUNCH_RD(7,RD_LG);             \
      LAUNCH_RD(8,RD_LG); LAUNCH_RD(9,RD_LG); LAUNCH_RD(10,RD_LG);            \
    default: assert( false );                                                 \
    } break;

    KPtr kfunc = NULL;

    switch ( radix_lg ) {
      LAUNCH_BLKS(4);
      LAUNCH_BLKS(5);
      LAUNCH_BLKS(6);
      LAUNCH_BLKS(8);
    default: assert( false );
    }

# undef LAUNCH_RD
# undef LAUNCH_BLKS

    if ( dg == 0 ) return &gpu_info->get_info(GPU_Info_Func(kfunc));

    kfunc<<<dg,db>>>(digit_pos,first_iter,last_iter);

    return NULL;
}

__host__ Kernel_Info*
sort_launch_pass_2
(GPU_Info *gpu_info, int dg, int db,
 int radix_lg, int digit_pos, bool first_iter, bool last_iter)
{
#define LAUNCH_RD(BLG,RD_LG)                                                  \
  case 1<<BLG: kfunc=radix_sort_pass_2<uint32_t,SO_Ascending,BLG,RD_LG>; break;

#define LAUNCH_BLKS(RD_LG)                                                    \
  case RD_LG: switch ( db ){                                                  \
    LAUNCH_RD(5,RD_LG); LAUNCH_RD(6,RD_LG); LAUNCH_RD(7,RD_LG);               \
    LAUNCH_RD(8,RD_LG); LAUNCH_RD(9,RD_LG); LAUNCH_RD(10,RD_LG);              \
  default: assert( false );                                                   \
  } break;

  KPtr kfunc = NULL;

  switch ( radix_lg ) {
    LAUNCH_BLKS(4);
    LAUNCH_BLKS(5);
    LAUNCH_BLKS(6);
    LAUNCH_BLKS(8);
    default: assert( false );
  }

  if ( dg == 0 ) return &gpu_info->get_info(GPU_Info_Func(kfunc));

  kfunc<<<dg,db>>>(digit_pos,first_iter,last_iter);

  return NULL;

# undef LAUNCH_RD
# undef LAUNCH_BLKS
}





template <typename T>
class pCUDA_Memory {
public:
  pCUDA_Memory()
  {
    data = NULL;  dev_addr = NULL; locked = false;
    elements = 0;
    chars_allocated_cuda = chars_allocated = 0;
  }
  ~pCUDA_Memory(){ free_memory(); }

  void free_memory(){ free_memory_host();  free_memory_cuda(); }
  void free_memory_host()
  {
    if ( !data ) return;
    if ( locked ) {CE(cudaFreeHost(data));} else { free(data); }
    data = NULL;
    chars_allocated = 0;
  }
  void free_memory_cuda()
  {
    if ( dev_addr ) CE(cudaFree(dev_addr));
    dev_addr = NULL;
    chars_allocated_cuda = 0;
  }

  T* alloc_locked_maybe(int elements_p, bool locked_p)
  {
    assert( !data );
    elements = elements_p;
    locked = locked_p;
    chars_allocated = chars = elements * sizeof(T);
    if ( locked )
      { CE(cudaMallocHost((void**)&data,chars)); }
    else
      { data = (T*) malloc(chars); }
    new (data) T[elements];
    return data;
  }

  T* realloc_locked(int nelem){ return realloc(nelem,true); }
  T* realloc(int nelem, bool locked_p = false)
  {
    const int chars_needed = nelem * sizeof(T);
    if ( chars_needed <= chars_allocated )
      {
        chars = chars_allocated;
        elements = nelem;
      }
    else
      {
        free_memory_host();
        alloc_locked_maybe(nelem,locked_p);
      }
    return data;
  }

  T* alloc(int nelem) { return alloc_locked_maybe(nelem,false); }
  T* alloc_locked(int nelem) { return alloc_locked_maybe(nelem,true); }

  T& operator [] (int idx) const { return data[idx]; }

private:
  void alloc_maybe(){ alloc_gpu_buffer(); }

  void alloc_gpu_buffer()
  {
    if ( dev_addr )
      {
        if ( chars_allocated_cuda >= chars ) return;
        free_memory_cuda();
      }
    CE(cudaMalloc(&dev_addr,chars));
    chars_allocated_cuda = chars;
  }

public:
  T* get_dev_addr()
  {
    alloc_maybe();
    return (T*)dev_addr;
  }

  void to_cuda()
  {
    if ( !elements ) return;
    alloc_gpu_buffer();
    CE(cudaMemcpy(dev_addr, data, chars, cudaMemcpyHostToDevice));
  }

  void from_cuda()
  {
    if ( !elements ) return;
    CE(cudaMemcpy(data, dev_addr, chars, cudaMemcpyDeviceToHost));
  }

  // Stuff below should be private to avoid abuse.
  void *dev_addr;
  int current;
  T *data;
  bool locked;
  int elements, chars, chars_allocated, chars_allocated_cuda;
};


class Sort {
public:

  Sort(int argc, char **argv){ init(argc,argv); }

  GPU_Info gpu_info;
  vector<cudaEvent_t> cuda_ces;

  Radix_Sort_GPU_Constants dapp;

  // Data Arrays.
  //
  // These classes automatically allocate storage on both CPU and GPU
  // and move data back and forth.
  //
  pCUDA_Memory<float> fkeys_in;
  pCUDA_Memory<float> fkeys_out;
  pCUDA_Memory<Sort_Elt> sort_in;     // Input array.
  pCUDA_Memory<Sort_Elt> sort_out;    // Output or intermediate array.
  pCUDA_Memory<Sort_Elt> sort_out_b;  // Output or intermediate array.
  pCUDA_Memory<uint16_t> sort_perm;
  pCUDA_Memory<Sort_Data> data_in, data_out, data_out_b;
  pCUDA_Memory<int> sort_histo;       // Histogram bins.
  pCUDA_Memory<int> sort_tile_histo;  // First elt in tile with dig val.

  int opt_wp_per_mp;
  int array_size;               // Number of elements in array.

  pTable table;
  pTable tab_bars;
  double pass_plot_scale;

  // Shadow Sort
  vector<Sort_Elt> ss_keys_1, ss_keys_2;
  vector<int> ss_thisto, ss_bhisto;

  void init(int argc, char **argv)
  {
    // Must be called before any CUDA API calls.
    NPerf_init(true);

    gpu_info_print();

    //  const int dev = gpu_choose_index();
    const int dev = 0;
    CE(cudaSetDevice(dev));
    printf("Using GPU %d\n",dev);
    gpu_info.get_gpu_info(dev);
    kernels_get_attr(&gpu_info);

    // Collect performance data using a wrapper to NVIDIA CUPTI event
    // counter API.
    //
    NPerf_metric_collect("inst_executed");
    NPerf_metric_collect("eligible_warps_per_cycle");
    NPerf_metric_collect("gld_efficiency");
    NPerf_metric_collect("gst_efficiency");
    //  NPerf_metric_collect("shared_efficiency"); // WRT 16 * 4 bytes / cycle.
    //  NPerf_metric_collect("shared_replay_overhead");
    NPerf_metric_collect("issue_slot_utilization");
    //
    // Note: The more metrics that are collected, the more times a kernel
    // will need to be run.

    // Prepare events used for timing.
    //
    const int max_digits = sizeof(Sort_Elt) * 8 * 4;
    cuda_ces.resize(max_digits);
    for ( auto& e: cuda_ces ) CE(cudaEventCreate(&e));

    // Print information about kernel.
    //

    const bool show_resource_usage = false;

    if ( show_resource_usage )
      {
        printf("\nCUDA Kernel Resource Usage:\n");

        for ( int i=0; i<gpu_info.num_kernels; i++ )
          {
            printf("For %s:\n", gpu_info.ki[i].name);
            printf("  %6zd shared, %zd const, %zd loc, %d regs; "
                   "%d max threads per block.\n",
                   gpu_info.ki[i].cfa.sharedSizeBytes,
                   gpu_info.ki[i].cfa.constSizeBytes,
                   gpu_info.ki[i].cfa.localSizeBytes,
                   gpu_info.ki[i].cfa.numRegs,
                   gpu_info.ki[i].cfa.maxThreadsPerBlock);
          }
      }


    srand48(1);                   // Seed random number generator.

    // Examine argument 1, block count, default is number of MPs.
    //
    const int arg1_int = argc < 2 ? 64 : atoi(argv[1]);
    opt_wp_per_mp = arg1_int;

    const int array_size_lg = argc > 2 ? atoi(argv[2]) : 20;

    //
    // Initialize Array Shape Variables
    //
    array_size = ( 1 << array_size_lg );

    const int max_block_lg = 10;
    const int max_elt_per_thd = 8;
    const int array_mult = max_elt_per_thd << max_block_lg;
    const int array_size_rup = rnd_up(array_size,array_mult);

    dapp.array_size = array_size;

    printf("Occ Limit %d warps.  List size %d ( 0x%x )  Elt / Thd %d\n",
           opt_wp_per_mp, array_size, array_size, elt_per_thread );

    fkeys_in.alloc(array_size);
    fkeys_out.alloc(array_size);
    sort_in.alloc(array_size);
    sort_out.alloc(array_size);
    sort_out_b.alloc(array_size);
    data_in.alloc(array_size);
    data_out.alloc(array_size);
    data_out_b.alloc(array_size);

    dapp.sort_in = sort_in.get_dev_addr();
    dapp.sort_out = sort_out.get_dev_addr();
    dapp.sort_out_b = sort_out_b.get_dev_addr();
    dapp.data_in = data_in.get_dev_addr();
    dapp.data_out = data_out.get_dev_addr();
    dapp.data_out_b = data_out_b.get_dev_addr();

    bzero(sort_out.data,array_size*sizeof(sort_out[0]));
    sort_out.to_cuda();

    // Initialize input arrays.
    //
    map<Sort_Elt,int> used;
    const int zero_index = random() % min(array_size,32);
    for ( int i=0; i<array_size; i++ )
      {
        const bool debug_friendly = false;
        if ( debug_friendly )
          {
            //  sort_in[i] = i << 1;
            //  sort_in[i] = ( ( random() & 0x5 ) << db_friendly_offset ) + i;
            sort_in[i] = ( ( random() & 0xf50 ) ) | 0x6000;
            //  sort_in[i] = -1;
              //  + i;
          }
        else
          {
            const bool zero = i == zero_index;
            const bool maxint = i+1 == zero_index;

            // Ensure that keys are uniq.
            for ( int iter = 0; iter < 1000; iter++ )
              {
                const float fval = zero ? 0.0 : drand48() * 10000 - 5000;
                const uint32_t val =
                  zero ? 0 : maxint ? uint32_t(-1) : fp_to_unsigned(fval);
                assert( iter < 100 );
                if ( used[val]++ ) continue;
                sort_in[i] = val;
                fkeys_in[i] = fval;
                break;
            }
          }
        data_in[i] = i;
      }
  }

  void shadow_sort_init(int block_size)
  {
    if ( ss_keys_1.size() != size_t(array_size) )
      {
        ss_keys_1.resize(array_size);
        ss_keys_2.resize(array_size);
      }

    for ( int i=0; i<array_size; i++ ) ss_keys_2[i] = sort_in[i];
    ss_thisto.resize(dapp.thisto_array_size_elts);
    ss_bhisto.resize(dapp.bhisto_array_size_elts);

  }

  void shadow_sort_advance(int digit_pos, int grid_size)
  {
    const int bit_pos = digit_pos * dapp.radix_lg;
    const int elt_per_tile = dapp.elt_per_tile;
    const int radix = dapp.radix;
    const Sort_Elt ss_digit_mask = radix - 1;
    const Sort_Elt digit_mask = ss_digit_mask << bit_pos;
    const int tiles_per_block = div_ceil(dapp.num_tiles,grid_size);
    ss_keys_1 = ss_keys_2;
    Sort_Elt* const all_keys = ss_keys_1.data();
    int* const all_thisto = ss_thisto.data();
    int* const all_bhisto = ss_bhisto.data();
    for ( auto& e: ss_bhisto ) e = 0;

    for ( int tile_idx = 0; tile_idx < dapp.num_tiles; tile_idx++ )
      {
        Sort_Elt* const keys = all_keys + tile_idx * elt_per_tile;
        stable_sort( keys, keys + elt_per_tile,
                     [&] (Sort_Elt a, Sort_Elt b)
                     { return ( a & digit_mask ) < ( b & digit_mask ); } );
        int* const thisto = all_thisto + tile_idx * radix;
        int* const bhisto =
          all_bhisto + (tile_idx/tiles_per_block) * radix;
        for ( int d=0; d<radix; d++ ) thisto[d] = 0;
        for ( int i=0; i<elt_per_tile; i++ )
          thisto[ keys[i] >> bit_pos & ss_digit_mask ]++;
        for ( int d=0; d<radix; d++ ) bhisto[d] += thisto[d];
      }
    vector<int> ghisto(radix);
    for ( auto& e: ghisto ) e = 0;
    for ( int i=0; i<grid_size; i++ )
      for ( int d=0; d<radix; d++ )
        ghisto[d] += ss_bhisto[i*radix+d];
    vector<int> psum(radix);
    psum[0] = 0;
    for ( int d=1; d<radix; d++ ) psum[d] = psum[d-1] + ghisto[d-1];
    const Sort_Elt ss_unused_key = 0;
    for ( auto& e: ss_keys_2 ) e = ss_unused_key;
    for ( int idx = 0;  idx < array_size;  idx++ )
      {
        Sort_Elt key = ss_keys_1[idx];
        Sort_Elt digit = key >> bit_pos & ss_digit_mask;
        const int idx_next = psum[digit]++;
        assert( ss_keys_2[idx_next] == ss_unused_key );
        ss_keys_2[idx_next] = key;
      }
  }

  void shadow_sort_check_pass_1(int digit_pos)
  {
    sort_tile_histo.from_cuda();
    sort_histo.from_cuda();

    int terr_count = 0;
    int berr_count = 0;
    int serr_count = 0;

    for ( int i = 0;  i < dapp.thisto_array_size_elts;  i++ )
      {
        if ( sort_tile_histo[i] == ss_thisto[i] ) continue;
        if ( terr_count++ > 10 ) continue;
        printf("D%dp1,  Digit %3d,  Tile %4d error: "
               "%3d != %3d (correct)\n",
               digit_pos,  i % dapp.radix, i / dapp.radix,
               sort_tile_histo[i], ss_thisto[i]);
      }
    for ( int i = 0;  i < dapp.bhisto_array_size_elts;  i++ )
      {
        if ( sort_histo[i] == ss_bhisto[i] ) continue;
        if ( berr_count++ > 10 ) continue;
        printf("D%dp1,  Digit %3d,  Block %4d error: "
               "%3d != %3d (correct)\n",
               digit_pos,  i % dapp.radix, i / dapp.radix,
               sort_histo[i], ss_bhisto[i]);
      }
    if ( false ) // Need to check permutation.
    for ( int i=0; i<array_size; i++ )
      {
        if ( sort_out_b[i] == ss_keys_1[i] ) continue;
        if ( serr_count++ > 10 ) continue;
        printf("D%dp1,  TPos %3d,  Tile %4d error: "
               "%#11x != %#11x (correct)\n",
               digit_pos,  i % dapp.elt_per_tile, i / dapp.elt_per_tile,
               sort_out_b[i], ss_keys_1[i]);

      }

    assert( terr_count == 0 );
    assert( berr_count == 0 );
    assert( serr_count == 0 );
  }

  void shadow_sort_check_pass_2(int digit_pos, int grid_size)
  {
    auto& sout = digit_pos & 1 ? sort_out_b : sort_out;

    sout.from_cuda();

    int serr_count = 0;
    const int tiles_per_block = div_ceil(dapp.num_tiles,grid_size);
    const int elt_per_block = tiles_per_block * dapp.elt_per_tile;

    for ( int i=0; i<array_size; i++ )
      {
        if ( sout[i] == ss_keys_2[i] ) continue;
        if ( serr_count++ > 10 ) continue;
        printf("D%dp2, B %2d  TPos %3d,  Tile %4d  %3d error: "
               "%#11x != %#11x (cor)\n",
               digit_pos, i/elt_per_block,
               i % dapp.elt_per_tile,
               i / dapp.elt_per_tile,
               i / dapp.elt_per_tile % tiles_per_block,
               sout[i], ss_keys_2[i]);

      }

    assert( serr_count == 0 );
  }

  void check_sort
  ( int block_size, int limit, bool always_sort_out = false,
    Sort_Order order = SO_Ascending )
  {
    int err_count = 0;
    vector<Sort_Elt> sort_check_short(limit), re_sort_out(limit);
    vector<int> perm(limit);
    auto& sout = always_sort_out || dapp.ndigits & 1 ? sort_out : sort_out_b;
    Sort_Elt* const sort_out_use = sout.data;
    auto& dout = always_sort_out || dapp.ndigits & 1 ? data_out : data_out_b;
    auto comp =
      [&](uint32_t a, uint32_t b)
      { return order == SO_Ascending ? a < b : a > b; };

    for ( int i=0; i<limit; i++ )
      {
        re_sort_out[i] = sort_out_use[i];
        sort_check_short[i] = sort_in[i];
        perm[i] = i;
      }
    sort(re_sort_out.begin(),re_sort_out.end(),comp);
    sort(sort_check_short.begin(),sort_check_short.end(),comp);
    sort(perm.begin(),perm.end(),
         [&](int a, int b){ return comp(sort_in[a],sort_in[b]); } );

    for ( int i=0; i<limit; i++ )
      assert( sort_in[perm[i]] == re_sort_out[i] );

    for ( int i=0; i<limit; i++ )
      {
        if ( re_sort_out[i] == sort_check_short[i] ) continue;
        printf("*** At element 0x%x, sorted mismatch %#010x != %#010x (c)\n",
               i, re_sort_out[i], sort_check_short[i]);
        if ( err_count++ > 15 ) break;
      }

    for ( int i=0; i<limit; i++ )
      {
        if ( sort_out_use[i] == sort_check_short[i] ) continue;
        printf("*** Incorrect result at %d  %#010x != %#010x (correct)\n",
               i, sort_out_use[i], sort_check_short[i]);
        if ( err_count++ > 15 ) break;
      }

    for ( int i=0; i<limit; i++ )
      {
        if ( data_in[perm[i]] == dout[i] ) continue;
        printf("*** Data mismatch at %d  %d != %d (correct)\n",
               i, dout[i], data_in[perm[i]]);
        if ( err_count++ > 15 ) break;
      }
    assert( !err_count );

  }
  void check_sort_fp(int block_size, int limit, Sort_Order order)
  {
    int err_count = 0;
    vector<float> sort_check(limit);
    vector<int> perm(limit);

    for ( int i=0; i<limit; i++ )
      {
        sort_check[i] = fkeys_in[i];
        perm[i] = i;
      }
    auto comp =
      [&](float a, float b) { return order == SO_Ascending ? a < b : a > b; };

    sort(sort_check.begin(),sort_check.end(),comp);
    sort( perm.begin(), perm.end(),
          [&](int a, int b) { return comp(fkeys_in[a],fkeys_in[b]);} );

    for ( int i=0; i<limit; i++ )
      {
        if ( fkeys_out[i] == sort_check[i] ) continue;
        printf("*** Incorrect result at %d  %f != %f (correct)\n",
               i, fkeys_out[i], sort_check[i]);
        if ( err_count++ > 15 ) return;
      }

    for ( int i=0; i<limit; i++ )
      {
        if ( data_in[perm[i]] == data_out[i] ) continue;
        printf("*** Data mismatch at %d  %d != %d (correct)\n",
               i, data_out[i], data_in[perm[i]]);
        if ( err_count++ > 15 ) return;
      }

  }

  vector<function<void()>> tab_bar_row_finish;

  void start()
  {
    bool just_test_sort_objects = true;

    if ( just_test_sort_objects ) { run_sorts(); return; }

    table.stream = stdout;
    pass_plot_scale = 0;

    //  for ( int r: {4,6,8} )
    for ( int r: {8} )
      for ( int b=6; b<=10; b++ )
        run_sort(b, opt_wp_per_mp, r);

    for ( auto& f: tab_bar_row_finish ) f();

    printf("%s\n",tab_bars.body_get());

    run_sorts(); return;

  }

  void run_sort(int block_lg, int wp_per_mp_limit, int sort_radix_lg)
  {
    const int block_size = 1 << block_lg;
    const int cpu_rounds = 1;
    const int num_mp = gpu_info.cuda_prop.multiProcessorCount;
    const int wp_lg = 5;

    Kernel_Info* const ki1 =
      sort_launch_pass_1
      (&gpu_info,0,block_size,sort_radix_lg, -1, false, false );
    Kernel_Info* const ki2 =
      sort_launch_pass_2
      (&gpu_info,0,block_size,sort_radix_lg, -1, false, false );

    // The maximum number of active blocks per MP for pass 1 and pass 2
    // kernels when launched with a block size of thd_per_block.
    //
    const int max_bl_per_mp_1 = ki1->get_max_active_blocks_per_mp(block_size);
    const int max_bl_per_mp_2 = ki2->get_max_active_blocks_per_mp(block_size);

    const int wp_per_bl_lg = block_lg - wp_lg;

    const int max_wp_per_mp_1 = max_bl_per_mp_1 << wp_per_bl_lg;
    const int max_wp_per_mp_2 = max_bl_per_mp_2 << wp_per_bl_lg;

    const int use_wp_per_mp_1 = min(max_wp_per_mp_1,wp_per_mp_limit);
    const int use_wp_per_mp_2 = min(max_wp_per_mp_2,wp_per_mp_limit);
    const int use_bl_per_mp_1 = max( 1, use_wp_per_mp_1 >> wp_per_bl_lg );
    const int use_bl_per_mp_2 = max( 1, use_wp_per_mp_2 >> wp_per_bl_lg );
    const int grid_size_1 = num_mp * use_bl_per_mp_1;
    const int grid_size_2 = num_mp * use_bl_per_mp_2;
    const int active_wp_1 =
      min(use_bl_per_mp_1,max_bl_per_mp_1) << wp_per_bl_lg;
    const int active_wp_2 =
      min(use_bl_per_mp_2,max_bl_per_mp_2) << wp_per_bl_lg;

    const int grid_size = max(grid_size_1,grid_size_2);

    const int sort_radix = 1 << sort_radix_lg;

    dapp.radix_lg = sort_radix_lg;
    dapp.radix = sort_radix;

    dapp.elt_per_tile = block_size * elt_per_thread;
    const int num_tiles = dapp.num_tiles =
      div_ceil( array_size, dapp.elt_per_tile );
    const int key_size_bits = 8 * sizeof(Sort_Elt);
    const int ndigits = div_ceil( key_size_bits, sort_radix_lg );
    dapp.ndigits = ndigits;
    const int tiles_per_block = div_ceil(num_tiles,grid_size);

    dapp.thisto_array_size_elts = sort_radix * num_tiles;
    dapp.bhisto_array_size_elts = sort_radix * grid_size;
    const size_t thisto_array_size_bytes =
      sizeof(sort_tile_histo[0]) * dapp.thisto_array_size_elts;
    const size_t bhisto_array_size_bytes =
      sizeof(sort_histo[0]) * dapp.bhisto_array_size_elts;

    sort_histo.realloc(bhisto_array_size_bytes);
    dapp.sort_histo = sort_histo.get_dev_addr();
    sort_tile_histo.realloc(thisto_array_size_bytes);
    dapp.sort_tile_histo = sort_tile_histo.get_dev_addr();
    sort_perm.realloc(array_size);
    dapp.perm = sort_perm.get_dev_addr();

    const size_t size_keys_bytes =
      num_tiles * dapp.elt_per_tile * sizeof(Sort_Elt);
    const size_t size_data_bytes =
      num_tiles * dapp.elt_per_tile * sizeof(Sort_Data);
    const size_t size_perm_bytes =
      num_tiles * dapp.elt_per_tile * sizeof(uint16_t);

    const size_t comm_keys_bytes_pass_1 = size_keys_bytes * ndigits;
    const size_t comm_elts_bytes_pass_2 =
      size_perm_bytes * ndigits
      + size_keys_bytes * ndigits * 2
      + size_data_bytes * ndigits * 2;

    const size_t size_histo_bytes =
      ( num_tiles + 1 ) * sort_radix * sizeof(int);

    const size_t comm_histo_bytes_pass_1 = size_histo_bytes * ndigits;
    const size_t comm_histo_bytes_pass_2 = size_histo_bytes * ndigits;

    const size_t comm_bytes_pass_1 =
      comm_keys_bytes_pass_1 + comm_histo_bytes_pass_1
      + size_perm_bytes * ndigits;
    const size_t comm_bytes_pass_2 =
      comm_elts_bytes_pass_2 + comm_histo_bytes_pass_2;

    const size_t work_per_round_pass_1 = array_size * sort_radix_lg;

    const size_t rR = sort_radix_lg << sort_radix_lg;  // Radix * lg Radix
    const size_t work_per_round_pass_2 =
      // Combine per-block histograms. Redundantly performed by each block.
      grid_size * grid_size * sort_radix
      // Compute per-block prefix sum from global histogram.
      + grid_size * rR
      // Compute per-tile prefix sum.
      + num_tiles * rR
      // Scatter keys.
      + array_size;
    //
    // Note: The work calculation above assumes that the amount of
    // work (say, instructions) for each component is the same. The
    // components are combine per-block histograms, compute per-block
    // prefix sum, compute -er-tile prefix sum, and scatter keys.

    // Function to write columns common to each output table.
    auto pop = [=](pTable *t)
      {
        t->row_start();
        t->entry("Wp","%2d",1<<wp_per_bl_lg);
        t->header_span_start("Ac Wp");
        t->entry("P1","%2d",active_wp_1);
        t->entry("P2","%2d",active_wp_2);
        t->header_span_end();
        t->entry("Bl","%2d",grid_size / num_mp );
        t->entry("Rd","%2d",sort_radix_lg);
      };

    pop(&table);

    cudaMemcpyToSymbol(::dapp,&dapp,sizeof(dapp));

    const bool ss_check = true;

    for ( int cpu_round=0; cpu_round<cpu_rounds; cpu_round++ )
      {
        if ( ss_check ) shadow_sort_init(block_size);

        // Zero result array and send to GPU, to detect
        // skipped-element errors occurring after a prior invocation
        // wrote the correct results.
        //
        for ( int i=0; i<array_size; i++ ) sort_out_b[i] = sort_out[i] = 0;
        CE( cudaMemset( data_out.get_dev_addr(), 0, data_out.chars ) );
        CE( cudaMemset( data_out_b.get_dev_addr(), 0, data_out_b.chars ) );
        sort_out.to_cuda();
        sort_out_b.to_cuda();

        sort_in.to_cuda(); // Move input array to CUDA.
        data_in.to_cuda(); // Move input array to CUDA.

        uint next_ce_idx = 0;

        NPerf_Kernel_Data kid1, kid2;

        // Amount of time for each pass of each round.
        vector<double> pass_times;
        const bool nperf_avail = NPerf_metrics_collection_get();

        for ( int digit_pos = 0; digit_pos < ndigits;  digit_pos++ )
          {
            const bool first_iter = digit_pos == 0;
            const bool last_iter = digit_pos + 1 == ndigits;

            if ( ss_check ) shadow_sort_advance(digit_pos,grid_size);

            //
            // Pass 1
            //
            CE(cudaEventRecord(cuda_ces[next_ce_idx++]));
            for ( NPerf_data_reset(); NPerf_need_run_get(); )
              sort_launch_pass_1
                ( &gpu_info, grid_size, block_size, sort_radix_lg, digit_pos,
                  first_iter, last_iter );
            CE(cudaEventRecord(cuda_ces[next_ce_idx++]));

            // Collect any performance-counter data (from CUpti API).
            if ( nperf_avail )
              {
                pass_times.push_back(NPerf_kernel_et_get());
                NPerf_kernel_data_append(kid1);
              }

            if ( ss_check ) shadow_sort_check_pass_1(digit_pos);

            //
            // Pass 2
            //
            CE(cudaEventRecord(cuda_ces[next_ce_idx++]));
            for ( NPerf_data_reset(); NPerf_need_run_get(); )
              sort_launch_pass_2
                ( &gpu_info, grid_size, block_size, sort_radix_lg, digit_pos,
                  first_iter, last_iter );
            CE(cudaEventRecord(cuda_ces[next_ce_idx++]));

            // Collect any performance-counter data (from CUpti API).
            if ( nperf_avail )
              {
                pass_times.push_back(NPerf_kernel_et_get());
                NPerf_kernel_data_append(kid2);
              }

            if ( ss_check ) shadow_sort_check_pass_2(digit_pos,grid_size);
          }
        assert( next_ce_idx <= cuda_ces.size() );

        // Retrieve data from GPU.
        //
        sort_out.from_cuda();

        double pass_1_ce_time_s = 0;
        double pass_2_ce_time_s = 0;

        // Retrieve timing collected using cudaEvent API. 
        //
        for ( int pos = 0; pos < ndigits;  pos++ )
          {
            float cuda_time_ms;
            int p1_idx = pos * 4;
            CE(cudaEventElapsedTime
               ( &cuda_time_ms, cuda_ces[p1_idx], cuda_ces[p1_idx+1]) );
            if ( !nperf_avail )
              pass_times.push_back( cuda_time_ms * 1e-3 );
            pass_1_ce_time_s += cuda_time_ms * 1e-3;
            CE(cudaEventElapsedTime
               ( &cuda_time_ms, cuda_ces[p1_idx+2], cuda_ces[p1_idx+3]) );
            if ( !nperf_avail )
              pass_times.push_back( cuda_time_ms * 1e-3 );
            pass_2_ce_time_s += cuda_time_ms * 1e-3;
          }

        if ( cpu_round + 1 < cpu_rounds ) continue;

        // Retrieve timing data from NPerf (uses CUpti API).
        const double pass_1_np_time_s = NPerf_kernel_et_get(kid1);
        const double pass_2_np_time_s = NPerf_kernel_et_get(kid2);

        const double pass_1_time_s =
          nperf_avail ? pass_1_np_time_s : pass_1_ce_time_s;
        const double pass_2_time_s =
          nperf_avail ? pass_2_np_time_s : pass_2_ce_time_s;
        const double rs_time_s = pass_1_time_s + pass_2_time_s;

        if ( rs_time_s > pass_plot_scale ) pass_plot_scale = rs_time_s;

        // Function to write row of execution time bar-graph table.
        // Function is executed after all runs are completed so that
        // time of longest run can be used to scale bar lengths.
        auto finish =
          [=]()
          {
            pop(&tab_bars);  // Write first few columns of table.
            const int row_len_so_far = tab_bars.row_len_get();
            const int line_len = stdout_width_get();
            const int pass_plot_len = line_len - row_len_so_far - 3;
            pStringF fmt("%%-%ds",pass_plot_len);
            const double pass_plot_tscale = pass_plot_len / pass_plot_scale;
            double pass_plot_t = 0;
            string pass_plot_txt = "";
            for ( int j=0; j<2; j++ )
              for ( int i=0; i<ndigits; i++ )
                {
                  const char c = j ? '.' : '1';
                  pass_plot_t += pass_times[2*i+j];
                  const int pp_len = pass_plot_t * pass_plot_tscale;
                  const int numc = pp_len - pass_plot_txt.length();
                  if ( numc > 0 ) pass_plot_txt += string(numc,c);
                }
            tab_bars.entry("Time",fmt,pass_plot_txt);
            tab_bars.row_end();
          };

        // Put function above in a list of things to execute later.
        tab_bar_row_finish.push_back(finish);

        sort_tile_histo.from_cuda();
        sort_histo.from_cuda();
        sort_out_b.from_cuda();
        data_out.from_cuda();
        data_out_b.from_cuda();

        assert( is_sorted( ss_keys_2.begin(), ss_keys_2.end() ) );

        const bool show_tile = false;
        if ( show_tile )
        for ( int tile = 0; tile < 4; tile++ )
          {
            int bin_sum = 0;
            printf("T %2d: ",tile);
            const int idx_base = tile * sort_radix;
            for ( int i=0; i<8; i++ )
              {
                printf("%4d  ",sort_tile_histo[idx_base +i]);
              }
            for ( int i=0; i<sort_radix; i++ )
              bin_sum += sort_tile_histo[ idx_base + i ];
            printf("  Sum = %d\n",bin_sum);
          }

        table.entry("T/B","%3d",tiles_per_block);
        table.entry("Me/s","%4.0f", array_size / ( rs_time_s * 1e6 ));

        for ( int pass : { 1, 2 } )
          {
            auto ker = pass == 1 ? kid1 : kid2;
            const size_t work_per_round =
              pass == 1 ? work_per_round_pass_1 : work_per_round_pass_2;
            const size_t comm_bytes =
              pass == 1 ? comm_bytes_pass_1 : comm_bytes_pass_2;
            const double time_s = pass == 1 ? pass_1_time_s : pass_2_time_s;

            table.header_span_start( pass == 1 ? "Pass 1" : "Pass 2");

            table.entry("t/ms", "%6.3f", time_s * 1e3);
            table.entry("GB/s","%5.1f", comm_bytes * 1e-9 / time_s);

            if ( nperf_avail )
              {
                table.entry
                  ("W/Cy","%4.1f",
                   NPerf_metric_value_get("eligible_warps_per_cycle",ker));
                if ( 0 )
                  table.entry
                    ("UIS","%3.0f",
                     NPerf_metric_value_get("issue_slot_utilization", ker));

                if ( pass == 2 )
                  table.entry
                    ("EfSt","%3.0f%%",
                     NPerf_metric_value_get("gst_efficiency",ker));

                table.entry
                  ("IW","%4.1f%",
                   NPerf_metric_value_get("inst_executed", ker)
                   * 32 / ( work_per_round * ndigits ));
              }

            table.header_span_end();
          }

        table.row_end();

        check_sort(block_size,array_size);

      }
  }

  void run_sorts()
  {
    const int block_lg = 10;
    const int block_size = 1 << block_lg;
    const bool nperf_state = NPerf_metrics_off();

    Radix_Sort_Kernels<uint32_t,SO_Ascending,block_lg,8> sua(gpu_info,32);
    Radix_Sort_Kernels<float,SO_Ascending,block_lg,8> sfa(gpu_info,32);
    Radix_Sort_Kernels<uint32_t,SO_Descending,block_lg,8> sud(gpu_info,32);
    Radix_Sort_Kernels<float,SO_Descending,block_lg,8> sfd(gpu_info,32);

    const int n_repeats = 3;

    int n_elts = array_size;

    while ( n_elts > 1 )
      {
        if ( n_elts < 50 ) n_elts = 1;
        vector<double> time_best(4);
        for ( int r = 0; r<n_repeats; r++ )
          {
            for ( Sort_Order order: {SO_Descending,SO_Ascending} )
              for ( bool fp: {false, true} )
                {
                  const int idx = int(order) * 2 + fp;

                // Zero result array and send to GPU, to detect
                // skipped-element errors occurring after a prior invocation
                // wrote the correct results.
                //
                for ( int i=0; i<array_size; i++ )
                  {
                    sort_out_b[i] = sort_out[i] = 0;
                    fkeys_out[i] = 0;
                  }
                CE( cudaMemset( data_out.get_dev_addr(), 0, data_out.chars ) );
                sort_out.to_cuda();
                fkeys_out.to_cuda();

                sort_in.to_cuda(); // Move input array to CUDA.
                fkeys_in.to_cuda(); // Move input array to CUDA.
                data_in.to_cuda(); // Move input array to CUDA.

                event_pair ev_sort;

                Sort_Data* const Do = data_out.get_dev_addr();
                Sort_Data* const di = data_in.get_dev_addr();
                uint32_t* const kui = sort_in.get_dev_addr();
                uint32_t* const kuo = sort_out.get_dev_addr();
                float* const kfi = fkeys_in.get_dev_addr();
                float* const kfo = fkeys_out.get_dev_addr();

                ev_sort.start();
                if ( fp )
                  {
                    if ( order == SO_Ascending )
                      sfa.sort(kfo,Do,kfi,di,n_elts);
                    else sfd.sort(kfo,Do,kfi,di,n_elts);
                  }
                else
                  {
                    if ( order == SO_Ascending )
                      sua.sort(kuo,Do,kui,di,n_elts);
                    else sud.sort(kuo,Do,kui,di,n_elts);
                  }
                ev_sort.end();

                // Retrieve data from GPU.
                //
                sort_out.from_cuda();
                fkeys_out.from_cuda();
                data_out.from_cuda();
                CE( cudaEventSynchronize(ev_sort.e_end) );
                if ( fp )
                  check_sort_fp(block_size,n_elts,order);
                else
                  check_sort(block_size,n_elts,true,order);
                const double dur = ev_sort.elapsed_s();

                if ( r == 0 || dur < time_best[idx] ) time_best[idx] = dur;

                if ( r + 1 < n_repeats ) continue;

                printf("Sort of %7d %s elts %s took %9.6f s,  %6.1f Me/s\n",
                       n_elts, fp ? "FP" : "UI",
                       order == SO_Ascending ? "asc" : "dsc",
                       time_best[idx], 1e-6 * n_elts / time_best[idx] );
              }
          }
        n_elts = n_elts * 0.7;
      }
  }

};


int
main(int argc, char** argv)
{
  Sort sort(argc,argv);
  sort.start();
  return 0;
}
