/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef SORT_CUH
#define SORT_CUH

#undef DEBUG_SORT

#include <stdint.h>

typedef unsigned Sort_Elt;
typedef uint32_t Sort_Data;
const int elt_per_thread = 8;

inline __device__ __host__
int div_ceil(int a, int b){ return ( a + b - 1 ) / b; }

struct Radix_Sort_GPU_Constants
{
  int array_size;               // Number of elements in array.
  int array_size_lg;            // ceil( log_2 ( array_size ) )
  int ndigits;
  int digit_first;

  int elt_per_tile;
  int num_tiles;
  int thisto_array_size_elts;
  int bhisto_array_size_elts;

  int radix_lg;
  int radix;

  Sort_Elt *sort_in, *sort_out, *sort_out_b;
  uint16_t *perm;
  Sort_Data *data_in, *data_out, *data_out_b;
  int *sort_tile_histo;
  int *sort_histo;
};




#endif
