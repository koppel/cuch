#ifndef __HEAP_H__
#define __HEAP_H__

#include "main.h"

#define LCHILD(i) 2*(i)
#define RCHILD(i) 2*(i)+1
#define PARENT(i) (i)/2

typedef struct heap_item
{ 
  nodeid_t node_id;
  weight_t distance;
}heap_item_t;

typedef struct bin_heap
{ 
  size_t num_items;

  //To simplify math, first item in store is the size of the heap and the root is at index 1.
  heap_item_t *store;
}bin_heap_t;

void bin_heap_ref_init(bin_heap_t **heap, size_t **ref, size_t size);
void bin_heap_ref_free(bin_heap_t *heap, size_t *ref);
int bin_heap_ref_add(bin_heap_t *heap, size_t *ref, nodeid_t node_id, weight_t distance);
int bin_heap_ref_get(bin_heap_t *heap, size_t *ref, nodeid_t *node_id, weight_t *distance);
void bin_heap_ref_update(bin_heap_t *heap, size_t *ref, nodeid_t node_id, weight_t distance);


#endif
 
