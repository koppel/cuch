/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __CU_CH_H__
#define __CU_CH_H__

#include "main.h"
#include "graphs.h"

constexpr bool opt_longcuts_watch = true;

const int cu_graph_ud_alloc_mult = 1; // See cu_CH_update_graph_1
const int edge_cache_n_edge_arrays = opt_elist_use ? 5 : 3;

// blocking factor of blocked apsp (do not modify)
#define APSP_BLSZ 64
// blocking factor of query OL (do not modify)
#define OL_BLSZ 16
// number of APSP elements loaded at each iteration of query_OL
#define APSP_SH_DIM 2048
// APSP max size (larger values will halve query_OL occupancy).
#define K_MAX 3328
// C (selection cut-off) minimum (for dynamic C)
#define C_MIN 0.2
#define C_MAX 0.6+0.001
// the 0.001 is to avoid headaches with float error.
#define C_INC 0.05
// Lambda (selection fraction) minimum (for dynamic C)
// To limit level counts
#define LAMBDA_MIN 0.02

constexpr nodeid_t nodeid_invalid = ~nodeid_t(0);

#define div_up(a,b) (((a)+(b)-1)/(b))

#ifdef __CUDACC__
#define QUAL_HOST_DEVICE __host__ __device__
#else
#define QUAL_HOST_DEVICE
#endif

QUAL_HOST_DEVICE inline bool
nodeid_is_special(nodeid_t nid)
{
  // At some point experiment with checking if negative.
  return nid == nodeid_invalid;
}

template<typename T> QUAL_HOST_DEVICE inline bool 
set_min(T& a, T b) { if ( b < a ) { a = b; return true; } return false; };

QUAL_HOST_DEVICE inline int rnd_up(int n, int m){ return m*((n+m-1)/m); };

enum CUDA_Mem_Trace_Opt 
{ CMT_Unset,
  CMT_None,
  CMT_Quiet,     // Remember place where low memory reached. Announce at end.
  CMT_Show_Global_Lows, // Print messages when each new low reached.
  CMT_Show_Line_Lows,   // Print message when a line's new low reached.
  CMT_Show_All          // Show amount of free memory for each TRACE_MEM_USAGE.
};

constexpr CUDA_Mem_Trace_Opt trace_opt_verb[] =
  { CMT_Quiet, CMT_Quiet, CMT_Show_Global_Lows };

void cu_cuda_init();
size_t cuda_mem_free_get();
void cu_cuda_memcheck_at_end();
bool trace_mem_tracing();  // Watching amount of memory, might not print.
bool trace_mem_showing(); // Printing messages during execution.
void trace_mem_usage_sample(const char* file_name, int line_number);

#define TRACE_MEM_USAGE() \
  trace_mem_usage_sample(__FILE__,__LINE__);

extern APSP_Bench_Info apsp_bench_info; // Instantiated in graphs.cpp.

std::string
shadow_wps
(cu_graph_CH_bi_t *g_d, nodeid_t *selected_list,
 cu_shc_CH_set *shc_set, cu_CH_stats_t *stats, uint32_t iter);
std::string
ch_edge_cull
(cu_graph_CH_bi_t *g_d, nodeid_t *selected_list,
 cu_CH_stats_t *stats,
 uint32_t iter);

void graph_cu_CH_bi_h2d(cu_graph_CH_bi_t **cu_graph_CH_d, cu_graph_CH_bi_t *cu_graph_CH_h);
void graph_cu_CH_bi_d2h(cu_graph_CH_bi_t **cu_graph_CH_h, cu_graph_CH_bi_t *cu_graph_CH_d);

// If graph is NULL free all cached graphs.
void graph_cu_CH_bi_free_device(cu_graph_CH_bi_t *graph = NULL);

// needed for testing graph_b_removed after the extraction kernel
void graph_cu_CH_d2h(cu_graph_CH_t **cu_graph_CH_h, cu_graph_CH_t *cu_graph_CH_d);


// init an empty cu_graph_CH_t on device (no edges, only num_nodes placeholder nodes) len=max_num_edges
void graph_cu_CH_init_d(cu_graph_CH_t **cu_graph_CH, uint32_t num_nodes, uint32_t len, uint32_t max_nnbr_init, bool UD);
// init an empty cu_graph_CH_bi_t on device (similar to graph_cu_CH_init_d)
void graph_cu_CH_bi_init_d
(cu_graph_CH_bi_t **cu_graph_CH_bi, nodeid_t num_nodes, nodeid_t n_edges);
void graph_cu_CH_bi_edges_realloc
(cu_graph_CH_bi_t *cu_graph_CH_bi_d, nodeid_t n_edges, int dir = 3);
void
graph_cu_CH_bi_edges_reuse_fwd_for_bwd
(cu_graph_CH_bi_t *graph_new, cu_graph_CH_bi_t *graph_old, nodeid_t n_edges);
void
graph_cu_CH_bi_edges_reuse_bwd_for_fwd
(cu_graph_CH_bi_t *graph_new, cu_graph_CH_bi_t *graph_old);

// note that for this initialization, edge_list_length and max_rank should be (over)estimates since
// unlike the CPU version where UD would be written at the end, in the GPU version, the graph is
// filled as the algorithm progresses, also, here node ranks and node levels are assumed to be the
// same since results from the CPU code show that redoing the levels has only minimal impact, so the
// node_ranks_u/d are redundant.
enum UD_Purpose { UDP_Contract, UDP_Compress };
void graph_cu_CH_UD_init_d
(cu_graph_CH_UD_t **graph_cu_UD_d, 
 nodeid_t num_nodes, size_t edge_list_length, nodeid_t max_rank, 
 nodeid_t K, nodeid_t overlay_edge_list_length, UD_Purpose purpose);

void
graph_cu_CH_UD_alloc_more(cu_graph_CH_UD_t& graph_ud, uint32_t K);

void graph_cu_CH_UD_d2h(cu_graph_CH_UD_t **cu_graph_UD_h, cu_graph_CH_UD_t *cu_graph_UD_d);
void graph_cu_CH_UD_h2d(cu_graph_CH_UD_t **cu_graph_UD_d, cu_graph_CH_UD_t *cu_graph_UD_h);

enum Path_Init_Arrays
  { PI_Dist_Array_Prev_Array, PI_Dist_Prev_Array, PI_Dist_Array };
void path_init_d
(path_t& path_hd, path_t*& path_d, nodeid_t num_nodes, nodeid_t src,
 Path_Init_Arrays a = PI_Dist_Array_Prev_Array );

void path_d2h(path_t& path_h, const path_t& path_dh);
void path_free_h(path_t& path_h);
void path_free_d(path_t& path_hd, path_t*& path_d);



void graph_cu_UD_free_device(cu_graph_CH_UD_t*& graph_UD_d);

void graph_UD_dump(FILE *f, cu_graph_CH_UD_t *graph);

int graph_UD_get_from_file(cu_graph_CH_UD_t **graph, FILE *f);

void graph_UD_bin_dump(FILE *f, cu_graph_CH_UD_t *graph);

int graph_UD_bin_get_from_file(cu_graph_CH_UD_t **graph, FILE *f);

void graph_UD_finish(cu_graph_CH_UD_t *graph_h);


#endif
 
