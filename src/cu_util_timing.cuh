/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef CU_UTIL_TIMING_CUH
#define CU_UTIL_TIMING_CUH

#include <cuda.h>
#include <assert.h>

#include "cu_util_timing.h"

inline double
elapsed_time_get_ms(const cudaEvent_t& e1, const cudaEvent_t& e2)
{
  float dur_ms;
  CE( cudaEventElapsedTime(&dur_ms, e1, e2) );
  return dur_ms;
}

inline double
elapsed_time_get(const cudaEvent_t& e1, const cudaEvent_t& e2)
{
  return elapsed_time_get_ms(e1,e2) * .001;
}

class cuda_event {
public:
  cuda_event(){ CE( cudaEventCreate(&e) ); }
  ~cuda_event(){ CE( cudaEventDestroy(e) ); }
  cuda_event& record(cudaStream_t stream = 0)
  { CE( cudaEventRecord(e,stream) ); return *this; }
  cuda_event& sync() { CE( cudaEventSynchronize(e) ); return *this; }
  operator cudaEvent_t () { return e; };
private:
  cudaEvent_t e;
};

class event_pair {
public:
  event_pair():state(s_new),dur_valid(false){}
  ~event_pair(){ assert( dur_valid || state == s_new ); }
  void start(cudaStream_t str = 0)
  {
    assert( state != s_running );
    stream = str;
    state = s_running;
    dur_valid = false;
    e_start.record(stream);
  }
  void end() {
    assert( state == s_running );
    e_end.record(stream);
    state = s_ran;
  }
  void sync() {
    assert( state == s_ran );
    e_end.sync();
  }
  double elapsed_s()
  {
    if ( !dur_valid )
      {
        assert( state == s_ran );
        sync();
        dur_s = elapsed_time_get(e_start,e_end);
        dur_valid = true;
      }
    return dur_s;
  }
  double elapsed_ms() { return 1e3 * elapsed_s(); }
  double elapsed_us() { return 1e6 * elapsed_s(); }

  cuda_event e_start, e_end;
  enum State { s_new, s_running, s_ran } state;
  cudaStream_t stream;
  double dur_s;
  bool dur_valid;
};

#endif
