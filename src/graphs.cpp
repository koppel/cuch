/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <cstring>
#include <assert.h>
#include <map>
#include <unordered_map>
#include <math.h>
#include "graphs.h"
#include "heap.h"
#include "APSP.h"
#include "cu_CH.h"
#include "cu_util_timing.h"

using namespace std;

static constexpr bool show_timing = false;
static constexpr bool show_weight_sum = false;

//
// path methods
//

void path_init(path_t **path, size_t num_nodes)
{
  size_t i;

  *path = (path_t *) malloc(sizeof(path_t));
  (*path)->num_nodes = num_nodes;
  (*path)->storage = nullptr;
  (*path)->src = NODEID_NULL;
  (*path)->dest = NODEID_NULL;
  (*path)->dist_prev = nullptr;
  (*path)->weight = (weight_t *) malloc(num_nodes*sizeof(weight_t));
  (*path)->prev = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));

  for ( i = 0; i < num_nodes; i++ )
    {
      (*path)->weight[i] = WEIGHT_INF;
      (*path)->prev[i] = NODEID_NULL;
    }
}

void path_free(path_t *path)
{
  if ( path->storage )
    free(path->storage);
  else
    {
      free(path->weight);
      free(path->prev);
    }
  free(path);
}

void path_dump(FILE *fp, path_t *path)
{
  size_t i;

  fprintf(fp, "%u, %u, %u\n", path->num_nodes, path->src, path->dest);
  for ( i = 0; i < path->num_nodes; i++ ){
    if ( !path->prev || path->prev[i] == NODEID_NULL )
      fprintf(fp, "-:%"FNODEID",%"FWEIGHT"\n", (nodeid_t)i, path->weight[i]);
    else
      fprintf(fp, "%"FNODEID":%"FNODEID",%"FWEIGHT"\n", path->prev[i], (nodeid_t)i, path->weight[i]);
  }
}

int path_get_from_file(path_t **path, FILE *f)
{
  size_t i;
  uint32_t num_nodes;
  uint32_t src, dest;
  char instr[256];

  if ( f == NULL ) return -1;

  ASSERTA(fscanf(f, "%u, %u, %u\n", &num_nodes, &src, &dest)==3);
  path_init(path, num_nodes);
  (*path)->src = src;
  (*path)->dest = dest;

  for ( i = 0; i < num_nodes; i++ )
    {
      nodeid_t ref, nodeid, weight;

      ASSERTA(!feof(f));

      //ASSERTA(fscanf(f, "%"SNODEID":%"SNODEID",%"SWEIGHT"\n", ref, &nodeid, &weight)==3);
      ASSERTA(fgets(instr, sizeof(instr), f) != NULL);
      if (instr[0] == '-')
        {
           sscanf(instr, "-:%"SNODEID",%"SWEIGHT, &nodeid, &weight);
           (*path)->prev[i] = NODEID_NULL;
           (*path)->weight[i] = weight;
        }
      else
        {
           sscanf(instr, "%"SNODEID":%"SNODEID",%"SWEIGHT, &ref, &nodeid, &weight);
           (*path)->prev[i] = ref;
           (*path)->weight[i] = weight;
        }

    }

  return 0;
}

//
// APSP_path methods
//

void APSP_path_init(APSP_path_t **path, size_t num_nodes)
{

  *path = (APSP_path_t *) malloc(sizeof(path_t));
  (*path)->num_nodes = num_nodes;
  (*path)->dist = (weight_t *) malloc(num_nodes*num_nodes*sizeof(weight_t));
  (*path)->prev = (nodeid_t *) malloc(num_nodes*num_nodes*sizeof(nodeid_t));
  (*path)->dist_pt = (weight_t **) malloc(num_nodes*sizeof(weight_t *));
  (*path)->prev_pt = (nodeid_t **) malloc(num_nodes*sizeof(nodeid_t *));

  for(size_t i=0; i<num_nodes; i++){
    (*path)->dist_pt[i] = &((*path)->dist[i*num_nodes]);
    (*path)->prev_pt[i] = &((*path)->prev[i*num_nodes]);
    for(size_t j=0; j<num_nodes; j++){
      (*path)->dist_pt[i][j] = WEIGHT_INF;
      (*path)->prev_pt[i][j] = NODEID_NULL;
      if(i==j){
	(*path)->dist_pt[i][j] = 0;
      }
    }
  }
}

void APSP_path_dump(FILE *fp, APSP_path_t *path)
{

  fprintf(fp, "%u\n", path->num_nodes);
  for (size_t i=0; i<path->num_nodes; i++){
    for(size_t j=0; j<path->num_nodes; j++){
      if ( path->prev_pt[i][j] == NODEID_NULL ){
	fprintf(fp, "-:%"FNODEID"->%"FNODEID",%"FWEIGHT"\n", (nodeid_t)i, (nodeid_t)j, path->dist_pt[i][j]);
      } else {
	fprintf(fp, "%"FNODEID":%"FNODEID"->%"FNODEID",%"FWEIGHT"\n", path->prev_pt[i][j], (nodeid_t)i, (nodeid_t)j, path->dist_pt[i][j]);
      }
    }
  }
}

int APSP_path_get_from_file(APSP_path_t **path, FILE *f)
{
  size_t i;
  size_t num_nodes;
  char instr[256];

  if ( f == NULL ) return -1;

  ASSERTA(fscanf(f, "%zu\n", &num_nodes)==1);
  APSP_path_init(path, num_nodes);

  for ( i = 0; i < num_nodes*num_nodes; i++ )
    {
      nodeid_t ref, node_i, node_j;
      weight_t weight;

      ASSERTA(!feof(f));

      ASSERTA(fgets(instr, sizeof(instr), f) != NULL);
      if (instr[0] == '-')
        {
	  sscanf(instr, "-:%"SNODEID"->%"SNODEID",%"SWEIGHT, &node_i, &node_j, &weight);
	  (*path)->prev_pt[node_i][node_j] = NODEID_NULL;
	  (*path)->dist_pt[node_i][node_j] = weight;
        }
      else
        {
	  sscanf(instr, "%"SNODEID":%"SNODEID"->%"SNODEID",%"SWEIGHT, &ref, &node_i, &node_j, &weight);
	  (*path)->prev_pt[node_i][node_j] = ref;
	  (*path)->dist_pt[node_i][node_j] = weight;
        }

    }

  return 0;
}

void APSP_path_free(APSP_path_t *path)
{
  free(path->dist_pt);
  free(path->prev_pt);
  free(path->dist);
  free(path->prev);
  free(path);
}


//
// graph_st methods
//

bool graph_st_get_norm_from_file(graph_st_t **graph_st, FILE *f)
{
  size_t i, j;
  size_t k = 0;

  Time_Wall_s timer;

  bool need_clean = false;

  graph_st_t *graph_out = (graph_st_t *) malloc(sizeof(graph_st_t));

  ASSERTA(fscanf(f, "%"SNODEID",%"SNODEID"\n", &graph_out->num_nodes, &graph_out->num_edges)==2);

  graph_out->neighbors_pt = (nodeid_t **) malloc(graph_out->num_nodes*sizeof(nodeid_t *));
  graph_out->weights_pt = (weight_t **) malloc(graph_out->num_nodes*sizeof(weight_t *));
  graph_out->num_neighbors = (nnbor_t *) malloc(graph_out->num_nodes*sizeof(nnbor_t));
  graph_out->neighbors = (nodeid_t *) malloc(graph_out->num_edges*sizeof(nodeid_t));
  graph_out->weights = (weight_t *) malloc(graph_out->num_edges*sizeof(weight_t));

  double weight_sum = 0;

  for ( i = 0; i < graph_out->num_nodes; i++ ){
    nodeid_t nodeid;
    nnbor_t nn;

    ASSERTA(!feof(f));

    ASSERTA(fscanf(f, "%"SNODEID",%"SNBOR"\n", &nodeid, &nn) == 2);

    assert( nodeid < graph_out->num_nodes );
    graph_out->num_neighbors[nodeid] = nn;

    graph_out->weights_pt[nodeid] = &graph_out->weights[k];

    graph_out->neighbors_pt[nodeid] = &graph_out->neighbors[k];

    map<nodeid_t,int> seen_dests;

    for ( j = 0; j < nn; j++ ){
      nodeid_t nborid, nbor;
      weight_t weight;

      ASSERTA(fscanf(f, "%"SNODEID":%"SNODEID",%"SWEIGHT"\n", &nborid, &nbor, &weight) == 3);

      assert( nborid == j );
      assert( nbor < graph_out->num_nodes );
      // Can't have self edges.
      if( nbor == nodeid ){
	need_clean = true;
      }
      // Can't have multiple edges to same dest.
      if( seen_dests[nbor]++ ){
	need_clean = true;
      }

      graph_out->neighbors_pt[nodeid][nborid] = nbor;
      graph_out->weights_pt[nodeid][nborid] = weight;
      weight_sum += weight;
      assert( k < graph_out->num_edges );
      k++;
    }
  }

  *graph_st = graph_out;

  if ( show_weight_sum )
    pr.user("Weight sum: %.20g (Need to clean %d)\n",weight_sum,need_clean);

  if ( show_timing )
    printf("Time to read norm graph %.6f s\n", timer.s());

  return need_clean;
}


bool graph_st_get_gr_from_file(graph_st_t **graph_st, FILE *f)
{
  size_t i = 0;
  char c;
  bool sp_found = false;
  nodeid_t *edge_list_source = NULL;
  nodeid_t *edge_list_destination = NULL;
  weight_t *edge_list_weight = NULL;
  vector<nnbor_t> nnbr;
  size_t duplicate_edges = 0, new_low_dups = 0, n_self_edges = 0;

  graph_st_t *graph_out = (graph_st_t *) malloc(sizeof(graph_st_t));

  Time_Wall_s timer;
  unordered_map<Ege,size_t> edge_idx;

  nodeid_t n_nodes_expected = 0, n_edges_expected = 0;

  while(EOF!=(c=fgetc(f))){
    if(c=='c'){
      // comment line, continue to next line
      while(fgetc(f)!='\n');
      
    } else if(c=='p'){
      // p sp line, contains num_nodes and num_edges (should happen only once)
      ASSERTA(!sp_found);
      ASSERTA(fscanf(f, " sp %"SNODEID" %"SNODEID"\n",
                     &n_nodes_expected, &n_edges_expected )==2);
      sp_found = true;
      
      // allocate the graph arrays
      graph_out->neighbors_pt = (nodeid_t **) malloc(n_nodes_expected*sizeof(nodeid_t *));
      graph_out->weights_pt = (weight_t **) malloc(n_nodes_expected*sizeof(weight_t *));
      graph_out->num_neighbors = (nnbor_t *) calloc(n_nodes_expected, sizeof(nnbor_t));
      graph_out->neighbors = (nodeid_t *) malloc(n_edges_expected*sizeof(nodeid_t));
      graph_out->weights = (weight_t *) malloc(n_edges_expected*sizeof(weight_t));
      // temporary storage for edge list.
      edge_list_source = (nodeid_t *) malloc(n_edges_expected*sizeof(nodeid_t));
      edge_list_destination = (nodeid_t *) malloc(n_edges_expected*sizeof(nodeid_t));
      edge_list_weight = (weight_t *) malloc(n_edges_expected*sizeof(weight_t));
      nnbr.resize(n_nodes_expected);
      
    } else if(c=='a'){
      ASSERTA( (edge_list_source != NULL) && (edge_list_destination != NULL) &&
	       (edge_list_weight != NULL) && !nnbr.empty() );
      // a line, contains an edge

      Ege e;
      weight_t wht;

      ASSERTA(fscanf(f, " %"SNODEID" %"SNODEID" %"SWEIGHT"\n",
                     &e.src, &e.dst, &wht ) == 3 );

      if ( e.src == e.dst ) { n_self_edges++;  continue; }

      // decrement the node IDs so they start from 0 instead of 1
      e.src--; e.dst--;

      ASSERTA( e.src < n_nodes_expected && e.dst < n_nodes_expected );
      ASSERTA( e.src != e.dst );

      if ( edge_idx.count(e) )
        {
          duplicate_edges++;
          if ( set_min( edge_list_weight[edge_idx[e]], wht ) ) new_low_dups++;
          continue;
        }

      edge_list_source[i] = e.src;
      edge_list_destination[i] = e.dst;
      edge_list_weight[i] = wht;
      edge_idx[e] = i;

      // increment nnbr of source
      nnbr[edge_list_source[i]]++;
      i++;
      ASSERTA( i <= n_edges_expected );
    } else {
      // corrupt file
      ASSERTA(false);
    }
  }

  const size_t n_edges_found = i;
  graph_out->num_nodes = n_nodes_expected;
  graph_out->num_edges = n_edges_found;

  if ( show_weight_sum )
    {
      double wht_sum = 0;
      for ( size_t j=0; j<n_edges_found; j++ ) wht_sum += edge_list_weight[j];
      pr.user("Weight sum: %.20g\n",wht_sum);
    }

  ASSERTA( n_edges_found + duplicate_edges + n_self_edges == n_edges_expected );
  pr.tune("Found %zu self edges and %zu duplicate edges, "
         "updated %zu original edge weights with new lows.\n",
         n_self_edges, duplicate_edges, new_low_dups);

  // set the pointers for each node based on nnbr
  graph_out->neighbors_pt[0] = &graph_out->neighbors[0];
  graph_out->weights_pt[0] = &graph_out->weights[0];
  for(i = 1; i<graph_out->num_nodes; i++){
    graph_out->neighbors_pt[i] = graph_out->neighbors_pt[i-1] + nnbr[i-1];
    graph_out->weights_pt[i] = graph_out->weights_pt[i-1] + nnbr[i-1];
  }
  
  // write the edge list to output
  for(i = 0; i<graph_out->num_edges; i++){
    graph_out->neighbors_pt[edge_list_source[i]][graph_out->num_neighbors[edge_list_source[i]]] = edge_list_destination[i];
    graph_out->weights_pt[edge_list_source[i]][graph_out->num_neighbors[edge_list_source[i]]] = edge_list_weight[i];
    graph_out->num_neighbors[edge_list_source[i]]++;
  }
  
  for(i = 0; i<graph_out->num_nodes; i++){
    ASSERTA(nnbr[i] == graph_out->num_neighbors[i]);
  }

  free(edge_list_source);
  free(edge_list_destination);
  free(edge_list_weight);

  *graph_st = graph_out;

  if ( show_timing )
    pr.tune("Time to read gr graph %.6f s\n", timer.s());

  return false;
}

int graph_st_remove_duplicates_self_loops(graph_st_t *graph)
{
  size_t i, j, k, l, num_dups_combined, dups;
  uint8_t *marked = (uint8_t *) calloc(graph->num_edges, sizeof(uint8_t));

  // removes self-loops regardless of weight
  // removes duplicates without comparing weight
  num_dups_combined = 0;
  l=0;
  for(i=0; i<graph->num_nodes; i++){
    dups=0;
    for(j=0; j<graph->num_neighbors[i]; j++){
      if(graph->neighbors_pt[i][j]==i){
	marked[l]=1;
	dups++;
      }
      for(k=0; k<j; k++){
	if((!marked[l]) && (graph->neighbors_pt[i][j]==graph->neighbors_pt[i][k])){
	  marked[l]=1;
	  dups++;
	}
      }
      l++;
    }
    graph->num_neighbors[i] -= dups;
    num_dups_combined += dups;
  }

  ASSERTA(l == graph->num_edges);

  nodeid_t *new_neighbors = (nodeid_t *) malloc((graph->num_edges - num_dups_combined)*sizeof(nodeid_t));
  weight_t *new_weights = (weight_t *) malloc((graph->num_edges - num_dups_combined)*sizeof(weight_t));

  l=0;
  for(i=0; i<graph->num_edges; i++){
    if(!marked[i]){
      new_weights[l] = graph->weights[i];
      new_neighbors[l] = graph->neighbors[i];
      l++;
    }
  }

  ASSERTA(l == graph->num_edges - num_dups_combined);

  free(graph->neighbors);
  free(graph->weights);
  
  graph->num_edges=l;
  graph->neighbors = new_neighbors;
  graph->weights = new_weights;

  k=0;
  for(i=0; i<graph->num_nodes; i++){
    graph->weights_pt[i] = &graph->weights[k];
    graph->neighbors_pt[i] = &graph->neighbors[k];
    k += graph->num_neighbors[i];
  }

  free(marked);

  return num_dups_combined;
} 

void graph_st_get_backwards(graph_st_t **graph_st_b, graph_st_t *graph_st_f)
{
  size_t i, j;
  size_t k = 0;

  nnbor_t *idx = (nnbor_t *) calloc(graph_st_f->num_nodes, sizeof(nnbor_t));

  graph_st_t *graph_out = (graph_st_t *) malloc(sizeof(graph_st_t));

  graph_out->num_nodes = graph_st_f->num_nodes;
  graph_out->num_edges = graph_st_f->num_edges;

  graph_out->neighbors_pt = (nodeid_t **) malloc(graph_out->num_nodes*sizeof(nodeid_t *));
  graph_out->weights_pt = (weight_t **) malloc(graph_out->num_nodes*sizeof(weight_t *));
  graph_out->num_neighbors = (nnbor_t *) calloc(graph_out->num_nodes, sizeof(nnbor_t));
  graph_out->neighbors = (nodeid_t *) malloc(graph_out->num_edges*sizeof(nodeid_t));
  graph_out->weights = (weight_t *) malloc(graph_out->num_edges*sizeof(weight_t));

  for ( i = 0; i < graph_out->num_edges; i++ ){
    graph_out->num_neighbors[graph_st_f->neighbors[i]]++;
  }

  for ( i = 0; i < graph_out->num_nodes; i++ ){
    graph_out->weights_pt[i] = &graph_out->weights[k];
    graph_out->neighbors_pt[i] = &graph_out->neighbors[k];
    k += graph_out->num_neighbors[i];
  }

  for ( i = 0; i < graph_st_f->num_nodes; i++ ){
    for ( j = 0; j < graph_st_f->num_neighbors[i]; j++ ){
      graph_out->neighbors_pt[graph_st_f->neighbors_pt[i][j]][idx[graph_st_f->neighbors_pt[i][j]]] = i;
      graph_out->weights_pt[graph_st_f->neighbors_pt[i][j]][idx[graph_st_f->neighbors_pt[i][j]]] = graph_st_f->weights_pt[i][j];
      idx[graph_st_f->neighbors_pt[i][j]]++;
    }
  }

  for ( i = 0; i < graph_st_f->num_nodes; i++ ){
    ASSERTA(idx[i] == graph_out->num_neighbors[i]);
  }

  *graph_st_b = graph_out;

}

void graph_st_get_stats(graph_st_t *graph)
{
  nodeid_t *degr_in = (nodeid_t *) calloc(graph->num_nodes, sizeof(nodeid_t));
  nodeid_t *degr_out= (nodeid_t *) calloc(graph->num_nodes, sizeof(nodeid_t));
  nodeid_t *idx_in = (nodeid_t *) malloc(graph->num_nodes*sizeof(nodeid_t));
  nodeid_t *idx_out= (nodeid_t *) malloc(graph->num_nodes*sizeof(nodeid_t));
  double degr_in_mean=0;
  double degr_out_mean=0;
  uint32_t degr_in_min, degr_in_max, degr_out_min, degr_out_max, weight_min, weight_max;

  weight_max = 0;
  weight_min = WEIGHT_INF;

  for(size_t i=0; i<graph->num_nodes; i++){
    idx_in[i] = i;
    idx_out[i] = i;
    degr_out[i]=graph->num_neighbors[i];
    for(size_t j=0; j<graph->num_neighbors[i]; j++){
      degr_in[graph->neighbors_pt[i][j]]++;
      if((weight_min > graph->weights_pt[i][j]) && (graph->weights_pt[i][j]!=0)){
	weight_min = graph->weights_pt[i][j];
      }
      if(weight_max < graph->weights_pt[i][j]){
	weight_max = graph->weights_pt[i][j];
      }
    }
  }

  nodeid_t *work = (nodeid_t *) malloc(2*graph->num_nodes*sizeof(nodeid_t));
  merge_sort_ref(degr_in, idx_in, work, &work[graph->num_nodes], graph->num_nodes);
  merge_sort_ref(degr_out, idx_out, work, &work[graph->num_nodes], graph->num_nodes);
  free(work);

  for(size_t i=0; i<graph->num_nodes; i++){
    degr_in_mean += degr_in[i];
    degr_out_mean += degr_out[i];
  }
  degr_in_mean /= graph->num_nodes;
  degr_out_mean /= graph->num_nodes;
  degr_in_min=degr_in[0];
  degr_out_min=degr_out[0];
  degr_in_max=degr_in[graph->num_nodes-1];
  degr_out_max=degr_out[graph->num_nodes-1];

  printf("in-degree: mean=%f, min=%d, max=%d\n",
	 degr_in_mean, degr_in_min, degr_in_max);
  printf("out-degree: mean=%f, min=%d, max=%d\n",
	 degr_out_mean, degr_out_min, degr_out_max);
  printf("min_edge_weight(non_zero)=%d, max_edge_weight=%d\n", weight_min, weight_max);

  free(degr_in);
  free(degr_out);
  free(idx_in);
  free(idx_out);
}

void graph_st_free(graph_st_t *graph)
{
  free(graph->num_neighbors);
  free(graph->neighbors_pt);
  free(graph->weights_pt);
  free(graph->neighbors);
  free(graph->weights);
  free(graph);
}

size_t graph_st_sizeof(graph_st_t *graph)
{
  size_t ret = 0;

  ret += sizeof(graph_st_t);
  ret += sizeof(nnbor_t)*graph->num_nodes;
  ret += sizeof(nodeid_t *)*graph->num_nodes;
  ret += sizeof(weight_t *)*graph->num_nodes;
  ret += sizeof(nodeid_t)*graph->num_edges;
  ret += sizeof(weight_t)*graph->num_edges;

  return ret;
}

void graph_st_norm_dump(FILE *f, graph_st_t *graph_st)
{
  size_t i, j;

  fprintf(f, "%"SNODEID",%"SNODEID"\n", graph_st->num_nodes, graph_st->num_edges);

  for ( i = 0; i < graph_st->num_nodes; i++ ){
    fprintf(f, "%"FNODEID",%"FNBOR"\n", (nodeid_t)i, graph_st->num_neighbors[i]);

    for ( j = 0; j < graph_st->num_neighbors[i]; j++ ){
      fprintf(f, "%zu:%"FNODEID",%"FWEIGHT"\n", j, 
	      graph_st->neighbors_pt[i][j],
	      graph_st->weights_pt[i][j]);

    }

  }
}


//
// sort methods
//

void merge_sort_ref(nodeid_t *list, nodeid_t *ref, nodeid_t *work_l, nodeid_t *work_r, nodeid_t length)
{

  if(length==1 || length ==0){
    return;
  }
  merge_sort_ref(list, ref, work_l, work_r, length>>1);
  merge_sort_ref(&list[length>>1], &ref[length>>1], &work_l[length>>1], &work_r[length>>1], length-(length>>1));

  for(size_t i=0; i<length; i++){
    work_l[i] = list[i];
    work_r[i] = ref[i];
  }

  size_t l=0;
  size_t r=length>>1;

  for(size_t i=0; i<length; i++){
    if(l==length>>1){
      list[i] = work_l[r];
      ref[i] = work_r[r];
      r++;
    } else if(r==length){
      list[i] = work_l[l];
      ref[i] = work_r[l];
      l++;
    } else if(work_l[l]<work_l[r]){
      list[i] = work_l[l];
      ref[i] = work_r[l];
      l++;
    } else {
      ASSERTA(work_l[l]>=work_l[r])
      list[i] = work_l[r];
      ref[i] = work_r[r];
      r++;
    }
  }

}

//
// write a simple array to disk (mainly a debugging/testing function)
//

void array_dump(FILE *f, uint32_t *A, uint32_t len)
{
  fprintf(f, "%u\n", len);
  for(size_t i=0; i<len; i++){
    fprintf(f, "%u\n", A[i]);
  }
}

void array_dump_f(FILE *f, float *A, uint32_t len)
{
  fprintf(f, "%u\n", len);
  for(size_t i=0; i<len; i++){
    fprintf(f, "%.21g\n", A[i]);
  }
}

uint32_t array_load(FILE *fp, uint32_t **A)
{
  uint32_t len;
  ASSERTA(fscanf(fp, "%u\n", &len)==1);
  uint32_t *A_out = (uint32_t *) malloc(len*sizeof(uint32_t));
  for(size_t i=0; i<len; i++){
    ASSERTA(fscanf(fp, "%u\n", &(A_out[i]))==1);
  }
  *A = A_out;
  return len;
}

uint32_t array_load_f(FILE *fp, float **A)
{
  uint32_t len;
  ASSERTA(fscanf(fp, "%u\n", &len)==1);
  float *A_out = (float *) malloc(len*sizeof(float));
  for(size_t i=0; i<len; i++){
    ASSERTA(fscanf(fp, "%f\n", &(A_out[i]))==1);
  }
  *A = A_out;
  return len;
}

bool
APSP_Bench_Info::load()
{
  len = K_MAX / APSP_BLSZ;
  string host_file_name = filename_make();
  vector<string> fnames({ host_file_name } );
  if ( !chopts.K_bm_autogen ) fnames.push_back("APSP_default.bench");
  vector<fspath> dirs({cuch_exe_dir,boost::filesystem::current_path()});

  auto look =
    [&]()
    { for ( fspath& d: dirs )
        for ( string& n: fnames )
          {
            fspath ptry = d / n;
            if ( boost::filesystem::exists(ptry) ) return ptry;
          }
      return fspath();
    };
  bm_path = look();
  if ( bm_path.empty() ) return false;

  FILE* file_APSP_bench = fopen(bm_path.native().c_str(), "r");
  if ( file_APSP_bench == nullptr ) return false;

  pr.user("Reading APSP timing data from %s\n",
          relative(bm_path).native().c_str());

  int len2 = array_load_f(file_APSP_bench, &times);
  fclose(file_APSP_bench);
  assert( len2 == len );
  inited = true;
  return true;
}

APSP_Bench_Info apsp_bench_info;

//
// graph_CH methods
//

graph_st_t*
graph_st_get_from_file(File_Open_Read& file_open_read)
{
  pr.user("Reading graph from %s\n",file_open_read.path_opened_get().c_str());
  FILE* const f = file_open_read;
  graph_st_t *graph_st_f;

  // Determine type of file.
  bool need_clean = false;
  if ( file_open_read.ext_match("el") ){
    need_clean = graph_st_get_norm_from_file(&graph_st_f, f);
  } else if ( file_open_read.ext_match("gr") ) {
    need_clean = graph_st_get_gr_from_file(&graph_st_f, f);
  } else {
    pr.fatal_user("Couldn't read graph, unrecognized file type.\n");
    return nullptr;
  }
  if( need_clean ){
    int dups = graph_st_remove_duplicates_self_loops(graph_st_f);
    pr.warn("errors detected in input graph, removed %d duplicate or self-loop edges\n", dups);
  }
  return graph_st_f;
}

int
graph_CH_get_from_file(graph_CH_bi_t **graph, File_Open_Read& file_open_read)
{
  graph_st_t *graph_st_f = graph_st_get_from_file(file_open_read);

  graph_st_t *graph_st_b;
  graph_CH_t *graph_CH_f;
  graph_CH_t *graph_CH_b;
  graph_CH_bi_t *graph_CH_bi = (graph_CH_bi_t *) malloc(sizeof(graph_CH_bi_t));

  graph_st_get_backwards(&graph_st_b, graph_st_f);

  graph_st_2_CH(&graph_CH_f, graph_st_f);
  graph_st_2_CH(&graph_CH_b, graph_st_b);

  graph_CH_bi->num_nodes = graph_CH_f->num_nodes;
  graph_CH_bi->graph_f = *graph_CH_f;
  graph_CH_bi->graph_b = *graph_CH_b;
  graph_CH_bi->node_ranks = (nodeid_t *) calloc(graph_CH_bi->num_nodes, sizeof(nodeid_t));
  graph_CH_bi->max_degree = 0;
  graph_CH_bi->mean_degree = 0;
  graph_CH_bi->max_weight = graph_CH_bi->graph_f.max_weight;
  graph_CH_bi->mean_weight = 0;
  graph_CH_bi->overlay_size = graph_CH_f->num_nodes;
  graph_CH_bi->max_rank = 0;
  double sum_weight = 0;
  size_t sum_degree = 0;
  size_t sum_degree_b = 0;
  for (size_t i=0; i<graph_CH_bi->num_nodes; i++){
    // in very large graphs floating point inaccuracies can result in incorrect averages, however
    // since we don't need very accurate average values, it should be fine.
    // it is possible to use either a more accurate average algorithm, or use a random sampling
    sum_degree += graph_CH_bi->graph_f.nodes[i].num_neighbors;
    sum_degree_b += graph_CH_bi->graph_b.nodes[i].num_neighbors;
    for(size_t j=0; j<graph_CH_bi->graph_f.nodes[i].num_neighbors; j++){
      sum_weight += graph_CH_bi->graph_f.nodes[i].weights[j];
    }
    if (graph_CH_bi->max_degree < graph_CH_bi->graph_f.nodes[i].num_neighbors){
      graph_CH_bi->max_degree = graph_CH_bi->graph_f.nodes[i].num_neighbors;
      graph_CH_bi->max_degree_overlay = graph_CH_bi->max_degree;
    }
    if (graph_CH_bi->max_degree < graph_CH_bi->graph_b.nodes[i].num_neighbors){
      graph_CH_bi->max_degree = graph_CH_bi->graph_b.nodes[i].num_neighbors;
      graph_CH_bi->max_degree_overlay = graph_CH_bi->max_degree;

    }
  }
  assert( sum_degree == sum_degree_b );
  graph_CH_bi->graph_f.num_edges = sum_degree;
  graph_CH_bi->graph_b.num_edges = sum_degree_b;

  // note that both mean and max degree are with respect to either in or out
  // degree. however while mean is the same for both in and out degree, max
  // of one can be higher than the other.
  graph_CH_bi->mean_degree = double(sum_degree) / graph_CH_bi->num_nodes;
  graph_CH_bi->mean_weight = sum_weight / sum_degree;
  graph_CH_bi->mean_degree_overlay = graph_CH_bi->mean_degree;

  graph_st_free(graph_st_f);
  graph_st_free(graph_st_b);

  *graph = graph_CH_bi;

  pr.user
    ("Graph has %zu nodes, %zu edges, mean deg %.2f.\n",
     graph_CH_bi->num_nodes, sum_degree, graph_CH_bi->mean_degree);

  return 1;
}


void graph_st_2_CH(graph_CH_t **graph_CH, graph_st_t *graph_st)
{

  graph_CH_t *graph_out = (graph_CH_t *) malloc(sizeof(graph_CH_t));

  graph_out->num_nodes = graph_st->num_nodes;
  graph_out->max_weight = 0;

  graph_out->nodes = (node_CH_t *) malloc(graph_st->num_nodes*sizeof(node_CH_t));

  for (size_t i=0; i<graph_st->num_nodes; i++){
    graph_out->nodes[i].num_neighbors = graph_st->num_neighbors[i];
    graph_out->nodes[i].num_neighbors_overlay = graph_out->nodes[i].num_neighbors;

    // initial list length == num_neighbors
    graph_out->nodes[i].llen = graph_out->nodes[i].num_neighbors = graph_st->num_neighbors[i];

    graph_out->nodes[i].neighbors = (nodeid_t *) malloc(graph_out->nodes[i].num_neighbors*sizeof(nodeid_t));
    graph_out->nodes[i].weights = (weight_t *) malloc(graph_out->nodes[i].num_neighbors*sizeof(weight_t));
    graph_out->nodes[i].midpoint = (nodeid_t *) malloc(graph_out->nodes[i].num_neighbors*sizeof(nodeid_t));
    graph_out->nodes[i].edge_flags = (uint8_t *) calloc(graph_out->nodes[i].num_neighbors, sizeof(uint8_t));
    for (int j=0; j<graph_out->nodes[i].num_neighbors; j++){
      graph_out->nodes[i].neighbors[j] = graph_st->neighbors_pt[i][j];
      graph_out->nodes[i].weights[j] = graph_st->weights_pt[i][j];
      graph_out->nodes[i].midpoint[j] = NODEID_NULL;
      if(graph_out->max_weight < graph_out->nodes[i].weights[j]){
	graph_out->max_weight = graph_out->nodes[i].weights[j];
      }
    }
  }

  *graph_CH = graph_out;

}

size_t graph_CH_sizeof(graph_CH_bi_t *graph)
{
  return 0;
}

void graph_CH_bi_free(graph_CH_bi_t *graph)
{
  for (size_t i=0; i<graph->num_nodes; i++){
    free(graph->graph_f.nodes[i].neighbors);
    free(graph->graph_f.nodes[i].weights);
    free(graph->graph_f.nodes[i].midpoint);
    free(graph->graph_f.nodes[i].edge_flags);
  }
  free(graph->graph_f.nodes);

  for (size_t i=0; i<graph->num_nodes; i++){
    free(graph->graph_b.nodes[i].neighbors);
    free(graph->graph_b.nodes[i].weights);
    free(graph->graph_b.nodes[i].midpoint);
    free(graph->graph_b.nodes[i].edge_flags);
  }
  free(graph->graph_b.nodes);

  free(graph->node_ranks);
  free(graph);
}

void graph_CH_dump(FILE *f, graph_CH_bi_t *graph)
{
  size_t i, j;

  fprintf(f, "%zu\n", graph->num_nodes);

  for ( i = 0; i < graph->num_nodes; i++ ){
    fprintf(f, "%"FNODEID",%"FNODEID",%"FNBOR"\n", (nodeid_t)i, graph->node_ranks[i], graph->graph_f.nodes[i].num_neighbors);

    for ( j = 0; j < graph->graph_f.nodes[i].num_neighbors; j++ ){
      fprintf(f, "%zu:%"FNODEID",%"FWEIGHT",%d,%d,%"FNODEID"\n", j, 
	      graph->graph_f.nodes[i].neighbors[j],
	      graph->graph_f.nodes[i].weights[j],
	      graph->graph_f.nodes[i].edge_flags[j]&EDGE_SHORTCUT,
	      graph->graph_f.nodes[i].edge_flags[j]&EDGE_ADDED,
	      graph->graph_f.nodes[i].midpoint[j]);

    }

  }

}


void graph_CH_2_cu_CH(cu_graph_CH_bi_t **cu_graph_CH, graph_CH_bi_t *graph_CH)
{
  assert( !*cu_graph_CH );

  Time_Wall_s timer;

  const uint elist_size_multiple = 1;
  const uint elist_min_padding = 1;
  const double edge_freelist_factor =
    1.0 + GRAPH_BI_EMPTY_SPACE_MULTIPLIER * chopts.C_set;

  cu_graph_CH_bi_t *graph_out = (cu_graph_CH_bi_t *) malloc(sizeof(cu_graph_CH_bi_t));
  graph_out->num_nodes = graph_CH->num_nodes;
  graph_out->max_degree = graph_CH->max_degree;
  graph_out->max_degree_overlay = graph_CH->max_degree_overlay;
  graph_out->mean_degree = (float) graph_CH->mean_degree;
  graph_out->mean_degree_overlay = (float) graph_CH->mean_degree_overlay;
  graph_out->max_weight = graph_CH->max_weight;
  graph_out->mean_weight = graph_CH->mean_weight;
  graph_out->mean_weight_inv = 1.0 / ( graph_CH->mean_weight ?: 1.0 );
  graph_out->mean_weight_0_inv = graph_out->mean_weight_inv;
  graph_out->overlay_size = graph_CH->overlay_size;
  graph_out->max_rank = graph_CH->max_rank;
  graph_out->node_ranks = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  graph_out->node_ids = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  for(size_t i=0; i<graph_out->num_nodes; i++){
    graph_out->node_ranks[i] = graph_CH->node_ranks[i];
    graph_out->node_ids[i] = i;
  }

  unordered_map<Ege,nodeid_t> bwd_eidx( graph_CH->num_nodes * 4 );

  auto init_graph = [&](cu_graph_CH_t& gout_dir, graph_CH_t& gin_dir)
    {
      const bool bwd = &gout_dir == &graph_out->graph_b;
      nodeid_t max_num_edges = 0;
      nodeid_t n_edges = 0;

      gout_dir.num_nodes = gin_dir.num_nodes;
      gout_dir.max_weight = gin_dir.max_weight;
      gout_dir.pointer = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
      gout_dir.num_neighbors = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
      gout_dir.max_num_neighbors_allowed = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));

      for(size_t i=0; i<graph_out->num_nodes; i++){
        n_edges += gin_dir.nodes[i].num_neighbors;
        gout_dir.num_neighbors[i] = gin_dir.nodes[i].num_neighbors;
        gout_dir.max_num_neighbors_allowed[i] =
          rnd_up( gin_dir.nodes[i].num_neighbors + elist_min_padding,
                  elist_size_multiple );

        max_num_edges += gout_dir.max_num_neighbors_allowed[i];
      }
      // Allocate extra storage as empty space to move overgrown nodes
      // into.
      max_num_edges = max_num_edges * edge_freelist_factor;
      gout_dir.max_num_edges = max_num_edges;
      gout_dir.num_edges_exact = n_edges;
      gout_dir.neighbors = (nodeid_t *) malloc(max_num_edges * sizeof(nodeid_t));
      gout_dir.weights = (weight_t *) malloc(max_num_edges * sizeof(weight_t));
      gout_dir.midpoint = (nodeid_t *) malloc(max_num_edges * sizeof(nodeid_t));

      nodeid_t current_p = 0; // idx of current pointers
      for(size_t i=0; i<graph_out->num_nodes; i++){
        gout_dir.pointer[i] = current_p;
        for(size_t j=0; j<gout_dir.num_neighbors[i]; j++){
          const nodeid_t nbr = gin_dir.nodes[i].neighbors[j];
          const nodeid_t eidx = current_p + j;
          if ( bwd )
            {
              bwd_eidx[Ege(i,nbr)] = eidx;
              gout_dir.midpoint[eidx] = gin_dir.nodes[i].midpoint[j];
            }
          else
            {
              const nodeid_t eidx_bwd = bwd_eidx[Ege(nbr,i)];
              gout_dir.midpoint[eidx] = eidx_bwd;
              assert( graph_out->graph_b.midpoint[eidx_bwd] ==
                      gin_dir.nodes[i].midpoint[j] );
            }
          gout_dir.neighbors[eidx] = nbr;
          gout_dir.weights[eidx] = gin_dir.nodes[i].weights[j];
        }
        current_p += gout_dir.max_num_neighbors_allowed[i];
      }
      gout_dir.empty_pointer = current_p; // indicates beginning of empty space
    };

  init_graph(graph_out->graph_b,graph_CH->graph_b);
  init_graph(graph_out->graph_f,graph_CH->graph_f);

  assert( graph_out->graph_f.num_edges_exact ==
          graph_out->graph_b.num_edges_exact );

  // determine number of threads assigned to each vertex based on max_degree
  // we'll keep tpn in log format to ease it's use in the gpu (to avoid division)
  nodeid_t max_deg = graph_out->max_degree;
  max_deg = max_deg >> 3; // divide by 8 to make sure 2^n*8 format
  uint32_t nn = 0;
  while(max_deg != 0){
    nn++;
    max_deg = max_deg>>1;
  }
  graph_out->thread_per_node = nn + 3;
  
  *cu_graph_CH = graph_out;

  if ( show_timing )
    printf("Time to convert CH -> cu_CH: %.6f s\n", timer.s());
  
}

void graph_cu_CH_2_CH(graph_CH_bi_t **graph_CH, cu_graph_CH_bi_t *cu_graph_CH)
{

  graph_CH_bi_t *graph_out = (graph_CH_bi_t *) malloc(sizeof(graph_CH_bi_t));
  graph_out->num_nodes = cu_graph_CH->num_nodes;
  graph_out->max_degree = cu_graph_CH->max_degree;
  graph_out->max_degree_overlay = cu_graph_CH->max_degree_overlay;
  graph_out->mean_degree = (double) cu_graph_CH->mean_degree;
  graph_out->mean_degree_overlay = (double) cu_graph_CH->mean_degree_overlay;
  graph_out->max_weight = cu_graph_CH->max_weight;
  graph_out->mean_weight = cu_graph_CH->mean_weight;
  graph_out->overlay_size = cu_graph_CH->overlay_size;
  graph_out->max_rank = cu_graph_CH->max_rank;
  graph_out->node_ranks = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  for(size_t i=0; i<graph_out->num_nodes; i++){
    graph_out->node_ranks[i] = cu_graph_CH->node_ranks[i];
  }

  graph_out->graph_f.num_nodes = cu_graph_CH->graph_f.num_nodes;
  graph_out->graph_f.num_edges = 0;
  graph_out->graph_f.max_weight = cu_graph_CH->graph_f.max_weight;
  graph_out->graph_f.nodes = (node_CH_t *) malloc(graph_out->num_nodes * sizeof(node_CH_t));
  for(size_t i=0; i<graph_out->num_nodes; i++){
    const int deg = cu_graph_CH->graph_f.num_neighbors[i];
    graph_out->graph_f.num_edges += deg;
    graph_out->graph_f.nodes[i].num_neighbors = deg;
    graph_out->graph_f.nodes[i].num_neighbors_overlay = deg;
    graph_out->graph_f.nodes[i].neighbors = (nodeid_t *) malloc(graph_out->graph_f.nodes[i].num_neighbors * sizeof(nodeid_t));
    graph_out->graph_f.nodes[i].weights = (weight_t *) malloc(graph_out->graph_f.nodes[i].num_neighbors * sizeof(weight_t));
    graph_out->graph_f.nodes[i].midpoint = (nodeid_t *) malloc(graph_out->graph_f.nodes[i].num_neighbors * sizeof(nodeid_t));
    graph_out->graph_f.nodes[i].edge_flags = (uint8_t *) calloc(graph_out->graph_f.nodes[i].num_neighbors, sizeof(uint8_t));
    for(size_t j=0; j< graph_out->graph_f.nodes[i].num_neighbors; j++){
      graph_out->graph_f.nodes[i].llen = graph_out->graph_f.nodes[i].num_neighbors;
      graph_out->graph_f.nodes[i].neighbors[j] = cu_graph_CH->graph_f.neighbors[cu_graph_CH->graph_f.pointer[i] + j];
      graph_out->graph_f.nodes[i].weights[j] = cu_graph_CH->graph_f.weights[cu_graph_CH->graph_f.pointer[i] + j];
      graph_out->graph_f.nodes[i].midpoint[j] = cu_graph_CH->graph_f.midpoint[cu_graph_CH->graph_f.pointer[i] + j];
    }
  }

  graph_out->graph_b.num_nodes = cu_graph_CH->graph_b.num_nodes;
  graph_out->graph_b.num_edges = 0;
  graph_out->graph_b.max_weight = cu_graph_CH->graph_b.max_weight;
  graph_out->graph_b.nodes = (node_CH_t *) malloc(graph_out->num_nodes * sizeof(node_CH_t));
  for(size_t i=0; i<graph_out->num_nodes; i++){
    const int deg = cu_graph_CH->graph_b.num_neighbors[i];
    graph_out->graph_b.num_edges += deg;
    graph_out->graph_b.nodes[i].num_neighbors = deg;
    graph_out->graph_b.nodes[i].num_neighbors_overlay = deg;
    graph_out->graph_b.nodes[i].neighbors = (nodeid_t *) malloc(graph_out->graph_b.nodes[i].num_neighbors * sizeof(nodeid_t));
    graph_out->graph_b.nodes[i].weights = (weight_t *) malloc(graph_out->graph_b.nodes[i].num_neighbors * sizeof(weight_t));
    graph_out->graph_b.nodes[i].midpoint = (nodeid_t *) malloc(graph_out->graph_b.nodes[i].num_neighbors * sizeof(nodeid_t));
    graph_out->graph_b.nodes[i].edge_flags = (uint8_t *) calloc(graph_out->graph_b.nodes[i].num_neighbors, sizeof(uint8_t));
    for(size_t j=0; j<graph_out->graph_b.nodes[i].num_neighbors; j++){
      graph_out->graph_b.nodes[i].llen = graph_out->graph_b.nodes[i].num_neighbors;
      graph_out->graph_b.nodes[i].neighbors[j] = cu_graph_CH->graph_b.neighbors[cu_graph_CH->graph_b.pointer[i] + j];
      graph_out->graph_b.nodes[i].weights[j] = cu_graph_CH->graph_b.weights[cu_graph_CH->graph_b.pointer[i] + j];
      graph_out->graph_b.nodes[i].midpoint[j] = cu_graph_CH->graph_b.midpoint[cu_graph_CH->graph_b.pointer[i] + j];
    }
  }

  *graph_CH = graph_out;

}

void graph_cu_CH_bi_free_host(cu_graph_CH_bi_t*& graph)
{
  if ( !graph ) return;

  free(graph->graph_f.pointer);
  free(graph->graph_f.num_neighbors);
  free(graph->graph_f.max_num_neighbors_allowed);
  free(graph->graph_f.neighbors);
  free(graph->graph_f.weights);
  free(graph->graph_f.midpoint);
  
  free(graph->graph_b.pointer);
  free(graph->graph_b.num_neighbors);
  free(graph->graph_b.max_num_neighbors_allowed);
  free(graph->graph_b.neighbors);
  free(graph->graph_b.weights);
  free(graph->graph_b.midpoint);

  free(graph->node_ranks);
  free(graph->node_ids);

  free(graph);

  graph = nullptr;
}

void graph_cu_CH_free_host(cu_graph_CH_t *graph)
{
  free(graph->pointer);
  free(graph->num_neighbors);
  if(graph->NNIEG != NULL){
    free(graph->NNIEG);
  }
  free(graph->max_num_neighbors_allowed);
  if(graph->vertex_ID != NULL){
    free(graph->vertex_ID);
  }
  free(graph->neighbors);
  free(graph->weights);
  free(graph->midpoint);

  free(graph);

}

void graph_cu_CH_UD_init(cu_graph_CH_UD_t **graph_cu_UD, nodeid_t num_nodes, uint32_t edge_list_length, nodeid_t max_rank)
{
  cu_graph_CH_UD_t *graph_out = // Make sure unused pointers set to null.
    (cu_graph_CH_UD_t *) calloc(1,sizeof(graph_CH_UD_t));
  graph_out->node_idx = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->node_idx_inv = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->node_ranks = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->node_levels = (nodeid_t *) calloc(num_nodes, sizeof(nodeid_t));

  graph_out->node_levels_pt = (nodeid_t *) calloc(max_rank, sizeof(nodeid_t));

  graph_out->graph_u_f.num_nodes = num_nodes;
  graph_out->graph_u_f.pointer = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_f.num_neighbors = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_f.neighbors = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  
  graph_out->graph_u_b.num_nodes = num_nodes;
  graph_out->graph_u_b.pointer = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.num_neighbors = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.neighbors = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_u_b.weights = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_u_b.midpoint = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));

  graph_out->graph_d_b.num_nodes = num_nodes;
  graph_out->graph_d_b.pointer = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.num_neighbors = (nodeid_t *) malloc(num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.neighbors = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_d_b.weights = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));
  graph_out->graph_d_b.midpoint = (nodeid_t *) malloc(edge_list_length*sizeof(nodeid_t));

  // note that overlay graph and the APSP path are uninitialized since they are the last step
  // also note that for the downward's DAG, care must be taken since all the data associated with it
  // should ideally be written in backwards order (this is to help predictive cache prefetching,
  // on the GPU, effect of this can be negligible)
  /// for now, we'll assume this won't matter and just write things backwards on the GPU side and access
  /// in a backwards stride.
  
}

void graph_cu_CH_UD_2_CH_UD(graph_CH_UD_t **graph_UD, cu_graph_CH_UD_t *graph_cu_UD)
{
  /// i'll come back to this ....
  ////////
  ////////
  ////////


}

void
cu_CH_stats_init_common
(cu_CH_stats_t *stats, const string file_name)
{
  stats->stats_file_name = file_name;
  stats->num_iterations = 0;

  const auto max_num_iters = stats->max_num_iters;

  stats->it.reserve(max_num_iters);
  stats->time_iter = (double *) calloc(max_num_iters, sizeof(double));
  stats->time_scoring = (float *) calloc(max_num_iters, sizeof(float));
  stats->time_MIS = (float *) calloc(max_num_iters, sizeof(float));
  stats->time_contraction = (float *) calloc(max_num_iters, sizeof(float));
  stats->time_update = (float *) calloc(max_num_iters, sizeof(float));
  stats->time_OL_extraction_0 = (float *) calloc(max_num_iters, sizeof(float));
  stats->time_OL_extraction_1 = (float *) calloc(max_num_iters, sizeof(float));
  stats->time_invert_0.reserve(max_num_iters);
  stats->time_invert_1.reserve(max_num_iters);
  stats->num_nodes_in_lvl = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->num_nodes_overlay = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->C = (double *) calloc(max_num_iters, sizeof(double));
  stats->max_degree = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->mean_degree = (double *) calloc(max_num_iters, sizeof(double));
  stats->deg = (double *) calloc(max_num_iters, sizeof(double));
  stats->wps_shadow = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_shadow_shc = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_1hs = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_1hs_loop = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_2hs = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_2hs_loop = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_ij = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_2hop = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_fp = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
  stats->wps_shc = (uint32_t *) calloc(max_num_iters, sizeof(uint32_t));
}

void
cu_CH_stats_init
(cu_CH_stats_t *stats, const cu_graph_CH_UD_t *graph, const string file_name)
{
  stats->contracting = false;
  stats->num_nodes = graph->num_nodes;
  stats->num_edges_f_original = 0;
  stats->num_edges_b_original = 0;
  stats->num_nodes_overlay_final = graph->overlay_CH.num_nodes;
  stats->max_num_iters = 1;
  cu_CH_stats_init_common(stats,file_name);
}

void
cu_CH_stats_init
(cu_CH_stats_t *stats, graph_CH_bi *graph,
 uint32_t max_num_iters, const string file_name)
{
  stats->contracting = true;
  stats->num_nodes = graph->num_nodes;
  uint64_t n_edges_f = 0, n_edges_b = 0;
  for ( nodeid_t nid=0; nid < graph->num_nodes; nid++ )
    {
      n_edges_f += graph->graph_f.nodes[nid].num_neighbors;
      n_edges_b += graph->graph_b.nodes[nid].num_neighbors;
    }
  stats->num_edges_f_original = n_edges_f;
  stats->num_edges_b_original = n_edges_b;
  stats->max_num_iters = max_num_iters;
  cu_CH_stats_init_common(stats,file_name);
}


static void pr_item(FILE *fp, bool b, const char* e)
{fprintf(fp, "%s%s", b ? "True" : "False", e);}
static void pr_item(FILE *fp, int i, const char* e)
{fprintf(fp, "%d%s", i, e);}
static void pr_item(FILE *fp, uint32_t i, const char *e)
{fprintf(fp, "%u%s", i, e);}
static void pr_item(FILE *fp, int64_t i, const char *e)
{fprintf(fp, "%ld%s", i, e);}
static void pr_item(FILE *fp, double d, const char *e)
{
  fprintf
    (fp, floor(d) == d ? "%.0f%s" : fabs(d) < 100 ? "%.9g%s" : "%f%s", d, e);
}

static void pr_item(FILE *fp, string s, const char *e)
{fprintf(fp, "%s%s", s.c_str(), e);}

void cu_CH_stats_dump(cu_CH_stats_t *ds)
{
# define PRo(m) { fprintf(fp, "chopts.%s\n",#m); pr_item(fp,chopts.m,"\n"); }
# define PRg(m) { fprintf(fp,"%s\n",#m); pr_item(fp,m,"\n"); }
# define PR(m) { fprintf(fp,"%s\n",#m); pr_item(fp,ds->m,"\n"); }
# define VPR(m) fprintf(fp, "%s\n",#m); \
  for ( auto t: ds->m ) pr_item(fp, t, ", "); fprintf(fp,"\n");
# define APR(a,m) fprintf(fp, "%s_%s\n",#a,#m); \
  if ( !unused_bytes.count(#a) ) unused_bytes[#a] = sizeof(ds->a[0]); \
  unused_bytes[#a] -= sizeof(ds->a[0].m); \
  for ( auto& t: ds->a ) pr_item(fp, t.m, ", "); fprintf(fp,"\n");

  map<string,size_t> unused_bytes;

  pr.user("Writing performance data to %s\n", ds->stats_file_name.c_str());

  FILE* const fp = fopen(ds->stats_file_name.c_str(), "w+");

  PR(env_host_name);
  PR(env_time_start_ue);
  PR(env_time_start_local);
  PR(env_gpu_name);
  PR(env_gpu_nmps);
  PR(env_gpu_cc);
  PR(env_cuda_driver_version);
  PR(env_cuda_runtime_version);
  PR(env_nvcc_version);

  PR(env_run_slowed_by_tuning_and_debug_options);
  PR(env_compiler_version);
  PR(env_verbosity);
  PR(env_cuda_debug);
  PR(env_contract_eval);
  PR(env_assertion_checking);
  PR(env_host_optimization);
  PR(env_apsp_benchmark_path);

  PR(in_graph_path);

  PRo(verify);
  PRo(verify_in_loops);
  PRo(verify_query);
  PRo(verify_contr_raw);
  PRo(verify_contr_final);
  PRo(asserts_costly_skip);

  PRo(wps_oracle_use);
  PRo(wps_oracle_perform);
  PRo(wps_oracle_hop_limit);

  PRg(opt_static_cull_want_age);

  PRo(cull_ideal_hops);
  PRo(cull_threshold_tpn);
  PRo(contract_one_hop);
  PRo(C_dynamic);
  PRo(C_set);

  PRo(score_edge_diff);
  PRo(score_max_deg_nbr);
  PRo(score_mean_weight);
  PRo(score_max_weight);
  PRo(score_deg);
  PRo(score_g_wht_0);
  PRo(score_precision);

  PRo(luby_deterministic);
  PRo(luby_dynamic_iter);
  PRo(longcuts_cull);
  PRo(K_bm_autogen);
  PRo(K_dynamic);
  PRo(K_set);
  PRo(wps_hash_loop_after_collision);

  PRo(query_distance_only);
  PRo(query_n_sparse_levels);

  fprintf(fp, "num_nodes\n");
  fprintf(fp, "%u\n", ds->num_nodes);
  fprintf(fp, "num_nodes_overlay_final (K_cco)\n");
  fprintf(fp, "%u\n", ds->num_nodes_overlay_final);
  PR(num_edges_f_original);
  PR(num_edges_b_original);
  fprintf(fp, "num_iterations\n");
  fprintf(fp, "%u\n", ds->num_iterations);

  if ( ds->contracting )
    {
      PR(num_iterations_invert);
      VPR(it_tpn);
      VPR(it_num_edges);
      VPR(it_num_longcuts_cut);
      VPR(it_num_longcuts_orig);
      VPR(it_avg_longcuts_age);
      VPR(it_num_longcuts_cut_ideal);
      fprintf(fp, "max_degree_OL\n");
      fprintf(fp, "%u\n", ds->max_degree_OL);
      fprintf(fp, "max_degree_u_f_DAG\n");
      fprintf(fp, "%u\n", ds->max_degree_u_f_DAG);
      fprintf(fp, "max_degree_u_b_DAG\n");
      fprintf(fp, "%u\n", ds->max_degree_u_b_DAG);
      fprintf(fp, "max_degree_d_b_DAG\n");
      fprintf(fp, "%u\n", ds->max_degree_d_b_DAG);
      fprintf(fp, "mean_degree_u_f_DAG\n");
      fprintf(fp, "%f\n", ds->mean_degree_u_f_DAG);
      fprintf(fp, "mean_degree_u_b_DAG\n");
      fprintf(fp, "%f\n", ds->mean_degree_u_b_DAG);
      fprintf(fp, "mean_degree_d_b_DAG\n");
      fprintf(fp, "%f\n", ds->mean_degree_d_b_DAG);
      fprintf(fp, "num_edges_u_b_DAG\n");
      fprintf(fp, "%u\n", ds->num_edges_u_b_DAG);
      fprintf(fp, "num_edges_d_b_DAG\n");
      fprintf(fp, "%u\n", ds->num_edges_d_b_DAG);

      PR(wall_time_verify_pre_s);
      PR(wall_time_shadow_wps_s);
      PR(wall_time_total_ms);
      PR(wall_time_pre_main_loop_ms);
      PR(wall_time_main_loop_ms);
      PR(wall_time_post_main_loop_ms);
      PR(wall_time_verify_post_s);

      fprintf(fp,"%s",ds->partition_app_text.c_str());

      fprintf(fp, "total_time\n");
      fprintf(fp, "%f\n", ds->time_total);

      VPR(it_kernel_scoring_nd_occ);
      VPR(it_kernel_contract_nd_occ);
      VPR(it_kernel_scoring_wp_occ);
      VPR(it_kernel_contract_wp_occ);

      fprintf(fp, "time_scoring\n");
      fprintf(fp, "%f\n", ds->time_total_scoring);
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_scoring[i]);
      }
  
      fprintf(fp, "\ntime_MIS\n");
      fprintf(fp, "%f\n", ds->time_total_MIS);
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_MIS[i]);
      }
      fprintf(fp,"\n");

      APR(it,time_select_fl_s);
      APR(it,time_select_sort1_s);
      APR(it,time_select_lo_s);
      APR(it,time_select_select_s);
      APR(it,luby_n_iter);
      APR(it,time_select_gather_s);
      APR(it,time_select_sort2_s);
  
      fprintf(fp, "time_contraction\n");
      fprintf(fp, "%f\n", ds->time_total_contraction);
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_contraction[i]);
      }

      fprintf(fp, "\ntime_update\n");
      fprintf(fp, "%f\n", ds->time_total_update);
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_update[i]);
      }

      fprintf(fp, "\ntime_OL_extraction_0\n");
      fprintf(fp, "%f\n", ds->time_total_OL_extraction_0);
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_OL_extraction_0[i]);
      }

      fprintf(fp, "\ntime_OL_extraction_1\n");
      fprintf(fp, "%f\n", ds->time_total_OL_extraction_1);
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_OL_extraction_1[i]);
      }
      fprintf(fp,"\n");

      PR(time_total_invert_0);
      VPR(time_invert_0);
      PR(time_total_invert_1);
      VPR(time_invert_1);

      VPR(wall_time_iter_ms);

      fprintf(fp, "time_iter\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->time_iter[i]);
      }
      fprintf(fp, "\n");

      PR(time_ud_invert);
      PR(time_ud_invert_0_s);
      PR(time_ud_invert_1_s);
      PR(time_ud_invert_2_s);
      PR(time_ud_invert_3_s);
      PR(time_APSP_prep_s);
      PR(time_ud_renum_s);
      PR(time_APSP_s);
      PR(time_APSP_0_s); PR(time_APSP_1_s); PR(time_APSP_2_s);
      PR(time_compress_0_s);
      PR(time_compress_1_s);
  
      fprintf(fp, "num_nodes_in_lvl\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->num_nodes_in_lvl[i]);
      }

      fprintf(fp, "\nnum_nodes_overlay\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->num_nodes_overlay[i]);
      }

      fprintf(fp, "\nC_sco\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->C[i]);
      }

      fprintf(fp, "\nmax_degree\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->max_degree[i]);
      }

      fprintf(fp, "\nmean_degree\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->mean_degree[i]);
      }

      fprintf(fp, "\ndeg\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%f, ", ds->deg[i]);
      }
      fprintf(fp, "\n");

      APR(it,mean_weight);

      fprintf(fp, "wps_shadow\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_shadow[i]);
      }

      fprintf(fp, "\nwps_shadow_shc\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_shadow_shc[i]);
      }

      fprintf(fp, "\nwps_1hs\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_1hs[i]);
      }

      fprintf(fp, "\nwps_1hs_loop\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_1hs_loop[i]);
      }

      fprintf(fp, "\nwps_2hs\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_2hs[i]);
      }

      fprintf(fp, "\nwps_2hs_loop\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_2hs_loop[i]);
      }

      fprintf(fp, "\nwps_ij\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_ij[i]);
      }

      fprintf(fp, "\nwps_2hop\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_2hop[i]);
      }

      fprintf(fp, "\nwps_fp\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_fp[i]);
      }

      fprintf(fp, "\nwps_shc\n");
      for(uint32_t i=0; i<ds->num_iterations; i++){
        fprintf(fp, "%u, ", ds->wps_shc[i]);
      }
      fprintf(fp, "\n");

    }

  if ( ds->q.size() )
    {
      VPR(edge_groups_in_level_u_b);
      VPR(edge_groups_in_level_d_b);

      APR(q,src);
      APR(q,src_lvl);
      APR(q,t_up_s);
      APR(q,t_down_s);
      APR(q,t_OL_s);
      APR(q,t_dist_kernel_s);
      APR(q,t_dist_wall_s);
      APR(q,t_unpack_kernel_s);
      APR(q,t_unpack_wall_s);

      APR(q,up_1_lvl_start); APR(q,up_1_lvl_stop);
      APR(q,n_small_group_up); APR(q,n_small_group_down);
      APR(q,tk_up_0_s);
      APR(q,tk_up_1_s);
      APR(q,tk_up_small_s);
      APR(q,tk_up_final_s);
      APR(q,tk_OL_s);
      APR(q,tk_down_small_s);
      APR(q,tk_down_normal_s);
      APR(q,tk_unpack_OL_s);
      APR(q,tk_unpack_renum_s);
      APR(q,tw_prep_sync_s);
      APR(q,tw_up_0_s); APR(q,tw_up_1_s); APR(q,tw_up_small_s);
      APR(q,tw_OL_s);
      APR(q,tw_down_small_s);
      APR(q,tw_down_normal_s);
      APR(q,tw_unpack_OL_s); APR(q,tw_unpack_renum_s);
    }

  fclose(fp);

  for ( auto& elt: unused_bytes )
    if ( elt.second )
      printf("In stats structure %s, %zd bytes not written.\n",
             elt.first.c_str(), elt.second);
  // Note: The warning above might be issued due to alignment gaps.
  // If so, add a padding member to the structure.


# undef PR
# undef PRB
# undef PRI
# undef PRU
# undef PRS
# undef APR

}

void cu_CH_stats_free(cu_CH_stats_t *ds)
{

  free(ds->time_iter);
  free(ds->time_scoring);
  free(ds->time_MIS);
  free(ds->time_contraction);
  free(ds->time_update);
  free(ds->time_OL_extraction_0);
  free(ds->time_OL_extraction_1);
  free(ds->num_nodes_in_lvl);
  free(ds->num_nodes_overlay);
  free(ds->max_degree);
  free(ds->mean_degree);
  free(ds->deg);
  free(ds->wps_shadow);
  free(ds->wps_shadow_shc);
  free(ds->wps_1hs);
  free(ds->wps_1hs_loop);
  free(ds->wps_2hs);
  free(ds->wps_2hs_loop);
  free(ds->wps_ij);
  free(ds->wps_2hop);
  free(ds->wps_fp);
  free(ds->wps_shc);

  free(ds);
  
}

void graph_cu_UD_free_host(cu_graph_CH_UD_t*& graph_UD_h)
{

  free(graph_UD_h->node_idx);
  free(graph_UD_h->node_idx_inv);
  free(graph_UD_h->node_ranks);
  free(graph_UD_h->node_levels);
  free(graph_UD_h->node_levels_pt);
  free(graph_UD_h->edge_groups_in_level_u_b);
  free(graph_UD_h->edge_groups_in_level_d_b);

  free(graph_UD_h->graph_u_f.pointer);
  free(graph_UD_h->graph_u_f.num_neighbors);
  free(graph_UD_h->graph_u_f.NNIEG);
  free(graph_UD_h->graph_u_f.neighbors);
  free(graph_UD_h->graph_u_f.vertex_ID);
  
  free(graph_UD_h->graph_u_b.pointer);
  free(graph_UD_h->graph_u_b.num_neighbors);
  free(graph_UD_h->graph_u_b.NNIEG);
  free(graph_UD_h->graph_u_b.neighbors);
  free(graph_UD_h->graph_u_b.vertex_ID);
  free(graph_UD_h->graph_u_b.weights);
  free(graph_UD_h->graph_u_b.midpoint);
  
  free(graph_UD_h->graph_d_b.pointer);
  free(graph_UD_h->graph_d_b.num_neighbors);
  free(graph_UD_h->graph_d_b.NNIEG);
  free(graph_UD_h->graph_d_b.neighbors);
  free(graph_UD_h->graph_d_b.vertex_ID);
  free(graph_UD_h->graph_d_b.weights);
  free(graph_UD_h->graph_d_b.midpoint);
  
  free(graph_UD_h->overlay_CH.pointer);
  free(graph_UD_h->overlay_CH.num_neighbors);
  free(graph_UD_h->overlay_CH.neighbors);
  free(graph_UD_h->overlay_CH.weights);
  free(graph_UD_h->overlay_CH.midpoint);

  free(graph_UD_h->overlay_APSP_path.prev);
  free(graph_UD_h->overlay_APSP_path.dist);

  free(graph_UD_h);

  graph_UD_h = nullptr;
}

void graph_CH_bi_2_st(graph_st_t **graph_st, graph_CH_bi_t *graph_CH)
{
  size_t i, j;
  size_t k = 0;

  graph_st_t *graph_out = (graph_st_t *) malloc(sizeof(graph_st_t));

  graph_out->num_nodes = graph_CH->graph_f.num_nodes;
  graph_out->num_edges = 0;

  graph_out->neighbors_pt = (nodeid_t **) malloc(graph_out->num_nodes*sizeof(nodeid_t *));
  graph_out->weights_pt = (weight_t **) malloc(graph_out->num_nodes*sizeof(weight_t *));
  graph_out->num_neighbors = (nnbor_t *) malloc(graph_out->num_nodes*sizeof(nnbor_t));

  for ( i = 0; i < graph_out->num_nodes; i++ ){
    graph_out->num_neighbors[i] = graph_CH->graph_f.nodes[i].num_neighbors;
    graph_out->num_edges += graph_out->num_neighbors[i];
  }

  graph_out->neighbors = (nodeid_t *) malloc(graph_out->num_edges*sizeof(nodeid_t));
  graph_out->weights = (weight_t *) malloc(graph_out->num_edges*sizeof(weight_t));

  for ( i = 0; i < graph_out->num_nodes; i++ ){

    graph_out->weights_pt[i] = &graph_out->weights[k];
    graph_out->neighbors_pt[i] = &graph_out->neighbors[k];

    for ( j = 0; j < graph_out->num_neighbors[i]; j++ ){

      graph_out->neighbors_pt[i][j] = graph_CH->graph_f.nodes[i].neighbors[j];
      graph_out->weights_pt[i][j] = graph_CH->graph_f.nodes[i].weights[j];;
      assert( k < graph_out->num_edges );
      k++;
    }
  }

  *graph_st = graph_out;

}

