/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef CU_CH_PREFIX_CUH
#define CU_CH_PREFIX_CUH

typedef nodeid_t prefix_value_t;
struct Prefix_Elt { prefix_value_t pfx_excl, pfx_incl, sum; };

__device__ inline Prefix_Elt
prefix_wp(prefix_value_t val, int n_vals_lg = 5)
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int lane = threadIdx.x % wp_sz;
  const uint32_t msk = 0xffffffff;
  assert( __activemask() == msk );

  // Compute intra-warp inclusive prefix.
  //
  prefix_value_t my_pf_wp = val;
  for ( int tree_level = 0; tree_level < n_vals_lg; tree_level++ )
    {
      const int dist = 1 << tree_level;
      const prefix_value_t neighbor_prefix = __shfl_up_sync(msk,my_pf_wp,dist);
      if ( dist <= lane ) my_pf_wp += neighbor_prefix;
    }
  return Prefix_Elt
    ( { my_pf_wp - val, my_pf_wp,
        __shfl_sync( msk, my_pf_wp, (1<<n_vals_lg)-1 ) } );
};

__device__ inline Prefix_Elt
prefix_bit(int blk_lg, int tpn, bool val)
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int lane = threadIdx.x % wp_sz;
  const uint32_t msk = 0xffffffff;
  assert( __activemask() == msk );
  const int TPN = 1 << tpn;
  const uint32_t keep_vec =__ballot_sync(msk,val);
  const int tnid = threadIdx.x % TPN;
  const int lid = threadIdx.x >> tpn;

  if ( tpn < wp_lg )
    {
      const int wp_seg = lane >> tpn;
      const uint32_t seg_msk = ( 1u << TPN  ) - 1  <<  wp_seg * TPN;
      const uint32_t pfx_msk = ( 1u << tnid ) - 1  <<  wp_seg * TPN;
      const prefix_value_t n_keep = __popc( keep_vec & seg_msk );
      const prefix_value_t keep_epfx = __popc( keep_vec & pfx_msk );
      return Prefix_Elt({keep_epfx,keep_epfx+val,n_keep});
    }
  else if ( tpn == wp_lg )
    {
      const uint32_t pfx_msk = ( 1 << tnid ) - 1;
      const prefix_value_t n_keep = __popc( keep_vec );
      const prefix_value_t keep_epfx = __popc( keep_vec & pfx_msk );
      return Prefix_Elt({keep_epfx,keep_epfx+val,n_keep});
    }
  else
    {
      const uint32_t pfx_msk = ( 1 << lane ) - 1;
      const int wp_n_keep = __popc( keep_vec );
      const int wp_keep_epfx = __popc( keep_vec & pfx_msk );
      const int wp_per_node = 1 << tpn - wp_lg;
      const int l_wp_idx = tnid >> wp_lg;
      __syncthreads();
      //  __shared__ uint8_t pfx[1<<blk_lg-tpn][wp_per_node];
      __shared__ uint16_t pfx[32][32];//set to maximum possible values (block_dim=1024)
      if ( !lane ) pfx[lid][l_wp_idx] = wp_n_keep;
      __syncthreads();
      const int o_n_keep = tnid < wp_per_node ? pfx[lid][tnid] : 0;
      __syncthreads();
      if ( tnid < wp_sz )
        {
          const Prefix_Elt pe = prefix_wp(o_n_keep,tpn-wp_lg);
          if ( tnid < wp_per_node ) pfx[lid][tnid] = pe.pfx_incl;
        }
      __syncthreads();
      const prefix_value_t n_keep = pfx[lid][wp_per_node-1];
      const prefix_value_t keep_epfx =
        wp_keep_epfx + ( l_wp_idx ? pfx[lid][l_wp_idx-1] : 0 );
      return Prefix_Elt({keep_epfx,keep_epfx+val,n_keep});
    }
}


template <typename T>
__device__ inline T
sum_wp(T val, int n_vals_lg = 5)
{
  const uint32_t msk = 0xffffffff;
  assert( __activemask() == msk );
  T s_wp = val;
  for ( int b = 0; b < n_vals_lg; b++ )
    s_wp += __shfl_xor_sync(msk,s_wp,1<<b);
  return s_wp;
};

template <typename T>
__device__ inline T
sum_block(T val)
{
  __shared__ T sb;
  __syncthreads();
  if ( threadIdx.x == 0 ) sb = 0;
  __syncthreads();
  atomicAdd(&sb,val);
  __syncthreads();
  return sb;
}

template <typename T>
__device__ inline T
sum_block_grp(int block_lg, int grp_lg, T val)
{
  assert( grp_lg <= 5 || grp_lg == block_lg );
  return grp_lg <= 5 ? sum_wp(val,grp_lg) : sum_block(val);
}

template <typename T>
__device__ inline T
max_wp(T val, int n_vals_lg = 5)
{
  const uint32_t msk = 0xffffffff;
  assert( __activemask() == msk );
  T accum_wp = val;
  for ( int b = 0; b < n_vals_lg; b++ )
    set_max( accum_wp, __shfl_xor_sync(msk,accum_wp,1<<b) );
  return accum_wp;
};

template <typename T>
__device__ inline T
max_block(T val)
{
  __shared__ T sb;
  __syncthreads();
  if ( threadIdx.x == 0 ) sb = val;
  __syncthreads();
  atomicMax(&sb,val);
  __syncthreads();
  return sb;
}

template <typename T>
__device__ inline T
max_block_grp(int block_lg, int grp_lg, T val)
{
  assert( grp_lg <= 5 || grp_lg == block_lg );
  return grp_lg <= 5 ? max_wp(val,grp_lg) : max_block(val);
}


#endif
