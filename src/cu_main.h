/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __CU_MAIN_H__
#define __CU_MAIN_H__

#include <vector>
#include "main.h"
#include "graphs.h"
#include "APSP.h"
#include "cu_verify.h"

class Node_Arg {
public:
  Node_Arg(char *str_in);
  size_t num_args() { return srcs.size() + n_rand_pending; }
  operator size_t() { return num_args(); }
  void nnodes_set(nodeid_t n);
  nodeid_t operator [] (int idx) { assert(!n_rand_pending); return srcs[idx]; }
  nodeid_t* begin() { assert( !n_rand_pending ); return srcs.data(); }
  nodeid_t* end() { return begin() + num_args(); }
  std::vector<nodeid_t> srcs_get()
  { assert(!n_rand_pending); return srcs; }
private:
  std::vector<nodeid_t> srcs;
  nodeid_t nnodes;
  int n_rand, n_rand_pending;
  uint32_t rand_seed;
};


class cu_Contract_Query_State {
public:
  cu_Contract_Query_State(Node_Arg& src_vals)
    :srcs(src_vals), graph_cu_CH_h(nullptr),
     graph_UD_h(nullptr), graph_UD_d(nullptr),
     n_queries_started(0), result_0_d(nullptr), result_1_d(nullptr),
     node_tags(nullptr)
  {
    result_h.num_nodes = 0;
  };
  ~cu_Contract_Query_State();

  Node_Arg& srcs;
  cu_CH_stats_t stats;
  Contract_Dist_Verify contract_dist_verify;
  cu_graph_CH_bi_t *graph_cu_CH_h;
  cu_graph_CH_UD_t *graph_UD_h, *graph_UD_d;
  int n_queries_started;
  path_t result_h;
  path_t *result_0_d, *result_1_d;
  path_t result_0_dh, result_1_dh;
  std::vector<void*> cuda_storage_free_at_end;
  uint32_t *node_tags;
};

void cu_APSP_benchmark();
float cu_APSP_main(weight_t *dist, nodeid_t *prev, uint32_t num_nodes);

void cu_CH_contract_main
(cu_graph_CH_bi_t **graph_cu_CH_d, cu_Contract_Query_State& cq_state);

void cu_CH_query_main
(nodeid_t src, cu_Contract_Query_State& cq_state);


#endif
 
 
