/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include "graphs.h"
#include "heap.h"
#include "dij.h"
#include "APSP.h"

// note: path->..._pt[i][j] is the path from i to j
void APSP_st(graph_st_t *graph, APSP_path_t *path)
{
  // form the initial adjacancy matrix which is the zero'th iteration of FW
  for(size_t i=0; i<graph->num_nodes; i++){
    for(size_t j=0; j<graph->num_neighbors[i]; j++){
      path->dist_pt[i][graph->neighbors_pt[i][j]] = graph->weights_pt[i][j];
      path->prev_pt[i][graph->neighbors_pt[i][j]] = i;
    }
  }

  // the tripple nested Floyd Warshal loop. on a GPU it will be blocked and
  // parallelized similar to a blocked matrix multiply (note that loop k
  // has to stay as outermost and it must be handled in order)
  for(size_t k=0; k<graph->num_nodes; k++){
    if(k%100==0){
      printf("iter = %zu\n", k);
    }
    for(size_t i=0; i<graph->num_nodes; i++){
      for(size_t j=0; j<graph->num_nodes; j++){
	// note that the second and third conditions are for overflow
	if((path->dist_pt[i][j]>(path->dist_pt[i][k] + path->dist_pt[k][j]))
	   && (path->prev_pt[i][k] != NODEID_NULL)
	   && (path->prev_pt[k][j] != NODEID_NULL)){
	  path->dist_pt[i][j] = path->dist_pt[i][k] + path->dist_pt[k][j];
	  path->prev_pt[i][j] = k;
	  // check for overflow (might affect performance)
	  ASSERTA((path->dist_pt[i][j] > path->dist_pt[i][k]) && (path->dist_pt[i][j] > path->dist_pt[k][j]));
	}
      }
    }
  }
}

// mainly for testing the APSP results
void APSP_dij_st(graph_st_t *graph, APSP_path_t *path)
{
  path_t *result;
  dij_stats_t stats;
  for(size_t i=0; i<graph->num_nodes; i++){
    printf("iter = %zu\n", i);
    path_init(&result, graph->num_nodes);
    stats = dij_st_ref_dest(graph, i, NODEID_NULL, result);
    for(size_t j=0; j<graph->num_nodes; j++){
      path->prev_pt[i][j] = result->prev[j];
      path->dist_pt[i][j] = result->weight[j];
    }
    path_free(result);
  }
}

void APSP_blocked_st(graph_st_t *graph, APSP_path_t *path)
{

  // form the working initial adjacancy matrix which is the zero'th iteration of FW
  for(size_t i=0; i<graph->num_nodes; i++){
    for(size_t j=0; j<graph->num_neighbors[i]; j++){
      path->dist_pt[i][graph->neighbors_pt[i][j]] = graph->weights_pt[i][j];
      path->prev_pt[i][graph->neighbors_pt[i][j]] = i;
    }
  }

  // working datasets
  weight_t *dist_w;
  nodeid_t *prev_w;
  weight_t **dist_w_pt;
  nodeid_t **prev_w_pt;
  uint32_t num_nodes_w;
  uint32_t block_factor;
  uint32_t num_nodes = graph->num_nodes;

  // make a working graph with a size divisible by 64 (both for simplicity and alignment)
  // pad unconnected nodes at the end if necessary
  if(num_nodes % 64 == 0){
    num_nodes_w = num_nodes;
    block_factor = num_nodes/64;
    dist_w = path->dist;
    prev_w = path->prev;
    dist_w_pt = path->dist_pt;
    prev_w_pt = path->prev_pt;
  } else {
    num_nodes_w = (num_nodes/64 + 1)*64;
    block_factor = num_nodes_w/64;
    dist_w = (uint32_t *) malloc(num_nodes_w*num_nodes_w*sizeof(uint32_t));
    prev_w = (uint32_t *) malloc(num_nodes_w*num_nodes_w*sizeof(uint32_t));
    dist_w_pt = (uint32_t **) malloc(num_nodes_w*sizeof(uint32_t *));
    prev_w_pt = (uint32_t **) malloc(num_nodes_w*sizeof(uint32_t *));
    for(uint32_t i=0; i<num_nodes_w; i++){
      for(uint32_t j=0; j<num_nodes_w; j++){
	if(i<num_nodes && j<num_nodes){
	  dist_w[i*num_nodes_w + j] = path->dist[i*num_nodes + j];
	  prev_w[i*num_nodes_w + j] = path->prev[i*num_nodes + j];
	} else {
	  if(i==j){
	    dist_w[i*num_nodes_w + j] = 0;
	    prev_w[i*num_nodes_w + j] = i;
	  } else {
	    dist_w[i*num_nodes_w + j] = WEIGHT_INF;
	    prev_w[i*num_nodes_w + j] = NODEID_NULL;
	  }
	}
      }
    }
    for(uint32_t i=0; i<num_nodes_w; i++){
      dist_w_pt[i] = &dist_w[i*num_nodes_w];
      prev_w_pt[i] = &prev_w[i*num_nodes_w];
    }

  }

  for(uint32_t iter=0; iter<block_factor; iter++){
    // phase 0
    for(size_t k=0; k<64; k++){
      for(size_t i=0; i<64; i++){
	for(size_t j=0; j<64; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[64*iter + i][64*iter + j]>(dist_w_pt[64*iter + i][64*iter + k] + dist_w_pt[64*iter + k][64*iter + j]))
	     && (prev_w_pt[64*iter + i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][64*iter + j] != NODEID_NULL)){
	    dist_w_pt[64*iter + i][64*iter + j] = dist_w_pt[64*iter + i][64*iter + k] + dist_w_pt[64*iter + k][64*iter + j];
	    prev_w_pt[64*iter + i][64*iter + j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[64*iter + i][64*iter + j] > dist_w_pt[64*iter + i][64*iter + k]) && (dist_w_pt[64*iter + i][64*iter + j] > dist_w_pt[64*iter + k][64*iter + j]));
	  }
	}
      }
    }

    // phase 1
    for(size_t k=0; k<64; k++){
      for(size_t i=0; i<64*iter; i++){
	for(size_t j=0; j<64; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[i][64*iter + j]>(dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][64*iter + j]))
	     && (prev_w_pt[i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][64*iter + j] != NODEID_NULL)){
	    dist_w_pt[i][64*iter + j] = dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][64*iter + j];
	    prev_w_pt[i][64*iter + j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[i][64*iter + j] > dist_w_pt[i][64*iter + k]) && (dist_w_pt[i][64*iter + j] > dist_w_pt[64*iter + k][64*iter + j]));
	  }
	}
      }
    }
    for(size_t k=0; k<64; k++){
      for(size_t i=64*(iter+1); i<num_nodes_w; i++){
	for(size_t j=0; j<64; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[i][64*iter + j]>(dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][64*iter + j]))
	     && (prev_w_pt[i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][64*iter + j] != NODEID_NULL)){
	    dist_w_pt[i][64*iter + j] = dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][64*iter + j];
	    prev_w_pt[i][64*iter + j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[i][64*iter + j] > dist_w_pt[i][64*iter + k]) && (dist_w_pt[i][64*iter + j] > dist_w_pt[64*iter + k][64*iter + j]));
	  }
	}
      }
    }
    for(size_t k=0; k<64; k++){
      for(size_t i=0; i<64; i++){
	for(size_t j=0; j<64*iter; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[64*iter + i][j]>(dist_w_pt[64*iter + i][64*iter + k] + dist_w_pt[64*iter + k][j]))
	     && (prev_w_pt[64*iter + i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][j] != NODEID_NULL)){
	    dist_w_pt[64*iter + i][j] = dist_w_pt[64*iter + i][64*iter + k] + dist_w_pt[64*iter + k][j];
	    prev_w_pt[64*iter + i][j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[64*iter + i][j] > dist_w_pt[64*iter + i][64*iter + k]) && (dist_w_pt[64*iter + i][j] > dist_w_pt[64*iter + k][j]));
	  }
	}
      }
    }
    for(size_t k=0; k<64; k++){
      for(size_t i=0; i<64; i++){
	for(size_t j=64*(iter+1); j<num_nodes_w; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[64*iter + i][j]>(dist_w_pt[64*iter + i][64*iter + k] + dist_w_pt[64*iter + k][j]))
	     && (prev_w_pt[64*iter + i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][j] != NODEID_NULL)){
	    dist_w_pt[64*iter + i][j] = dist_w_pt[64*iter + i][64*iter + k] + dist_w_pt[64*iter + k][j];
	    prev_w_pt[64*iter + i][j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[64*iter + i][j] > dist_w_pt[64*iter + i][64*iter + k]) && (dist_w_pt[64*iter + i][j] > dist_w_pt[64*iter + k][j]));
	  }
	}
      }
    }

    // phase 2
    for(size_t k=0; k<64; k++){
      for(size_t i=0; i<64*iter; i++){
	for(size_t j=0; j<64*iter; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[i][j]>(dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j]))
	     && (prev_w_pt[i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][j] != NODEID_NULL)){
	    dist_w_pt[i][j] = dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j];
	    prev_w_pt[i][j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[i][j] > dist_w_pt[i][64*iter + k]) && (dist_w_pt[i][j] > dist_w_pt[64*iter + k][j]));
	  }
	}
      }
    }
    for(size_t k=0; k<64; k++){
      for(size_t i=0; i<64*iter; i++){
	for(size_t j=64*(iter+1); j<num_nodes_w; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[i][j]>(dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j]))
	     && (prev_w_pt[i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][j] != NODEID_NULL)){
	    dist_w_pt[i][j] = dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j];
	    prev_w_pt[i][j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[i][j] > dist_w_pt[i][64*iter + k]) && (dist_w_pt[i][j] > dist_w_pt[64*iter + k][j]));
	  }
	}
      }
    }
    for(size_t k=0; k<64; k++){
      for(size_t i=64*(iter+1); i<num_nodes_w; i++){
	for(size_t j=0; j<64*iter; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[i][j]>(dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j]))
	     && (prev_w_pt[i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][j] != NODEID_NULL)){
	    dist_w_pt[i][j] = dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j];
	    prev_w_pt[i][j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[i][j] > dist_w_pt[i][64*iter + k]) && (dist_w_pt[i][j] > dist_w_pt[64*iter + k][j]));
	  }
	}
      }
    }
    for(size_t k=0; k<64; k++){
      for(size_t i=64*(iter+1); i<num_nodes_w; i++){
	for(size_t j=64*(iter+1); j<num_nodes_w; j++){
	  // note that the second and third conditions are for overflow
	  if((dist_w_pt[i][j]>(dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j]))
	     && (prev_w_pt[i][64*iter + k] != NODEID_NULL)
	     && (prev_w_pt[64*iter + k][j] != NODEID_NULL)){
	    dist_w_pt[i][j] = dist_w_pt[i][64*iter + k] + dist_w_pt[64*iter + k][j];
	    prev_w_pt[i][j] = 64*iter + k;
	    // check for overflow (might affect performance)
	    ASSERTA((dist_w_pt[i][j] > dist_w_pt[i][64*iter + k]) && (dist_w_pt[i][j] > dist_w_pt[64*iter + k][j]));
	  }
	}
      }
    }
    
  }

  if(num_nodes != num_nodes_w){
    for(uint32_t i=0; i<num_nodes; i++){
      for(uint32_t j=0; j<num_nodes; j++){
	path->dist_pt[i][j] = dist_w_pt[i][j];
	path->prev_pt[i][j] = prev_w_pt[i][j];
      }
    }
    free(dist_w);
    free(prev_w);
    free(dist_w_pt);
    free(prev_w_pt);
  }  



}

