/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>

#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"

const bool __attribute__((unused)) debug_sm_idx = false;
#if __CUDACC_VER_MAJOR__ >= 9

#define __shfl(v,src) \
 ({ assert( __activemask() == ~0 ); __shfl_sync(~0,v,src); })

#endif

__constant__ cu_graph_CH_UD_t graph_UD_c;
__constant__ cu_graph_CH_bi_t graph_bi_c;
__constant__ CH_Options chopts_c;

// If true, don't replace an edge with a shortcut of same or higher
// weight. This must be true if the witness path search might miss an
// edge between the broken path endpoints.
const bool shc_repl_wht_check = true;

__host__ void
cu_CH_update_graph_pre_launch
(cu_graph_CH_UD_t *graph_UD, cu_graph_CH_bi_t *graph_bi_d)
{
  CE( cudaMemcpyToSymbolAsync
      (graph_UD_c,graph_UD,sizeof(graph_UD_c), 0, cudaMemcpyDeviceToDevice) );
  CE( cudaMemcpyToSymbolAsync
      (graph_bi_c,graph_bi_d,sizeof(graph_bi_c), 0, cudaMemcpyDeviceToDevice) );

  cuda_sym_host_to_dev(chopts_c,&chopts);
}

__device__ void max_nnbr_enlarge(uint32_t& max_nnbr, int tpn)
{
  max_nnbr += tpn < 5 ? 4 : 1 << tpn - 2;
}


__global__ void
cu_CH_update_graph_01
( cu_graph_CH_bi_t *graph_d, cu_shc_CH_t shc_list )
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int lane = threadIdx.x % wp_sz;
  const int block_lg = 5;
  const int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );

  cu_graph_CH_bi_t* const graph = &graph_bi_c;

  const int tpn = graph_d->thread_per_node;

  // phase 1, write back the shortcuts onto the working graph, handled by multiple blocks

  /// we assume this kernel is called with the same grid_dim as the contraction kernel, thus allowing
  /// each block to write back the shortcuts written by the corresponding block.
  /// assert(gridDim.x == shc_list.n_segs);

  // number of vertices handled at each iteration by each block

  constexpr int n_wps = block_dim >> wp_lg;
  const uint32_t shc_per_iter = n_wps;
  nodeid_t current_pos = threadIdx.x >> 5; // current warp's position within the shc_list
  nodeid_t lid = threadIdx.x >> 5; // warp ID

  const uint32_t tnid = threadIdx.x - (lid<<5); // thread's id within corresponding warp

  // length of the forward and backward edge lists
  const uint32_t max_num_edges_backward = graph->graph_b.max_num_edges;

  // current block's shc_list length
  const uint32_t llen = shc_list.len_dst[blockIdx.x];

  const uint32_t llen_max = shc_list.max_len_per_seg;

  uint32_t node_p;
  uint32_t nnbr;
  uint32_t max_nnbr;

  /// each warp handles exactly 1 shc. because a) max_degree/tpn can change as this kernel progresses

  while(current_pos < llen){
    shc_t ei = shc_list.shc_dst[llen_max*blockIdx.x + current_pos];

    // attempt to get a lock on the destination vertex of the shc (only one thread attempts)

    node_p = graph->graph_b.pointer[ei.dst_id];
    nnbr = graph->graph_b.num_neighbors[ei.dst_id];
    max_nnbr = graph->graph_b.max_num_neighbors_allowed[ei.dst_id];

    // check to see if an edge already exists, if so update it.
    volatile __shared__ bool done;
    assert( n_wps == 1 );
    if ( tnid == 0 ) done = false;
    int bwd_e_idx_avail = -1;

    for ( int nbr_0 = 0;  nbr_0 < nnbr;  nbr_0 += wp_sz ) {
      __syncwarp();
      const int nbr = nbr_0 + lane;
      if ( done || nbr >= nnbr ) break;

      const nodeid_t bwd_e_idx = node_p + nbr;
      const nodeid_t bwd_nbr = graph->graph_b.neighbors[bwd_e_idx];
      if ( nodeid_is_special(bwd_nbr) ) { bwd_e_idx_avail=bwd_e_idx; continue; }
      if ( ei.src_id != bwd_nbr ) continue;

      if ( !shc_repl_wht_check || ei.wht < graph->graph_b.weights[bwd_e_idx] )
        {
          graph->graph_b.weights[bwd_e_idx] = ei.wht;
          // midpoint uses the input IDs
          graph->graph_b.midpoint[bwd_e_idx] = ei.mid_oid;
        }
      done=1;
    }

    __syncwarp();

    if ( !done )
      {
        const bool found_slot = bwd_e_idx_avail >= 0;
        const uint32_t space_vec = __ballot_sync( ~0, found_slot );
        const uint32_t lower_mask = ( 1 << lane ) - 1;
        if ( found_slot && ! ( space_vec & lower_mask ) )
          {
            done = 1;
            graph->graph_b.neighbors[bwd_e_idx_avail] = ei.src_id;
            graph->graph_b.weights[bwd_e_idx_avail] = ei.wht;
            // midpoint uses the input IDs
            graph->graph_b.midpoint[bwd_e_idx_avail] = ei.mid_oid;
            atomicAdd( &graph->graph_f.num_neighbors[ei.src_id], 1 );
          }
      }

    __syncwarp();

    if ( !done ) {
      // if there is room to add shc, do it, otherwise, move the node to the end of empty space
      /// need to add the contingency plan for when no more empty space is available.

      if ( !tnid )
        atomicAdd( &graph->graph_f.num_neighbors[ei.src_id], 1 );

      if(nnbr<max_nnbr){
        if(tnid == 0){
          graph->graph_b.num_neighbors[ei.dst_id] = nnbr + 1;

          graph->graph_b.neighbors[node_p + nnbr] = ei.src_id;
          graph->graph_b.weights[node_p + nnbr] = ei.wht;
          // midpoint uses the input IDs
          graph->graph_b.midpoint[node_p + nnbr] = ei.mid_oid;
        }
      } else {
        uint32_t new_node_p;
        // adding empty slots is in accordance with our design of cu_graph_CH
        max_nnbr_enlarge(max_nnbr,tpn);
        if(tnid == 0){
          // move the empty pointer forward and use the space to move the entire vertex into
          new_node_p = atomicAdd(&(graph_d->graph_b.empty_pointer), max_nnbr);
	  
          // make sure that there is still empty space left
          if(new_node_p + max_nnbr > max_num_edges_backward){
            /// need to add the contingency plan for when no more empty space is available.
            /// goal is to rewrite the entire remaining overlay graph with transformed vertex ids.
            assert( false );
            //  printf("ran out of space!!! CONTINGENCY PLAN NOT FOUND, ABORT, line: %u\n", __LINE__);
            ///////
          }

          // update the pointer/nnbr/nnbr_ol/max_nnbr
          graph->graph_b.pointer[ei.dst_id] = new_node_p;
          graph->graph_b.num_neighbors[ei.dst_id] = nnbr + 1;
          graph->graph_b.max_num_neighbors_allowed[ei.dst_id] = max_nnbr;
        }
        // broadcast the new pointer to all warps
        __syncwarp();
        new_node_p = __shfl(new_node_p, 0);

        // copy the vertex to its new location
        for ( int nbr = tnid;  nbr < nnbr;  nbr += wp_sz ) {
          graph->graph_b.neighbors[new_node_p + nbr] = graph->graph_b.neighbors[node_p + nbr];
          graph->graph_b.weights[new_node_p + nbr] = graph->graph_b.weights[node_p + nbr];
          graph->graph_b.midpoint[new_node_p + nbr] = graph->graph_b.midpoint[node_p + nbr];
        }
        if(tnid == 0){
          // add the new edge
          graph->graph_b.neighbors[new_node_p + nnbr] = ei.src_id;
          graph->graph_b.weights[new_node_p + nnbr] = ei.wht;
          // midpoint uses the input IDs
          graph->graph_b.midpoint[new_node_p + nnbr] = ei.mid_oid;
        }
      }
    }
    current_pos += shc_per_iter; // update current_pos for next iteration
  }
}


template <uint32_t tpn> __global__ void  __launch_bounds__(block_dim_update_1)
cu_CH_update_graph_1(cu_graph_CH_UD_t *graph_UD_d, cu_graph_CH_bi_t *graph_d, nodeid_t *node_list, nodeid_t list_length, uint32_t rank)
{
  cu_graph_CH_bi_t* const graph = &graph_bi_c;
  cu_graph_CH_UD_t* const graph_UD = &graph_UD_c;

  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int wp_num = threadIdx.x >> wp_lg;
  const int lane = threadIdx.x & ( wp_sz - 1 );

  const int block_lg = block_lg_update_1;
  const int block_size = 1 << block_lg;
  assert( block_size == blockDim.x );

  const int e_tpn = tpn > block_lg_update_1 ? block_lg_update_1 : tpn; // tpn capped at 8
  const int E_TPN = 1 << e_tpn;

  const int iter_per_node = 1 << (tpn - e_tpn);

  // Assign work to different warps to conserve registers and maybe
  // increase memory-level parallelism.
  const int n_indep_warps = 2;
  assert( block_size >> wp_lg >= n_indep_warps );
# define SELWP(w0,w1) ( wp_num == 0 ? w0 : w1 )

  // number of vertices handled at each iteration by each block
  const int vertex_per_iter_lg = block_lg - e_tpn;
  const int vertex_per_iter = 1 << vertex_per_iter_lg;

  // Make sure that one warp can handle per-vertex work.
  assert( vertex_per_iter_lg <= wp_lg );

  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * block_size;
  // compute num_threads
  const int num_thd = block_size * gridDim.x;

  // phase 2; mark contracted vertices in the working graph and add
  // them to the output UD graph
  //
  // note, we won't remove contracted nodes and their edges here, a
  // cleanup kernel will handle that
  //
  // note 2, the edge list here will use original node id domain info,
  // once the entire graph is contracted, a sweep of the edge list
  // will re-number all edges according to their new id while possible
  // to do that here, doing it at the end allows much more
  // parallelization opportunities

  /// might be possible to re-order phase 1 and phase 2, that way in
  /// case we've ran out of empty space in current iteration, our
  /// overlay will not include the vertices contracted in current
  /// iteration.
  
  // we'll do everything by TPN, and with multiple blocks

  /// IMPORTANT NOTE: TPN was modified in phase one of this kernel,
  // however for phase two we are going to be using the old TPN
  // value which was passed in as a template parameter and care must
  // be taken not to load the updated TPN. Use of old TPN is
  // possible because we're guaranteed that no shortcuts were added
  // to the nodes contracted in this iteration.
    
  // increment value in each iteration
  const int vertex_inc = num_thd >> e_tpn;
  // calculate num_iters to avoid using a while loop to avoid syncthreads imbalance
  const nodeid_t num_iters = div_up(list_length, vertex_inc);
  
  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)

  constexpr int n_elists = 6, n_nlists = 10;
  const int n_selts = ((n_nlists*(block_size>>e_tpn)) + n_elists*block_size);
  __shared__ uint32_t shm[n_selts];

  int make_f_pos = 0;
  auto make_f = [&](uint n) {
    const int pos = make_f_pos;
    make_f_pos += n;
    return [=](uint idx) -> uint32_t&
    { assert( !debug_sm_idx || idx < n ); return shm[pos+idx]; };  };

  auto SH_CURRENT_ID = make_f(vertex_per_iter);
  auto SH_CURRENT_ID_ORIGIN = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_F = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_B = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_F = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_B = make_f(vertex_per_iter);
  auto SH_UF_POINTER = make_f(vertex_per_iter);
  auto SH_UF_NNBR = make_f(vertex_per_iter);
  auto SH_UF_NBR = make_f(block_size);
  auto SH_UF_WEIGHT = make_f(block_size);
  auto SH_UF_MIDPOINT = make_f(block_size);
  auto SH_DB_NBR = make_f(block_size);
  auto SH_DB_WEIGHT = make_f(block_size);
  auto SH_DB_MIDPOINT = make_f(block_size);
  auto SH_DB_POINTER = make_f(vertex_per_iter);
  auto SH_DB_NNBR = make_f(vertex_per_iter);

  assert( n_selts == make_f_pos );

  nodeid_t current_pos = tid>>e_tpn; // current thread group's position within node_list
  // local_ID, thread group's working set id within current block (needed for shared mem accesses)
  nodeid_t lid = threadIdx.x >> e_tpn;
  // starting current_pos per block (current_pos of thread 0)
  nodeid_t current_pos_0 = (blockIdx.x * block_size) >> e_tpn;

  uint32_t levels_pt = graph_UD->node_levels_pt[rank-1];
  nodeid_t max_num_edges_uf = graph_UD->graph_u_f.max_num_edges;
  nodeid_t max_num_edges_db = graph_UD->graph_d_b.max_num_edges;
  nodeid_t UD_max_deg = graph_UD->max_degree;

  // have one thread update num_nodes and set levels_pt for next level
  if(blockIdx.x==0 && threadIdx.x==0){
    graph_UD_d->graph_u_f.num_nodes += list_length;
    graph_UD_d->graph_d_b.num_nodes += list_length;
    assert(graph_UD->max_rank > rank);
    /// if not enough space available in the UD graph, will have to reallocate with more space,
    /// copy everything already in UD, and then resume everything (code to be added)
    ///////////////
    graph_UD->node_levels_pt[rank] = levels_pt + list_length;
    graph_d->overlay_size -= list_length;
  }

  const uint32_t tnid = threadIdx.x & ( E_TPN - 1 );

  for(uint32_t iter = 0; iter < num_iters; iter++){
    // load current iteration's node IDs and the corresponding pointer/nnbr into shared
    if(threadIdx.x < vertex_per_iter && (current_pos_0+threadIdx.x < list_length)){
      const nodeid_t gnid = node_list[current_pos_0 + threadIdx.x];
      const nodeid_t onid = graph->node_ids[gnid];
      SH_CURRENT_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[gnid];
      SH_CURRENT_POINTER_B( threadIdx.x ) = graph->graph_b.pointer[gnid];
      SH_CURRENT_NNBR_F( threadIdx.x ) = graph->graph_f.num_neighbors[gnid];
      SH_CURRENT_NNBR_B( threadIdx.x ) = graph->graph_b.num_neighbors[gnid];
      SH_CURRENT_ID( threadIdx.x ) = gnid;
      SH_CURRENT_ID_ORIGIN( threadIdx.x ) = onid;
      graph->node_ranks[gnid] = rank;

      SH_UF_NNBR( threadIdx.x ) = 0;
      SH_DB_NNBR( threadIdx.x ) = 0;
    }

    // sync needed here because the first warps are cooperating in
    // loading the entire block's data

    __syncthreads();
    const bool nd_here = current_pos < list_length;

    const int deg_max = max( SH_CURRENT_NNBR_F(lid), SH_CURRENT_NNBR_B(lid) );
    const int ipn_check = div_up( deg_max, E_TPN );
    assert( !nd_here || iter_per_node >= ipn_check );

    // prepare uf/ub/db nodes
    for(int i=0; i<iter_per_node; i++){
      const int e_tnid = i*E_TPN + tnid;

      const bool fwd_edge_elt_here =
        e_tnid < SH_CURRENT_NNBR_F( lid ) && nd_here;
      const bool bwd_edge_elt_here =
        e_tnid < SH_CURRENT_NNBR_B( lid ) && nd_here;

      const nodeid_t fwd_eidx = SH_CURRENT_POINTER_F( lid ) + e_tnid;
      const nodeid_t fwd_nbr =
        fwd_edge_elt_here ? graph->graph_f.neighbors[fwd_eidx] : nodeid_invalid;
      const bool fwd_edge_here = !nodeid_is_special(fwd_nbr);

      const nodeid_t bwd_eidx = SH_CURRENT_POINTER_B( lid ) + e_tnid;
      const nodeid_t bwd_nbr =
        bwd_edge_elt_here ? graph->graph_b.neighbors[bwd_eidx] : nodeid_invalid;
      const bool bwd_edge_here = !nodeid_is_special(bwd_nbr);

      Prefix_Elt pfx_fwd_keep = prefix_bit(block_lg, e_tpn, fwd_edge_here);
      Prefix_Elt pfx_bwd_keep = prefix_bit(block_lg, e_tpn, bwd_edge_here);
      if ( tnid == 0 )
        {
          SH_UF_NNBR( lid ) += pfx_fwd_keep.sum;
          SH_DB_NNBR( lid ) += pfx_bwd_keep.sum;
        }
    }

    __syncthreads();

    if ( wp_num < 2 ) {
      const bool participant = lane < vertex_per_iter && current_pos_0 + lane < list_length;
      const nodeid_t deg = !participant ? 0 :
        SELWP( SH_UF_NNBR(lane), SH_DB_NNBR(lane) );
      const int deg_8 = rnd_up(deg,cu_graph_ud_alloc_mult);

      set_max( UD_max_deg, deg );

      Prefix_Elt pfx = prefix_wp(deg_8,vertex_per_iter_lg);

      uint32_t pointer_base;

      if( lane == 0 ){

	// allocate and assign the space for storing edge data

	nodeid_t* const empty_ptr_ptr =
	  SELWP( &graph_UD_d->graph_u_f.empty_pointer,
		 &graph_UD_d->graph_d_b.empty_pointer );

	pointer_base = atomicAdd(empty_ptr_ptr, pfx.sum);

	const nodeid_t max_num_edges =
	  SELWP( max_num_edges_uf, max_num_edges_db );

	assert( pointer_base + pfx.sum < max_num_edges );

	/// if not enough space available in the UD graph, will
	/// have to reallocate with more space, copy everything
	/// already in UD, and then resume everything (code to be
	/// added)
	///////////////
	
      }

      // add the base pointer to each of the node pointers
      __syncwarp();
      const nodeid_t ptr_next = pfx.pfx_excl + __shfl(pointer_base,0);
      if ( participant ) {
	SELWP( SH_UF_POINTER( lane ), SH_DB_POINTER( lane ) ) = ptr_next;
      }
    }

    __syncthreads();
    
    // write UF/UB/DB into global
      
    if(threadIdx.x < vertex_per_iter && (current_pos_0+threadIdx.x < list_length)){
      graph_UD->graph_u_f.pointer[levels_pt + current_pos_0 + threadIdx.x] = SH_UF_POINTER( threadIdx.x );
      graph_UD->graph_d_b.pointer[levels_pt + current_pos_0 + threadIdx.x] = SH_DB_POINTER( threadIdx.x );
      graph_UD->graph_u_f.num_neighbors[levels_pt + current_pos_0 + threadIdx.x] = SH_UF_NNBR( threadIdx.x );
      graph_UD->graph_d_b.num_neighbors[levels_pt + current_pos_0 + threadIdx.x] = SH_DB_NNBR( threadIdx.x );

      graph_UD->node_ranks[levels_pt + current_pos_0 + threadIdx.x] = rank;
      graph_UD->node_levels[levels_pt + current_pos_0 + threadIdx.x] = rank-1;
      graph_UD->node_idx[levels_pt + current_pos_0 + threadIdx.x] = SH_CURRENT_ID_ORIGIN( threadIdx.x );
      graph_UD->node_idx_inv[SH_CURRENT_ID_ORIGIN( threadIdx.x )] = levels_pt + current_pos_0 + threadIdx.x;
    }

    int uf_tnid = tnid;
    int db_tnid = tnid;
    for(int i=0; i<iter_per_node; i++){

      const int e_tnid = i*E_TPN + tnid;

      const bool fwd_edge_elt_here =
        e_tnid < SH_CURRENT_NNBR_F( lid ) && current_pos < list_length;
      const bool bwd_edge_elt_here =
        e_tnid < SH_CURRENT_NNBR_B( lid ) && current_pos < list_length;

      const nodeid_t fwd_eidx = SH_CURRENT_POINTER_F( lid ) + e_tnid;
      const nodeid_t fwd_nbr =
        fwd_edge_elt_here ? graph->graph_f.neighbors[fwd_eidx] : nodeid_invalid;
      const bool fwd_edge_here = !nodeid_is_special(fwd_nbr);

      const nodeid_t bwd_eidx = SH_CURRENT_POINTER_B( lid ) + e_tnid;
      const nodeid_t bwd_nbr =
        bwd_edge_elt_here ? graph->graph_b.neighbors[bwd_eidx] : nodeid_invalid;
      const bool bwd_edge_here = !nodeid_is_special(bwd_nbr);

      Prefix_Elt pfx_fwd_keep = prefix_bit(block_lg, e_tpn, fwd_edge_here);
      Prefix_Elt pfx_bwd_keep = prefix_bit(block_lg, e_tpn, bwd_edge_here);
      if ( tnid == 0 )
        {
          SH_UF_NNBR( lid ) = pfx_fwd_keep.sum;
          SH_DB_NNBR( lid ) = pfx_bwd_keep.sum;
        }

      const int sidx_0 = lid << e_tpn;
      if ( fwd_edge_here )
        {
          const int sidx = sidx_0 + pfx_fwd_keep.pfx_excl;
          const nodeid_t mid_maybe = graph->graph_f.midpoint[ fwd_eidx ];
          SH_UF_NBR( sidx ) = graph->node_ids[ fwd_nbr ];
          SH_UF_WEIGHT( sidx ) = graph->graph_f.weights[ fwd_eidx ];
          SH_UF_MIDPOINT( sidx ) = graph->graph_b.midpoint[ mid_maybe ];
        }

      if ( bwd_edge_here )
        {
          // load the current iteration's backward edges.
          const int sidx = sidx_0 + pfx_bwd_keep.pfx_excl;
          SH_DB_WEIGHT( sidx ) = graph->graph_b.weights[bwd_eidx];
          SH_DB_MIDPOINT( sidx ) = graph->graph_b.midpoint[bwd_eidx];
          SH_DB_NBR( sidx ) = graph->node_ids[bwd_nbr];
        }

      __syncthreads();

      if(current_pos < list_length){
	// write u_f edges
	if(tnid < SH_UF_NNBR( lid )){
          const nodeid_t geidx = SH_UF_POINTER( lid ) + uf_tnid;
	  graph_UD->graph_u_f.neighbors[ geidx ] = SH_UF_NBR( threadIdx.x );
	  graph_UD->graph_u_f.weights[ geidx ] = SH_UF_WEIGHT( threadIdx.x );
	  graph_UD->graph_u_f.midpoint[ geidx ] = SH_UF_MIDPOINT( threadIdx.x );
	}
	// write d_b edges
	if(tnid < SH_DB_NNBR( lid )){
	  const int sidx = threadIdx.x;
	  graph_UD->graph_d_b.neighbors[SH_DB_POINTER( lid ) + db_tnid] = SH_DB_NBR( sidx );
	  graph_UD->graph_d_b.weights[SH_DB_POINTER( lid ) + db_tnid] = SH_DB_WEIGHT( sidx );
	  graph_UD->graph_d_b.midpoint[SH_DB_POINTER( lid ) + db_tnid] = SH_DB_MIDPOINT( sidx );
	}
      }
      uf_tnid += SH_UF_NNBR( lid );
      db_tnid += SH_DB_NNBR( lid );
    }
    // setup next iteration
    current_pos += vertex_inc;
    current_pos_0 += vertex_inc;
    __syncthreads();
  }

  // update max degree of the UD graph
  if ( wp_num < 2 ) {
    int ud_max = max_wp(UD_max_deg,vertex_per_iter_lg);
    if ( lane == 0 ) atomicMax(&graph_UD_d->max_degree, ud_max);
  }
}

#define INST(tpn)							\
  template __global__ void cu_CH_update_graph_1<tpn>			\
  (cu_graph_CH_UD_t *graph_UD, cu_graph_CH_bi_t* graph_d, nodeid_t *node_list, nodeid_t list_length, uint32_t rank);


INST(3); INST(4); INST(5); INST(6); INST(7); INST(8); INST(9); INST(10); INST(11); INST(12);
#undef INST

