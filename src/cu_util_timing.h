/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef CU_UTIL_TIMING_H
#define CU_UTIL_TIMING_H

#include <time.h>
#include <string>
#include <map>

inline double
time_wall_fp()
{
  struct timespec now;
  clock_gettime(CLOCK_REALTIME,&now);
  return now.tv_sec + ((double)now.tv_nsec) * 1e-9;
}

class Time_Wall_s {
public:
  Time_Wall_s():time_start_ues(time_wall_fp()){};
  operator double() const { return s(); }
  double s() const { return time_wall_fp() - time_start_ues; }
private:
  const double time_start_ues;
};

class P_Timer {
public:
  P_Timer(const char* namep)
    :name(namep), text(""),
    start_event_ues(time_wall_fp()), last_event_ues(start_event_ues) {}
  const std::string name;
  std::string text;
  std::map<std::string,double> events_ues;
  std::map<std::string,int> events_n;
  double start_event_ues;
  double last_event_ues;
  double dur_total_s()
    {
      const double dur = last_event_ues - start_event_ues;
      text += name + " -- TOTAL TIME -- s\n" + std::to_string(dur) + "\n";
      return dur;
    }
  double dur_s(const char *descr = NULL)
    {
      const double prev = last_event_ues;
      if ( descr )
        {
          events_ues[descr] = last_event_ues;
          events_n[descr]++;
        }
      last_event_ues = time_wall_fp();
      const double dur = last_event_ues - prev;
      const double dur_from_start = last_event_ues - start_event_ues;
      if ( descr )
        text += name + " -- " + descr + " -- s\n"
          + std::to_string(dur) + "   " + std::to_string(dur_from_start)
          + "\n";
      return dur;
    }
  double time_ues(const char *ename)
    {
      assert( events_n[ename] == 1 );
      return events_ues[ename];
    }
  double dur_starting_from_s(const char *first_ename)
    {
      return last_event_ues - time_ues(first_ename);
    }
};

#endif
