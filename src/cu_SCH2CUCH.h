/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __CU_SCH2CUCH_H__
#define __CU_SCH2CUCH_H__

#include "main.h"
#include "graphs.h"
#include "cu_CH.h"


void graph_SCH_remove_dups(graph_CH_bi_t *graph);
void graph_SCH_2_CUCH_d(cu_graph_CH_UD_t **graph, graph_CH_bi_t *graph_in, uint32_t *node_levels);

int graph_GCH_get_from_file(graph_CH_bi_t **graph, FILE *f);


void SCH_assign_levels(graph_CH_bi_t *graph, uint32_t *node_levels);





#endif 
