/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include "cu_CH.cuh"
#include <vector>
#include <algorithm>

using namespace std;


__global__ void
cu_degree_sum_get(cu_graph_CH_bi *graph, int *degree_sums)
{
  nodeid_t* const nn_fwd = graph->graph_f.num_neighbors;
  nodeid_t* const ranks = graph->node_ranks;
  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  const nodeid_t nnodes = graph->num_nodes;
  assert( blockDim.x == 1024 );
  const int grid_size = 1024 * gridDim.x;
  int dsum = 0;
  for ( int i = tid; i < nnodes; i += grid_size )
    {
      assert( !ranks[i] );
      dsum += nn_fwd[i];
    }
  __shared__ int dsums[1024];
  const int tidx = threadIdx.x;
  dsums[tidx] = dsum;
  sync_threads();
  for ( int dist = 512; dist > 0; dist = dist >> 1 )
    {
      if ( tidx < dist ) dsums[tidx] += dsums[tidx+dist];
      sync_threads();
    }
  if ( tidx == 0 ) degree_sums[blockIdx.x] = dsums[0];
}

__device__ void
max_degree_update(cu_graph_CH_t* graph)
{
  // Set max_deg member in cu_graph_CH_t. Intended for performance
  // tuning and debugging.

  constexpr int block_lg = 10;
  constexpr int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int tid = blockIdx.x * block_dim + threadIdx.x;
  const nodeid_t n_nodes = graph->num_nodes;
  const nodeid_t n_thds = gridDim.x * block_dim;
  nodeid_t* const g_deg = graph->num_neighbors;
  uint t_max_deg = 0;
  for ( nodeid_t nid = tid;  nid < n_nodes;  nid += n_thds )
    set_max( t_max_deg, g_deg[nid] );
  atomicMax( &graph->look_max_degree, t_max_deg );
}

__global__ void
max_degree_update(cu_graph_CH_bi_t* graph)
{
  max_degree_update(&graph->graph_f);
  max_degree_update(&graph->graph_b);
}
__global__ void
max_degree_update(cu_graph_CH_UD_t* graph)
{
  max_degree_update(&graph->graph_d_b);
  max_degree_update(&graph->graph_u_f);
  max_degree_update(&graph->graph_u_b);
}


void
tune_elist_sort(cu_graph_CH_bi_t *g_d)
{
  cu_graph_CH_bi_t *g;
  cu_graph_CH_bi_t gd = cuda_dev_to_host(g_d);
  graph_cu_CH_bi_d2h(&g, g_d);
  const nodeid_t nnodes = g->num_nodes;
  const int max_deg = 4096;
  vector<nodeid_t> cpy(max_deg);

  auto esort = [&](cu_graph_CH_t& gr)
    {
      for ( nodeid_t nid = 0; nid < nnodes; nid++ )
        {
          int nnbr = gr.num_neighbors[nid];
          nodeid_t e0 = gr.pointer[nid];
          vector<nodeid_t> ord(nnbr);
          for ( int i=0; i<nnbr; i++ ) ord[i] = e0+i;
          sort(ord.begin(),ord.end(),
               [&](nodeid_t ea, nodeid_t eb)
               {return gr.neighbors[ea] < gr.neighbors[eb];} );
          for ( nodeid_t* base: { gr.neighbors, gr.weights, gr.midpoint } )
            {
              for ( int i=0; i<nnbr; i++ ) cpy[i] = base[e0+i];
              for ( int i=0; i<nnbr; i++ ) base[e0+i] = cpy[ord[i]-e0];
            }
        }
    };

  esort(g->graph_f);
  esort(g->graph_b);

  auto htod = [&](cu_graph_CH_t& grd, cu_graph_CH_t& grh)
    {
      auto htod = [&](nodeid_t *dst, nodeid_t *src)
                  { CE( cudaMemcpyAsync
                        ( dst, src, grd.empty_pointer * sizeof(src[0]),
                          cudaMemcpyHostToDevice ) ); };
      htod(grd.neighbors,grh.neighbors);
      htod(grd.weights,grh.weights);
      htod(grd.midpoint,grh.midpoint);
    };
  htod(gd.graph_f,g->graph_f);
  htod(gd.graph_b,g->graph_b);

  graph_cu_CH_bi_free_host(g);
};
