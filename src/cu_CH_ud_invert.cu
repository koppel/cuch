/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>
#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"
#include "cu_util_timing.cuh"

using namespace std;

void
cu_CH_ud_invert_launch
( GPU_Info& gpu_info, vector<cuda_event>& ev_array, cu_CH_stats_t *stats,
  cu_graph_CH_bi_t *ov_graph_d,
  cu_graph_CH_UD_t& ud_graph, cu_graph_CH_UD_t* ud_graph_d )
{
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;
  const int mp_thds_max = gpu_info.cuda_prop.maxThreadsPerMultiProcessor;
  const int udi_block_dim = 1024;
  const int bl_per_mp = mp_thds_max / udi_block_dim;
  assert( bl_per_mp );
  const int udi_grid_dim = bl_per_mp * nmps;

  ev_array[0].record();
  cu_CH_ud_invert_0<<<udi_grid_dim,udi_block_dim>>>
    (ov_graph_d,ud_graph, ud_graph_d);
  ev_array[1].record();
  cu_CH_ud_invert_1<<<udi_grid_dim,udi_block_dim>>>(ud_graph);
  ev_array[2].record();
  cu_CH_ud_invert_2<<<udi_grid_dim,udi_block_dim>>>(ud_graph,ud_graph_d);
  ev_array[3].record();
  cu_CH_ud_invert_3<<<udi_grid_dim,udi_block_dim>>>(ud_graph);
  ev_array[4].record();

  ev_array[4].sync();
  double* const t_inv_i = &stats->time_ud_invert_0_s; // Don't do this at home.
  for ( int i=0; i<4; i++ )
    t_inv_i[i] = elapsed_time_get( ev_array[i], ev_array[i+1] );

  const bool tune = false;
  if ( !tune ) return;
  pr.tune("UD Invert  %6.0f %6.0f %6.0f %6.0f\n",
          1e6*t_inv_i[0], 1e6*t_inv_i[1], 1e6*t_inv_i[2], 1e6*t_inv_i[3]);
}

__global__ void
cu_CH_ud_invert_0
(cu_graph_CH_bi_t* ograph, cu_graph_CH_UD_t graph, cu_graph_CH_UD_t* graph_d)
{
  constexpr int block_lg = 10;
  constexpr int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int n_threads = gridDim.x << block_lg;
  const int tid = blockIdx.x * block_dim + threadIdx.x;

  const nodeid_t nnodes_uf = graph.graph_u_f.num_nodes;
  const nodeid_t nnodes_or = graph.num_nodes;
  const int k_size = nnodes_or - nnodes_uf;
  assert( nnodes_uf < nnodes_or );
  assert( k_size > 128 && k_size <= n_threads );

  // Add remaining overlay nodes to up/down graph's node maps.
  {
    const nodeid_t nid_ov = tid; // Overlay node id.
    const nodeid_t nid_ud = nnodes_uf + tid;  // Up/down graph node id.
    if ( tid < k_size )
      {
        const nodeid_t nid_or = ograph->node_ids[nid_ov];
        assert( graph.node_idx_inv[nid_or] == nodeid_invalid );
        graph.node_idx[nid_ud] = nid_or;
        graph.node_idx_inv[nid_or] = nid_ud;
      }
    if ( tid + 1 == n_threads ) graph_d->graph_u_b.num_nodes = nnodes_or;
  }

  // Determine degree of up-backward graph's nodes.
  //
  nodeid_t* const u_b_deg = graph.graph_u_b.pointer; // Borrow this storage.
  uint t_max_deg = 0;
  const nodeid_t elist_sz = graph.graph_u_f.empty_pointer;
  for ( nodeid_t e_idx = tid;  e_idx < elist_sz;  e_idx += n_threads )
    {
      const nodeid_t nid_or = graph.graph_u_f.neighbors[e_idx];
      const uint deg = 1 + atomicAdd( &u_b_deg[nid_or], 1 );
      set_max( t_max_deg, deg );
    }
  atomicMax( &graph_d->max_degree, t_max_deg );
}

__global__ void __launch_bounds__(1024,1)
cu_CH_ud_invert_1(cu_graph_CH_UD_t graph)
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const uint32_t ln_msk = wp_sz - 1;
  const int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int wp_per_block = block_dim >> wp_lg;
  const int wp_per_grid = wp_per_block * gridDim.x;

  const uint32_t lane = threadIdx.x & ln_msk;
  const int wp_idx = threadIdx.x >> wp_lg;
  const int g_wp_idx_block = blockIdx.x * wp_per_block;
  const int g_wp_idx = g_wp_idx_block + wp_idx;

  const nodeid_t nnodes_or = graph.num_nodes;
  const int nd_per_wp = rnd_up( div_up( nnodes_or, wp_per_grid ), wp_sz );
  const nodeid_t nd_start = nd_per_wp * g_wp_idx + lane;

  // Use pointer for degree in original node order.
  const nodeid_t* const gub_xdeg = graph.graph_u_b.pointer;
  nodeid_t* const gub_deg = graph.graph_u_b.num_neighbors;
  nodeid_t t_n_edges = 0;

  // Write node degrees into UD-indexed (nid_ud) array.
  // Accumulate total number of edges.
  for ( int i = 0; i < nd_per_wp; i += wp_sz )
    {
      const nodeid_t nid_ud = nd_start + i;
      if ( nid_ud >= nnodes_or ) break;
      const nodeid_t nid_or = graph.node_idx[nid_ud];
      assert( nid_or < nnodes_or );
      const uint deg = gub_xdeg[nid_or];
      gub_deg[nid_ud] = deg;
      t_n_edges += deg;
    }

  // Write prefix of number of edges found by each warp.
  // The last element of the prefix is the total number of edges found
  // by this block.

  __syncwarp();
  const nodeid_t wp_sum = sum_wp(t_n_edges);
  __shared__ nodeid_t b_sum[wp_per_block];
  if ( !lane ) b_sum[wp_idx] = wp_sum;
  __syncthreads();
  if ( wp_idx ) return;
  nodeid_t* const g_deg_prefix = graph.graph_u_b.neighbors;
  g_deg_prefix[ g_wp_idx_block + lane ] = prefix_wp(b_sum[lane]).pfx_incl;
}

__global__ void __launch_bounds__(1024,1)
cu_CH_ud_invert_2(cu_graph_CH_UD_t graph, cu_graph_CH_UD_t* graph_d)
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const uint32_t ln_msk = wp_sz - 1;
  const int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int wp_per_block = block_dim >> wp_lg;
  const int wp_per_grid = wp_per_block * gridDim.x;

  const int wp_idx = threadIdx.x >> wp_lg;
  const int g_wp_idx = blockIdx.x * wp_per_block + wp_idx;

  nodeid_t* const g_deg_prefix = graph.graph_u_b.neighbors;

  __shared__ nodeid_t b_ebs;

  if ( threadIdx.x == block_dim - 1 ) b_ebs = 0;

  const bool do_gsum1 = threadIdx.x < blockIdx.x;
  const bool do_gsum2 = threadIdx.x < rnd_up(blockIdx.x,wp_sz);
  assert( block_dim >= gridDim.x );

  __syncthreads();
  if ( do_gsum2 )
    {
      const nodeid_t t_ebs =
        do_gsum1 ? g_deg_prefix[ ( threadIdx.x + 1 ) * wp_per_block - 1 ] : 0;
      atomicAdd(&b_ebs,t_ebs);
    }
  __syncthreads();

  if ( blockIdx.x == gridDim.x - 1 && !threadIdx.x )
    graph_d->graph_u_b.empty_pointer =
      b_ebs + g_deg_prefix[ gridDim.x * wp_per_block - 1 ];

  const nodeid_t nnodes_or = graph.num_nodes;
  const int nd_per_wp = rnd_up( div_up( nnodes_or, wp_per_grid ), wp_sz );
  const nodeid_t wp_nd_start = nd_per_wp * g_wp_idx;
  const nodeid_t w_ebs = wp_idx ? g_deg_prefix[g_wp_idx-1] : 0;
  nodeid_t i_ebs = w_ebs + b_ebs;

  const nodeid_t* const gub_nnbr = graph.graph_u_b.num_neighbors;
  nodeid_t* const gub_pointer = graph.graph_u_b.pointer;
  nodeid_t* const gub_pointer_cpy = graph.graph_u_b.NNIEG;
  const uint32_t lane = threadIdx.x & ln_msk;

  for ( int i = 0; i < nd_per_wp; i += wp_sz )
    {
      const nodeid_t nid_ud_0 = wp_nd_start + i;
      const nodeid_t nid_ud = nid_ud_0 + lane;
      const bool work = nid_ud < nnodes_or;
      if ( nid_ud_0 >= nnodes_or ) break;
      const uint nnbr = work ? gub_nnbr[nid_ud] : 0;
      __syncwarp();
      Prefix_Elt pfx_fes = prefix_wp(nnbr);
      if ( !work ) break;
      const nodeid_t ef_eidx_0 = i_ebs + pfx_fes.pfx_excl;
      assert( ef_eidx_0 < graph.graph_u_f.max_num_edges );
      gub_pointer[nid_ud] = ef_eidx_0;
      const nodeid_t nid_or = graph.node_idx[nid_ud];
      gub_pointer_cpy[nid_or] = ef_eidx_0;
      i_ebs += pfx_fes.sum;
    }
}

__global__ void __launch_bounds__(1024,1)
cu_CH_ud_invert_3(cu_graph_CH_UD_t graph)
{
  // Create forward edge list using backward edge list.
  constexpr bool debug = false;

  constexpr int block_lg = 10;
  constexpr int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int tid = blockIdx.x * block_dim + threadIdx.x;

  const nodeid_t nnodes_uf = graph.graph_u_f.num_nodes;
  const int thd_per_nd_lg = 5; // Can't be larger than wp_lg.
  const int thd_per_nd = 1 << thd_per_nd_lg;
  const nodeid_t nd_per_grid_iter = gridDim.x << block_lg - thd_per_nd_lg;
  const int nlane = threadIdx.x & ( ( 1 << thd_per_nd_lg ) - 1 );
  const nodeid_t nid_start = tid >> thd_per_nd_lg;
  cu_graph_CH_t& guf = graph.graph_u_f;
  cu_graph_CH_t& gub = graph.graph_u_b;
  nodeid_t* const gub_pointer_cpy = gub.NNIEG;

  __syncthreads();

  for ( nodeid_t nid_ud = nid_start;
        nid_ud < nnodes_uf; nid_ud += nd_per_grid_iter )
    {
      const uint32_t amask = __activemask();
      const int nnbr = guf.num_neighbors[nid_ud];
      const nodeid_t e_gidx0 = guf.pointer[nid_ud];
      for ( int e_eidx = nlane; e_eidx < nnbr; e_eidx += thd_per_nd )
        {
          const nodeid_t ef_gidx = e_gidx0 + e_eidx;
          const nodeid_t fwd_nid_or = guf.neighbors[ef_gidx];
          const nodeid_t eb_gidx = atomicAdd( &gub_pointer_cpy[fwd_nid_or], 1 );
          if ( debug )
            {
              const nodeid_t fwd_nid_ud = graph.node_idx_inv[fwd_nid_or];
              const int nnbr_ub = gub.num_neighbors[fwd_nid_ud];
              const nodeid_t eb_gidx0 = gub.pointer[fwd_nid_ud];
              assert( eb_gidx < eb_gidx0 + nnbr_ub );
            }
          gub.neighbors[eb_gidx] = nid_ud;
          gub.weights[eb_gidx] = guf.weights[ef_gidx];
          gub.midpoint[eb_gidx] = guf.midpoint[ef_gidx];
        }
      __syncwarp(amask);
    }
}
