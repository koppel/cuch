#include "dij.h"
#include "graphs.h"
#include "main.h"

#include <sys/time.h>

//Taken from GNU doc: http://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html#Elapsed-Time
int timeval_subtract (timeval_t *result, timeval_t *x, timeval_t *y)
{
  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_usec < y->tv_usec) {
      int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
      y->tv_usec -= 1000000 * nsec;
      y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
      int nsec = (x->tv_usec - y->tv_usec) / 1000000;
      y->tv_usec += 1000000 * nsec;
      y->tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}

dij_stats_t dij_st_ref_dest(graph_st_t *graph, nodeid_t src, nodeid_t dest, path_t *result)
{
  dij_stats_t stats;
  bin_heap_t *heap;
  size_t *ref;
  size_t i;
  timeval_t begin, end, dif;
  size_t err;

  gettimeofday(&begin, NULL);

  dij_stats_init(&stats);

  bin_heap_ref_init(&heap, &ref, graph->num_nodes);

  result->src = src;
  result->dest = dest;
  result->weight[src] = 0;

  /*
    for ( i = 0; i < graph->num_nodes; i++ )
    {
    bin_heap_ref_add(heap, ref, i, result->weight[i]);
    }
  */
  // don't fill the heap from the beginning! add stuff only when needed.
  bin_heap_ref_add(heap, ref, src, result->weight[src]);


  while ( heap->num_items > 0 ){ //Break condition inside
    nodeid_t u_id;
    weight_t u_dist;
      
    err = bin_heap_ref_get(heap, ref, &u_id, &u_dist);
    if ( err )
      break;

    ASSERTA(u_dist==result->weight[u_id]);
      
    if ( u_id == dest ) 
      break;

    for ( i = 0; i < graph->num_neighbors[u_id]; i++ ){
      weight_t dist_c = result->weight[u_id] + graph->weights_pt[u_id][i];
      if ( dist_c < result->weight[graph->neighbors_pt[u_id][i]] ){
	result->weight[graph->neighbors_pt[u_id][i]] = dist_c;
	result->prev[graph->neighbors_pt[u_id][i]] = u_id;
	if ( ref[graph->neighbors_pt[u_id][i]] == 0 ){
	  bin_heap_ref_add(heap, ref, graph->neighbors_pt[u_id][i], dist_c);
	}else{
	  bin_heap_ref_update(heap, ref, graph->neighbors_pt[u_id][i], dist_c);
	}
	stats.num_updated_edges += 1;
      }
      stats.num_relaxed_edges += 1;
    }
  }

  bin_heap_ref_free(heap, ref);

  gettimeofday(&end, NULL);
  timeval_subtract(&dif, &end, &begin);
  stats.time_wall_us = 1000000*dif.tv_sec + dif.tv_usec;

  return stats;
}

void dij_stats_init(dij_stats_t *ds)
{
  ds->num_updated_edges = 0;
  ds->num_relaxed_edges = 0;
  ds->num_settled_nodes = 0;
  ds->time_wall_us = 0;
}

void dij_stats_dump(FILE *fp, dij_stats_t ds)
{
  fprintf(stderr, "Number of relaxed edges: %zu\n", ds.num_relaxed_edges);
  fprintf(stderr, "Number of result path updates: %zu\n", ds.num_updated_edges);
  fprintf(stderr, "Wall time for completion: %ldus\n", ds.time_wall_us);
}


dij_stats_t dij_bi_st_ref_dest(graph_st_t *graph_f, graph_st_t *graph_b, nodeid_t src, nodeid_t dest, path_t *result)
{
  dij_stats_t stats;
  bin_heap_t *heap_f, *heap_b;
  size_t *ref_f, *ref_b;
  uint8_t *settled;
  size_t i;
  timeval_t begin, end, dif;
  size_t err;
  path_t *result_b;
  size_t path_exists = 0;
  nodeid_t idx = NODEID_NULL;

  gettimeofday(&begin, NULL);

  dij_stats_init(&stats);

  bin_heap_ref_init(&heap_f, &ref_f, graph_f->num_nodes);
  bin_heap_ref_init(&heap_b, &ref_b, graph_f->num_nodes);

  result->src = src;
  result->dest = dest;

  settled = (uint8_t *) calloc(graph_f->num_nodes, sizeof(uint8_t));

  path_init(&result_b, graph_f->num_nodes);
  result->weight[src] = 0;
  result_b->weight[dest] = 0;

  /*
    for ( i = 0; i < graph->num_nodes; i++ )
    {
    bin_heap_ref_add(heap, ref, i, result->weight[i]);
    }
  */
  // don't fill the heap from the beginning! add stuff only when needed.
  bin_heap_ref_add(heap_f, ref_f, src, result->weight[src]);
  bin_heap_ref_add(heap_b, ref_b, dest, result_b->weight[dest]);


  while ( heap_f->num_items > 0 && heap_b->num_items > 0 ){ //Break condition inside
    if ( heap_f->store[1].distance <= heap_b->store[1].distance && heap_f->num_items > 0){ // forward search
      nodeid_t u_id;
      weight_t u_dist;
      
      err = bin_heap_ref_get(heap_f, ref_f, &u_id, &u_dist);
      if ( err )
	break;

      ASSERTA(u_dist==result->weight[u_id]);

      settled[u_id]++;

      if ( u_id == dest || settled[u_id] == 2 ){
	path_exists = 1;
	idx = u_id;
	break;
      }

      for ( i = 0; i < graph_f->num_neighbors[u_id]; i++ ){
	weight_t dist_c = result->weight[u_id] + graph_f->weights_pt[u_id][i];
	if ( dist_c < result->weight[graph_f->neighbors_pt[u_id][i]] ){
	  result->weight[graph_f->neighbors_pt[u_id][i]] = dist_c;
	  result->prev[graph_f->neighbors_pt[u_id][i]] = u_id;
	  if ( ref_f[graph_f->neighbors_pt[u_id][i]] == 0 ){
	    bin_heap_ref_add(heap_f, ref_f, graph_f->neighbors_pt[u_id][i], dist_c);
	  }else{
	    bin_heap_ref_update(heap_f, ref_f, graph_f->neighbors_pt[u_id][i], dist_c);
	  }
	  stats.num_updated_edges += 1;
	}
	stats.num_relaxed_edges += 1;
      }
    }else{ // backwards search
      nodeid_t u_id;
      weight_t u_dist;
      
      err = bin_heap_ref_get(heap_b, ref_b, &u_id, &u_dist);
      if ( err )
	break;

      ASSERTA(u_dist==result_b->weight[u_id]);

      settled[u_id]++;
      
      if ( u_id == src || settled[u_id] == 2 ){
	path_exists = 1;
	idx = u_id;
	break;
      }

      for ( i = 0; i < graph_b->num_neighbors[u_id]; i++ ){
	weight_t dist_c = result_b->weight[u_id] + graph_b->weights_pt[u_id][i];
	if ( dist_c < result_b->weight[graph_b->neighbors_pt[u_id][i]] ){
	  result_b->weight[graph_b->neighbors_pt[u_id][i]] = dist_c;
	  result_b->prev[graph_b->neighbors_pt[u_id][i]] = u_id;
	  if ( ref_b[graph_b->neighbors_pt[u_id][i]] == 0 ){
	    bin_heap_ref_add(heap_b, ref_b, graph_b->neighbors_pt[u_id][i], dist_c);
	  }else{
	    bin_heap_ref_update(heap_b, ref_b, graph_b->neighbors_pt[u_id][i], dist_c);
	  }
	  stats.num_updated_edges += 1;
	}
	stats.num_relaxed_edges += 1;
      }
    }
  }

  if ( path_exists != 0 ){
    while ( result_b->prev[idx] != WEIGHT_INF ){
      result->weight[result_b->prev[idx]] = (result_b->weight[idx] - result_b->weight[result_b->prev[idx]]) + result->weight[idx];
      result->prev[result_b->prev[idx]] = idx;
      idx = result_b->prev[idx];
    }
  }else{
    result->weight[dest] = WEIGHT_INF;
  }

  bin_heap_ref_free(heap_f, ref_f);
  bin_heap_ref_free(heap_b, ref_b);
  path_free(result_b);
  free(settled);

  gettimeofday(&end, NULL);
  timeval_subtract(&dif, &end, &begin);
  stats.time_wall_us = 1000000*dif.tv_sec + dif.tv_usec;

  return stats;
}
