/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>
#include <initializer_list>

#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"

const bool __attribute__((unused)) debug_sm_idx = false;

__constant__ CH_Options chopts_c;

__host__ void
cu_CH_extract_pre_launch(cu_graph_CH_bi_t *graph_bi_d)
{
  cuda_sym_host_to_dev(chopts_c,&chopts);
}

template<int tpn> __global__ void
cu_CH_invert_0(cu_graph_CH_bi_t graph)
{
  // Create forward edge list using backward edge list.
  constexpr bool debug = false;

  constexpr int wp_lg = 5;
  constexpr int block_lg = 10;
  constexpr int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int tid = blockIdx.x * block_dim + threadIdx.x;

  const nodeid_t n_nodes = graph.num_nodes;
  const int thd_per_nd_lg = wp_lg;
  const int wp_sz = 1 << wp_lg;
  const int thd_per_nd = 1 << thd_per_nd_lg;
  const nodeid_t nd_per_grid_iter = gridDim.x << block_lg - thd_per_nd_lg;
  const int nlane = threadIdx.x & thd_per_nd - 1;
  const nodeid_t nid_start = tid >> thd_per_nd_lg;

  __syncthreads();

  for ( nodeid_t nid = nid_start; nid < n_nodes; nid += nd_per_grid_iter )
    {
      const int nnbr = graph.graph_b.num_neighbors[nid];
      const nodeid_t e_gidx0 = graph.graph_b.pointer[nid];
      auto body = [&](int e_eidx)
      {
          const nodeid_t eb_gidx = e_gidx0 + e_eidx;
          const nodeid_t dst = graph.graph_b.neighbors[eb_gidx];
          const nodeid_t ef_gidx =
            atomicAdd( &graph.graph_f.num_neighbors[dst], 1 );
          if ( debug && dst < n_nodes - 1 )
            {
              const nodeid_t e_gidx0_nfwd = graph.graph_f.pointer[dst+1];
              assert( ef_gidx < e_gidx0_nfwd );
            }
          graph.graph_f.neighbors[ef_gidx] = nid;
          graph.graph_f.weights[ef_gidx] = graph.graph_b.weights[eb_gidx];
          graph.graph_f.midpoint[ef_gidx] = eb_gidx;
        };

      // Notes: Performance is much better with the first loop on CUDA
      //  10.1, perhaps because it loads graph_b.pointer at the same
      //  time as nnbr rather than after nnbr is tested.
      for ( int e_eidx = nlane;  e_eidx < wp_sz;  e_eidx += thd_per_nd )
        if ( e_eidx < nnbr ) body(e_eidx);
      for ( int e_eidx = wp_sz + nlane;  e_eidx < nnbr;  e_eidx += thd_per_nd )
        body(e_eidx);
    }
}

__global__ void
cu_CH_invert_1(cu_graph_CH_bi_t graph)
{
  // Update the forward node degrees.
  constexpr int block_lg = 10;
  constexpr int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int tid = blockIdx.x * block_dim + threadIdx.x;
  const nodeid_t n_nodes = graph.num_nodes;
  const nodeid_t n_thds = gridDim.x * block_dim;
  int t_max_deg = 0;
  for ( nodeid_t nid = tid;  nid < n_nodes;  nid += n_thds )
    {
      const int deg =
        graph.graph_f.num_neighbors[nid] -= graph.graph_f.pointer[nid];
      set_max( t_max_deg, deg );
    }
  nodeid_t prev_md = atomicMax( &graph.self_d->max_degree, t_max_deg );
  if ( prev_md >= t_max_deg ) return;
  const uint32_t tpn_new = max(3,log2p1(t_max_deg));
  atomicMax( &graph.self_d->thread_per_node, tpn_new );
}

__global__ void __launch_bounds__(1024,1)
cu_CH_extract_overlay_00
(cu_graph_CH_bi_t *graph_in, Extract_Block_Storage *g_ebs)
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const uint32_t ln_msk = wp_sz - 1;
  const int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int wp_per_block = block_dim >> wp_lg;
  const int wp_per_grid = wp_per_block * gridDim.x;

  const uint32_t lane = threadIdx.x & ln_msk;
  const int wp_idx = threadIdx.x >> wp_lg;
  const int g_wp_idx_block = blockIdx.x * wp_per_block;
  const int g_wp_idx = g_wp_idx_block + wp_idx;

  const nodeid_t num_nodes_in = graph_in->num_nodes;
  const int nd_per_wp = rnd_up( div_up( num_nodes_in, wp_per_grid ), wp_sz );
  const nodeid_t nd_start = nd_per_wp * g_wp_idx + lane;

  const nodeid_t* const gi_node_ranks = graph_in->node_ranks;
  const nodeid_t* const gif_nnbr = graph_in->graph_f.num_neighbors;
  const nodeid_t* const gib_mnnbr = graph_in->graph_b.max_num_neighbors_allowed;
  const nodeid_t* const gif_deg = gif_nnbr;

  const Extract_Block_Storage z_ebs = {0,0,0,0};
  Extract_Block_Storage t_ebs = z_ebs;

  for ( int i = 0; i < nd_per_wp; i += wp_sz )
    {
      const nodeid_t onid = nd_start + i;
      if ( onid >= num_nodes_in || gi_node_ranks[onid] != 0 ) continue;
      t_ebs.n_nds_keep++;
      t_ebs.n_fes_keep += rnd_up(gif_deg[onid],4);
      t_ebs.n_bes_keep += rnd_up(gib_mnnbr[onid],4);
    }
  const nodeid_t wp_sums[] =
    { sum_wp(t_ebs.n_nds_keep), sum_wp(t_ebs.n_fes_keep),
      sum_wp(t_ebs.n_bes_keep) };
  __shared__ nodeid_t b_ebs[3][wp_per_block];
  if ( !lane ) for ( int i: {0,1,2} ) b_ebs[i][wp_idx] = wp_sums[i];
  __syncthreads();
  if ( wp_idx ) return;
  auto get_pfx = [&](int m) { return prefix_wp(b_ebs[m][lane]).pfx_incl; };
  Extract_Block_Storage bp_ebs = { get_pfx(0), get_pfx(1), get_pfx(2), 0 };
  g_ebs[ g_wp_idx_block + lane ] = bp_ebs;
}


__global__ void __launch_bounds__(1024,1)
cu_CH_extract_overlay_01
(cu_graph_CH_bi_t *graph_out, cu_graph_CH_bi_t *graph_in,
 nodeid_t *node_id, nodeid_t *node_id_inv, Extract_Block_Storage *g_ebs)
{
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const uint32_t ln_msk = wp_sz - 1;
  const int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int wp_per_block = block_dim >> wp_lg;
  const int wp_per_grid = wp_per_block * gridDim.x;

  const int wp_idx = threadIdx.x >> wp_lg;
  const int g_wp_idx = blockIdx.x * wp_per_block + wp_idx;

  __shared__ Extract_Block_Storage b_ebs;
  const Extract_Block_Storage z_ebs = {0,0,0,0};

  if ( threadIdx.x == block_dim - 1 ) b_ebs = z_ebs;

  const bool do_gsum1 = threadIdx.x < blockIdx.x;
  const bool do_gsum2 = threadIdx.x < rnd_up(blockIdx.x,wp_sz);
  assert( block_dim >= gridDim.x );

  __syncthreads();
  if ( do_gsum2 )
    {
      const Extract_Block_Storage t_ebs =
        do_gsum1 ? g_ebs[ ( threadIdx.x + 1 ) * wp_per_block - 1 ] : z_ebs;

      // Optimization possibility: avoid three lock acq/rel by using own code.
      atomicAdd(&b_ebs.n_nds_keep,t_ebs.n_nds_keep);
      atomicAdd(&b_ebs.n_fes_keep,t_ebs.n_fes_keep);
      atomicAdd(&b_ebs.n_bes_keep,t_ebs.n_bes_keep);
    }
  __syncthreads();

  const nodeid_t num_nodes_in = graph_in->num_nodes;
  const int nd_per_wp = rnd_up( div_up( num_nodes_in, wp_per_grid ), wp_sz );
  const nodeid_t wp_nd_start = nd_per_wp * g_wp_idx;
  const nodeid_t wp_nd_stop = wp_nd_start + nd_per_wp;

  const Extract_Block_Storage w_ebs = wp_idx ? g_ebs[g_wp_idx-1] : z_ebs;

  Extract_Block_Storage i_ebs = w_ebs;
  i_ebs.n_nds_keep += b_ebs.n_nds_keep;
  i_ebs.n_fes_keep += b_ebs.n_fes_keep;
  i_ebs.n_bes_keep += b_ebs.n_bes_keep;

  nodeid_t* const go_node_ids = graph_out->node_ids;
  const nodeid_t* const gi_node_ids = graph_in->node_ids;
  nodeid_t* const gof_mnnbr
    = graph_out->graph_f.max_num_neighbors_allowed;
  nodeid_t* const gob_mnnbr
    = graph_out->graph_b.max_num_neighbors_allowed;
  nodeid_t* const gof_pointer = graph_out->graph_f.pointer;
  nodeid_t* const gof_e0 = graph_out->graph_f.num_neighbors;
  nodeid_t* const gob_pointer = graph_out->graph_b.pointer;
  const nodeid_t* const gi_node_ranks = graph_in->node_ranks;
  const nodeid_t* const gif_nnbr = graph_in->graph_f.num_neighbors;
  const nodeid_t* const gib_mnnbr = graph_in->graph_b.max_num_neighbors_allowed;
  const nodeid_t* const gif_deg = gif_nnbr;
  const uint32_t lane = threadIdx.x & ln_msk;
  const uint32_t pfx_mask = ( uint32_t(1) << lane ) - 1;

  for ( int i = 0; i < nd_per_wp; i += wp_sz )
    {
      const nodeid_t gidx_old = wp_nd_start + i + lane;
      const bool is_keep =
        gidx_old < num_nodes_in && gi_node_ranks[gidx_old] == 0;
      const nodeid_t t_fes_keep = is_keep ? gif_deg[gidx_old] : 0;
      const nodeid_t t_bes_keep = is_keep ? gib_mnnbr[gidx_old] : 0;
      const uint32_t msk = 0xffffffff;
      const uint32_t keep_vec =__ballot_sync(msk,is_keep);
      const int pfx_n_nds = __popc( keep_vec & pfx_mask );
      Prefix_Elt pfx_fes = prefix_wp(t_fes_keep);
      Prefix_Elt pfx_bes = prefix_wp(t_bes_keep);
      if ( is_keep )
        {
          const nodeid_t gidx_new = pfx_n_nds + i_ebs.n_nds_keep;
          node_id_inv[gidx_old] = gidx_new;
          node_id[gidx_new] = gidx_old;
          go_node_ids[gidx_new] = gi_node_ids[gidx_old];
          gof_mnnbr[gidx_new] = t_fes_keep;
          gob_mnnbr[gidx_new] = t_bes_keep;
          const nodeid_t ef_eidx_0 = i_ebs.n_fes_keep + pfx_fes.pfx_excl;
          gof_e0[gidx_new] = ef_eidx_0;
          gof_pointer[gidx_new] = ef_eidx_0;
          gob_pointer[gidx_new] = i_ebs.n_bes_keep + pfx_bes.pfx_excl;
        }
      i_ebs.n_fes_keep += pfx_fes.sum;
      i_ebs.n_bes_keep += pfx_bes.sum;
      i_ebs.n_nds_keep += __popc(keep_vec);
    }

  if ( wp_nd_stop < num_nodes_in || lane != 31 ) return;

  // copy any remaining stats/... data that needs to be set in the
  // output graph
  graph_out->work_item_next = 0;
  graph_out->num_nodes = i_ebs.n_nds_keep;
  graph_out->max_degree = 0;
  graph_out->thread_per_node = 0;
  graph_out->max_degree_overlay = graph_in->max_degree_overlay;
  graph_out->mean_degree = graph_in->mean_degree_overlay;
  graph_out->mean_degree_overlay = graph_in->mean_degree_overlay;
  graph_out->max_weight = graph_in->max_weight; /// incorrect value here
  graph_out->mean_weight_0_inv = graph_in->mean_weight_0_inv;
  graph_out->overlay_size = graph_in->overlay_size;
  assert(graph_in->overlay_size == graph_out->num_nodes);
  graph_out->max_rank = graph_in->max_rank;
  graph_out->graph_f.num_nodes = graph_out->graph_b.num_nodes
    = i_ebs.n_nds_keep;
  graph_out->graph_f.num_edges_exact = graph_out->graph_b.num_edges_exact = 0;
  graph_out->graph_f.max_weight = graph_in->graph_f.max_weight; /// incorrect!
  graph_out->graph_b.max_weight = graph_in->graph_b.max_weight; /// incorrect!
  graph_out->graph_b.weight_sum = 0;
  graph_out->graph_f.empty_pointer = i_ebs.n_fes_keep;
  graph_out->graph_b.empty_pointer = i_ebs.n_bes_keep;
}


// multi-block, go over each overlay node renaming nbr_ids for overlay edges to new domain, copying
// the results into the new graph.
template <uint32_t tpn> __global__ void __launch_bounds__(block_dim_overlay_1,1)
cu_CH_extract_overlay_1
(cu_graph_CH_bi_t graph_out, cu_graph_CH_bi_t graph_in,
 const nodeid_t* __restrict__ node_id, const nodeid_t* __restrict__ node_id_inv)
{
  const int block_lg = block_lg_overlay_1;
  const int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );

  const int e_tpn = tpn > block_lg_overlay_1 ? block_lg_overlay_1 : tpn; // tpn capped at 8
  const int E_TPN = 1 << e_tpn;

  const uint32_t num_nodes_out = graph_out.graph_f.num_nodes;

  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * block_dim;
  // compute num_threads
  const int num_thd = block_dim * gridDim.x;

  nodeid_t current_pos = tid>>e_tpn; // current thread group's position within node_id
  nodeid_t current_pos_0 = (blockIdx.x * block_dim) >> e_tpn;
  // local_ID, thread group's working set id within current block (needed for shared mem accesses)
  const nodeid_t lid = threadIdx.x >> e_tpn;

  const uint32_t tnid = threadIdx.x & ( E_TPN - 1 ); // thread's neighbor id, tid%(1<<tpn)
  
  // increment value in each iteration
  const int vertex_inc = num_thd >> e_tpn;

  // number of vertices handled at each iteration by each block
  const int vertex_per_iter = block_dim >> e_tpn;

  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)

  auto make_f = [&](nodeid_t* ptr) {
      return [=](uint idx) -> uint32_t&
      { assert( !debug_sm_idx || idx < 2048 ); return ptr[idx]; };  };

  __shared__ nodeid_t sh_current_pointer_b[vertex_per_iter];
  __shared__ nodeid_t sh_current_nnbr_b[vertex_per_iter];

  auto SH_CURRENT_POINTER_B = make_f(sh_current_pointer_b);
  auto SH_CURRENT_NNBR_B = make_f(sh_current_nnbr_b);

# define gkb graph_out.graph_b
# define gib graph_in.graph_b

  uint32_t iter = 0;
  int max_degree = 0;
  nodeid_t deg_b_sum = 0;
  Wht_Sum wht_sum = 0;

  while ( current_pos_0 < num_nodes_out ){

    // gk: graph keep.
    __shared__ int gk_pointer_b[vertex_per_iter];

    // load uncontracted nodes (by tpn)
    if ( threadIdx.x < vertex_per_iter )
      {
        if ( current_pos_0+threadIdx.x < num_nodes_out )
          {
            const nodeid_t current = node_id[current_pos_0 + threadIdx.x];
            SH_CURRENT_POINTER_B( threadIdx.x ) = gib.pointer[current];
            SH_CURRENT_NNBR_B( threadIdx.x ) = gib.num_neighbors[current];
            gk_pointer_b[threadIdx.x] = gkb.pointer[current_pos_0+threadIdx.x];
          }
        else
          {
            SH_CURRENT_NNBR_B( threadIdx.x ) = 0;
          }
      }

    // sync needed here because the first warps are cooperating in loading the entire block's data
    __syncthreads();

    int kb_lpt_next = 0;

    const int n_iters_lid = div_up( SH_CURRENT_NNBR_B(lid), E_TPN );

    __shared__ int n_iters;
    if ( vertex_per_iter > 1 )
      {
        if ( !threadIdx.x ) n_iters = 0;
        __syncthreads();
        if ( !tnid ) atomicMax( &n_iters, n_iters_lid );
        __syncthreads();
      }

    const int iter_per_node = vertex_per_iter > 1 ? n_iters : n_iters_lid;
    const int nnbr_bwd = SH_CURRENT_NNBR_B( lid );

    for ( int i=0; i<iter_per_node; i++ ){

      const int e_tnid = i*E_TPN + tnid;
      const nodeid_t inpt_bwd_gidx = SH_CURRENT_POINTER_B(lid) + e_tnid;
      const int kb_lpt = kb_lpt_next;
      const bool bwd_here_maybe = e_tnid < nnbr_bwd;

      const nodeid_t bwd_nbr =
        bwd_here_maybe
        ? graph_in.graph_b.neighbors[inpt_bwd_gidx] : nodeid_invalid;
      const bool bwd_here = !nodeid_is_special(bwd_nbr);
      const bool keep_bwd = bwd_here && !graph_in.node_ranks[bwd_nbr];

      Prefix_Elt pfx_bwd = prefix_bit(block_lg, e_tpn, keep_bwd);
      const int n_keep_bwd = pfx_bwd.sum;
      kb_lpt_next += n_keep_bwd;
      deg_b_sum += keep_bwd;

      if ( keep_bwd ){
        const nodeid_t keep_bwd_gidx = gk_pointer_b[lid] + kb_lpt + pfx_bwd.pfx_excl;
        const nodeid_t mapped_bwd_nbr = node_id_inv[ bwd_nbr ];
        const nodeid_t wht = gib.weights[inpt_bwd_gidx];
        const nodeid_t mid = gib.midpoint[inpt_bwd_gidx];

        gkb.neighbors[keep_bwd_gidx] = mapped_bwd_nbr;
        gkb.weights[keep_bwd_gidx] = wht;
        gkb.midpoint[keep_bwd_gidx] = mid;
        wht_sum += wht;
      }

    }
      
    if(tnid==0){///RRRRRRRRRRR
      SH_CURRENT_NNBR_B( lid ) = kb_lpt_next;
      set_max( max_degree, kb_lpt_next );
    }
    __syncthreads();
    
    // write the NNBR of the nodes
    if(threadIdx.x < vertex_per_iter && (current_pos_0+threadIdx.x < num_nodes_out)){
      gkb.num_neighbors[ current_pos_0+threadIdx.x ] = SH_CURRENT_NNBR_B( threadIdx.x );
    }

    // setup next iteration
    iter++;
    current_pos += vertex_inc;
    current_pos_0 += vertex_inc;
    __syncthreads();
  }

  atomicAdd( &graph_out.self_d->graph_b.num_edges_exact, deg_b_sum );
  atomicAdd( &graph_out.self_d->graph_b.weight_sum, wht_sum );

  if ( tnid ) return;
  nodeid_t prev_md = atomicMax( &graph_out.self_d->max_degree, max_degree );
  if ( prev_md >= max_degree ) return;
  const uint32_t tpn_new = max(3,log2p1(max_degree));
  atomicMax( &graph_out.self_d->thread_per_node, tpn_new );
}



// instantiate the templates

#define INST(tpn)                                                             \
template __global__ void cu_CH_extract_overlay_1<tpn>                         \
(cu_graph_CH_bi_t graph_out,                                                  \
 cu_graph_CH_bi_t graph_in,                                                   \
 const nodeid_t* __restrict__ node_id,                                        \
 const nodeid_t* __restrict__ node_id_inv);                                   \
template __global__ void cu_CH_invert_0<tpn>(cu_graph_CH_bi_t graph);


INST(3); INST(4); INST(5); INST(6); INST(7); INST(8); INST(9); INST(10); INST(11); INST(12);
#undef INST
