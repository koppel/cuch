#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "heap.h"
 
/*
 * Binary Heap
 * a reference table added to help with search and decrese key
 */

void bin_heap_ref_init(bin_heap_t **heap, size_t **ref, size_t size)
{
  *heap = (bin_heap_t *) malloc(sizeof(bin_heap_t));

  (*heap)->store = (heap_item_t *) malloc((size+1)*sizeof(heap_item_t));
  *ref = (size_t *) calloc(size, sizeof(size_t));

  (*heap)->store[0].node_id = size;
  (*heap)->store[0].distance = size;

  (*heap)->num_items = 0;
}

void bin_heap_ref_free(bin_heap_t *heap, size_t *ref)
{
  free(heap->store);
  free(heap);
  free(ref);
}

static void downheap_ref(bin_heap_t *heap, size_t *ref, size_t i)
{
  while ( 1 ){ 
    size_t lowest = i;
    heap_item_t old_child;
    
    if ( LCHILD(i) < heap->num_items && heap->store[LCHILD(i)].distance < heap->store[i].distance )
      lowest = LCHILD(i);
    
    if ( RCHILD(i) < heap->num_items && heap->store[RCHILD(i)].distance < heap->store[lowest].distance )
      lowest = RCHILD(i);
    
    if ( lowest == i )
      break;
    
    old_child = heap->store[lowest];
    heap->store[lowest] = heap->store[i];
    ref[heap->store[i].node_id] = lowest;
    heap->store[i] = old_child;
    ref[old_child.node_id] = i;
    i = lowest;
  }
}

static void upheap_ref(bin_heap_t *heap, size_t *ref, size_t i)
{
  while ( i > 1 && heap->store[i].distance < heap->store[PARENT(i)].distance ){ 
    heap_item_t old_par = heap->store[PARENT(i)];
    heap->store[PARENT(i)] = heap->store[i];
    ref[heap->store[i].node_id] = PARENT(i);
    heap->store[i] = old_par;
    ref[old_par.node_id] = i;
    i = PARENT(i);
  }
}

int bin_heap_ref_add(bin_heap_t *heap, size_t *ref, nodeid_t node_id, weight_t distance)
{
  size_t i;

  if ( heap->num_items == heap->store[0].node_id ) return 1;

  i = heap->num_items + 1;
  heap->num_items = i;
  heap->store[i].node_id = node_id;
  heap->store[i].distance = distance;
  ref[node_id] = i;

  upheap_ref(heap, ref, i);

  return 0;
}

int bin_heap_ref_get(bin_heap_t *heap, size_t *ref, nodeid_t *node_id, weight_t *distance)
{
  size_t i = 1;

  if ( heap->num_items == 0 ) return 1;

  *node_id = heap->store[1].node_id;
  *distance = heap->store[1].distance;
  heap->store[1] = heap->store[heap->num_items];
  ref[heap->store[heap->num_items].node_id] = 1;

  downheap_ref(heap, ref, i);

  heap->num_items -= 1;

  return 0;
}

void bin_heap_ref_update(bin_heap_t *heap, size_t *ref, nodeid_t node_id, weight_t new_distance)
{
  heap->store[ref[node_id]].distance = new_distance;

  // with dijkstra, weights only decrease so downheap not necessary
  // downheap_ref(heap, ref, ref[node_id]);
  upheap_ref(heap, ref, ref[node_id]);
}

