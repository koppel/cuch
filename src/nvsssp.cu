#include <assert.h>
#include <nppdefs.h>

#include "main.h"
#include "nvsssp.h"

#ifdef HAVE_NVGRAPH
#include "nvgraph.h"
#endif

inline double
time_wall_fp()
{
  struct timespec now;
  clock_gettime(CLOCK_REALTIME,&now);
  return now.tv_sec + ((double)now.tv_nsec) * 1e-9;
}

#ifdef HAVE_NVGRAPH
void check(nvgraphStatus_t status) {
    if (status != NVGRAPH_STATUS_SUCCESS) {
        printf("ERROR : %d\n",status);
        exit(0);
    }
}
#endif

void nv_sssp_run(path_t *path, graph_st *graph_in, uint32_t src)
{
#ifdef HAVE_NVGRAPH
  double t_start_s, t_end_s;
  
  const uint32_t num_nodes = graph_in->num_nodes;
  const uint32_t num_edges = graph_in->num_edges;

  const size_t  vertex_numsets = 1, edge_numsets = 1;
  float *sssp_1_h = (float *)malloc(num_nodes*sizeof(float));
  void** vertex_dim  = (void**)malloc(vertex_numsets*sizeof(void*));
  nvgraphHandle_t handle;
  nvgraphGraphDescr_t graph;
  cudaDataType_t edge_dimT = CUDA_R_32F;
  cudaDataType_t* vertex_dimT = (cudaDataType_t*)malloc(vertex_numsets*sizeof(cudaDataType_t));
  nvgraphCSCTopology32I_t CSC_input = (nvgraphCSCTopology32I_t) malloc(sizeof(struct nvgraphCSCTopology32I_st));
  vertex_dim[0]= (void*)sssp_1_h; vertex_dimT[0] = CUDA_R_32F;
  
  float *weights_h = (float *)malloc(num_edges*sizeof(float));
  int *destination_offsets_h = (int *)malloc((num_nodes+1)*sizeof(int));
  int *source_indices_h = (int *)malloc(num_edges*sizeof(int));

  uint32_t *nnbr_b= (uint32_t *) calloc(num_nodes, sizeof(uint32_t));
  uint32_t *nnbr_b_2= (uint32_t *) calloc(num_nodes, sizeof(uint32_t));
  // determine nnbr_b
  for(uint32_t i=0; i<num_nodes; i++){
    for(uint32_t j=0; j<graph_in->num_neighbors[i]; j++){
      nnbr_b[graph_in->neighbors_pt[i][j]]++;
    }
  }
  printf("nnbr_b set.\n");
  // set destination offsets using nnbr_b
  destination_offsets_h[0] = 0;
  for(uint32_t i=0; i<num_nodes; i++){
    destination_offsets_h[i+1] = destination_offsets_h[i] + nnbr_b[i];
  }
  printf("destination offset set.\n");
  // copy edge data
  assert(((uint32_t)destination_offsets_h[num_nodes] == num_edges)
	 && (destination_offsets_h[num_nodes] >= 0));
  for(uint32_t i=0; i<num_nodes; i++){
    for(uint32_t j=0; j<graph_in->num_neighbors[i]; j++){
      uint32_t dest = graph_in->neighbors_pt[i][j];
      source_indices_h[destination_offsets_h[dest] + nnbr_b_2[dest]] = i;
      weights_h[destination_offsets_h[dest] + nnbr_b_2[dest]] = (float) graph_in->weights_pt[i][j];
      nnbr_b_2[dest]++;
    }
  }
  free(nnbr_b_2);
  printf("edges copied.\n");
  // make sure edge data is in order (nvgraph uses CSC, but it might not be necessary actually)
  for(uint32_t i=0; i<num_nodes; i++){
    for(uint32_t j=0; j<nnbr_b[i]; j++){
      for(uint32_t k=j+1; k<nnbr_b[i]; k++){
	if(source_indices_h[destination_offsets_h[i]+j] > source_indices_h[destination_offsets_h[i]+k]){
	  uint32_t tnd = source_indices_h[destination_offsets_h[i]+j];
	  float tw = weights_h[destination_offsets_h[i]+j];
	  source_indices_h[destination_offsets_h[i]+j] = source_indices_h[destination_offsets_h[i]+k];
	  weights_h[destination_offsets_h[i]+j] = weights_h[destination_offsets_h[i]+k];
	  source_indices_h[destination_offsets_h[i]+k] = tnd;
	  weights_h[destination_offsets_h[i]+k] = tw;
	}
      }
    }
  }
  printf("edge order established.\n");
    
  check( nvgraphCreate(&handle) );
  check( nvgraphCreateGraphDescr (handle, &graph) );
  CSC_input->nvertices = num_nodes; CSC_input->nedges = num_edges;
  CSC_input->destination_offsets = destination_offsets_h;
  CSC_input->source_indices = source_indices_h;

  // allocate and move data
  check( nvgraphSetGraphStructure(handle, graph, (void*)CSC_input, NVGRAPH_CSC_32) );
  check( nvgraphAllocateVertexData(handle, graph, vertex_numsets, vertex_dimT) );
  check( nvgraphAllocateEdgeData  (handle, graph, edge_numsets, &edge_dimT) );
  check( nvgraphSetEdgeData(handle, graph, (void*)weights_h, 0) );

  // solve SSSP
  int source_vert = (int)src;
  
  t_start_s = time_wall_fp();
  
  check( nvgraphSssp(handle, graph, 0,  &source_vert, 0) );

  t_end_s = time_wall_fp();

  printf(" Time %7.3f ms\n", (t_end_s - t_start_s) * 1e+3);

  // Get and print result
  check(nvgraphGetVertexData(handle, graph, (void*)sssp_1_h, 0));

  // copy and translate result to path
  for(uint32_t i=0; i<num_nodes; i++){
    if(sssp_1_h[i] == NPP_MAXABS_32F){
      path->weight[i] = NPP_MAX_32U;
    } else {
      path->weight[i] = (uint32_t) sssp_1_h[i];
    }
  }
  
  //Clean 
  free(sssp_1_h); free(vertex_dim);
  free(vertex_dimT); free(CSC_input);
  check(nvgraphDestroyGraphDescr(handle, graph));
  check(nvgraphDestroy(handle));

#else

  printf("NVSSSP not available, perhaps due to CUDA toolkit version.\n");

#endif
}
