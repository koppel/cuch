/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <unistd.h>
#include "cu_main.h"
#include "cu_CH.h"
#include "cu_CH.cuh"
#include "util/cuda-gpuinfo.h"
#include "util/pstring.h"
#include "util/ptable.h"
#include "cu_util_timing.cuh"
#include "cu_verify.h"

// CUPTI performance counters library wrappers developed for LSU EE 7722.
#include <nperf.h>

#include <nppdefs.h>
#include <vector>
#include <map>
#include <functional>
using namespace std;

#include "sort-kernels.cu"

enum Stats_Policy { Stats_None, Stats_Later, Stats_Immediate };

cu_Contract_Query_State::~cu_Contract_Query_State()
{
  cuda_storage_free_at_end.push_back(node_tags);
  for ( auto p: cuda_storage_free_at_end ) if ( p ) CE( cudaFree( p ) );
}

class APSP_Check {
public:
  APSP_Check():dim(0){}
  APSP_Check(weight_t *distp, nodeid_t *prev, uint32_t num_nodes)
  { copy(distp,prev,num_nodes); }
  APSP_Check(const APSP_path_t& kgr){ copy(kgr); }
  void copy(const APSP_path_t& kgr){ copy(kgr.dist,kgr.prev,kgr.num_nodes); }
  void copy(weight_t *distp, nodeid_t *prev, uint32_t num_nodes)
  {
    dim = num_nodes;
    n_elts = dim * dim;
    dist.resize(n_elts);
    memcpy(dist.data(),distp,n_elts*sizeof(distp[0]));
  }
  bool check(const APSP_path_t& kgr)
  { return check(kgr.dist,kgr.prev,kgr.num_nodes); }
  bool check(weight_t *distp, nodeid_t *prev, uint32_t num_nodes)
  {
    auto cd = [&](uint s, uint d) -> weight_t& { return dist[s*dim+d]; };
    assert( num_nodes == dim );

    for ( uint m=0; m<dim; m++ )
      for ( uint s=0; s<dim; s++ )
        {
          const weight_t sm = cd(s,m);
          if ( sm == WEIGHT_INF || s == m ) continue;
          for ( uint d=0; d<dim; d++ )
            if ( cd(m,d) != WEIGHT_INF ) set_min( cd(s,d), sm + cd(m,d) );
        }

    n_err = 0;
    for ( uint i=0; i<n_elts; i++ )
      if ( distp[i] != dist[i] ) { n_err++;  assert( false ); }
    return n_err == 0;
  }

  uint32_t dim, n_elts;
  vector<weight_t> dist;
  int n_err;
};

class APSP_Run {
public:
  APSP_Run():
    inited(false),dist_w_d(nullptr),prev_w_d(nullptr), alloc_sz_chars(0) {};

  ~APSP_Run()
  {
    if ( dist_w_d ) CE( cudaFree( dist_w_d ) );
    if ( prev_w_d ) CE( cudaFree( prev_w_d ) );
    if ( inited )
      {
        CE( cudaEventDestroy(start_0) );
        CE( cudaEventDestroy(stop_0) );
        CE( cudaEventDestroy(start_1) );
        CE( cudaEventDestroy(stop_1) );
        CE( cudaEventDestroy(start_2) );
        CE( cudaEventDestroy(stop_2) );
      }
  }

  void init()
  {
    if ( inited ) return;
    assert(WEIGHT_INF == NPP_MAX_32U);
    CE( cudaEventCreate(&start_0) );
    CE( cudaEventCreate(&stop_0) );
    CE( cudaEventCreate(&start_1) );
    CE( cudaEventCreate(&stop_1) );
    CE( cudaEventCreate(&start_2) );
    CE( cudaEventCreate(&stop_2) );
    inited = true;
  }
  void alloc(int alloc_n_nodes)
  {
    assert( sizeof(nodeid_t) == sizeof(weight_t) );
    assert( alloc_sz_chars == 0 );
    alloc_sz_elts = alloc_n_nodes * alloc_n_nodes;
    alloc_sz_chars = alloc_sz_elts * sizeof(dist_w_d[0]);
    CE( cudaMalloc(&dist_w_d, alloc_sz_chars) );
    CE( cudaMalloc(&prev_w_d, alloc_sz_chars) );
  }

  void to_gpu(APSP_path_t& r)
  {
    size_t n_elts = r.num_nodes * r.num_nodes;
    cuda_host_to_dev(dist_w_d,r.dist,n_elts);
    cuda_host_to_dev(prev_w_d,r.prev,n_elts);
  }

  void from_gpu(APSP_path_t& r)
  {
    size_t n_elts = r.num_nodes * r.num_nodes;
    cuda_dev_to_host(r.dist,dist_w_d,n_elts);
    cuda_dev_to_host(r.prev,prev_w_d,n_elts);
  }

  double run(weight_t *dist, nodeid_t *prev, uint32_t num_nodes)
  {
    APSP_path_t kgr; // Complete Graph
    vector<weight_t> dist_w;
    vector<nodeid_t> prev_w;

    const uint32_t num_nodes_w = rnd_up(num_nodes,APSP_BLSZ);
    kgr.num_nodes = num_nodes_w;

    if ( num_nodes_w == num_nodes ) {

      kgr.dist = dist;
      kgr.prev = prev;

    } else {

      const uint32_t n_elts = num_nodes_w * num_nodes_w;

      dist_w.resize(n_elts);
      prev_w.resize(n_elts);
      kgr.dist = dist_w.data();
      kgr.prev = prev_w.data();

      for(uint32_t i=0; i<num_nodes_w; i++)
        for(uint32_t j=0; j<num_nodes_w; j++)
          if(i<num_nodes && j<num_nodes){
            dist_w[i*num_nodes_w + j] = dist[i*num_nodes + j];
            prev_w[i*num_nodes_w + j] = prev[i*num_nodes + j];
          } else {
            if(i==j){
              dist_w[i*num_nodes_w + j] = 0;
              prev_w[i*num_nodes_w + j] = i;
            } else {
              dist_w[i*num_nodes_w + j] = NPP_MAX_32U;
              prev_w[i*num_nodes_w + j] = NPP_MAX_32U;
            }}
    }

    init();
    alloc(kgr.num_nodes);
    to_gpu(kgr);
    const double run_time = inner(kgr);
    from_gpu(kgr);

    if ( num_nodes_w != num_nodes )
      for(uint32_t i=0; i<num_nodes; i++)
        for(uint32_t j=0; j<num_nodes; j++){
          dist[i*num_nodes + j] = dist_w[i*num_nodes_w + j];
          prev[i*num_nodes + j] = prev_w[i*num_nodes_w + j];
        }

    return run_time;
  }

  void run_benchmark()
  {
    const double bm_start = time_wall_fp();
    double total_kernels = 0;
    init();
    alloc(K_MAX);
    const uint check_upto_dim =
      !chopts.verify ? 0 :
      chopts.verify_contr_final == 1 ? 2 * APSP_BLSZ : K_MAX;
    APSP_Check apsp_check;

    vector<weight_t> dist(alloc_sz_elts), dist_check;
    vector<nodeid_t> prev(alloc_sz_elts);
    if ( check_upto_dim ) dist_check.resize(alloc_sz_elts);
    APSP_path_t result;
    result.dist = dist.data();
    result.prev = prev.data();

    assert( WEIGHT_INF == ~weight_t(0) );
    assert( NODEID_NULL == ~nodeid_t(0) );

    string bm_file_name = apsp_bench_info.filename_make();
    fspath bm_file_path_abs = cuch_exe_dir / bm_file_name;
    fspath bm_file_path = relative(bm_file_path_abs);
    string bm_file_path_s = bm_file_path.native();

    srand(0);
    std::vector<float> time_APSP;
    const uint32_t repetition = 5;// number of times benchmark is repeated
    uint32_t iter = 0;
    for(uint32_t dim=APSP_BLSZ; dim <= K_MAX; dim += APSP_BLSZ){

      result.num_nodes = dim;
      const size_t curr_sz_chars = dim * dim * sizeof(nodeid_t);

      vector<double> times_s;

      for(uint32_t r=0; r<repetition; r++){

        memset(result.dist,~0,curr_sz_chars);
        memset(result.prev,~0,curr_sz_chars);
        for ( uint d=0; d<dim; d++ ) result.dist[d*dim+d] = 0;
        for(size_t i=0; i<dim; i++){
          for(size_t j=0; j<dim; j++){
            if( i != j ){
              int not_zero = rand() & 0x1;
              if( not_zero ){
                result.dist[i*dim+j] = (rand() & 0xFFF) + 1;
                result.prev[i*dim+j] = i;
              }
            }
          }
        }
        to_gpu(result);
        const double time_s = inner(result);
        total_kernels += time_s;
        times_s.push_back( time_s );
      }

      if ( dim <= check_upto_dim ) apsp_check.copy(result);

      from_gpu(result);

      if ( dim <= check_upto_dim ) apsp_check.check(result);

      // Use the median time.
      sort(times_s.begin(),times_s.end());
      time_APSP.push_back( times_s[repetition/2] );
      iter++;
    }

    for(uint32_t i=0; i<iter; i++){
      pr.loop("APSP time (s) with num_nodes = %u : %f\n",
              (i+1)*APSP_BLSZ, time_APSP[i]);
    }

    if ( check_upto_dim )
      pr.user("Checked APSP results for correctness up to size %u of %u.\n",
             check_upto_dim, K_MAX);
    else
      pr.tune("Did not check APSP benchmark results for correctness.\n");

    pr.user("Writing APSP benchmark data to %s.\n", bm_file_path_s.c_str());

    FILE *file_out = fopen(bm_file_path_s.c_str(), "w");
    array_dump_f(file_out, time_APSP.data(), iter);
    fclose(file_out);

    const double bm_end = time_wall_fp();
    pr.user("Time to prepare APSP benchmark data: %.3f s wall,  %.3f s ker\n",
           bm_end - bm_start, total_kernels);
  }

  double inner(APSP_path_t& path)
    {
      // calculate the blocked APSP
      const bool timing_details = chopts.verify_in_loops;
      const double inner_start = time_wall_fp();

      const uint32_t num_nodes_w = path.num_nodes;
      const uint32_t block_num = num_nodes_w/APSP_BLSZ;
      assert( block_num * APSP_BLSZ == num_nodes_w );
      float kernel_time = 0;
      double total_time = 0;

      uint32_t block_dim = 1024;
      dim3 grid_dim(block_num, block_num);
      for(uint32_t iter=0; iter<block_num; iter++){

        // phase 0, update block iter,iter
        CE( cudaEventRecord(start_0, 0) );

        cu_APSP_blocked<false><<<grid_dim,block_dim>>>(dist_w_d, prev_w_d, num_nodes_w, iter, 0);
    
        if ( timing_details )
          {
            CE( cudaEventRecord(stop_0, 0) );
            CE( cudaEventSynchronize(stop_0) );
            kernel_time = 0;
            CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
            total_time += kernel_time * 0.001;

            pr.tune("time for phase 0 of iter %u of the APSP kernel = %f s\n",
                    iter, kernel_time * 0.001);

            CE( cudaDeviceSynchronize() ); CE( cudaGetLastError() );
            CE( cudaEventRecord(start_1, 0) );
          }

        // phase 1, update the remainder of column iter and row iter
        cu_APSP_blocked<false><<<grid_dim,block_dim>>>(dist_w_d, prev_w_d, num_nodes_w, iter, 1);

        if ( timing_details )
          {
            CE( cudaEventRecord(stop_1, 0) );
            CE( cudaEventSynchronize(stop_1) );
            kernel_time = 0;
            CE( cudaEventElapsedTime(&kernel_time, start_1, stop_1) );
            total_time += kernel_time * 0.001;

            pr.tune("time for phase 1 of iter %u of the APSP kernel = %f s\n",
                     iter, kernel_time * 0.001);

            CE( cudaDeviceSynchronize() ); CE( cudaGetLastError() );
            CE( cudaEventRecord(start_2, 0) );
          }

        // phase 2, update everything else
        cu_APSP_blocked<false><<<grid_dim,block_dim>>>(dist_w_d, prev_w_d, num_nodes_w, iter, 2);
    
        if ( timing_details )
          {
            CE( cudaEventRecord(stop_2, 0) );
            CE( cudaEventSynchronize(stop_2) );
            kernel_time = 0;
            CE( cudaEventElapsedTime(&kernel_time, start_2, stop_2) );
            total_time += kernel_time * 0.001;
            pr.tune("time for phase 2 of iter %u of the APSP kernel = %f s\n",
                    iter, kernel_time * 0.001);

            CE( cudaDeviceSynchronize() ); CE( cudaGetLastError() );
          }
      }

      CE( cudaDeviceSynchronize() ); CE( cudaGetLastError() );
      const double inner_wall_s = time_wall_fp() - inner_start;
      pr.tune("total time for %u-nd APSP = %.6f s k,  %.6f s wall\n",
              num_nodes_w, total_time, inner_wall_s);
      return timing_details ? total_time : inner_wall_s;
    }

  bool inited;
  weight_t *dist_w_d;
  nodeid_t *prev_w_d;
  size_t alloc_sz_elts, alloc_sz_chars;

  cudaEvent_t start_0, stop_0, start_1, stop_1, start_2, stop_2;
};

void
cu_APSP_benchmark()
{
  APSP_Run apsp_run;
  apsp_run.run_benchmark();
}

float
cu_APSP_main(weight_t *dist, nodeid_t *prev, uint32_t num_nodes)
{
  APSP_Check check(dist,prev,num_nodes);
  if ( verbosity > 1 ) check.copy(dist,prev,num_nodes);
  APSP_Run apsp_run;
  float rv = apsp_run.run(dist,prev,num_nodes);
  if ( chopts.verify )
    {
      pr.user("Checking APSP query...");
      check.check(dist,prev,num_nodes);
      pr.user("correct.\n");
    }
  return rv;
}

LConfig
configs_select
(map<int,vector<LConfig>>& configs, uint32_t tpn, uint32_t list_len)
{
  LConfig *c_best = nullptr;
  assert( configs.count(tpn) );
  for ( auto& c: configs[tpn] ){
    if ( !c_best ){
      c_best = &c;
    } else {
      assert(tpn > 6);
      const float n_iter_c = 0.5;
      const int bpn_sqrt = ((1<<tpn)/c.block_dim);
      const int bpn = bpn_sqrt * bpn_sqrt;
      const int bpn_n_sqrt = ((1<<tpn)/c_best->block_dim);
      const int bpn_n = bpn_n_sqrt * bpn_n_sqrt;
      float n_iter = list_len * bpn / c.grid_dim;
      float n_iter_n = list_len * bpn_n / c_best->grid_dim;
      if( n_iter_n > n_iter_c ){
	if( n_iter > n_iter_c ){
	  if(n_iter > n_iter_n){
	    c_best = &c;
	  }
	} else {
	  c_best = &c;
	}
      } else {
	if( n_iter <= n_iter_c && n_iter_n > n_iter ){
	  c_best = &c;
	}
      }
    }
  }

  return *c_best;
  
}

class CUDA_Mem_Trace {
public:
  CUDA_Mem_Trace():inited(false),opt(CMT_Unset),free_bytes_min(~size_t(0)){}
  bool inited;
  CUDA_Mem_Trace_Opt opt;
  size_t total_bytes, free_bytes_start, free_bytes_min;
  const char* min_free_file;
  int min_free_line;
  unordered_map<string,size_t> place_low;
  void init()
  {
    uint verb = min(uint(verbosity),2);
    opt = trace_opt_verb[verb];
    CE( cudaMemGetInfo( &free_bytes_start, &total_bytes ) );
    pr.tune("CUDA %5zu MiB free out of %5zu MiB total.\n",
           free_bytes_start>>20, total_bytes>>20);
    inited = true;
  }
  void sample(const char* file_name, int line_number)
  {
    if ( opt == CMT_None ) return;
    const size_t amt = cuda_mem_free_get();
    const bool new_low = set_min(free_bytes_min,amt);
    const string key = string(file_name) + to_string(line_number);
    size_t& amt_here  = place_low[key];
    const bool new_low_here = !amt_here || amt < amt_here;
    const bool show =
      opt == CMT_Show_All
      || opt == CMT_Show_Global_Lows && new_low
      || opt == CMT_Show_Line_Lows && new_low_here;
    if ( show )
      pr.tune("CUDA %5zu MiB free at %s::%d %s\n",
             amt>>20, file_name, line_number, new_low ? "** New Low **" : "");
    if ( new_low )
      {
        min_free_file = file_name;
        min_free_line = line_number;
      }
    amt_here = amt;
  }
  void at_end()
  {
    if ( opt > CMT_None )
      pr.tune("CUDA mem low point:  %5zu MiB free at %s:%d\n",
             free_bytes_min>>20, min_free_file, min_free_line);
    assert( inited );
    const size_t free_bytes = cuda_mem_free_get();
    const ssize_t leaked_bytes = ssize_t(free_bytes_start) - free_bytes;
    if ( opt > CMT_Quiet )
      pr.tune("CUDA storage leaked: %zd B  (%zu MiB of %zu MiB now free)\n",
             leaked_bytes, free_bytes >> 20, total_bytes >> 20);
  }
} CUDA_mem_trace;

void
trace_mem_usage_sample(const char* file_name, int line_number)
{ CUDA_mem_trace.sample(file_name,line_number); }

bool trace_mem_tracing() { return CUDA_mem_trace.opt > CMT_None; }
bool trace_mem_showing() { return CUDA_mem_trace.opt > CMT_Quiet; }


// Set to true to turn on collection of performance counter data.
const bool nperf_on = false;
bool cu_cuda_init_called = false;

void
cu_cuda_init()
{
  if ( cu_cuda_init_called ) return;
  cu_cuda_init_called = true;

  NPerf_init(nperf_on);
  NPerf_metrics_off();

  if ( chopts.print_tuning )
    gpu_info_print();

  const int dev =
    gpu_choose_index(chopts.gpu_require_nmps,chopts.gpu_require_idx);

  if ( dev < 0 )
    {
      fprintf
        (stderr,
         "A suitable GPU could not be found. (See --gpu-require flags.)\n");
      exit(1);
    }

  CE(cudaSetDevice(dev));
  GPU_Info gpu_info(dev);
  pr.user("Using GPU %d: %s\n",dev,gpu_info.cuda_prop.name);

  // To get a list of available metric run:
  //   ./nperf/cupti_query -getmetrics
  NPerf_metric_collect("l2_read_throughput");
  NPerf_metric_collect("dram_read_throughput");
  NPerf_metric_collect("warp_execution_efficiency");
  NPerf_metric_collect("gld_efficiency");
  NPerf_metric_collect("eligible_warps_per_cycle");
  NPerf_metric_collect("inst_executed");
  NPerf_metric_collect("ipc");
  NPerf_metric_collect("issue_slot_utilization");
  NPerf_metric_collect("ldst_fu_utilization");
  NPerf_metric_collect("l2_utilization");

  CUDA_mem_trace.init();
}

string
APSP_Bench_Info::filename_make()
{
  assert( cu_cuda_init_called );
  int dev_idx;
  CE( cudaGetDevice(&dev_idx) );
  char host_buf[HOST_NAME_MAX];
  if ( gethostname(host_buf,sizeof(host_buf)) )
    {
      fprintf(stderr,"Could not get host name: %s\n",
              strerror(errno));
      exit(1);
    }
  pStringF fname("APSP-host-%s-%d.bench",host_buf,dev_idx);
  return fname.ss();
}

size_t
cuda_mem_free_get()
{
  size_t free_byte, total_byte;
  CE( cudaMemGetInfo( &free_byte, &total_byte ) );
  return free_byte;
}

void cu_cuda_memcheck_at_end() { CUDA_mem_trace.at_end(); }

void
cu_CH_ASCII_art(cu_CH_stats_t* stats, GPU_Info& gpu_info)
{
  auto printf = pr_tune;
  const int n_iters = stats->num_iterations;
  const int pieces = 10;
  const bool use_kernel_times =
    chopts.wps_oracle || chopts.elist_sort || verbosity > 1
    || chopts.cull_ideal_hops;

  enum Categories
    { C_Score, C_Select, C_Contr, C_Update, C_Extr, C_Inv,
      C_Total, C_n_iters, C_SIZE };
  const char clabel[] = { 's', 't', 'C', 'u', 'e', 'i', '~', 'n' };

  double t_cat[pieces][C_SIZE];
  memset(t_cat,0,sizeof(t_cat));
  vector<uint32_t> max_deg_p(pieces), n_contracted_p(pieces);
  vector<uint32_t> n_longcuts_cut(pieces), n_edges(pieces);
  vector<int> max_tpn(pieces);
  vector<double> t_kernel_p(pieces), avg_deg(pieces);
  double* const t_iter_a = use_kernel_times
    ? stats->time_iter : stats->wall_time_iter_ms.data();
  double dur_total = 0;
  for ( int i=0; i<n_iters; i++ ) dur_total += t_iter_a[i];
  const double dur_region = dur_total / pieces;
  double t_next_region = dur_region;
  double t_curr = 0;
  const bool have_ideal_lc = stats->it_num_longcuts_cut_ideal.size() > 0;
  for ( int iter = 0, p = 0; iter < n_iters; iter++ )
    {
      if ( p < pieces - 1 && t_curr > t_next_region )
        {
          p++;
          t_next_region += dur_region;
        }
      const double dur_iter_ms = t_iter_a[iter];
      t_curr += dur_iter_ms;
      avg_deg[p] = stats->mean_degree[iter];
      set_max(max_deg_p[p], stats->max_degree[iter]);
      set_max(max_tpn[p], stats->it_tpn[iter]);
      t_kernel_p[p] += stats->time_iter[iter];
      n_edges[p] += stats->it_num_edges[iter];
      n_contracted_p[p] += stats->num_nodes_in_lvl[iter];
      n_longcuts_cut[p] += stats->it_num_longcuts_cut[iter];
      if ( have_ideal_lc )
        n_longcuts_cut[p] += stats->it_num_longcuts_cut_ideal[iter];
      t_cat[p][C_n_iters]++;
      t_cat[p][C_Total] += dur_iter_ms;
      t_cat[p][C_Score] += stats->time_scoring[iter];
      t_cat[p][C_Select] += stats->time_MIS[iter];
      t_cat[p][C_Contr] += stats->time_contraction[iter];
      t_cat[p][C_Update] += stats->time_update[iter];
      t_cat[p][C_Extr] +=
        stats->time_OL_extraction_0[iter] + stats->time_OL_extraction_1[iter];
      t_cat[p][C_Inv] +=
        stats->time_invert_0[iter] + stats->time_invert_1[iter];
    }

  double max_time_ms = 0;
  for ( int p=0; p<pieces; p++ ) set_max(max_time_ms,t_cat[p][C_Total]);

  uint32_t total_contracted = 0;
  for ( auto n: n_contracted_p ) total_contracted += n;
  const double total_contracted_inv = 1.0 / max(1,total_contracted);

  const int bar_len = 60;

  printf("%3s %3s %1s %3s %5s %s\n",
         "nIt", "Deg", "T", " C‰", "LC/e5",
         "s:score, t:select, C:contract, u:update, e:extr+inv, ~:other");

  for ( int p=0; p<pieces; p++ )
    {
      const int n_iters = t_cat[p][C_n_iters];
      if ( !n_iters ) break;
      const double dur_piece = t_cat[p][C_Total];
      const double scale = dur_piece ? bar_len / dur_piece : 0;
      const int pm_piece =
        n_contracted_p[p] * total_contracted_inv * 1000 + 0.5;
      double pos = 0;
      string line = "";
      const int n_per_iter = 0.5 + n_contracted_p[p] / n_iters;
      string s_rate = n_per_iter < 100
        ? string("=") + to_string(n_per_iter)
        : pm_piece > 999 ? "===" : to_string(pm_piece);
      for ( int c = 0;  c <= C_Total;  c++ )
        {
          pos = ( c == C_Total ? 0 : pos ) + t_cat[p][c] * scale;
          int inpos = pos + 0.5;
          int wid = inpos - line.length();
          if ( wid <= 0 ) continue;
          line += string(wid,clabel[c]);
        }
      printf
        ("%3d %3d %1x %3s %5d %s\n",
         n_iters,
         int(avg_deg[p]), max_tpn[p],
         s_rate.c_str(),
         int(1e5*double(n_longcuts_cut[p]) / n_edges[p]),
         line.c_str());
    }
  printf
    ("%3d %3s %1s %3s %5s %s\n", n_iters, "", "", "", "",
     "C‰: per mil contr. if >1‰; otherwise =n, n contr. per iter.\n");

  const bool show_per_iter_data = false;

  struct Per_TPN {
    Per_TPN(){memset(this,0,sizeof(*this));}
    int tpn, iter_last, n_iters;
    double sc_dur_ms, co_dur_ms;
    int64_t n_nodes, n_contracted;
    double scoring_nd_occ, contract_nd_occ;
  };

  int max_tpn_all = 0;
  for ( auto tpn: max_tpn ) set_max(max_tpn_all,tpn);

  vector<Per_TPN> per_tpn(max_tpn_all+1);
  const double wp_sz = 1 << 5;
  const double wp_sz_nmps_inv = 1.0 / ( wp_sz * stats->env_gpu_nmps );

  pTable table(stdout);
  table.opt_underline = true;

  for ( int iter = 0; iter < n_iters; iter++ )
    {
      const int tpn = stats->it_tpn[iter];
      const double TPN = 1 << tpn;
      const double deg = stats->mean_degree[iter];

      const double data_elts_score_p_node =
        4 // CC Degree and edge list pointers.
        + deg * 6 // CC edges' weight, dest, dest degree, dest edge tr
        + deg * deg * 2; // BWD FWD edges' weight and dest.

      const double nd_per_block = max(1.0,32/TPN);

      const double data_xelts_score_p_node =
        8 / nd_per_block *
        4 // CC Degree and edge list pointers.
        + rnd_up(deg,8) * 6 // CC edges' weight, dest, dest degree, dest edge tr
        + deg * rnd_up(deg,8) * 2; // BWD FWD edges' weight and dest.

      int64_t n_nodes = stats->num_nodes_overlay[iter];

      const double data_chars_score_p_node =
        data_elts_score_p_node * sizeof(nodeid_t);
      const double rate_score_dlimit_p_ms =
        1e-3 * gpu_info.chip_bw_Bps / data_chars_score_p_node;
      const double rate_score_xdlimit_p_ms =
        1e-3 * gpu_info.chip_bw_Bps /
        ( data_xelts_score_p_node*sizeof(nodeid_t) );

      Per_TPN& p = per_tpn[tpn];

      p.tpn = tpn;
      p.iter_last = iter;
      p.n_iters++;
      p.sc_dur_ms += stats->time_scoring[iter];
      p.co_dur_ms += stats->time_contraction[iter];
      p.n_nodes += n_nodes;
      p.n_contracted += stats->num_nodes_in_lvl[iter];

      if ( !show_per_iter_data ) continue;

      double rate = double(n_nodes) / stats->time_scoring[iter];

      pTable_Row row(table);
      table.entry("Itr"," %3d", iter);
      table.entry("TPN"," %2d", tpn);
      table.entry("Deg","%5.1f", deg);
      table.entry("N nds","%9ld", n_nodes);
      table.header_span_start("Score");
      table.header_span_start("Rate n/ms");
      table.entry("Meas","%8.1f", rate);
      table.entry("Id-Sl","%6.1f", rate_score_dlimit_p_ms / rate );
      table.entry("Ix-Sl","%6.1f", rate_score_xdlimit_p_ms / rate );
      table.header_span_end();
      table.entry("n/ms/bl","%8.1f",
                  rate / stats->it_kernel_scoring_nd_occ[iter] );
      table.header_span_end();
      table.header_span_start("WP/MP");
      table.entry
        ("Sc","%2.0f",
         stats->it_kernel_scoring_nd_occ[iter] * TPN * wp_sz_nmps_inv);
      table.entry
        ("Co","%2.0f",
         stats->it_kernel_contract_nd_occ[iter] * TPN * wp_sz_nmps_inv);
      table.header_span_end();
    }

  pTable ttpn(stdout);
  ttpn.opt_underline = true;
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;

  for ( auto& p: per_tpn )
    {
      if ( !p.n_nodes ) continue;
      const int tpn = p.tpn;
      const int liter = p.iter_last;
      pTable_Row row(ttpn);
      ttpn.entry("tpn", " %2d", tpn);
      ttpn.entry("Iters", p.n_iters);
      ttpn.entry("N nds", "%9ld", p.n_nodes);
      ttpn.entry("N Con", "%9ld", p.n_contracted);
      ttpn.header_span_start("Rate, nd/ms");
      ttpn.entry
        ("Score", "%8.1f", double(p.n_nodes) / p.sc_dur_ms);
      ttpn.entry
        ("Contract", "%8.1f", double(p.n_contracted) / p.co_dur_ms);
      ttpn.header_span_end();
      ttpn.header_span_start("WP/MP");
      ttpn.entry
        ("Sc","%2d", stats->it_kernel_scoring_wp_occ[liter] / nmps);
      ttpn.entry
        ("Co","%2d", stats->it_kernel_contract_wp_occ[liter] / nmps);
      ttpn.header_span_end();
      ttpn.header_span_start("Nd Occ");
      ttpn.entry("Sc","%6.1f", stats->it_kernel_scoring_nd_occ[liter] );
      ttpn.entry("Co","%6.1f", stats->it_kernel_contract_nd_occ[liter] );
      ttpn.header_span_end();
    }
}

void
cu_status_init(cu_Contract_Query_State& cq_state)
{
  cu_CH_stats_t* const stats = &cq_state.stats;
  GPU_Info gpu_info;
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;
  stats->env_time_start_ue = time_wall_fp();
  {
    time_t tt = time(nullptr);
    tm* const tmptr = localtime(&tt);
    char buf[100];
    strftime(buf, sizeof(buf)-1, "%A, %d %B %Y %T %Z",tmptr);
    stats->env_time_start_local = buf;
  }
  stats->env_gpu_name = gpu_info.cuda_prop.name;
  stats->env_gpu_cc =
    pStringF("%d.%d", gpu_info.cuda_prop.major, gpu_info.cuda_prop.minor);
  stats->env_gpu_nmps = nmps;
  stats->env_nvcc_version =
    pStringF("%d.%d.%d",
             __CUDACC_VER_MAJOR__, __CUDACC_VER_MINOR__, __CUDACC_VER_BUILD__);
  char host_name[HOST_NAME_MAX+1];
  gethostname(&host_name[0],HOST_NAME_MAX);
  stats->env_host_name = host_name;
  CE( cudaRuntimeGetVersion( &stats->env_cuda_runtime_version ) );
  CE( cudaRuntimeGetVersion( &stats->env_cuda_driver_version ) );

  // Values assigned below can be changed.
  int assertion_checking = 0;
  stats->env_cuda_debug = false;
  stats->env_host_optimization = false;
  stats->env_contract_eval = false;
  stats->env_verbosity = Stats_Policy(verbosity);
  assert( ++assertion_checking );
  stats->env_assertion_checking = assertion_checking;
#ifdef CONTRACTION_EVAL
  stats->env_contract_eval = true;
#endif
#ifdef __CUDACC_DEBUG__
  stats->env_cuda_debug = true;
#endif
#ifdef __OPTIMIZE__
  stats->env_host_optimization = true;
#endif

  stats->env_run_slowed_by_tuning_and_debug_options =
    stats->env_assertion_checking
    || stats->env_cuda_debug
    || stats->env_contract_eval
    || !stats->env_host_optimization
    || stats->env_verbosity != 0
    || opt_static_cull_want_age;

  stats->env_compiler_version = __VERSION__;
}

void cu_CH_contract_main
(cu_graph_CH_bi_t **graph, cu_Contract_Query_State& cq_state)
{
  auto printf = pr_tune;

  pr.user("Starting graph contraction...");
  pr.tune("\n");

  // #define CONTRACTION_EVAL

  cu_CH_stats_t* const stats = &cq_state.stats;
  cu_graph_CH_UD_t** graph_UD = &cq_state.graph_UD_d;

  cu_status_init(cq_state);

  GPU_Info gpu_info;
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;

#ifdef CONTRACTION_EVAL
  const Stats_Policy stats_policy = Stats_Immediate;
#else
  const Stats_Policy stats_policy = Stats_Policy(verbosity);
#endif

# define cuda_error_check() \
  { if ( verbosity ) { CE(cudaDeviceSynchronize()); CE(cudaGetLastError()); } }

  printf("Detailed statistics collection: %s.\n"
         "  (See stats_policy in cu_main.cu)\n",
         stats_policy == Stats_None ? "turned off" :
         stats_policy == Stats_Immediate ? "immediate" :
         "at end of run");

  TRACE_MEM_USAGE();

  // Seconds in the Unix Epoch.
  const double t_contract_start_ues = time_wall_fp();

  // enables testing the contraction steps (should be off by default)
  // also prints out info about each of the kernels

#ifdef CONTRACTION_EVAL
  const bool want_score_blockcentric_stats = true;
  const bool shc_details = true;
#else
#ifdef NDEBUG
  const bool want_score_blockcentric_stats = false;
#else
  const bool want_score_blockcentric_stats = false;
#endif
  const bool shc_details = false;
#endif

  const bool need_selected_list = chopts.wps_oracle_perform;

  const bool opt_monitor_deg = chopts.print_tuning_in_loops;

  TRACE_MEM_USAGE();
  
  cu_graph_CH_UD_t *graph_UD_h = NULL;
  // Prepare events used for timing.
  //
  double total_time = 0;

  stats->time_total_scoring = stats->time_total_MIS
    = stats->time_total_contraction = stats->time_total_update
    = stats->time_total_OL_extraction_0 = stats->time_total_OL_extraction_1
    = stats->time_total_invert_0 = stats->time_total_invert_1;

  stats->wall_time_shadow_wps_s = 0;
  stats->wall_time_verify_post_s = 0;

  float kernel_time;
  vector<cuda_event> ev_array(5);
  cuda_event start_0, start_1, start_2, stop_0, stop_2;
  event_pair e_select_fl, e_select_sort1, e_select_lo;
  event_pair e_select_select, e_select_gather, e_select_sort2;
  event_pair e_num_edges;
  event_pair e_invert_0, e_invert_1;

  const double t_verify_init_ues = time_wall_fp();
  Contract_Dist_Verify& contract_dist_verify = cq_state.contract_dist_verify;
  if ( chopts.verify )
    {
      contract_dist_verify.init( *graph );
      contract_dist_verify.n_queries_per_iter_set( cq_state.srcs.num_args() );
    }
  stats->wall_time_verify_pre_s = time_wall_fp() - t_verify_init_ues;

  cu_graph_CH_bi_t *graph_cu_CH_d;
  graph_cu_CH_d = *graph;
  
  cu_graph_CH_UD_t *graph_UD_d;
  graph_UD_d = *graph_UD;
  cu_graph_CH_UD_t *graph_UD_d_compressed;

  cu_graph_CH_UD_t graph_UD_h_orig = cuda_dev_to_host(graph_UD_d);
  const int max_rank_alloc = graph_UD_h_orig.max_rank;

  nodeid_t *d_DB_est_h = (nodeid_t *) malloc(sizeof(nodeid_t));
  nodeid_t *d_DB_est_d = NULL;
  CE( cudaMalloc( &d_DB_est_d, sizeof(d_DB_est_d[0]) ) );

  // load num_nodes and tpn from graph
  nodeid_t num_nodes;
  nodeid_t num_nodes_init;
  uint32_t tpn;
  uint32_t max_degree;
  uint32_t overlay_size;
  cu_graph_CH_bi_t *graph_cu_CH_h = (cu_graph_CH_bi_t *) malloc(sizeof(cu_graph_CH_bi_t));
  CE( cudaMemcpy(graph_cu_CH_h, graph_cu_CH_d, sizeof(cu_graph_CH_bi_t), cudaMemcpyDeviceToHost) );

  // Initialized midpoints with original edges.
  CE( cudaMemcpyAsync
      ( graph_cu_CH_h->graph_b.midpoint,
        graph_cu_CH_h->graph_b.neighbors,
        graph_cu_CH_h->graph_b.empty_pointer
        * sizeof(graph_cu_CH_h->graph_b.midpoint[0]),
        cudaMemcpyDeviceToDevice ) );

  num_nodes = graph_cu_CH_h->num_nodes;
  num_nodes_init = graph_cu_CH_h->num_nodes;
  tpn = graph_cu_CH_h->thread_per_node;
  max_degree = graph_cu_CH_h->max_degree;
  overlay_size = graph_cu_CH_h->overlay_size;
  size_t e_space_used_new_ov = graph_cu_CH_h->graph_f.empty_pointer;
  printf("initially: tpn = %u, max_deg = %u, num_nodes = %u, overlay_size = %u\n", tpn, max_degree, num_nodes, overlay_size);

  uint block_dim;
  size_t shared_dim;
  const double orig_avg_deg =
    double(graph_cu_CH_h->graph_f.num_edges_exact) / num_nodes;

  // Information on host and CUDA shortcut lists written by
  // the contract kernel and read by the update kernels.
  cu_shc_CH_set shc_set;

  int iter = 0;

  double C_sco;

  switch(chopts.C_dynamic){
  case 0:
    // constant C
    if(chopts.C_set > 1 || chopts.C_set <=0){
      printf("C must be larger than 0.0 and smaller or equal to 1.0, using default value\n");
      chopts.C_set = 0.3;
    }
    C_sco = chopts.C_set;
    break;
  case 1:
    // start with C_MIN
    C_sco = C_MIN;
    break;
  case 2:
    // start with C_MIN
    C_sco = C_MIN;
    break;
  default:
    printf("invalid --C-dynamic option\n");
    exit(1);
    break;
  }

  const double clk_s = 1e-3 / gpu_info.cuda_prop.clockRate; // Clock period.

  std::vector<void*> to_be_freed;

  const int degree_sum_nblk = nmps * 32;
  std::vector<int> degree_sums_h(degree_sum_nblk);
  int* degree_sums_d = NULL;
  CE( cudaMalloc( &degree_sums_d, degree_sum_nblk * sizeof(degree_sums_d[0])) );
  to_be_freed.push_back(degree_sums_d);

  const int max_n_warps = nmps * 64;
  Extract_Block_Storage *ebs_d = NULL;
  CE( cudaMalloc( &ebs_d, max_n_warps * sizeof(ebs_d[0]) ) );
  to_be_freed.push_back(ebs_d);

  cu_CH_query_stats *qstats_d = NULL;
  if ( want_score_blockcentric_stats )
    {
      CE( cudaMalloc(&qstats_d,degree_sum_nblk*sizeof(*qstats_d)) );
      to_be_freed.push_back(qstats_d);
    }


  map<int,array<LConfig,2>>&& configs_score_nodes = configs_score_get(gpu_info);
  map<int,LConfig> configs_score_nodes_blocked;
  map<int,vector<LConfig>>&& configs_co_dyn = configs_contract_get_all(gpu_info);
  map<int,LConfig>&& configs_co = configs_contract_get(gpu_info);
  map<int,LConfig> configs_update_graph_1;

#define occupancy_k_blksz(k,tpn,blksz)                                        \
  {                                                                           \
    LConfig& config = configs_##k[tpn];                                       \
    config.inited = true;                                                     \
    config.shared_dim = 0;                                                    \
    config.block_dim = blksz;                                                 \
    CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor                         \
	( &config.blks_per_mp, cu_CH_##k<tpn>,                                \
	  config.block_dim, config.shared_dim) );                             \
    /* For size of qstats_d. */                                               \
    assert(config.blks_per_mp<=degree_sum_nblk/nmps);                         \
    config.grid_dim = config.blks_per_mp * nmps;                              \
    const int TPN = 1 << tpn;                                                 \
    const bool blocked = blksz < TPN;                                         \
    const double thd_per_tpn = blksz / double( TPN );                         \
    config.nd_per_blk_iter =                                                  \
      blocked ? thd_per_tpn * thd_per_tpn : thd_per_tpn;                      \
    config.nd_occ = config.nd_per_blk_iter * config.grid_dim;                 \
  }

 occupancy_k_blksz(update_graph_1,3,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,4,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,5,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,6,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,7,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,8,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,9,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,10,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,11,block_dim_update_1);
 occupancy_k_blksz(update_graph_1,12,block_dim_update_1);

 auto config_make = [&](Func_Kernel kernel, int block_sz)
  {
    LConfig config;
    config.inited = true;
    config.shared_dim = 0;
    config.block_dim = block_sz;
    CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
	( &config.blks_per_mp, kernel, config.block_dim, config.shared_dim) );
    config.grid_dim = config.blks_per_mp * nmps;
    return config;
  };

 LConfig&& config_score_blocked_2 =
   config_make(Func_Kernel(cu_CH_score_nodes_blocked_2),32);

 LConfig&& configs_update_graph_01 =
   config_make(Func_Kernel(cu_CH_update_graph_01),32);

#undef occupancy_blocked_contract  

  int blks_per_mp_select_nodes_gl[10];
  
#define occupancy(i)                                                          \
  {                                                                           \
    const int block_dim = 1 << select_nodes_tpn_to_block_lg(i);               \
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor                           \
      (&blks_per_mp_select_nodes_gl[i-3], cu_CH_select_nodes_gl<i>, block_dim, 0) ); \
  }

  occupancy(3); occupancy(4); occupancy(5);
  occupancy(6); occupancy(7); occupancy(8);
  occupancy(9); occupancy(10); occupancy(11);
  occupancy(12);
  
#undef occupancy
  
#define LAUNCH_SELECT_NODES_GL(tpn)                                           \
  case tpn:                                                                   \
    {                                                                         \
      const int block_dim = 1 << select_nodes_tpn_to_block_lg(tpn);           \
      const int grid_dim = blks_per_mp_select_nodes_gl[tpn-3] * nmps;         \
      cu_CH_select_nodes_gl<tpn><<<grid_dim,block_dim>>>                      \
	(selected_list, iscore_in, iscore_out, nid);                          \
      break;                                                                  \
    }

  /// RRRR note that grid dim for contract_nodes and update_graph have to be the same.
  /// so we'll just use 2*nmps rather than max occupancy of each kernel

#define LAUNCH_UPDATE_GRAPH(tpn)                                              \
  case tpn:                                                                   \
    {                                                                         \
      CE( cudaEventRecord(start_1, 0) );                                      \
      cu_CH_update_graph_01<<<cu01.grid_dim,cu01.block_dim>>>		      \
	(graph_cu_CH_d, shc_list_dh);                                         \
      CE( cudaEventRecord(start_2, 0) );                                      \
      cu_CH_update_graph_1<tpn><<<cu1.grid_dim,cu1.block_dim>>>               \
	(graph_UD_d, graph_cu_CH_d, &selected_list[1], list_length_h, iter+1);\
      break;                                                                  \
    }

  int blks_per_mp_extract_overlay_1[10];
  
#define occupancy(i)							\
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor			\
      (&blks_per_mp_extract_overlay_1[i-3], cu_CH_extract_overlay_1<i>, block_dim_overlay_1, 0) );

  occupancy(3); occupancy(4); occupancy(5);
  occupancy(6); occupancy(7); occupancy(8);
  occupancy(9); occupancy(10); occupancy(11);
  occupancy(12);
  
#undef occupancy
  
#define LAUNCH_EXTRACT_OVERLAY_1(tpn)                                         \
  case tpn:                                                                   \
    {                                                                         \
      cu_CH_extract_overlay_1<tpn>                                            \
	<<<blks_per_mp_extract_overlay_1[tpn-3]*nmps,block_dim_overlay_1,0>>> \
	( graph_cu_CH_new_h, graph_cu_CH_h,                                   \
          node_id, node_id_inv);                                              \
      break;                                                                  \
    }
      
#define LAUNCH_APSP_PREP(tpn)					\
  case tpn:							\
    {								\
      cu_CH_APSP_prep<tpn><<<grid_dim,block_dim_APSP_prep,shared_dim>>>	\
	(graph_UD_d, graph_cu_CH_d);		                \
      break;							\
    }

  auto n_edges_get = [&](cu_graph_CH_bi_t* gr_cu_d)
    {
      cu_degree_sum_get<<<degree_sum_nblk,1024>>>(gr_cu_d,degree_sums_d);
      CE( cudaMemcpy
          ( degree_sums_h.data(), degree_sums_d,
            degree_sum_nblk * sizeof(degree_sums_h[0]),
            cudaMemcpyDeviceToHost) );
      int64_t n_edges = 0;
      for ( auto d: degree_sums_h ) n_edges += d;
      return n_edges;
    };

  auto qstats_print = [&](const char* name, LConfig& c)
    {
      auto printf = pr_loop;
      vector<cu_CH_query_stats> qstats_ha(c.grid_dim);
      CE( cudaMemcpy
          ( qstats_ha.data(), qstats_d, c.grid_dim * sizeof(qstats_d[0]),
            cudaMemcpyDeviceToHost) );
      cu_CH_query_stats& qstats_h = qstats_ha[0];
#define ACC(m) n_##m += q.n_##m;
      int n_ij = 0, n_2hop = 0, n_fpath = 0, n_1hshc = 0, n_2hshc = 0;
      int n_fph1 = 0, n_fph2 = 0, n_coll = 0, n_iter = 0, n_1hop_iter = 0;
      for ( auto& q : qstats_ha )
        { ACC(ij); ACC(2hop); ACC(fpath); ACC(1hshc); ACC(2hshc);
          ACC(fph1); ACC(fph2); ACC(coll); ACC(iter); ACC(1hop_iter); }
#undef ACC

      printf
        ("%s iter %3d: %2d wp/mp  %6.3f ms b/c1/c2/f  "
         "%6.3f /%6.3f /%6.3f / %5.3f\n",
         name, iter,
         ( c.blks_per_mp * c.block_dim ) >> 5,
         ( qstats_h.qend - qstats_h.qstart ) * clk_s * 1000,
         qstats_h.dbuf * clk_s * 1000,
         qstats_h.dcomp1 * clk_s * 1000,
         qstats_h.dcomp2 * clk_s * 1000,
         qstats_h.dfinish * clk_s * 1000 );
      if ( string(name) == "Contr" )
        {
          stats->wps_1hs[iter] = n_1hshc;
          stats->wps_1hs_loop[iter] = n_fph1;
          stats->wps_2hs[iter] = n_2hshc;
          stats->wps_2hs_loop[iter] = n_fph2;
          stats->wps_ij[iter] = n_ij;
          stats->wps_2hop[iter] = n_2hop;
          stats->wps_fp[iter] = n_fpath;
          printf
            ("%s i %3d nij %d, 1hs %d, %d, n2h %d, rat %.3f,  "
             "nfp %d, %.3f  2hs %4d, %d, %.3f\n",
             name, iter, n_ij, n_1hshc, n_fph1,
             n_2hop, 1.0 * n_2hop / max(1,n_ij),
             n_fpath, 1.0 * n_fpath / max(1,n_ij),
             n_2hshc, n_fph2, 1.0 * n_2hshc / max(1,n_ij));
        }
      else
        {
          // Score statistics.
          printf
            ("%s i %3d nij %d: (h,i) (%d,%d)  Hsh cl: %.4f  Loops: %d\n",
             name, iter, n_ij,
             n_1hshc, n_1hop_iter,
             double(n_coll)/max(1,n_coll+n_1hshc),
             n_iter);
        }

    };

  int64_t n_edges = n_edges_get(graph_cu_CH_d);
  const int64_t n_edges_start = n_edges;
  const nodeid_t num_nodes_start = num_nodes;

  Score_Components *score_components_d = nullptr; // Used by blocked kernels.

  vector<float> scores_h;
  vector<nodeid_t> selected_list_h;
  vector<int> selected_list_inv(num_nodes);
  const int wp_lg = 5;

  Radix_Sort_Kernels<float,SO_Descending>
    sort_scores(gpu_info,chopts.score_precision);
  Radix_Sort_Kernels<uint32_t,SO_Ascending> sort_selected(gpu_info,32);
  const int sort_tile_size = sort_scores.dapp.elt_per_tile;
  sort_scores.prepare_for_n_elts(rnd_up(num_nodes,sort_tile_size));
  sort_selected.prepare_for_n_elts(num_nodes);

  const int na_num_arrays = 7;
  nodeid_t na_num_nodes_allocated = 0;
  nodeid_t *na_storage_d = NULL;

  double tdur_wall_iter_s = 0;
  double tdur_wall_start_to_select_start_s = 0;
  double tdur_wall_select_start_end_s = 0;
  double tdur_wall_select_end_update_end_s = 0;
  double tdur_wall_update_end_iter_end_s = 0;

  // Future stats. That is, a possible future member for the stats array.
  vector<double> fstats_time_update_01_s(max_rank_alloc);

  const double t_main_loop_start_ues = time_wall_fp();

  vector<function<void()>> stats_deferred_funcs;

  auto stats_func_handle = [&]( function<void()>&& f )
    {
      switch ( stats_policy ) {
      case Stats_None: break;
      case Stats_Immediate: f(); break;
      case Stats_Later: stats_deferred_funcs.push_back(f); break;
      }
    };
  auto stats_string_handle = [&](string&& s)
    { stats_func_handle([=](){printf("%s",s.c_str());}); };

  // It's zero now but we don't want surprises after adding kernels above.
  const double dur_pre_main_kernels_s = total_time;

  TRACE_MEM_USAGE();

  double t_main_iter_next_start_ues = time_wall_fp();

  // contraction cut-off (K)
  uint32_t K_cco = 0;

  float* const APSP_bench_times = apsp_bench_info.times_get();

  bool continue_contraction;

  int query_blks_per_mp;
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor			\
      (&query_blks_per_mp, cu_CH_query_d, 1024, 0) );
  uint32_t query_occupancy = query_blks_per_mp * nmps;

  switch(chopts.K_dynamic){
  case 0:
    // must be within valid range
    if(chopts.K_set > K_MAX || chopts.K_set < 0){
      pr.warn("K must be positive and smaller or equal to %u, using default value\n", K_MAX);
      K_cco = chopts.K_set = 1024;
    } else {
      K_cco = chopts.K_set;
    }
    
    continue_contraction = num_nodes > K_cco;
    break;
  case 1:
    continue_contraction = true; // do at least one iteration of CUCH!
    break;
  case 2:
    continue_contraction = true; // do at least one iteration of CUCH!
    break;
  default:
    pr.fatal_user("invalid --K-dynamic option, %d\n",chopts.K_dynamic);
    break;
  }

  while(continue_contraction){

    stats->it.emplace_back();
    cu_CH_stats_per_iter& iter_stats = stats->it.back();

    auto printf = pr_loop;

    assert( iter < max_rank_alloc );

    cu_graph_CH_bi_t& graph_cu_CH_h = graph_cache_lookup(graph_cu_CH_d);

    const double avg_deg = double(n_edges) / num_nodes;
    stats->mean_degree[iter] = avg_deg;

    const double t_main_iter_start_ues = t_main_iter_next_start_ues;

    const double dur_update_01_prev_ms =
      iter ? 1e3*fstats_time_update_01_s[iter-1] : 0;
    const double dur_invert_prev_ms = iter
      ? stats->time_invert_0[iter-1] + stats->time_invert_1[iter-1] : 0;

    stats->it_tpn.push_back(tpn);
    stats->it_num_edges.push_back(n_edges);

    int na_num_distributed = 0;
    auto na_get =
      [&]() {
        assert( na_num_distributed < na_num_arrays );
        return &na_storage_d[ na_num_nodes_allocated * na_num_distributed++ ];
      };

    if ( na_num_nodes_allocated < num_nodes )
      {
        if ( na_storage_d ) CE( cudaFree( na_storage_d ) );
        na_num_nodes_allocated = rnd_up(num_nodes,sort_tile_size);
        const size_t na_amt_bytes =
          na_num_arrays * na_num_nodes_allocated * sizeof( *na_storage_d );
        CE( cudaMalloc( &na_storage_d, na_amt_bytes ) );
      }

    stats->num_nodes_overlay[iter] = num_nodes;

    if ( chopts.elist_sort ) tune_elist_sort(graph_cu_CH_d);

    if ( chopts.cull_ideal_hops )
      {
        stats_string_handle
          ( ch_edge_cull
            ( graph_cu_CH_d, selected_list_h.data(), stats, iter) );
        if ( chopts.verify_in_loops )
          contract_dist_verify.verify_self_consistency(graph_cu_CH_d,true);
      }

    //
    // node score
    //

    if ( nperf_on ) NPerf_metrics_on();
    float* score = (float*) na_get();

    // scores need to be initialized to zero for blocked score kernel
    if(tpn > score_nodes_tpn_to_block_size_blocked_lg){

      const size_t sc_sz_bytes = num_nodes * sizeof(*score_components_d);
      if ( !score_components_d )
        {
          CE( cudaMalloc ( &score_components_d, sc_sz_bytes ) );
          to_be_freed.push_back(score_components_d);
        }
      CE( cudaMemsetAsync(score_components_d, 0, sc_sz_bytes) );
      CE( cudaMemsetAsync(score, 0, num_nodes * sizeof(score[0])) );
    }

    if ( opt_longcuts_watch || chopts.longcuts_cull )
      {
        cuda_host_to_dev( &graph_cu_CH_d->num_longcuts, 0 );
        cuda_host_to_dev( &graph_cu_CH_d->num_longcuts_cut, 0 );
        if ( opt_static_cull_want_age )
          {
            cuda_host_to_dev( &graph_cu_CH_d->num_longcuts_orig, 0 );
            cuda_host_to_dev( &graph_cu_CH_d->sum_longcuts_rank, 0 );
          }
      }

    if ( opt_elist_use )
    {
      elist_populate<<<nmps,1024>>>(graph_cu_CH_h);
      CE( cudaDeviceSynchronize() );
      CE( cudaGetLastError() );
    }

    CE( cudaEventRecord(start_0, 0) );
    cu_CH_score_nodes_pre_launch(NULL,graph_UD_d);

    const int cull_score =
      chopts.cull_score && int(tpn) >= chopts.cull_threshold_tpn;

    LConfig& c_sc = configs_score_nodes[tpn][cull_score];
    LConfig& c2 = config_score_blocked_2;
    assert( c_sc.inited );
    {
      iter_stats.mean_weight = graph_cu_CH_h.mean_weight;
      for ( NPerf_data_reset(); NPerf_need_run_get(); )
        c_sc.k_sc<<<c_sc.grid_dim,c_sc.block_dim,c_sc.shared_dim>>>
          (graph_cu_CH_h, score, qstats_d, score_components_d);

      if ( c_sc.nd_per_blk_iter < 1 )
        cu_CH_score_nodes_blocked_2<<<c2.grid_dim,c2.block_dim>>>
          (graph_cu_CH_h, score_components_d, score);
    }
    stats->it_kernel_scoring_nd_occ.push_back(c_sc.nd_occ);
    stats->it_kernel_scoring_wp_occ.push_back
      ( c_sc.grid_dim * c_sc.block_dim >> wp_lg );

    CE( cudaEventRecord(stop_0, 0) );
    CE( cudaEventSynchronize(stop_0) );

    if ( qstats_d ) qstats_print("Sc",c_sc);

    if ( false )
      contract_dist_verify.verify_self_consistency(graph_cu_CH_d,true);

    float cu_score_time_ms = 0;
    CE( cudaEventElapsedTime(&cu_score_time_ms, start_0, stop_0) );

    // Use better method for measuring kernel execution time.
    kernel_time =
      NPerf_metrics_collection_get()
      ? NPerf_kernel_et_get() * 1000 : cu_score_time_ms;

    total_time += kernel_time * 0.001;
    stats_func_handle
      ([=]() {
         printf("time for scoring kernel = %f\n", kernel_time * 0.001); } );
    stats->time_scoring[iter] = kernel_time;
    stats->time_total_scoring += (double)kernel_time;

    auto rndup = [](int n, int m){ return m * ( (n+m-1)/m ); };
    const int nd_per_block = max(1.0,c_sc.nd_per_blk_iter);
    const double uz = 8; // Request underutilization penalty.
    // Request underutilization for per-node accesses.
    const double uy = max(1,8/nd_per_block);
    const int TPN = 1 << tpn;
    const double avg_deg_rr = rndup(avg_deg,uz);
    const double avg_deg_rw = rndup(avg_deg,min(TPN,32));
    const double avg_deg_rt = TPN;
    // Unique Data: 
    //    Node: ptr_f, ptr_b, nnbr_f, nnbr_b
    //  : Edge: nbr_f, nbr_b, wht_f, wht_b
    const double score_data_acc_uniq_elts =
      4 * num_nodes + 4 * num_nodes * avg_deg;
    const double score_data_acc_elts =
      4 * num_nodes * ( 1 + avg_deg + avg_deg )
      + 2 * num_nodes * avg_deg * avg_deg;
    const double score_data_req_elts =
      4 * num_nodes * ( uy + avg_deg_rr + avg_deg * uz )
      + 2 * num_nodes * avg_deg * avg_deg_rr;
    const double sc_handling_factor = 8;
    const double score_work_2 =
      4 * num_nodes * ( 1 + 2 * avg_deg_rt )
      + 2 * sc_handling_factor * num_nodes * avg_deg_rt * avg_deg_rw;

    const double score_dur_ns = kernel_time * 1e6;
    stats_func_handle
      ([=]() {
    printf("N nodes %d, mean unr deg %.1f. "
           "Per n/nd/nd^2  %5.1f / %6.3f / %6.3f ns\n",
           num_nodes, avg_deg,
           score_dur_ns / num_nodes,
           score_dur_ns / ( num_nodes * avg_deg ),
           score_dur_ns / ( num_nodes * avg_deg * avg_deg ) ); } ) ;

    const double scr_elts_to_gbs =
      sizeof(nodeid_t) / ( 1e9 * 1e-3 * kernel_time );

    stats_func_handle
      ([=]() {
         printf("Score, iter %2d: "
                "to mp   %6.2f GB/s     unique %6.2f GB/s\n",
                iter,
                score_data_acc_elts * scr_elts_to_gbs,
                score_data_acc_uniq_elts * scr_elts_to_gbs);
         printf("Score, iter %2d: "
                "to mp   %6.2f GB/s     unique %6.2f GB/s\n",
                iter,
                score_data_req_elts * scr_elts_to_gbs,
                score_data_acc_uniq_elts * scr_elts_to_gbs);
       });

    if ( NPerf_metrics_collection_get() )
      {
        printf
          ("Score, iter %2d: "
           "L2 Rd θ %6.2f GB/s  DRAM Rd θ %6.2f GB/s  Ratio: %.3f\n",
           iter,
           NPerf_metric_value_get("l2_read_throughput") * 1e-9,
           NPerf_metric_value_get("dram_read_throughput") * 1e-9,
           NPerf_metric_value_get("dram_read_throughput")
           / max(1.0,NPerf_metric_value_get("l2_read_throughput")) );
        printf("Score, iter %2d: D %1d wp/cy %.2f, weff %3.f, "
               "citn %6.2f  "
               "insn %6.2f  %4.1f \n",
               iter, tpn,
               NPerf_metric_value_get("eligible_warps_per_cycle"),
               NPerf_metric_value_get("warp_execution_efficiency"),
               NPerf_metric_value_get("inst_executed") * 32
               / score_data_acc_elts,
               NPerf_metric_value_get("inst_executed") * 1e-6,
               NPerf_metric_value_get("inst_executed") * 32
               / score_work_2 );

        printf("Score, iter %2d: ipc %.2f  "
               "util (is,ldst,l2)  %2.0f %2.0f %2.0f  leff %3.f%%\n",
               iter,
               NPerf_metric_value_get("ipc"),
               NPerf_metric_value_get("issue_slot_utilization"),
               NPerf_metric_value_get("ldst_fu_utilization"),
               NPerf_metric_value_get("l2_utilization"),
               NPerf_metric_value_get("gld_efficiency"));

      }

    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );

    NPerf_metrics_off();
    
    //
    // node selection based on the scores (MIS of top C_sco% nodes)
    //

    block_dim = 1024;
    
    int blks_per_mp = 0;
    CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
	(&blks_per_mp, array_fill_linear, block_dim, 0) );
    assert(blks_per_mp<=8); /* For size of qstats_d. */
    
    int grid_dim = blks_per_mp * nmps;
    const nodeid_t max_num_selected = num_nodes * double(C_sco);
    cuda_host_to_dev( &graph_cu_CH_d->max_num_selected, max_num_selected );
    const nodeid_t selected_list_n_elts = max_num_selected + 100;
    assert( selected_list_n_elts <= na_num_nodes_allocated );
    nodeid_t* const selected_list = na_get();

    const double t_select_start_ues = time_wall_fp();
    e_select_fl.start();

    nodeid_t *nid = na_get();
    array_fill_linear<<<grid_dim,block_dim>>>(nid, num_nodes);

    /// Note: score array re-writen to a positive decreasing sequence.

    e_select_fl.end();

    float* const node_scores_sorted = (float*) na_get();

    e_select_sort1.start();
    nodeid_t* const nid_out = na_get();

    sort_scores.sort
      (node_scores_sorted, nid_out, score, nid, num_nodes);

    e_select_sort1.end();
    nid = nid_out;

    nodeid_t *iscore = (nodeid_t*) score;
    score = NULL;
    assert( sizeof(*iscore) == sizeof(*score) );

    CE( cudaMemsetAsync(iscore, 0, num_nodes * sizeof(iscore[0])) );
    
    block_dim = 1024;
    
    CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
    (&blks_per_mp, cu_CH_linear_order, block_dim, 0) );
    assert(blks_per_mp<=8); /* For size of qstats_d. */
    
    grid_dim = blks_per_mp * nmps;
    
    e_select_lo.start();
    cu_CH_linear_order<<<grid_dim,block_dim>>>(iscore, nid, graph_cu_CH_d);
    e_select_lo.end();

    /// node_scores_sorted storage reused after this point.
    
    e_select_select.start();

    block_dim = 1024;
      
    // renumber node_scores to make sure that the contesting nodes have a linear order
    CE( cudaMemset(selected_list, 0, 1 * sizeof(uint32_t)) );
    /// looking at the numbers on a few real world graphs, adding a second iters improves the MIS selection
    /// only marginally (the 3+ iteration usually doesn't change results at all)
    uint32_t num_luby_iters = chopts.luby_deterministic ? 3 : 2;
    nodeid_t *iscore_in = iscore;
    nodeid_t *iscore_out =
      chopts.luby_deterministic ? (nodeid_t*)node_scores_sorted : iscore;

    uint32_t llen_iter_prev = 0;
    uint32_t llen_iter = 0;
    uint32_t llen_prev = 0;
    uint32_t llen = 0;

    uint32_t luby_iter = 0;
    bool done = false;
    const float luby_iter_ratio = 0.05;
    const uint32_t luby_iter_min = 2;
    const bool luby_tune = false && chopts.luby_dynamic_iter;
    if ( luby_tune ) printf("Iter %d Luby: ",iter);
    cu_CH_select_nodes_pre_launch(graph_cu_CH_d);

    while(!done){

      if ( iscore_in != iscore_out )
	CE( cudaMemcpyAsync
	    ( iscore_out, iscore_in, num_nodes * sizeof( iscore_out[0] ),
	      cudaMemcpyDeviceToDevice ) );

      switch(tpn){
	LAUNCH_SELECT_NODES_GL(3); LAUNCH_SELECT_NODES_GL(4);
	LAUNCH_SELECT_NODES_GL(5); LAUNCH_SELECT_NODES_GL(6);
	LAUNCH_SELECT_NODES_GL(7); LAUNCH_SELECT_NODES_GL(8);
	LAUNCH_SELECT_NODES_GL(9); LAUNCH_SELECT_NODES_GL(10);
	LAUNCH_SELECT_NODES_GL(11); LAUNCH_SELECT_NODES_GL(12);
      default:
	{
	  assert( tpn <= 12 );
	  break;
	}
      }
      if ( chopts.luby_dynamic_iter ){
	luby_iter++;
	llen_prev = llen;
	llen_iter_prev = llen_iter;
	CE( cudaMemcpy(&llen, selected_list, sizeof(uint32_t), cudaMemcpyDeviceToHost) );
	llen_iter = llen - llen_prev;
	if ( luby_tune ) printf(" %d",llen_iter);
	// do at least 2 iterations, past that depending on performance.
	done = ( llen_iter < (luby_iter_ratio * llen_iter_prev) || llen_iter < luby_iter_min )
			     && llen_iter_prev !=0;
      } else {
	luby_iter++;
	done = luby_iter >= num_luby_iters;
      }
      swap( iscore_in, iscore_out );
    }
    if ( luby_tune ) printf(" done, %u iterations\n", luby_iter);
    iter_stats.luby_n_iter = luby_iter;

    e_select_select.end();

    // sort the selected list based on degree to improve performance of the contraction kernel
    // this should help with improving load balance
    // we will re_use the nid array to load the degrees of the selected nodes
    block_dim = 1024;
    
    CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
    (&blks_per_mp, cu_CH_gather_s_key, block_dim, 0) );
    assert(blks_per_mp<=8); /* For size of qstats_d. */
    
    grid_dim = blks_per_mp * nmps;
    

    e_select_gather.start();
    cu_CH_gather_s_key<<<grid_dim,block_dim>>>
      (nid, selected_list, graph_cu_CH_d);
    e_select_gather.end();

    nodeid_t num_selected;
    CE( cudaMemcpy(&num_selected, selected_list, sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
    e_select_sort2.start();
    assert( num_selected ); // Detects a problem with -G. (2019-07-17)

    sort_selected.sort(nid, selected_list + 1,
                       nid, selected_list + 1, num_selected);

    e_select_sort2.end();

    CE( cudaEventSynchronize(e_select_sort2.e_end) );
    const double t_select_end_ues = time_wall_fp();
    const double dur_select_s = t_select_end_ues - t_select_start_ues;
    kernel_time = 0;
    CE( cudaEventElapsedTime
        (&kernel_time, e_select_fl.e_start, e_select_sort2.e_end) );
    total_time += (double) kernel_time * (double) 0.001;
    const double dur_sel_parts_s =
      e_select_fl.elapsed_s() + e_select_sort1.elapsed_s() +
      e_select_lo.elapsed_s() + e_select_select.elapsed_s() +
      e_select_gather.elapsed_s() + e_select_sort2.elapsed_s();

#define ASSIGN(n)                                                             \
    const double kdur_##n##_s                                                 \
      = iter_stats.time_select_##n##_s = e_select_##n.elapsed_s();

    ASSIGN(fl); ASSIGN(sort1); ASSIGN(lo);
    ASSIGN(select); ASSIGN(gather); ASSIGN(sort2);
#undef ASSIGN

    stats_func_handle
      ([=]()
       {
    printf("Sel, %2d fl,s1,lo, se,g,s2,x %4.f %5.f %4.f  %5.f %5.f %5.f %5.f\n",
           iter, kdur_fl_s * 1e6,
           kdur_sort1_s * 1e6, kdur_lo_s * 1e6,
           kdur_select_s * 1e6, kdur_gather_s * 1e6,
           kdur_sort2_s * 1e6,
           1e6*(dur_select_s-dur_sel_parts_s));

    printf("Sort, iter %2d  S1 %8u %5.1f Me/s  S2 %8u %5.1f Me/s\n",
           iter,
           num_nodes, 1e-6 * num_nodes/kdur_sort1_s,
           num_selected, 1e-6 * num_selected/kdur_sort2_s
           );

    printf("time for selection (MIS) kernel = %f  parts %.6f s  host %.6f s\n",
           kernel_time * 0.001, dur_sel_parts_s, dur_select_s);
    } );

    stats->time_MIS[iter] = kernel_time;
    stats->time_total_MIS += (double)kernel_time;

    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );


    if ( need_selected_list )
      {
        Time_Wall_s t_sl_copy;
        selected_list_h.resize( max_num_selected );

        // test correctness of selected_list (test MIS and score threshold)

        CE( cudaMemcpy(selected_list_h.data(), selected_list,
                       selected_list_h.size() * sizeof(selected_list_h[0]),
                       cudaMemcpyDeviceToHost) );
        if ( chopts.wps_oracle_perform )
          stats->wall_time_shadow_wps_s += t_sl_copy;
      }

    
    //
    // node contraction of nodes in selected_list
    //

    // allocate the shc_list
    /// we'll have to memcpy the actual value once the kernel is called from GPU, this won't be a problem.
    /// alternatively, we can modify the kernel call parameters to either get the list_length by pointer or not at
    /// all, and just read it from first value of selected_list as it is currently set
    uint32_t list_length_h;
    CE( cudaMemcpy(&list_length_h, selected_list, sizeof(nodeid_t), cudaMemcpyDeviceToHost) );

    stats_func_handle
      ([=]() {
         printf("final selected list length = %u\n", list_length_h); } );
    // maximum number of vertices contracted by any block
    LConfig& cu01 = configs_update_graph_01;
    const int upd_n_segs = cu01.grid_dim * ( cu01.block_dim >> wp_lg );

    /// note: vertex_per_block is NOT block_dim/max_deg, it's the
    /// total during the entire contraction step
    uint32_t vpb = max(4,div_up(list_length_h,upd_n_segs));
    const int alloc_shc_nd = num_nodes > num_nodes_init / 2
      ? int( orig_avg_deg * orig_avg_deg ) :
      min( 1 << tpn + min(2,tpn/2), int(avg_deg*avg_deg) );
    const int upd_max_len_per_seg = alloc_shc_nd * vpb;

    shc_set.alloc(upd_n_segs, upd_max_len_per_seg);
    cu_shc_CH_t& shc_list_dh = shc_set.dh;
    cu_shc_CH_t* const shc_list = shc_set.d;

    LConfig c_co;

    if(config_dynamic){
      c_co = configs_select(configs_co_dyn, tpn, list_length_h);
      assert( c_co.inited );
    } else {
      c_co = configs_co[tpn];
      assert( c_co.inited );
    }

    CE( cudaEventRecord(start_0, 0) );
    cu_CH_contract_nodes_pre_launch(NULL);

    if ( c_co.nd_per_blk_iter < 1 )
      cuda_host_to_dev(&graph_cu_CH_d->work_item_next,nodeid_t(0));

    // Launch Contract Kernel
    //
    c_co.k_ptr<<<c_co.grid_dim,c_co.block_dim,c_co.shared_dim>>>
      (graph_cu_CH_h, &selected_list[1], list_length_h, shc_list,
       qstats_d, d_DB_est_d);
    stats->it_kernel_contract_nd_occ.push_back(c_co.nd_occ);
    stats->it_kernel_contract_wp_occ.push_back
      ( c_co.grid_dim * c_co.block_dim >> wp_lg );

    CE( cudaMemcpyAsync(d_DB_est_h, d_DB_est_d, sizeof(d_DB_est_d[0]), cudaMemcpyDeviceToHost) );

    CE( cudaEventRecord(stop_0, 0) );
    CE( cudaEventSynchronize(stop_0) );
    kernel_time = 0;
    CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
    total_time += (double) kernel_time * (double) 0.001;
    if ( opt_longcuts_watch || chopts.longcuts_cull )
      {
        const nodeid_t n_longcuts =
          cuda_dev_to_host( &graph_cu_CH_d->num_longcuts );
        const nodeid_t n_longcuts_cut =
          cuda_dev_to_host( &graph_cu_CH_d->num_longcuts_cut );
        const nodeid_t n_longcuts_orig =
          cuda_dev_to_host( &graph_cu_CH_d->num_longcuts_orig );
        const nodeid_t s_longcuts_rank =
          cuda_dev_to_host( &graph_cu_CH_d->sum_longcuts_rank );
        const nodeid_t n_shc_culled = n_longcuts_cut - n_longcuts_orig;
        const int64_t sum_longcuts_age =
          int64_t(n_shc_culled) * iter - s_longcuts_rank;
        const double avg_age =
          opt_static_cull_want_age
          ? double(sum_longcuts_age) / max(1,n_shc_culled) : -1;
        assert( !opt_static_cull_want_age || sum_longcuts_age >= 0 );

        stats->it_num_longcuts_cut.push_back(n_longcuts_cut);
        stats->it_num_longcuts_orig.push_back(n_longcuts_orig);
        stats->it_avg_longcuts_age.push_back(avg_age);
        stats_func_handle
          ([=]() {
             printf("Longcuts at iter %3d: %6u, cut %4u+%4u, age %7.3f\n",
                    iter, n_longcuts, n_longcuts_orig,
                    n_shc_culled, avg_age ); } );
      }
    stats_func_handle
      ([=]() {
         printf("time for contraction kernel = %f\n", kernel_time * 0.001); } );
    stats->time_contraction[iter] = kernel_time;
    stats->time_total_contraction += (double)kernel_time;

    if ( qstats_d ) qstats_print("Contr",c_co);

    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );

    if ( chopts.verify_in_loops )
      contract_dist_verify.verify_self_consistency
        (graph_cu_CH_d,!chopts.longcuts_cull);

    // uses the shadow WPS results in contraction (use only for testing CH behavior)
    if ( chopts.wps_oracle_perform )
      {
        Time_Wall_s t_shadow;
        stats_string_handle
          ( shadow_wps
            ( graph_cu_CH_d, selected_list_h.data(), &shc_set, stats, iter ) );
        stats->wall_time_shadow_wps_s += t_shadow;
      }

    //
    // update the working graph and write the output UD graph
    //

    /// RRRR note that grid dim for contract_nodes and update_graph have to be the same.

    LConfig& cu1 = configs_update_graph_1[tpn];

    cu_CH_update_graph_pre_launch(graph_UD_d, graph_cu_CH_d);

    CE( cudaEventRecord(start_0, 0) );

    switch(tpn){
      LAUNCH_UPDATE_GRAPH(3); LAUNCH_UPDATE_GRAPH(4);
      LAUNCH_UPDATE_GRAPH(5); LAUNCH_UPDATE_GRAPH(6);
      LAUNCH_UPDATE_GRAPH(7); LAUNCH_UPDATE_GRAPH(8);
      LAUNCH_UPDATE_GRAPH(9); LAUNCH_UPDATE_GRAPH(10);
      LAUNCH_UPDATE_GRAPH(11); LAUNCH_UPDATE_GRAPH(12);
    default:
      {
        assert( tpn <= 12 );
	break;
      }
    }
  
    CE( cudaEventRecord(stop_0, 0) );
    CE( cudaEventSynchronize(stop_0) );

    const double t_update_end_ues = time_wall_fp();

    const double dur_update_00_s = elapsed_time_get(start_0,start_1);
    const double dur_update_01_s = elapsed_time_get(start_1,start_2);
    const double dur_update_1_s = elapsed_time_get(start_2,stop_0);
    const double dur_update_s =
      dur_update_00_s + dur_update_01_s + dur_update_1_s;
    total_time += dur_update_s;
    fstats_time_update_01_s[iter] = dur_update_01_s;

    stats_func_handle
      ([=]() {
         printf
           ("Update, iter %2d  %6d nds  %8.6f  %8.6f  %8.6f\n",
            iter, list_length_h,
            dur_update_00_s, dur_update_01_s, dur_update_1_s);
    });

    int n_edge_replace = 0, n_edge_append = 0, n_edge_realloc = 0;
    if ( qstats_d )
      {
        vector<cu_CH_query_stats> qstats_ha(cu01.grid_dim);
        CE( cudaMemcpy
            ( qstats_ha.data(), qstats_d, cu01.grid_dim * sizeof(qstats_d[0]),
              cudaMemcpyDeviceToHost) );
#define ACC(m) n_##m += q.n_##m;
      for ( auto& q : qstats_ha )
        { ACC(edge_replace); ACC(edge_append); ACC(edge_realloc); }
#undef ACC
      }

    if ( shc_details )
      {
        vector<typeof(shc_list_dh.len_dst[0])> shc_list_len(cu01.grid_dim);
        CE( cudaMemcpy( shc_list_len.data(), shc_list_dh.len_dst,
                        cu01.grid_dim * sizeof(shc_list_dh.len_dst[0]),
                        cudaMemcpyDeviceToHost ) );

        int n_shc = 0;
        for ( auto n: shc_list_len ) n_shc += n;
        const int n_shc_2 = n_edge_replace + n_edge_append + n_edge_realloc;
        stats->wps_shc[iter] = n_shc;

        // Common data accessed per shortcut.
        //  Per shc elt: src, dst, wht, mid
        //  All data below subject to request underutilization;
        //  Per node: RD:  ptr, nnbr, max_nnbr
        //  Per edge: nbr
        // Data accessed when edge appended to existing list:
        //  All data below subject to request underutilization;
        //  Per node: RD:   nnbr_over, flags
        //  Per node: WR: nnbr, nnbr_over,   nbr, wht, mid, flags.
        // Data accessed when new storage allocated for an edge:
        //  Atomic add: empty_pointer
        //  Per node: WR: ptr, nnbr, nnbr_over, max_allowed
        //  Per edge: WR: nbr, wht, med, flags
        //  Per node: WR: nnbr, nnbr_over,   nbr, wht, mid, flags.
        const double up_00_elts =
          n_shc * ( 4 + 3 + avg_deg )
          + n_edge_replace * ( 1 + 3 )
          + n_edge_append * ( 2 + 2 + 4 )
          + n_edge_realloc * ( 2 + 4 + avg_deg * 4 + 2 + 4 );

        // Per node RD: node_list, node_ids, ptrf, ptrb, ptrr, nnbrf, nnbrb, nnbrr
        // Per edge RD: nbrf, nbrb, rankf, rankb, whtb, midb, flagsb, node_idx(fb)
        // Atomic per iter: (uf/ub/db).empty_pointer
        // Per node WR:  (uf/ub/db)x(ptr,nnbr) (u/d)x(ranks,levels,idx,idx_inv)
        // Per edge RD: [removed] wht,mid,flg 
        // Per edge WR: (uf,ub,db).nbr (ub,db)x(wht,mid,flg)
        const double up_1_elts =
          list_length_h * ( 8 + 3*2 + 2*4 )
          + list_length_h * avg_deg * ( 8 + 3 + 3 + 2*3 )
          + list_length_h * cu1.block_dim / TPN * ( 4 /* cost of atomic */ * 3 );

        stats_func_handle
          ([=]() {
             printf
               ("Update, iter %2d  %6d shc %c %5.1f %5.1f GB/s  %d/%d/%d  "
                "u1 %2d wp/mp\n",
                iter, n_shc, n_shc == n_shc_2 ? ' ' : 'x',
                up_00_elts * sizeof(nodeid_t) * 1e-9 / dur_update_00_s,
                up_1_elts * sizeof(nodeid_t) * 1e-9 / dur_update_1_s,
                n_edge_replace, n_edge_append, n_edge_realloc,
                div_up( cu1.grid_dim * cu1.block_dim >> 5, nmps)
                );
           } );

      }

    stats->time_update[iter] = dur_update_s * 1000;
    stats->time_total_update += dur_update_s * 1000;

    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );

    if ( chopts.verify_in_loops )
      contract_dist_verify.verify_self_consistency(graph_cu_CH_d,false);

    //
    // extract the overlay graph (remove all contracted nodes and
    // their corresponding edges) while renumber the trimmed graph.
    //
    if ( opt_monitor_deg )
      {
        cuda_host_to_dev( &graph_cu_CH_d->graph_f.look_max_degree, 0 );
        cuda_host_to_dev( &graph_cu_CH_d->graph_b.look_max_degree, 0 );
        max_degree_update<<<nmps,1024>>>(graph_cu_CH_d);
      }

    // update the graph values in host
    graph_cache_lookup_struct_to_host(graph_cu_CH_d);

    const nodeid_t prev_nn = num_nodes;
    num_nodes = graph_cu_CH_h.num_nodes;
    assert( prev_nn == num_nodes );
    overlay_size = graph_cu_CH_h.overlay_size;
    int deg_fwd = opt_monitor_deg ? graph_cu_CH_h.graph_f.look_max_degree:0;
    int deg_bwd = opt_monitor_deg ? graph_cu_CH_h.graph_b.look_max_degree:0;

    stats_func_handle
      ([=]() {
        printf("Before extr in iter %u: tpn = %u, max_deg = %u (%d,%d), num_nodes = %u, overlay_size = %u\n",
               iter, tpn, max_degree, deg_bwd, deg_fwd, num_nodes, overlay_size);
      } );

    stats->num_nodes_in_lvl[iter] = num_nodes - overlay_size;

    const size_t free_byte = cuda_mem_free_get();
    stats_func_handle
      ([=]() {
        printf("At iter %2d, free MiB = %zu\n", iter, free_byte>>20); } );

    CE( cudaEventRecord(start_0, 0) );

    cu_graph_CH_bi_t *graph_cu_CH_new_d;
    graph_cu_CH_bi_init_d(&graph_cu_CH_new_d, overlay_size, 0);
    cu_CH_extract_pre_launch(graph_cu_CH_d);

    const nodeid_t elist_sz_have =
      graph_cache_n_edges_allocated_get(graph_cu_CH_new_d);

    CE( cudaEventRecord(stop_0, 0) );
    CE( cudaEventRecord(start_1, 0) );

    const int block_dim_ex_00 = 1024;
    const int grid_dim_ex_00_max = div_up(num_nodes, block_dim_ex_00);
    const int grid_dim_ex_00 = min(nmps,grid_dim_ex_00_max);
    nodeid_t* const node_id = na_get();
    nodeid_t* const node_id_inv = na_get();

    cu_CH_extract_overlay_00<<<grid_dim_ex_00,block_dim_ex_00>>>
      (graph_cu_CH_d, ebs_d);
    CE( cudaEventRecord(start_2, 0) );
    cu_CH_extract_overlay_01<<<grid_dim_ex_00,block_dim_ex_00>>>
      (graph_cu_CH_new_d, graph_cu_CH_d,
       node_id, node_id_inv, ebs_d);

    CE( cudaEventRecord(stop_2, 0) );

    cu_graph_CH_bi_t& graph_cu_CH_new_h =
      graph_cache_lookup_struct_to_host( graph_cu_CH_new_d );

    const double edge_freelist_factor =
      1.0 + GRAPH_BI_EMPTY_SPACE_MULTIPLIER * C_sco;
    e_space_used_new_ov =
      max( graph_cu_CH_new_h.graph_f.empty_pointer,
           graph_cu_CH_new_h.graph_b.empty_pointer);
    const nodeid_t elist_sz = e_space_used_new_ov * edge_freelist_factor;

    const size_t mem_free_pre_ov_bytes = cuda_mem_free_get();

    // Transfers old and new struct's to GPU.
    graph_cu_CH_bi_edges_reuse_fwd_for_bwd
      (graph_cu_CH_new_d, graph_cu_CH_d, elist_sz);

    CE( cudaEventSynchronize(stop_2) );
    const double dur_ex_setup_s = elapsed_time_get(start_0,stop_0);
    const double dur_extract_00 = elapsed_time_get(start_1,start_2);
    const double dur_extract_01 = elapsed_time_get(start_2,stop_2);

    kernel_time = ( dur_ex_setup_s + dur_extract_00 + dur_extract_01 ) * 1e3;
    total_time += dur_ex_setup_s + dur_extract_00 + dur_extract_01;
    stats->time_OL_extraction_0[iter] = kernel_time;
    stats->time_total_OL_extraction_0 += (double)kernel_time;

    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );

    CE( cudaEventRecord(start_0, 0) );

    const int grid_dim_overlay_1 = blks_per_mp_extract_overlay_1[tpn-3];

    switch(tpn){
      LAUNCH_EXTRACT_OVERLAY_1(3); LAUNCH_EXTRACT_OVERLAY_1(4);
      LAUNCH_EXTRACT_OVERLAY_1(5); LAUNCH_EXTRACT_OVERLAY_1(6);
      LAUNCH_EXTRACT_OVERLAY_1(7); LAUNCH_EXTRACT_OVERLAY_1(8);
      LAUNCH_EXTRACT_OVERLAY_1(9); LAUNCH_EXTRACT_OVERLAY_1(10);
      LAUNCH_EXTRACT_OVERLAY_1(11); LAUNCH_EXTRACT_OVERLAY_1(12);
    default:
      {
        assert( tpn <= 12 );
        break;
      }
    }

    CE( cudaEventRecord(stop_0, 0) );

#define LAUNCH_INVERT(tpn)                                              \
    case tpn:                                                           \
      cu_CH_invert_0<tpn><<<grid_dim_invert_0,block_dim_invert_0>>>     \
        (graph_cu_CH_new_h); break;

    {
      stats->num_iterations_invert++;
      const int grid_dim_invert_0 = nmps;
      const int block_dim_invert_0 = 1024;
      const int grid_dim_invert_1 = nmps;
      const int block_dim_invert_1 = 1024;
      graph_cu_CH_bi_edges_reuse_bwd_for_fwd
        (graph_cu_CH_new_d, graph_cu_CH_d);
      e_invert_0.start();
      switch ( tpn ) {
        LAUNCH_INVERT(3); LAUNCH_INVERT(4); LAUNCH_INVERT(5);
        LAUNCH_INVERT(6); LAUNCH_INVERT(7); LAUNCH_INVERT(8);
        LAUNCH_INVERT(9); LAUNCH_INVERT(10);LAUNCH_INVERT(11);
        LAUNCH_INVERT(12);
      default: assert( false );
      }
      e_invert_0.end();
      e_invert_1.start();
      cu_CH_invert_1<<<grid_dim_invert_1,block_dim_invert_1>>>
        (graph_cu_CH_new_h);
      e_invert_1.end();

      e_invert_0.sync();
      e_invert_1.sync();
    }

    CE( cudaEventSynchronize(stop_0) );

    kernel_time = 0;
    CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
    total_time += (double) kernel_time * (double) 0.001;

    stats->time_OL_extraction_1[iter] = kernel_time;
    stats->time_total_OL_extraction_1 += (double)kernel_time;

    const double e_invert_0_ms = e_invert_0.elapsed_ms();
    const double e_invert_1_ms = e_invert_1.elapsed_ms();
    total_time += ( e_invert_0_ms + e_invert_1_ms ) * 1e-3;
    stats->time_invert_0.push_back(e_invert_0_ms);
    stats->time_invert_1.push_back(e_invert_1_ms);

    stats_func_handle
      ([=]() {
        printf("time for graph overlay extraction_1 kernel = %f\n", kernel_time * 0.001);
        printf("Extract, iter %3d: (su,00,01,1,i0,i1)  "
               "%6.f, %6.f %6.f %6.f %5.f,%5.0f  %2d %2d\n",
               iter, 1e6*dur_ex_setup_s, 1e6*dur_extract_00, 1e6*dur_extract_01,
               1e3*kernel_time,
               1e3 * e_invert_0_ms, 1e3 * e_invert_1_ms,
               grid_dim_ex_00, grid_dim_overlay_1);
      } );

    CE( cudaDeviceSynchronize() );
    CE( cudaGetLastError() );


    // free the old working graph and replace it with the new trimmed working graph
    graph_cu_CH_bi_free_device(graph_cu_CH_d);
    graph_cu_CH_d = nullptr;
    *graph = graph_cu_CH_new_d;

    if ( chopts.verify_in_loops )
      {
        contract_dist_verify.verify_self_consistency(graph_cu_CH_new_d,true);
        contract_dist_verify.verify(&cq_state,graph_cu_CH_new_d,graph_UD_d);
      }

    const int prev_n_nodes = num_nodes;

    stats_func_handle
      ([=]() {
    printf("total time so far = %f\n", total_time); } );
    stats->time_iter[iter] = stats->time_scoring[iter] + stats->time_MIS[iter] + stats->time_contraction[iter] + stats->time_update[iter] + stats->time_OL_extraction_0[iter] + stats->time_OL_extraction_1[iter]
      + stats->time_invert_0[iter] + stats->time_invert_1[iter];

    // update the graph values in host
    graph_cache_lookup_struct_to_host(graph_cu_CH_new_d);

    const nodeid_t nn_edges = graph_cu_CH_new_h.graph_b.num_edges_exact;

    const double mean_wht =
      double(graph_cu_CH_new_h.graph_b.weight_sum)/nn_edges;
    graph_cu_CH_new_h.mean_weight = mean_wht;
    graph_cu_CH_new_h.mean_weight_inv = 1.0 / ( mean_wht ?: 1.0 );
    cuda_host_to_dev
      ( &graph_cu_CH_new_d->mean_weight,graph_cu_CH_new_h.mean_weight );
    cuda_host_to_dev
      ( &graph_cu_CH_new_d->mean_weight_inv,graph_cu_CH_new_h.mean_weight_inv );

    num_nodes = graph_cu_CH_new_h.num_nodes;
    tpn = graph_cu_CH_new_h.thread_per_node;
    max_degree = graph_cu_CH_new_h.max_degree;
    overlay_size = graph_cu_CH_new_h.overlay_size;

    stats->max_degree_OL = max_degree;

    if ( opt_monitor_deg )
      {
        cuda_host_to_dev( &graph_cu_CH_new_d->graph_f.look_max_degree, 0 );
        cuda_host_to_dev( &graph_cu_CH_new_d->graph_b.look_max_degree, 0 );
        max_degree_update<<<nmps,1024>>>(graph_cu_CH_new_d);
        cu_graph_CH_bi_t gex = cuda_dev_to_host(graph_cu_CH_new_d);
        const int md_bwd = gex.graph_b.look_max_degree;
        const int md_fwd = gex.graph_f.look_max_degree;
        const uint32_t md = max(md_bwd,md_fwd);
        const int tpn_prev = tpn;
        const int tpn_opt = max(3,log2p1(md));
        stats_func_handle
          ([=]() {
             printf("After extract iter %d, max degs: %d, %d (%d). "
                    "tpn %d -> %d  %d\n",
                    iter, md_bwd, md_fwd, max_degree,
                    tpn_prev, tpn, tpn_opt);} );
      }

    stats_func_handle
      ([=]() {
         printf("at end of iter %3u: tpn %2u, max_deg %3u, num_nodes %9u, "
                "n_edges %9u\n",
           iter, tpn, max_degree, num_nodes, nn_edges); });

    stats_func_handle
      ([=]() {
        const double c = C_sco;
        const int64_t n_contracted = prev_n_nodes - num_nodes;
        const int64_t d_edges = nn_edges - n_edges;
        const double lambda = double(n_contracted) / prev_n_nodes;
        const double avg_deg_prev = double(n_edges) / prev_n_nodes;
        const double ediff_alt =
          ( nn_edges - num_nodes * avg_deg_prev ) / n_contracted;
        printf("Contr r λ = %4.3f %4.3f %4.3f "
               "avg deg=%5.2f  ediff= %5.2f %5.2f  %7.3f%%\n",
               lambda, lambda * c * avg_deg_prev,
               lambda * pow(c*avg_deg_prev,0.5),
               avg_deg_prev,
               d_edges/double(max((typeof n_contracted)(1),n_contracted)),
               ediff_alt,
               100 * double(prev_n_nodes)/num_nodes_start
               );

      } );

    n_edges = nn_edges;

    const double t_main_iter_end_ues = time_wall_fp();
    t_main_iter_next_start_ues = t_main_iter_end_ues;
    const double dur_wall_iter_s = t_main_iter_end_ues - t_main_iter_start_ues;
    tdur_wall_iter_s += dur_wall_iter_s;
    const double dur_wall_start_to_select_start_s =
      t_select_start_ues - t_main_iter_start_ues;
    tdur_wall_start_to_select_start_s += dur_wall_start_to_select_start_s;
    const double dur_wall_select_start_end_s =
      t_select_end_ues - t_select_start_ues;
    tdur_wall_select_start_end_s += dur_wall_select_start_end_s;
    const double dur_wall_select_end_update_end_s =
      t_update_end_ues - t_select_end_ues;
    tdur_wall_select_end_update_end_s += dur_wall_select_end_update_end_s;
    const double dur_wall_update_end_iter_end_s =
      t_main_iter_end_ues - t_update_end_ues;
    tdur_wall_update_end_iter_end_s += dur_wall_update_end_iter_end_s;

    stats->C[iter] = C_sco;
    stats->max_degree[iter] = max_degree;
    stats->wall_time_iter_ms.push_back(dur_wall_iter_s*1e3);

    stats_func_handle
      ([=]() {
    printf("Iter %2d wall %6.f = (sc,se,cu,e) %6.f  %6.f %6.f %6.f\n",
           iter, 1e6 * dur_wall_iter_s,
           1e6 * dur_wall_start_to_select_start_s,
           1e6 * dur_wall_select_start_end_s,
           1e6 * dur_wall_select_end_update_end_s,
           1e6 * dur_wall_update_end_iter_end_s); } );
    iter++;

    uint32_t NEG = d_DB_est_h[0] > 512 ? 1 : (d_DB_est_h[0] * num_selected) / 1024; 
    switch(chopts.K_dynamic){
    case 0:
      continue_contraction = num_nodes > K_cco;
      break;
    case 1:
      // update current value of K (needed for APSP allocation etc. post main loop)
      K_cco = ((num_nodes+APSP_BLSZ-1)/APSP_BLSZ)*APSP_BLSZ;
      // K impacts query_OL time O(V^2), but we assume that to be equivalant to phast O(E).
      // Fair assumption when d is comparable to V. Only remaining optimization factor is occupancy
      // Ideally EG per level should be higher than query occupancy
      // EG per level is not known at this time. Assume a 1.0 EG to node ratio and use num_selected
      // in very large graphs this ratio is typically larger than 1.0 and vice versa
      continue_contraction = NEG > query_occupancy || K_cco > K_MAX;
      break;
    case 2:
      // optimized for contraction
      // needs benchmarking APSP with different size during make
      // alternatively assuming fixed APSP time for different hardware
      /// RRRR logic to be added, currently same as constant_K
      K_cco = ((num_nodes+APSP_BLSZ-1)/APSP_BLSZ)*APSP_BLSZ;
      if( iter > 3 && K_cco <= K_MAX){
	if(K_cco == 64){
	  // stop it already! (realistically this would never happen)
	  continue_contraction = false;
	} else {
	  uint32_t K_cco_next = K_cco - APSP_BLSZ;
	  float remaining_time_K = APSP_bench_times[K_cco/APSP_BLSZ - 1] -
	    APSP_bench_times[K_cco_next/APSP_BLSZ - 2];
	  float time_iter_est = (stats->wall_time_iter_ms[iter-1] +
				 stats->wall_time_iter_ms[iter-2] + 
				 stats->wall_time_iter_ms[iter-3]) / 3.0e3;
	  float num_selected_est = (stats->num_nodes_in_lvl[iter-1] +
				    stats->num_nodes_in_lvl[iter-2] + 
				    stats->num_nodes_in_lvl[iter-3]) / 3.0;
	  float remaining_time_cuch = ((float)APSP_BLSZ / num_selected_est) * time_iter_est;
	  continue_contraction = remaining_time_K > remaining_time_cuch;
	}
      } else {
	continue_contraction = true;
      }
      break;
    default:
      pr.fatal_user("invalid --K-dynamic option\n");
      break;
    }

    // update C (if dynamic)
    switch(chopts.C_dynamic){
    case 0:
      // nothing to do
      break;
    case 1:{
      // Increase C by C_MIN if needed
      // Avoid query thread underutilization, ensure level count doesn't get too high
      float lambda = (float) num_selected / (float) (num_nodes + num_selected);
      if((lambda < LAMBDA_MIN || NEG < query_occupancy) && (C_sco+C_INC) < C_MAX){
	C_sco += C_INC;
      }
      break;
    }
    case 2:{
      // Increase C by C_MIN if needed
      // Avoid preprocessing thread underutilization, ensure level count doesn't get too high
      // RRRR does not work correctly with dynamic block (dynamic block doesn't work correctly itself)
      uint32_t work_count_effective = (d_DB_est_h[0] > 256) ?
	(d_DB_est_h[0] >> 8) * (d_DB_est_h[0] >> 8) * num_selected : num_selected;
      float lambda = (float) num_selected / (float) (num_nodes + num_selected);
      if ( ( lambda < LAMBDA_MIN || work_count_effective < uint(c_co.grid_dim) )
           && C_sco + C_INC < C_MAX ) {
	C_sco += C_INC;
      }
      break;
    }
    default:
      pr.fatal_user("invalid --C-dynamic option\n");
      break;
    }

    graph_cu_CH_d = graph_cu_CH_new_d;

  }
  ///
  /// End of main loop.
  ///

  const double t_main_loop_end_ues = time_wall_fp();
  const double dur_main_loop_kernels_s = total_time - dur_pre_main_kernels_s;

  TRACE_MEM_USAGE();

  const bool free_early = true;
  if ( free_early )
    {
      if ( na_storage_d ) { CE( cudaFree(na_storage_d) ); na_storage_d = NULL; }
      shc_set.free();
      TRACE_MEM_USAGE();
    }

  event_pair e_ud_invert;
  e_ud_invert.start();

  // Prepare up-backward storage and other members needed for post-processing.
  //
  cu_graph_CH_UD_t gr_ud = cuda_dev_to_host(graph_UD_d);
  graph_cu_CH_UD_alloc_more(gr_ud, K_cco);
  TRACE_MEM_USAGE();
  cuda_host_to_dev(graph_UD_d,gr_ud);
  CE( cudaMemsetAsync
      ( gr_ud.graph_u_b.pointer, 0, // Expected to be zero.
        gr_ud.num_nodes * sizeof(gr_ud.graph_u_b.pointer[0] ) ) );

  // Construct the up-backward graph from the up-forward graph.
  //
  cu_CH_ud_invert_launch
    (gpu_info, ev_array, stats, graph_cu_CH_d, gr_ud, graph_UD_d);

  e_ud_invert.end();

  Time_Wall_s t_ud_verify_ues;
  if ( chopts.verify_contr_raw )
    contract_dist_verify.verify(&cq_state,graph_cu_CH_d,graph_UD_d);
  stats->wall_time_verify_post_s += t_ud_verify_ues;

  if ( opt_monitor_deg || verbosity > Stats_None )
  {
    for ( auto g: { &graph_UD_d->graph_d_b, &graph_UD_d->graph_u_f,
                    &graph_UD_d->graph_u_b } )
      cuda_host_to_dev( &g->look_max_degree, 0 );
    max_degree_update<<<nmps,1024>>>(graph_UD_d);

    cu_graph_CH_UD_t g = cuda_dev_to_host(graph_UD_d);
    stats_func_handle
      ([=]() {
         printf("UD Degrees: main %u,  checked (db,uf,ub) %d, %d, %d\n",
                g.max_degree, g.graph_d_b.look_max_degree,
                g.graph_u_f.look_max_degree, g.graph_u_b.look_max_degree); } );
  }

  stats->num_iterations = iter;  
  stats->num_nodes_overlay_final = K_cco;
  stats->time_total = stats->time_total_scoring + stats->time_total_MIS + stats->time_total_contraction + stats->time_total_update + stats->time_total_OL_extraction_0 + stats->time_total_OL_extraction_1
    + stats->time_total_invert_0 + stats->time_total_invert_1;
  
  // prepare and run the APSP on the overlay graph
  block_dim = block_dim_APSP_prep;
  int grid_dim = 3;

  // Required amount of shared per block for APSP_prep kernel.
  shared_dim = (6*block_dim) * sizeof(nodeid_t);

  CE( cudaEventRecord(start_0, 0) );

  switch(tpn){
    LAUNCH_APSP_PREP(3); LAUNCH_APSP_PREP(4);
    LAUNCH_APSP_PREP(5); LAUNCH_APSP_PREP(6);
    LAUNCH_APSP_PREP(7); LAUNCH_APSP_PREP(8);
    LAUNCH_APSP_PREP(9); LAUNCH_APSP_PREP(10);
    LAUNCH_APSP_PREP(11); LAUNCH_APSP_PREP(12);
  default:
    {
      assert( false && tpn <= 12 );
      break;
    }
  }
  
  CE( cudaEventRecord(stop_0, 0) );
  CE( cudaEventSynchronize(stop_0) );

  stats->time_ud_invert = e_ud_invert.elapsed_ms();
  stats->time_total += e_ud_invert.elapsed_ms();


  kernel_time = 0;
  CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
  const double dur_APSP_prep_ms = kernel_time;
  stats->time_APSP_prep_s = kernel_time * .001;
  total_time += stats->time_APSP_prep_s;

  stats_func_handle
    ([=]() {
       printf("time for APSP_prep kernel = %f s\n", kernel_time * 0.001); } );
  stats->time_total += (double)kernel_time;

  CE( cudaDeviceSynchronize() );
  CE( cudaGetLastError() );

  stats_func_handle
    ([=]() {
       printf("APSP prep kernel finished\n"); } );

  // do a sweep over all edges in UD graph and renumber based on the UD id space
  block_dim = 1024;
  grid_dim = 2 * nmps;

  CE( cudaEventRecord(start_0, 0) );

  cu_CH_UD_renum<<<grid_dim,block_dim>>>(graph_UD_d);

  CE( cudaEventRecord(stop_0, 0) );
  CE( cudaEventSynchronize(stop_0) );
  kernel_time = 0;
  CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
  const double dur_UD_renum_ms = kernel_time;
  stats->time_ud_renum_s = dur_UD_renum_ms * .001;
  total_time += stats->time_ud_renum_s;

  stats_func_handle
    ([=]() {
       printf("time for UD_renum kernel = %f\n", kernel_time * 0.001); } );
  stats->time_total += (double)kernel_time;

  CE( cudaDeviceSynchronize() );
  CE( cudaGetLastError() );

  stats_func_handle
    ([=]() {
       printf("UD_renum kernel finished\n"); } );

  // run APSP
  cu_graph_CH_UD_t *graph_UD_h_apsp = (cu_graph_CH_UD_t *) malloc(sizeof(cu_graph_CH_UD_t));
  CE( cudaMemcpy(graph_UD_h_apsp, graph_UD_d, sizeof(cu_graph_CH_UD_t), cudaMemcpyDeviceToHost) );
  // make sure path is allocated correctly (multiple of APSP_BLSZ)
  assert(graph_UD_h_apsp->overlay_APSP_path.num_nodes % APSP_BLSZ == 0);
  
  // calculate the blocked APSP
  uint32_t block_num = graph_UD_h_apsp->overlay_APSP_path.num_nodes / APSP_BLSZ;
  block_dim = 1024;
  dim3 grid_dim_3(block_num, block_num);

  CE( cudaDeviceSynchronize() );
  Time_Wall_s time_wall_APSP_start;
  double dur_APSP_phases_ms[3] = {0,0,0};

  ////
  /// Launch APSP Blocked Kernels
  ///

  for ( uint32_t iter=0; iter<block_num; iter++)
    {
      // phase 0: update block iter,iter
      // phase 1: update the remainder of column iter and row iter
      // phase 2: update everything else

      for ( int p=0; p<3; p++ )
        {
          ev_array[p].record();
          ( chopts.verify ? cu_APSP_blocked<true> : cu_APSP_blocked<false> )
           <<<grid_dim_3,block_dim>>>
            ( graph_UD_h_apsp->overlay_APSP_path.dist,
              graph_UD_h_apsp->overlay_APSP_path.prev,
              graph_UD_h_apsp->overlay_APSP_path.num_nodes, iter, p);
          cuda_error_check();
        }

      ev_array[3].record().sync();

      for ( int p=0; p<3; p++ )
        {
          const double dur_ms = elapsed_time_get_ms(ev_array[p],ev_array[p+1]);
          dur_APSP_phases_ms[p] += dur_ms;
          stats_func_handle([=]() { pr.loop
                ("time for phase %i of iter %u of the APSP kernel = %f s\n",
                 p, iter, dur_ms * .001); } );
        }
    }

  double* APSP_p = &stats->time_APSP_0_s;
  stats->time_APSP_s = 0;
  for ( auto dur_ms: dur_APSP_phases_ms )
    {
      const double dur_s = dur_ms * .001;
      stats->time_APSP_s += dur_s;
      *APSP_p++ = dur_s;
      total_time += dur_s;
      stats->time_total += dur_ms;
    }

  const double dur_wall_APSP_s = time_wall_APSP_start;

  stats_func_handle
    ([=]() {
       printf("Total APSP time %.6f s  wall %.6f s, block num %d.\n",
              stats->time_APSP_s, dur_wall_APSP_s, block_num);
       } );

  e_num_edges.start();
  cu_graph_CH_UD_c_update(graph_UD_d);
  cu_CH_UD_edge_count<<<nmps,1024>>>(graph_UD_d);
  cu_graph_CH_UD_t graph_UD_ec_h;
  CE( cudaMemcpy(&graph_UD_ec_h,graph_UD_d,sizeof(graph_UD_ec_h),
                 cudaMemcpyDeviceToHost) );
  e_num_edges.end();

  stats->num_edges_u_b_DAG = graph_UD_ec_h.graph_u_b.num_edges_exact;
  stats->num_edges_d_b_DAG = graph_UD_ec_h.graph_d_b.num_edges_exact;

  if ( trace_mem_showing() )
    {
      auto udg = [](cu_graph_CH_t& g, const char *lab)
                 { pr.tune
                     ("Usage %s: =%9u ~%9u / %9u %5zu MiB, %9u free.\n",
                      lab, g.num_edges_exact, g.empty_pointer, g.max_num_edges,
                      size_t(g.max_num_edges) * 3 * sizeof(g.weights[0]) >> 20,
                      g.max_num_edges - g.empty_pointer); };
      udg(graph_UD_ec_h.graph_u_f,"u_f");
      udg(graph_UD_ec_h.graph_u_b,"u_b");
      udg(graph_UD_ec_h.graph_d_b,"d_b");
    }

  e_num_edges.sync();
  total_time += e_num_edges.elapsed_s();
  stats->time_total += e_num_edges.elapsed_ms();

  stats->mean_degree_u_f_DAG = 0;
  stats->mean_degree_u_b_DAG = 0;
  stats->mean_degree_d_b_DAG = 0;
  stats->max_degree_u_f_DAG = 0;
  stats->max_degree_u_b_DAG = 0;
  stats->max_degree_d_b_DAG = 0;

#ifdef CONTRACTION_EVAL
  // collect graph_UD stats
  graph_cu_CH_UD_d2h(&graph_UD_h, graph_UD_d);
  uint32_t zeroes = 0;
  double nnz_u = 0;
  double nnz_d = 0;
  uint32_t num_edges_u_f = 0;
  uint32_t num_edges_u_b = 0;
  uint32_t num_edges_d_b = 0;
  for(uint32_t i=0; i<stats->num_nodes; i++){
    num_edges_u_f += graph_UD_h->graph_u_f.num_neighbors[i];
    num_edges_u_b += graph_UD_h->graph_u_b.num_neighbors[i];
    num_edges_d_b += graph_UD_h->graph_d_b.num_neighbors[i];
    if(graph_UD_h->graph_u_b.num_neighbors[i] == 0){
      zeroes++;
    }
    if(i >= graph_UD_h->node_levels_pt[1]){
      nnz_u = nnz_u + 1;
      stats->mean_degree_u_f_DAG += graph_UD_h->graph_u_f.num_neighbors[i];
      stats->mean_degree_u_b_DAG += graph_UD_h->graph_u_b.num_neighbors[i];
      if(stats->max_degree_u_f_DAG < graph_UD_h->graph_u_f.num_neighbors[i]){
	stats->max_degree_u_f_DAG = graph_UD_h->graph_u_f.num_neighbors[i];
      }
      if(stats->max_degree_u_b_DAG < graph_UD_h->graph_u_b.num_neighbors[i]){
	stats->max_degree_u_b_DAG = graph_UD_h->graph_u_b.num_neighbors[i];
      }

    }
    
    if(i < graph_UD_h->node_levels_pt[graph_UD_h->node_levels[stats->num_nodes - 1]]){
      nnz_d = nnz_d + 1;
      stats->mean_degree_d_b_DAG += graph_UD_h->graph_d_b.num_neighbors[i];
      if(stats->max_degree_d_b_DAG < graph_UD_h->graph_d_b.num_neighbors[i]){
	stats->max_degree_d_b_DAG = graph_UD_h->graph_d_b.num_neighbors[i];
      }
    }
  }
  printf("zeroes = %d\n", zeroes);
  stats->mean_degree_u_f_DAG = stats->mean_degree_u_f_DAG / nnz_u;
  stats->mean_degree_u_b_DAG = stats->mean_degree_u_b_DAG / nnz_u;
  stats->mean_degree_d_b_DAG = stats->mean_degree_d_b_DAG / nnz_d;
  
  assert( graph_UD_ec_h.graph_u_f.num_edges_exact == num_edges_u_f );
  assert( graph_UD_ec_h.graph_u_b.num_edges_exact == num_edges_u_b );
  assert( graph_UD_ec_h.graph_d_b.num_edges_exact == num_edges_d_b );
#endif

  // compress graph_UD
  
  nodeid_t num_edges =
    max( max( graph_UD_ec_h.graph_u_f.num_edges_exact,
              graph_UD_ec_h.graph_u_b.num_edges_exact ),
              graph_UD_ec_h.graph_d_b.num_edges_exact );
  nodeid_t max_rank = stats->num_iterations + 1;
  uint32_t edge_list_length =  num_edges * 1.1 + 1024*max_rank;// extra space for padding
  // no benefit in compressing OL graph, so will be copying directly
  uint32_t overlay_edge_list_length =
    graph_UD_ec_h.overlay_CH.empty_pointer + 64;

  // mus initialize with more than actual num_nodes to avoid a seg_fault (1 extra space is enough)
  graph_cu_CH_UD_init_d(&graph_UD_d_compressed, num_nodes_init + 10, edge_list_length, max_rank, K_cco, overlay_edge_list_length, UDP_Compress);

  block_dim = 512;
  grid_dim = 3;
  
  CE( cudaEventRecord(start_0, 0) );

  cu_CH_UD_compress_pre_launch(graph_UD_d_compressed, graph_UD_d);
  cu_CH_UD_compress_0<<<grid_dim,block_dim>>>(graph_UD_d_compressed, graph_UD_d);
  
  CE( cudaEventRecord(stop_0, 0) );
  CE( cudaEventSynchronize(stop_0) );
  kernel_time = 0;
  CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
  const double dur_compess_0_ms = kernel_time;
  stats->time_compress_0_s = dur_compess_0_ms * .001;
  total_time += stats->time_compress_0_s;
  stats_func_handle
    ([=]() {
  printf("time for compress_0 kernel = %f s\n", stats->time_compress_0_s ); } );
  stats->time_total += (double)kernel_time;
 
  block_dim = 1024;
  grid_dim = 2 * nmps;
  
  CE( cudaEventRecord(start_0, 0) );

  cu_CH_UD_compress_pre_launch(graph_UD_d_compressed, graph_UD_d);
  cu_CH_UD_compress_1<<<grid_dim,block_dim>>>(graph_UD_d_compressed, graph_UD_d);
  
  CE( cudaEventRecord(stop_0, 0) );
  CE( cudaEventSynchronize(stop_0) );
  kernel_time = 0;
  CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
  const double dur_compess_1_ms = kernel_time;
  stats->time_compress_1_s = dur_compess_1_ms * .001;
  total_time += stats->time_compress_1_s;
  stats_func_handle
    ([=]() {
  printf("time for compress_1 kernel = %f s\n", stats->time_compress_1_s ); } );
  stats->time_total += (double)kernel_time;



#ifdef CONTRACTION_EVAL
  // test UD compression for correctness
  cu_graph_CH_UD_t *graph_UD_h_old;
  graph_cu_CH_UD_d2h(&graph_UD_h, graph_UD_d_compressed);
  graph_cu_CH_UD_d2h(&graph_UD_h_old, graph_UD_d);
  assert(graph_UD_h->graph_u_f.num_nodes == graph_UD_h_old->graph_u_f.num_nodes);
  assert(graph_UD_h->graph_u_b.num_nodes == graph_UD_h_old->graph_u_b.num_nodes);
  assert(graph_UD_h->graph_d_b.num_nodes == graph_UD_h_old->graph_d_b.num_nodes);
  for(uint32_t i=0; i<graph_UD_h->graph_u_f.num_nodes; i++){
    assert(graph_UD_h->node_ranks[i] == graph_UD_h_old->node_ranks[i]);
    assert(graph_UD_h->node_levels[i] == graph_UD_h_old->node_levels[i]);
    assert(graph_UD_h->node_idx[i] == graph_UD_h_old->node_idx[i]);
    assert(graph_UD_h->node_idx_inv[i] == graph_UD_h_old->node_idx_inv[i]);

    assert(graph_UD_h->graph_u_f.num_neighbors[i] == graph_UD_h_old->graph_u_f.num_neighbors[i]);
    assert(graph_UD_h->graph_u_b.num_neighbors[i] == graph_UD_h_old->graph_u_b.num_neighbors[i]);
    assert(graph_UD_h->graph_d_b.num_neighbors[i] == graph_UD_h_old->graph_d_b.num_neighbors[i]);
  }


  auto el_check = [&](cu_graph_CH_t& graph)
    {
      vector<nodeid_t> edge_list_user(edge_list_length);
      for ( nodeid_t i=0; i<graph.num_nodes; i++)
        for ( nodeid_t j=0; j<graph.num_neighbors[i]; j++)
          {
            uint32_t idx = graph.pointer[i] + j;
            assert( idx < edge_list_length );
            assert( ! edge_list_user[idx] );
            edge_list_user[idx] = i;
          }
    };

  el_check(graph_UD_h->graph_d_b);
  el_check(graph_UD_h->graph_u_b);
  el_check(graph_UD_h->graph_u_f);

  for(uint32_t i=0; i<graph_UD_h->graph_u_f.num_nodes; i++){

    for(uint32_t j=0; j<graph_UD_h->graph_u_f.num_neighbors[i]; j++){
      if(graph_UD_h->graph_u_f.neighbors[graph_UD_h->graph_u_f.pointer[i] + j] != graph_UD_h_old->graph_u_f.neighbors[graph_UD_h_old->graph_u_f.pointer[i] + j]){
	printf("failed at i=%u, j=%u\n", i, j);
      }
      assert(graph_UD_h->graph_u_f.neighbors[graph_UD_h->graph_u_f.pointer[i] + j] == graph_UD_h_old->graph_u_f.neighbors[graph_UD_h_old->graph_u_f.pointer[i] + j]);
    }

    for(uint32_t j=0; j<graph_UD_h->graph_u_b.num_neighbors[i]; j++){
      assert(graph_UD_h->graph_u_b.neighbors[graph_UD_h->graph_u_b.pointer[i] + j] == graph_UD_h_old->graph_u_b.neighbors[graph_UD_h_old->graph_u_b.pointer[i] + j]);
      assert(graph_UD_h->graph_u_b.weights[graph_UD_h->graph_u_b.pointer[i] + j] == graph_UD_h_old->graph_u_b.weights[graph_UD_h_old->graph_u_b.pointer[i] + j]);
      assert(graph_UD_h->graph_u_b.midpoint[graph_UD_h->graph_u_b.pointer[i] + j] == graph_UD_h_old->graph_u_b.midpoint[graph_UD_h_old->graph_u_b.pointer[i] + j]);
    }
	
    for(uint32_t j=0; j<graph_UD_h->graph_d_b.num_neighbors[i]; j++){
      assert(graph_UD_h->graph_d_b.neighbors[graph_UD_h->graph_d_b.pointer[i] + j] == graph_UD_h_old->graph_d_b.neighbors[graph_UD_h_old->graph_d_b.pointer[i] + j]);
      assert(graph_UD_h->graph_d_b.weights[graph_UD_h->graph_d_b.pointer[i] + j] == graph_UD_h_old->graph_d_b.weights[graph_UD_h_old->graph_d_b.pointer[i] + j]);
      assert(graph_UD_h->graph_d_b.midpoint[graph_UD_h->graph_d_b.pointer[i] + j] == graph_UD_h_old->graph_d_b.midpoint[graph_UD_h_old->graph_d_b.pointer[i] + j]);
    }
  }
  for(uint32_t i=0; i<graph_UD_h->overlay_CH.num_nodes; i++){
    assert(graph_UD_h->overlay_CH.num_neighbors[i] == graph_UD_h_old->overlay_CH.num_neighbors[i]);
    for(uint32_t j=0; j<graph_UD_h->overlay_CH.num_neighbors[i]; j++){
      assert(graph_UD_h->overlay_CH.neighbors[graph_UD_h->overlay_CH.pointer[i] + j] == graph_UD_h_old->overlay_CH.neighbors[graph_UD_h_old->overlay_CH.pointer[i] + j]);
      assert(graph_UD_h->overlay_CH.weights[graph_UD_h->overlay_CH.pointer[i] + j] == graph_UD_h_old->overlay_CH.weights[graph_UD_h_old->overlay_CH.pointer[i] + j]);
      assert(graph_UD_h->overlay_CH.midpoint[graph_UD_h->overlay_CH.pointer[i] + j] == graph_UD_h_old->overlay_CH.midpoint[graph_UD_h_old->overlay_CH.pointer[i] + j]);
    }
  }
  
  // check correctness of NIEG
  {
    // u_f
    uint32_t lvl = 0;
    uint32_t NIEG = 0;
    uint32_t EG = 0;
    uint32_t EG_lvl = 0;
    uint32_t next_level_pt;
    const uint32_t max_level = graph_UD_h->node_levels[num_nodes_init-1];
    if(lvl == max_level){
      next_level_pt = num_nodes_init;
    } else {
      next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
    }
    for(uint32_t i=0; i<num_nodes_init; i++){
      if(i >= next_level_pt){
	if(NIEG != graph_UD_h->graph_u_f.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]) printf("failed at i = %u, lvl = %u, NIEG = %u, EG = %u, EG_lvl = %u\n", i, lvl, NIEG, EG, EG_lvl);
	assert(NIEG == graph_UD_h->graph_u_f.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]);
	lvl++;
	if(lvl == max_level){
	  next_level_pt = num_nodes_init;
	} else {
	  next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
	}
	EG++;
	EG_lvl = 0;
	NIEG = 0;
      }
      if(graph_UD_h->graph_u_f.pointer[i] == (EG+1)*1024){
	// it is possible for multiple nodes without neighbors to land on the edge, NIEG can point
	// to any of them randomly, the following is to avoid false positives from the test code
	if( (NIEG != graph_UD_h->graph_u_f.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]) && (graph_UD_h->graph_u_f.pointer[i] == graph_UD_h->graph_u_f.pointer[i+1]) ){
	  assert(i < next_level_pt);
	} else {
	  assert(NIEG == graph_UD_h->graph_u_f.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]);
	  EG++;
	  EG_lvl++;
	  NIEG = 0;
	}
      }
      NIEG++;
    }
    
    // u_b
    lvl = 0;
    NIEG = 0;
    EG = 0;
    EG_lvl = 0;
    if(lvl == max_level){
      next_level_pt = num_nodes_init;
    } else {
      next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
    }
    for(uint32_t i=0; i<num_nodes_init; i++){
      if(i >= next_level_pt){
	if(NIEG != graph_UD_h->graph_u_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]) printf("failed at i = %u, lvl = %u, NIEG = %u, EG = %u, EG_lvl = %u\n", i, lvl, NIEG, EG, EG_lvl);
	assert(NIEG == graph_UD_h->graph_u_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]);
	lvl++;
	if(lvl == max_level){
	  next_level_pt = num_nodes_init;
	} else {
	  next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
	}
	EG++;
	EG_lvl = 0;
	NIEG = 0;
      }
      if(graph_UD_h->graph_u_b.pointer[i] == (EG+1)*1024){
	// it is possible for multiple nodes without neighbors to land on the edge, NIEG can point
	// to any of them randomly, the following is to avoid false positives from the test code
	if( (NIEG != graph_UD_h->graph_u_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]) && (graph_UD_h->graph_u_b.pointer[i] == graph_UD_h->graph_u_b.pointer[i+1]) ){
	  assert(i < next_level_pt);
	} else {
	  assert(NIEG == graph_UD_h->graph_u_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]);
	  EG++;
	  EG_lvl++;
	  NIEG = 0;
	}
      }
      NIEG++;
    }
    
    // d_b
    lvl = 0;
    NIEG = 0;
    EG = 0;
    EG_lvl = 0;
    if(lvl == max_level){
      next_level_pt = num_nodes_init;
    } else {
      next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
    }
    for(uint32_t i=0; i<num_nodes_init; i++){
      if(i >= next_level_pt){
	if(NIEG != graph_UD_h->graph_d_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]) printf("failed at i = %u, lvl = %u, NIEG = %u, EG = %u, EG_lvl = %u\n", i, lvl, NIEG, EG, EG_lvl);
	assert(NIEG == graph_UD_h->graph_d_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]);
	lvl++;
	if(lvl == max_level){
	  next_level_pt = num_nodes_init;
	} else {
	  next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
	}
	EG++;
	EG_lvl = 0;
	NIEG = 0;
      }
      if(graph_UD_h->graph_d_b.pointer[i] == (EG+1)*1024){
	// it is possible for multiple nodes without neighbors to land on the edge, NIEG can point
	// to any of them randomly, the following is to avoid false positives from the test code
	if( (NIEG != graph_UD_h->graph_d_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]) && (graph_UD_h->graph_d_b.pointer[i] == graph_UD_h->graph_d_b.pointer[i+1]) ){
	  assert(i < next_level_pt);
	} else {
	  assert(NIEG == graph_UD_h->graph_d_b.NNIEG[graph_UD_h->node_levels_pt[lvl] + EG_lvl]);
	  EG++;
	  EG_lvl++;
	  NIEG = 0;
	}
      }
      NIEG++;
    }
  }
  

#endif

#undef LAUNCH_SELECT_NODES_GL
#undef LAUNCH_UPDATE_GRAPH
#undef LAUNCH_EXTRACT_OVERLAY_1
#undef LAUNCH_APSP_PREP

  const double t_verify_start_ues = time_wall_fp();
  if ( chopts.verify_contr_final )
    {
      string verify_msg =
        contract_dist_verify.verify(&cq_state,graph_UD_d_compressed);
      stats_func_handle([=]() { printf("%s",verify_msg.c_str()); } );
    }

  const double t_free_start_ues = time_wall_fp();
  stats->wall_time_verify_post_s += t_free_start_ues - t_verify_start_ues;

  if ( na_storage_d ) { CE( cudaFree( na_storage_d ) ); na_storage_d = NULL; }
  shc_set.free();

  const double t_free_graphs_start_ues = time_wall_fp();

  graph_cu_UD_free_device(graph_UD_d);
  if ( graph_UD_h ) graph_cu_UD_free_host(graph_UD_h);

  *graph_UD = graph_UD_d_compressed;

  const double t_free_misc_start_ues = time_wall_fp();

  for ( auto p: to_be_freed ) if ( p ) CE( cudaFree( p ) );

  const double t_contract_end_ues = time_wall_fp();
  const double dur_pre_s = t_main_loop_start_ues - t_contract_start_ues;
  const double dur_main_loop_s = t_main_loop_end_ues - t_main_loop_start_ues;
  const double dur_post_s = t_contract_end_ues - t_main_loop_end_ues;
  const double dur_wall_s = t_contract_end_ues - t_contract_start_ues;
  stats->wall_time_total_ms = dur_wall_s * 1e3;
  stats->wall_time_pre_main_loop_ms = dur_pre_s * 1e3;
  stats->wall_time_main_loop_ms = dur_main_loop_s * 1e3;
  stats->wall_time_post_main_loop_ms = dur_post_s * 1e3;

  assert( stats->time_invert_0.size() == stats->num_iterations );
  assert( stats->time_invert_1.size() == stats->num_iterations );
  for ( auto t: stats->time_invert_0 ) stats->time_total_invert_0 += t;
  for ( auto t: stats->time_invert_1 ) stats->time_total_invert_1 += t;
  for ( auto& f: stats_deferred_funcs ) f();

  const double edge_growth =
    ( stats->num_edges_u_b_DAG + stats->num_edges_d_b_DAG )
    / double( stats->num_edges_f_original + stats->num_edges_b_original );

  pr.tune("Contraction ");
  pr.user("completed in %.3f ms, edge growth %.3f.\n",
          dur_wall_s * 1e3, edge_growth);

  printf("Num Edges: orig f,b, %u + %u, ud,db  %u + %u, ratio %.3f\n",
         stats->num_edges_f_original,
         stats->num_edges_b_original,
         stats->num_edges_u_b_DAG,
         stats->num_edges_d_b_DAG,
         edge_growth);

  printf("\"Free\" times/s: (ns,gr,mi) %.6f %.6f %.6f\n",
        t_free_graphs_start_ues - t_free_start_ues,
        t_free_misc_start_ues - t_free_graphs_start_ues,
        t_contract_end_ues - t_free_misc_start_ues);

#define ASSIGN(n) \
  double kdur_##n##_us = 0; \
  for ( const auto& e: stats->it ) kdur_##n##_us += 1e6*e.time_select_##n##_s;
  ASSIGN(fl); ASSIGN(sort1); ASSIGN(lo);
  ASSIGN(select); ASSIGN(gather); ASSIGN(sort2);
#undef ASSIGN

  pr.tune("Sel Total (fl,s1,lo, se,g,s2) %4.f %5.f %4.f  %5.f %5.f %5.f \n",
          kdur_fl_us, kdur_sort1_us, kdur_lo_us,
          kdur_select_us, kdur_gather_us, kdur_sort2_us);

  printf("Total (sc,se,co,up,e0,e1) %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n",
         stats->time_total_scoring * 1e-3,
         stats->time_total_MIS * 1e-3,
         stats->time_total_contraction * 1e-3,
         stats->time_total_update * 1e-3,
         stats->time_total_OL_extraction_0 * 1e-3,
         stats->time_total_OL_extraction_1 * 1e-3);
  printf("Total (up,e0,e1,i0,i1,iu) %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n",
         stats->time_total_update * 1e-3,
         stats->time_total_OL_extraction_0 * 1e-3,
         stats->time_total_OL_extraction_1 * 1e-3,
         stats->time_total_invert_0 * 1e-3,
         stats->time_total_invert_1 * 1e-3,
         e_ud_invert.elapsed_s());

  printf("Total (in,pr,re, 0,1,2, 0,1) "
         "%5.0f %5.0f %5.0f   %5.0f %5.0f %5.0f  %5.0f %5.0f\n",
         e_ud_invert.elapsed_us(),
         dur_APSP_prep_ms * 1e3,
         dur_UD_renum_ms * 1e3,
         dur_APSP_phases_ms[0] * 1e3,
         dur_APSP_phases_ms[1] * 1e3,
         dur_APSP_phases_ms[2] * 1e3,
         dur_compess_0_ms * 1e3,
         dur_compess_1_ms * 1e3 );

  const double dur_kernels_post_ms =
    e_ud_invert.elapsed_ms() + dur_APSP_prep_ms + dur_UD_renum_ms +
    dur_APSP_phases_ms[0] + dur_APSP_phases_ms[1] + dur_APSP_phases_ms[2] +
    e_num_edges.elapsed_ms() + dur_compess_0_ms + dur_compess_1_ms;

  printf("Iter All wall %7.f µs = (sc,se,cu,e) %6.f  %6.f %6.f %6.f\n",
         1e6 * tdur_wall_iter_s,
         1e6 * tdur_wall_start_to_select_start_s,
         1e6 * tdur_wall_select_start_end_s,
         1e6 * tdur_wall_select_end_update_end_s,
         1e6 * tdur_wall_update_end_iter_end_s);
  printf("Iter All kern %7.f µs = (sc,se,cu,e) %6.f  %6.f %6.f %6.f\n",
         1e6 * dur_main_loop_kernels_s,
         stats->time_total_scoring * 1e3,
         stats->time_total_MIS * 1e3,
         stats->time_total_contraction * 1e3 +
         stats->time_total_update * 1e3,
         ( stats->time_total_OL_extraction_0 +
           stats->time_total_OL_extraction_1 +
           stats->time_total_invert_0 + stats->time_total_invert_1 ) * 1e3 );

  const double dur_ignore_s =
    stats->wall_time_verify_pre_s + stats->wall_time_shadow_wps_s
    + stats->wall_time_verify_post_s;

  printf("total time = %10.6f  "
         "(to, pr,mn,po)/s: %6.3f  %6.3f %6.3f %6.3f -%6.1f\n",
         total_time,  dur_wall_s,  dur_pre_s, dur_main_loop_s, dur_post_s,
         dur_ignore_s);


  //      total time =   0.000000  (to, pr,mn,po)/s:
  printf("Kernel times =           "
         "(to, pr,mn,po)/s: %6.3f  %6.3f %6.3f %6.3f\n",
         total_time,
         dur_pre_main_kernels_s, dur_main_loop_kernels_s,
         dur_kernels_post_ms * 1e-3 );

  const double dur_wall_no_v_s = dur_wall_s - dur_ignore_s;

  printf("Rate Wall:  %.0f nd/s  %.0f e/s   Kernel:  %.0f nd/s  %.0f e/s\n",
         num_nodes_start / dur_wall_no_v_s, n_edges_start / dur_wall_no_v_s,
         num_nodes_start / total_time, n_edges_start / total_time);

  printf("Compilation :  Co_Ev %s, verbosity %d,  asserts %s, CUDA db %s, "
         "host opt %s\n",
         stats->env_contract_eval ? "ON" : "off",
         stats->env_verbosity,
         stats->env_assertion_checking ? "on" : "OFF",
         stats->env_cuda_debug ? "ON" : "off",
         stats->env_host_optimization ? "on" : "off");

  string wps_txt = !chopts.wps_oracle_use ? "-"
    : chopts.wps_oracle_hop_limit > (1<<29) ? "*"
    : to_string(chopts.wps_oracle_hop_limit);

  string K_txt = chopts.K_dynamic==0 ? "Static"
    : chopts.K_dynamic==1 ? "Dynamic (optimize query)"
    : chopts.K_dynamic==2 ? "Dynamic (optimize preprocessing)"
    : "HOW DID THIS HAPPEN?!";
  
  string C_txt = chopts.C_dynamic==0 ? "Static"
    : chopts.C_dynamic==1 ? "Dynamic (optimize query)"
    : chopts.C_dynamic==2 ? "Dynamic (optimize preprocessing)"
    : "HOW DID THIS HAPPEN?!";
  
  printf("Options: K %s\n", K_txt.c_str());
  printf("K = %u\n", K_cco);
  printf("Options: C %s\n", C_txt.c_str());
  printf("C last iter = %.3f\n", C_sco);

  printf("Options: Luby %s %s,  Hash Loop %d%d  EList %d\n",
         chopts.luby_deterministic ? "det" : "non-det",
         chopts.luby_dynamic_iter ? "dynamic" : "fixed",
         static_wps_hash_loop_after_collision,
         chopts.wps_hash_loop_after_collision,
         opt_elist_use);

  printf("Options: co1h %.2f cull (co,id) (%c>%d,%i)  Oracle WPS %s\n",
         chopts.contract_one_hop,
         chopts.longcuts_cull == 1 ? 's' : chopts.longcuts_cull == 2 ? 'c'
         : chopts.longcuts_cull == 0 ? '-' : '?',
         chopts.cull_threshold_tpn,
         chopts.cull_ideal_hops,
         wps_txt.c_str() );

  if ( chopts.print_tuning )
    cu_CH_ASCII_art(stats,gpu_info);

  if ( stats->env_run_slowed_by_tuning_and_debug_options )
    printf("*** EXECUTION TIME SLOWED BY TUNING AND DEBUG OPTIONS.\n");
  else
    printf("Execution time not slowed by tuning and debug options.\n");
}


void cu_CH_query_main
(nodeid_t src, cu_Contract_Query_State& cq_state)
{
  auto printf = pr_tune;
  P_Timer pt("cu_CH_query_main");

  const int q_num = cq_state.n_queries_started++;
  const bool first_query = !q_num;

  if ( first_query ) cu_status_init(cq_state);

  const Stats_Policy stats_policy = Stats_Policy(verbosity);
  if ( first_query )
    printf("Detailed statistics collection: %s.\n"
           "  (See stats_policy in cu_main.cu)\n",
           stats_policy == Stats_None
           ? "no-stats not supported, stats at end of run" :
           stats_policy == Stats_Immediate ? "immediate" :
           "at end of run");

  const bool print_details =
    chopts.print_tuning_in_loops ||
    chopts.print_tuning && first_query;

  cu_CH_stats_t* const stats = &cq_state.stats;
  cu_graph_CH_UD_t* const graph_UD_d = cq_state.graph_UD_d;
  cu_graph_CH_UD_t* const graph_UD_h = cq_state.graph_UD_h;

  if ( stats ) stats->q.emplace_back();
  cu_CH_query_data elt_pad;
  cu_CH_query_data& elt = stats ? stats->q.back() : elt_pad;
  elt.src = src;

  const nodeid_t src_ud = graph_UD_h->node_idx_inv[src];
  const uint32_t src_level = graph_UD_h->node_levels[src_ud];
  elt.src_lvl = src_level;

  pr.user
    ("SSSP querying node %u (level %u), distance %s.\n",
     src, src_level, chopts.query_distance_only ? "only" : "and predecessor");

  if ( chopts.print_tuning && first_query )
    {
      pTable table;
      table.opt_underline = true;
#define T(f) make_pair(#f,(void(*)())(f))
      for ( auto p: {T(cu_CH_query_u_0), T(cu_CH_query_u_1),
                     T(cu_CH_query_u_1_multi_iter), T(cu_CH_query_OL),
                     T(cu_CH_query_d), T(cu_CH_query_d_multi_iter),
                     T(cu_CH_path_unpack_OL), T(cu_CH_path_unpack_renum) } )
        {
          cudaFuncAttributes cfa;
          CE( cudaFuncGetAttributes(&cfa, p.second) );
          pTable_Row row(table);
          table.entry("Kernel","%27s",p.first);
          table.entry("Shared","%6zd", cfa.sharedSizeBytes);
          table.entry("Const","%6zd", cfa.constSizeBytes);
          table.entry("Local","%6zd", cfa.localSizeBytes);
          table.entry("N Regs","%6d", cfa.numRegs );
          table.entry("Max Thd/Bl","%10d", cfa.maxThreadsPerBlock);
        }
#undef T
      pr.tune("%s",table.body_get());
    }

  cudaDeviceProp cuda_prop;
  int dev;
  CE( cudaGetDevice(&dev) );
  CE( cudaGetDeviceProperties(&cuda_prop,dev) );
  const int nmps = cuda_prop.multiProcessorCount;
  
  const uint32_t num_nodes = graph_UD_h->graph_u_f.num_nodes;

  // check correctness of NIEG
  if ( first_query && chopts.verify_contr_raw )
  {
    // u_f
    uint32_t lvl = 0;
    uint32_t NIEG = 0;
    uint32_t EG = 0;
    uint32_t EG_lvl = 0;
    uint32_t next_level_pt;
    uint32_t nnieg_pt = 0;
    const uint32_t max_level = graph_UD_h->node_levels[num_nodes-1];
    if(lvl == max_level){
      next_level_pt = num_nodes;
    } else {
      next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
    }
    nnieg_pt = lvl < NNIEG_OVER_ALLOC_CUT_OFF ? graph_UD_h->node_levels_pt[lvl] :
      graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF] + NNIEG_OVER_ALLOC_RATIO *
      (graph_UD_h->node_levels_pt[lvl] - graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF]);
    for(uint32_t i=0; i<num_nodes; i++){
      if(i >= next_level_pt){
	assert(graph_UD_h->graph_u_f.pointer[i] == (EG+1)*1024);
	if( graph_UD_h->graph_u_f.num_neighbors[i-1] <= 1024){
	  if(NIEG != graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]) printf("failed at i = %u, lvl = %u, NIEG = %u, EG = %u, EG_lvl = %u\n", i, lvl, NIEG, EG, EG_lvl);
	  assert(NIEG == graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]);
	} else {
	  assert(0x40000001 == graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]);
	}
	EG++;
	lvl++;
	if(lvl == max_level){
	  next_level_pt = num_nodes;
	} else {
	  next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
	}
	nnieg_pt = lvl < NNIEG_OVER_ALLOC_CUT_OFF ? graph_UD_h->node_levels_pt[lvl] :
	  graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF] + NNIEG_OVER_ALLOC_RATIO *
	  (graph_UD_h->node_levels_pt[lvl] - graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF]);
	EG_lvl = 0;
	NIEG = 0;
      }
      if(graph_UD_h->graph_u_f.pointer[i] == (EG+1)*1024){
	// it is possible for multiple nodes without neighbors to land on the edge, NIEG can point
	// to any of them randomly, the following is to avoid false positives from the test code
	if( graph_UD_h->graph_u_f.num_neighbors[i-1] <= 1024){
	  if( (NIEG != graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]) && (graph_UD_h->graph_u_f.pointer[i] == graph_UD_h->graph_u_f.pointer[i+1]) ){
	    assert(i < next_level_pt);
	  } else {
	    assert(NIEG == graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]);
	    EG++;
	    EG_lvl++;
	    NIEG = 0;
	  }
	} else {
	  // if previous node was multi-EG, its last EG is handled here
	  assert(0x40000001 == graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]);
	  EG++;
	  EG_lvl++;
	  NIEG = 0;
	}
      }
      // handle multi-EG nodes
      if( graph_UD_h->graph_u_f.num_neighbors[i] > 1024 ){
	uint32_t nnbr = graph_UD_h->graph_u_f.num_neighbors[i];
	uint32_t egc_inc = (nnbr - 1) >> 10;
	uint32_t NIEG_real = 0x80000001 + ((egc_inc+1)<<16);
	assert(NIEG_real == graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]);
	EG++;
	EG_lvl++;
	NIEG_real = 0x40000001;
	for(uint32_t j=0; j<egc_inc; j++){
	  assert(NIEG_real == graph_UD_h->graph_u_f.NNIEG[nnieg_pt + EG_lvl]);
	  EG++;
	  EG_lvl++;
	}
	NIEG = 0;
      }
      NIEG++;
    }
    
    // u_b
    lvl = 0;
    NIEG = 0;
    EG = 0;
    EG_lvl = 0;
    if(lvl == max_level){
      next_level_pt = num_nodes;
    } else {
      next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
    }
    nnieg_pt = lvl < NNIEG_OVER_ALLOC_CUT_OFF ? graph_UD_h->node_levels_pt[lvl] :
      graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF] + NNIEG_OVER_ALLOC_RATIO *
      (graph_UD_h->node_levels_pt[lvl] - graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF]);
    for(uint32_t i=0; i<num_nodes; i++){
      if(i >= next_level_pt){
	assert(graph_UD_h->graph_u_b.pointer[i] == (EG+1)*1024);
	if( graph_UD_h->graph_u_b.num_neighbors[i-1] <= 1024){
	  if(NIEG != graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]) printf("failed at i = %u, lvl = %u, NIEG = %u, EG = %u, EG_lvl = %u\n", i, lvl, NIEG, EG, EG_lvl);
	  assert(NIEG == graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]);
	} else {
	  assert(0x40000001 == graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]);
	}
	EG++;
	lvl++;
	if(lvl == max_level){
	  next_level_pt = num_nodes;
	} else {
	  next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
	}
	nnieg_pt = lvl < NNIEG_OVER_ALLOC_CUT_OFF ? graph_UD_h->node_levels_pt[lvl] :
	  graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF] + NNIEG_OVER_ALLOC_RATIO *
	  (graph_UD_h->node_levels_pt[lvl] - graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF]);
	EG_lvl = 0;
	NIEG = 0;
      }
      if(graph_UD_h->graph_u_b.pointer[i] == (EG+1)*1024){
	// it is possible for multiple nodes without neighbors to land on the edge, NIEG can point
	// to any of them randomly, the following is to avoid false positives from the test code
	if( graph_UD_h->graph_u_b.num_neighbors[i-1] <= 1024){
	  if( (NIEG != graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]) && (graph_UD_h->graph_u_b.pointer[i] == graph_UD_h->graph_u_b.pointer[i+1]) ){
	    assert(i < next_level_pt);
	  } else {
	    assert(NIEG == graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]);
	    EG++;
	    EG_lvl++;
	    NIEG = 0;
	  }
	} else {
	  // if previous node was multi-EG, its last EG is handled here
	  assert(0x40000001 == graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]);
	  EG++;
	  EG_lvl++;
	  NIEG = 0;
	}
      }
      // handle multi-EG nodes
      if( graph_UD_h->graph_u_b.num_neighbors[i] > 1024 ){
	uint32_t nnbr = graph_UD_h->graph_u_b.num_neighbors[i];
	uint32_t egc_inc = (nnbr - 1) >> 10;
	uint32_t NIEG_real = 0x80000001 + ((egc_inc+1)<<16);
	assert(NIEG_real == graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]);
	EG++;
	EG_lvl++;
	NIEG_real = 0x40000001;
	for(uint32_t j=0; j<egc_inc-1; j++){
	  assert(NIEG_real == graph_UD_h->graph_u_b.NNIEG[nnieg_pt + EG_lvl]);
	  EG++;
	  EG_lvl++;
	}
	NIEG = 0;
      }
      NIEG++;
    }
    
    // d_b
    lvl = 0;
    NIEG = 0;
    EG = 0;
    EG_lvl = 0;
    if(lvl == max_level){
      next_level_pt = num_nodes;
    } else {
      next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
    }
    nnieg_pt = lvl < NNIEG_OVER_ALLOC_CUT_OFF ? graph_UD_h->node_levels_pt[lvl] :
      graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF] + NNIEG_OVER_ALLOC_RATIO *
      (graph_UD_h->node_levels_pt[lvl] - graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF]);
    for(uint32_t i=0; i<num_nodes; i++){
      if(i >= next_level_pt){
	assert(graph_UD_h->graph_d_b.pointer[i] == (EG+1)*1024);
	if( graph_UD_h->graph_d_b.num_neighbors[i-1] <= 1024){
	  if(NIEG != graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]) printf("failed at i = %u, lvl = %u, NIEG = %u, EG = %u, EG_lvl = %u\n", i, lvl, NIEG, EG, EG_lvl);
	  assert(NIEG == graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]);
	} else {
	  assert(0x40000001 == graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]);
	}
	EG++;
	lvl++;
	if(lvl == max_level){
	  next_level_pt = num_nodes;
	} else {
	  next_level_pt = graph_UD_h->node_levels_pt[lvl+1];
	}
	nnieg_pt = lvl < NNIEG_OVER_ALLOC_CUT_OFF ? graph_UD_h->node_levels_pt[lvl] :
	  graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF] + NNIEG_OVER_ALLOC_RATIO *
	  (graph_UD_h->node_levels_pt[lvl] - graph_UD_h->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF]);
	EG_lvl = 0;
	NIEG = 0;
      }
      if(graph_UD_h->graph_d_b.pointer[i] == (EG+1)*1024){
	// it is possible for multiple nodes without neighbors to land on the edge, NIEG can point
	// to any of them randomly, the following is to avoid false positives from the test code
	if( graph_UD_h->graph_d_b.num_neighbors[i-1] <= 1024){
	  if( (NIEG != graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]) && (graph_UD_h->graph_d_b.pointer[i] == graph_UD_h->graph_d_b.pointer[i+1]) ){
	    assert(i < next_level_pt);
	  } else {
	    assert(NIEG == graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]);
	    EG++;
	    EG_lvl++;
	    NIEG = 0;
	  }
	} else {
	  // if previous node was multi-EG, its last EG is handled here
	  assert(0x40000001 == graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]);
	  EG++;
	  EG_lvl++;
	  NIEG = 0;
	}
      }
      // handle multi-EG nodes
      if( graph_UD_h->graph_d_b.num_neighbors[i] > 1024 ){
	uint32_t nnbr = graph_UD_h->graph_d_b.num_neighbors[i];
	uint32_t egc_inc = (nnbr - 1) >> 10;
	uint32_t NIEG_real = 0x80000001 + ((egc_inc+1)<<16);
	assert(NIEG_real == graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]);
	EG++;
	EG_lvl++;
	NIEG_real = 0x40000001;
	for(uint32_t j=0; j<egc_inc; j++){
	  assert(NIEG_real == graph_UD_h->graph_d_b.NNIEG[nnieg_pt + EG_lvl]);
	  EG++;
	  EG_lvl++;
	}
	NIEG = 0;
      }
      NIEG++;
    }
  }

  vector<function<void()>> stats_deferred_funcs;
  auto stats_func_handle = [&]( function<void()>&& f )
    {
      switch ( stats_policy ) {
      case Stats_Immediate: f(); break;
      case Stats_None: break;
      case Stats_Later:
        if ( print_details ) stats_deferred_funcs.push_back(f);
        break;
      }
    };

  auto stats_func_handle_loop = [&]( function<void()>&& f )
  { if ( chopts.print_tuning_in_loops )
      stats_func_handle(forward<function<void()>>(f)); };

  pt.dur_s("Host graph check");
  
  // Prepare events used for timing.
  //
  double total_time_s = 0;
  float kernel_time;
  cudaEvent_t start_0, stop_0;
  CE( cudaEventCreate(&start_0) );
  CE( cudaEventCreate(&stop_0) );

  int block_dim;
  int grid_dim;
  
  block_dim = 256;
  grid_dim = 1;

  // initiate result_0 and result_1 (result_0 is initial and result_1 is renumbered)
  path_t*& result_0 = cq_state.result_0_d;
  path_t*& result_1 = cq_state.result_1_d;
  path_init_d
    ( cq_state.result_0_dh, result_0, num_nodes, src,
      chopts.query_distance_only ? PI_Dist_Array : PI_Dist_Array_Prev_Array );
  path_init_d
    ( cq_state.result_1_dh, result_1, num_nodes, src,
      chopts.query_distance_only ? PI_Dist_Array : PI_Dist_Prev_Array );

  const int num_levels = graph_UD_h->node_levels[num_nodes-1] + 1;

  // Start upward pass using the sparse up kernel, cu_CH_query_u_0.
  //
  const int u0_n_lvls = chopts.query_n_sparse_levels;
  const int u0_lvl_stop_max = 31;
  const int u0_lvl_start = src_level + 1;
  const int u0_lvl_stop =
    min( { u0_lvl_stop_max, u0_lvl_start + u0_n_lvls, num_levels - 1 } );
  const int u1_lvl_start = max( u0_lvl_start, u0_lvl_stop );
  const bool use_u0 = u0_lvl_start < u0_lvl_stop;

  uint32_t*& node_tags = cq_state.node_tags;
  if ( use_u0 ){

    // allocate temporary empty space for node_tags (+ a 513 frame to
    // avoid branch on load)

    const size_t nd_per_nt = 8 * sizeof(node_tags[0]);

    const size_t nt_sz_chars =
      ( div_up(num_nodes,nd_per_nt) + u0_lvl_stop_max * query_u_0_block_dim )
      * sizeof(node_tags[0]);

    if ( !node_tags ) CE( cudaMalloc( &node_tags, nt_sz_chars ) );
    CE( cudaMemsetAsync( node_tags, 0, nt_sz_chars) );
  }

  CE( cudaDeviceSynchronize() );
  elt.tw_prep_sync_s = pt.dur_s("Prep sync");

  // Initialize distance of source node.
  cuda_host_to_dev( &cq_state.result_0_dh.weight[src_ud], 0 );

  event_pair ep_u_0;
  ep_u_0.start();
  cu_CH_query_pre_launch( graph_UD_d, result_0, result_1 );
  if ( use_u0 )
    cu_CH_query_u_0<<<1,query_u_0_block_dim>>>
      (src, u0_lvl_start, u0_lvl_stop, node_tags);
  ep_u_0.end();

  const double dur_u0_s = ep_u_0.elapsed_s();
  elt.tk_up_0_s = dur_u0_s;
  total_time_s += dur_u0_s;

  stats_func_handle
    ([&]() {pr.tune("time for query_u_0 kernel = %f\n", dur_u0_s);} );
  
  elt.tw_up_0_s = pt.dur_s("cu_CH_query_u_0");
  
  block_dim = 1024;
  int blks_per_mp_query_u_1;
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor			\
      (&blks_per_mp_query_u_1, cu_CH_query_u_1, block_dim, 0) );

  int co_lvl_i = u1_lvl_start + 1;

  uint cut_off_edge_group_count = 2;
  while(graph_UD_h->edge_groups_in_level_u_b[co_lvl_i] > cut_off_edge_group_count){
    co_lvl_i++;
  }
  const int cut_off_level_u = co_lvl_i;
  const int small_group_count_u = num_levels - (cut_off_level_u + 1);
  elt.n_small_group_up = small_group_count_u;
  
  vector<cuda_event> q_events(cut_off_level_u+1);
  

  const int e_idx_start = u1_lvl_start-1;
  int e_idx_end = -1;

  CE( cudaEventRecord(q_events[e_idx_start], 0) );

  elt.up_1_lvl_start = u1_lvl_start;
  elt.up_1_lvl_stop = cut_off_level_u;

  // upwards levels (between 31 and the cut_off_level_u)
  for(int lvl = u1_lvl_start; lvl < cut_off_level_u; lvl++){
    cu_CH_query_u_1<<<blks_per_mp_query_u_1*nmps,block_dim>>>(src, lvl);
    CE( cudaEventRecord(q_events[e_idx_end=lvl], 0) );
  }
  
  CE( cudaEventSynchronize( q_events[e_idx_end] ) );
  elt.tw_up_1_s = pt.dur_s("cu_CH_query_u_1");

  const double tk_up_1_s =
    elapsed_time_get(q_events[e_idx_start],q_events[e_idx_end]);

  elt.tk_up_1_s = tk_up_1_s;
  total_time_s += tk_up_1_s;

  stats_func_handle_loop
    ([&]()
     {
       CE( cudaEventSynchronize( q_events[cut_off_level_u-1] ) );
       for(int lvl = u1_lvl_start; lvl < cut_off_level_u; lvl++){
	 const double dur_s = elapsed_time_get(q_events[lvl-1],q_events[lvl]);
	 pr.loop
	   ("time for query_u_1 kernel at level %u with %u edge_groups = %f\n",
	    lvl, graph_UD_h->edge_groups_in_level_u_b[lvl], dur_s);
       }
     } );
  
  event_pair ep_u_1mi, ep_u_1;

  // upwards levels (levels with few edge groups, between cut_off_level_u and num_levels - 1)
  if(small_group_count_u > 0){
    block_dim = 1024;
    grid_dim = 1;

    ep_u_1mi.start();

    for ( int mi_level = cut_off_level_u;  mi_level < int(num_levels) - 1;
          mi_level += query_multi_max_iters )
      cu_CH_query_u_1_multi_iter<<<grid_dim,block_dim>>>
        ( mi_level, min( num_levels - mi_level - 1, query_multi_max_iters ) );
    ep_u_1mi.end();

    const double dur_u1m_s = ep_u_1mi.elapsed_s();
    elt.tk_up_small_s = dur_u1m_s;
    total_time_s += dur_u1m_s;

    stats_func_handle
      ([&]() {
         pr.tune
           ("time for query_u_1_multi_iter kernel on highest %d levels = %f\n",
            small_group_count_u, dur_u1m_s); } );

    // last upwards level (OL level)
    block_dim = 1024;

    ep_u_1.start();

    cu_CH_query_u_1<<<blks_per_mp_query_u_1*nmps,block_dim>>>
      (src, num_levels-1);

    ep_u_1.end();

    const double dur_u1_s = ep_u_1.elapsed_s();
    elt.tk_up_final_s = dur_u1_s;
    total_time_s += dur_u1_s;

    stats_func_handle_loop
      ([&]() {
         pr.loop
           ("time for query_u_1 kernel at level %u with %u edge_groups = %f\n",
            num_levels-1, graph_UD_h->edge_groups_in_level_u_b[num_levels-1],
            dur_u1_s ); } );
  }

  elt.tw_up_small_s = pt.dur_s("cu_CH_query_u_1 small groups");

  double query_up_time = total_time_s;

  event_pair ep_OL;

  uint32_t APSP_nnodes = graph_UD_h->overlay_APSP_path.num_nodes;
  
  block_dim = 1024;
  grid_dim = APSP_nnodes / OL_BLSZ;

  ep_OL.start();

  cu_CH_query_OL<<<grid_dim,block_dim>>>();

  ep_OL.end();

  const double query_OL_time = ep_OL.elapsed_s();
  elt.tk_OL_s += query_OL_time;
  total_time_s += query_OL_time;
  elt.tw_OL_s = pt.dur_s("cu_CH_query_OL");

  // downwards pass
  // top levels (levels with few edge groups)
  block_dim = 1024;
  grid_dim = 1;

  int co_lvl_d_i = 0;
  while(graph_UD_h->edge_groups_in_level_d_b[co_lvl_d_i] > cut_off_edge_group_count){
    co_lvl_d_i++;
  }
  const int cut_off_level_d = co_lvl_d_i;
  const int small_group_count_d = num_levels - (cut_off_level_d + 1);
  elt.n_small_group_down = small_group_count_d;
  event_pair ep_dm;
  
  if(small_group_count_d > 0){

    ep_dm.start();
    for ( int it = 0; it < small_group_count_d; it += query_multi_max_iters )
      {
        int lvl_en = num_levels - 1 - it;
        int lvl_st = max( cut_off_level_d, lvl_en -int(query_multi_max_iters) );
        cu_CH_query_d_multi_iter<<<grid_dim,block_dim>>>
          ( lvl_st, lvl_en - lvl_st );
      }

    ep_dm.end();
    const double dur_dm_s = ep_dm.elapsed_s();
    elt.tk_down_small_s = dur_dm_s;
    total_time_s += dur_dm_s;

    stats_func_handle
      ([&]() {
         pr.tune
           ("time for query_d_multi_iter kernel on highest %d levels = %f\n",
            small_group_count_d, dur_dm_s); } );
  }

  elt.tw_down_small_s = pt.dur_s("cu_CH_query_d_mult_iter");
  
  // remaining levels
  block_dim = 1024;
  int blks_per_mp_query_d;
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor			\
      (&blks_per_mp_query_d, cu_CH_query_d, block_dim, 0) );

  vector<cuda_event> qd_events(cut_off_level_d+1);

  CE( cudaEventRecord(qd_events[cut_off_level_d], 0) );

  for(int lvl = cut_off_level_d - 1; lvl >= 0; lvl--){

    cu_CH_query_d<<<blks_per_mp_query_d*nmps,block_dim>>>(lvl);
    CE( cudaEventRecord(qd_events[lvl], 0) );
  }

  // Force wall-clock timing to reflect completion of all kernels.
  CE( cudaDeviceSynchronize() );

  CE( cudaEventSynchronize( qd_events[0] ) );
  const double dur_q_d =
    elapsed_time_get( qd_events[cut_off_level_d], qd_events[0] );
  elt.tk_down_normal_s = dur_q_d;
  total_time_s += dur_q_d;

  stats_func_handle_loop
    ([&]() {
       for(int lvl = cut_off_level_d - 1; lvl >= 0; lvl--)
         pr.loop
           ("time for query_d kernel at level %u with %u edge_groups = %f\n",
            lvl, graph_UD_h->edge_groups_in_level_d_b[lvl],
            elapsed_time_get( qd_events[lvl+1], qd_events[lvl] ) );
     } );

  elt.tw_down_normal_s = pt.dur_s("cu_CH_query_d sync");
  const double dur_query_s = pt.dur_starting_from_s("cu_CH_query_u_0");
  const double query_down_s = total_time_s - query_up_time - query_OL_time;

  elt.t_up_s = query_up_time;
  elt.t_down_s = query_down_s;
  elt.t_OL_s = query_OL_time;
  elt.t_dist_kernel_s = total_time_s;
  elt.t_dist_wall_s = dur_query_s;

  stats_func_handle
    ([&]()
     {
       printf("time for query_u kernels (combined) = %f\n", query_up_time);
       printf("time for query_OL kernel = %f\n", query_OL_time);
       printf("time for query_d kernels (combined) = %f\n", query_down_s );
       printf("time for query (combined) = %f s (kernels)  %f s (wall)\n",
              elt.t_dist_kernel_s, elt.t_dist_wall_s);

       if ( first_query )
         {
           uint32_t nedges_u = 0;
           uint32_t nedges_d = 0;
           for ( uint i=0; i<num_nodes; i++ ){
             nedges_u += graph_UD_h->graph_u_b.num_neighbors[i];
             nedges_d += graph_UD_h->graph_d_b.num_neighbors[i];
           }

           printf("Nodes %u,  edges_u %u, edges_d %u\n",
                  num_nodes, nedges_u, nedges_d);
         }

       printf("query kernel finished\n");
     } );

  block_dim = 1024;
  int blks_per_mp_unpack_OL;
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor			\
      (&blks_per_mp_unpack_OL, cu_CH_path_unpack_OL, block_dim, 0) );
  
  CE( cudaEventRecord(start_0, 0) );

  if ( !chopts.query_distance_only )
    cu_CH_path_unpack_OL<<<blks_per_mp_unpack_OL*nmps,block_dim>>>();

  CE( cudaEventRecord(stop_0, 0) );
  CE( cudaEventSynchronize(stop_0) );
  const double dur_unpack_OL_s = pt.dur_s("cu_CH_path_unpack_OL");
  elt.tw_unpack_OL_s = dur_unpack_OL_s;

  kernel_time = 0;
  CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
  const double durk_unpack_OL_s = kernel_time * 0.001;
  elt.tk_unpack_OL_s = durk_unpack_OL_s;
  total_time_s += durk_unpack_OL_s;

  block_dim = 1024;
  int blks_per_mp_unpack_renum;
  CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor			\
      (&blks_per_mp_unpack_renum, cu_CH_path_unpack_renum, block_dim, 0) );
  
  CE( cudaEventRecord(start_0, 0) );

  cu_CH_path_unpack_renum<<<blks_per_mp_unpack_renum*nmps,block_dim>>>();

  CE( cudaEventRecord(stop_0, 0) );
  CE( cudaEventSynchronize(stop_0) );
  const double dur_unpack_renum_s = pt.dur_s("cu_CH_path_unpack_renum");
  elt.tw_unpack_renum_s = dur_unpack_renum_s;

  pt.dur_s("free, stats");

  for ( auto& f: stats_deferred_funcs ) f();

  kernel_time = 0;
  CE( cudaEventElapsedTime(&kernel_time, start_0, stop_0) );
  const double durk_unpack_renum_s = kernel_time * 0.001;
  elt.tk_unpack_renum_s = durk_unpack_renum_s;
  total_time_s += durk_unpack_renum_s;

  elt.t_unpack_kernel_s = durk_unpack_renum_s;
  elt.t_unpack_wall_s = dur_unpack_renum_s + dur_unpack_OL_s;

  if ( print_details )
    printf("time for path_unpack_renum kernel = %f s (kernel)  %f s (wall)\n",
           elt.t_unpack_kernel_s, elt.t_unpack_wall_s);

  CE( cudaDeviceSynchronize() );
  CE( cudaGetLastError() );

  if ( print_details )
    printf("path_unpack_renum kernel finished\n");

  printf("total time = %f s\n", total_time_s);
  // copy path result into host and return
  path_d2h( cq_state.result_h, cq_state.result_1_dh );

  const double dur_dtoh_ms = 1000 * pt.dur_s("path_d2h");
  const double dur_total_ms = 1000 * pt.dur_total_s();
  if ( print_details )
    printf("P_Timing:\n%s\n",pt.text.c_str());

  pr.user("Query complete, %.3f ms query + %.3f ms result to host.\n",
          dur_total_ms - dur_dtoh_ms, dur_dtoh_ms );

}
