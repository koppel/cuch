/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __GRAPHS_H__
#define __GRAPHS_H__

#include "main.h"
#include "heap.h"
#include <string>
#include <vector>

#define GRAPH_BI_EMPTY_SPACE_MULTIPLIER 2
// overall allocation ratio of NNIEG to cover levels with more than one EG per node on average
#define NNIEG_OVER_ALLOC 1.2
// cut-off level to start overallocation of NNIEG
#define NNIEG_OVER_ALLOC_CUT_OFF 100
// overallocation multiplier for over-alloc levels (lvl > NIEG_OVER_ALLOC_CUT_OFF
#define NNIEG_OVER_ALLOC_RATIO 2


constexpr bool opt_static_cull_want_age = false;


#ifndef __CUDACC__
#define __builtin_align__(x)
#endif


/*
 * using a static cache friendly CSR like structure for graphs
 */

typedef struct graph_st
{
  nodeid_t num_nodes;
  nodeid_t num_edges;
  nnbor_t *num_neighbors;
  nodeid_t **neighbors_pt;
  weight_t **weights_pt;
  nodeid_t *neighbors;
  weight_t *weights;
}graph_st_t;

graph_st_t* graph_st_get_from_file(File_Open_Read& file_open_read);
bool graph_st_get_norm_from_file(graph_st_t **graph_st, FILE *f);
bool graph_st_get_gr_from_file(graph_st_t **graph_st, FILE *f);
int graph_st_remove_duplicates_self_loops(graph_st_t *graph);
void graph_st_get_backwards(graph_st_t **graph_st_b, graph_st_t *graph_st_f);
void graph_st_get_stats(graph_st_t *graph);
void graph_st_free(graph_st_t *graph);
size_t graph_st_sizeof(graph_st_t *graph);
void graph_st_norm_dump(FILE *f, graph_st_t *graph);

/*
 * Path (solution) structure
 */
struct __builtin_align__(2*sizeof(nodeid_t)) Dist_Prev {
  weight_t dist;
  nodeid_t prev;
};

struct path_t
{
  nodeid_t num_nodes;
  nodeid_t src;
  nodeid_t dest;
  char *storage;
  Dist_Prev *dist_prev;
  weight_t *weight; //Aggregate weight for indexed node
  nodeid_t *prev; //Penultimate node for indexed node
};

void path_init(path_t **path, size_t num_nodes);
void path_dump(FILE *fp, path_t *path);
int path_lengths_are_equal(path_t *a, path_t *b);
int path_get_from_file(path_t **path, FILE *f);
void path_free(path_t *path);

/*
 *  Class for APSP benchmark data.
 */

class APSP_Bench_Info {
public:
  APSP_Bench_Info():inited(false),times(nullptr){};
  ~APSP_Bench_Info(){ if ( times ) free(times); }
  bool load();
  std::string filename_make();
  std::string bm_path_get() { return bm_path.native(); }
  float* times_get() { return times; }
private:
  bool inited;
  fspath bm_path;
  float *times;
  int len;
};


/*
 * APSP Path
 */

typedef struct APSP_path
{
  nodeid_t num_nodes;
  nodeid_t **prev_pt;
  weight_t **dist_pt;
  nodeid_t *prev;
  weight_t *dist;
}APSP_path_t;

void APSP_path_init(APSP_path_t **path, size_t num_nodes);
void APSP_path_dump(FILE *fp, APSP_path_t *path);
int APSP_path_get_from_file(APSP_path_t **path, FILE *f);
void APSP_path_free(APSP_path_t *path);

/*
 * sort with ref
 */

void merge_sort_ref(nodeid_t *list, nodeid_t *ref, nodeid_t *work_l, nodeid_t *work_r, nodeid_t length);

/*
 * read/write a simple array to disk (mainly a debugging/testing function)
 */

void array_dump(FILE *fp, uint32_t *A, uint32_t len);
void array_dump_f(FILE *fp, float *A, uint32_t len);
uint32_t array_load(FILE *fp, uint32_t **A);
uint32_t array_load_f(FILE *fp, float **A);

/*
 * graph_CH
 */


enum {
  NODE_CONTRACTED=1<<0,
  NODE_MARKED=1<<1
};

enum {
  EDGE_SHORTCUT=1<<0,
  EDGE_ADDED=1<<1
};

typedef struct node_CH
{
  nnbor_t num_neighbors;
  nnbor_t num_neighbors_overlay;
  nnbor_t llen;
  nodeid_t *neighbors;
  weight_t *weights;
  nodeid_t *midpoint;
  uint8_t *edge_flags;
}node_CH_t;

typedef struct graph_CH
{
  size_t num_nodes;
  size_t num_edges;
  weight_t max_weight;
  node_CH_t *nodes;
}graph_CH_t;

typedef struct graph_CH_bi
{
  size_t num_nodes;
  graph_CH_t graph_f;
  graph_CH_t graph_b;
  nodeid_t max_degree;
  nodeid_t max_degree_overlay;
  double mean_degree;
  double mean_degree_overlay;
  weight_t max_weight;
  weight_t mean_weight;
  nodeid_t *node_ranks;
  nodeid_t overlay_size;
  nodeid_t max_rank;
}graph_CH_bi_t;

typedef struct graph_CH_UD
{
  graph_CH_t graph_u_f; // not full graph, only index (no weight/etc.)
  graph_CH_t graph_u_b;
  graph_CH_t graph_d_b;
  graph_CH_t overlay_CH; // needed for unpacking path
  nodeid_t *node_ranks_u;
  nodeid_t *node_ranks_d; // this is just backwards ranks_u (for $ behavior)
  nodeid_t *node_levels_u; // not necessarily different from ranks_u
  nodeid_t *node_levels_d; // not necessarily different from ranks_d
  nodeid_t *node_levels_u_pt; // marks first node from each level
  nodeid_t *node_levels_d_pt; // last element is equal to num_nodes
  nodeid_t *node_idx_u; // graph sorted by rank/level in this representation
  nodeid_t *node_idx_d; // this is just backwards idx_u (for $ behavior)
  nodeid_t *node_idx_u_inv; // ref for easy access using original idx
  nodeid_t *node_idx_d_inv; // only used to speedup finding src in query
  nodeid_t overlay_size;
  nodeid_t max_rank;
  APSP_path_t *overlay_APSP_path; // APSP results
}graph_CH_UD_t;

int graph_CH_get_from_file(graph_CH_bi_t **graph, File_Open_Read &f);
void graph_st_2_CH(graph_CH_t **graph_CH, graph_st_t *graph_st);
size_t graph_CH_sizeof(graph_CH_bi_t *graph);
void graph_CH_bi_free(graph_CH_bi_t *graph);
void graph_CH_dump(FILE *f, graph_CH_bi_t *graph);

// for testing purposes on GCH/SCH graphs (no other application currently)
void graph_CH_bi_2_st(graph_st_t **graph_st, graph_CH_bi_t *graph_CH);

/*
 * cu_graph_CH
 */

constexpr bool opt_elist_use = false;


struct __builtin_align__(2*sizeof(nodeid_t)) Edge_Nbr_Wht
{
  nodeid_t neighbor;
  weight_t weight;
};

typedef unsigned long long int Wht_Sum;

typedef struct cu_graph_CH
{
  nodeid_t num_nodes;
  weight_t max_weight;// not tracked properly, may be used for better weight normalization for score
  nodeid_t max_num_edges;// length of the edge list
  nodeid_t num_edges_exact;
  Wht_Sum weight_sum;  // The entire graph.
  int look_max_degree; // Updated when certain tuning and debug options set.
  nodeid_t empty_pointer;// pointer to the empty space at the end of list (move overgrown nodes here)
  nodeid_t *pointer;
  nodeid_t *num_neighbors;
  nodeid_t *NNIEG;// NULL in bi graph / number of nodes in edge group in UD graph
  nodeid_t *max_num_neighbors_allowed;// used for when updating graph (adding shortcut edges)
  uint32_t *vertex_ID;// NULL in bi graph / vertex ID in UD graph during query 

  Edge_Nbr_Wht *elist;
  nodeid_t *neighbors;
  weight_t *weights;
  nodeid_t *midpoint;
}cu_graph_CH_t;

typedef struct cu_graph_CH_bi
{
  cu_graph_CH_bi *self_d;
  nodeid_t work_item_next;
  nodeid_t num_nodes;
  nodeid_t max_num_selected;
  cu_graph_CH_t graph_f;
  cu_graph_CH_t graph_b;
  nodeid_t max_degree;// incorrect value right after graph_extraction (until the next update kernel)
  nodeid_t max_degree_overlay;// not tracked properly, only use is to get max_degree after extraction
  uint32_t thread_per_node; // stored in log format, use with shift (1<<tpn)
  float mean_degree;// not tracked properly, (no algorithmic purpose, just a statistic)
  float mean_degree_overlay;// not tracked properly, (no algorithmic purpose, just a statistic)
  weight_t max_weight;// not tracked properly, may be used for better weight normalization for score
  weight_t mean_weight;    // Mean edge weight of current graph.
  float mean_weight_inv;   // Reciprocal of mean edge weight of current graph.
  float mean_weight_0_inv; // Reciprocal of mean edge weight of original graph.
  nodeid_t *node_ranks;
  nodeid_t *node_ids;
  nodeid_t overlay_size;
  nodeid_t max_rank;
  nodeid_t num_longcuts;
  nodeid_t num_longcuts_cut;
  nodeid_t num_longcuts_orig;
  nodeid_t sum_longcuts_rank; // Sum of rank of culled longcuts' midpoint.
}cu_graph_CH_bi_t;

typedef struct cu_graph_CH_UD
{
  cu_graph_CH_t graph_u_f; // not full graph, only index (no weight/etc.)
  cu_graph_CH_t graph_u_b;
  cu_graph_CH_t graph_d_b;
  cu_graph_CH_t overlay_CH; // needed for unpacking path
  nodeid_t num_nodes; // Number of nodes in original graph.
  nodeid_t max_degree;
  uint32_t thread_per_node; // stored in log format, use with shift (1<<tpn)
  nodeid_t *node_ranks;
  nodeid_t *node_levels; // not necessarily different from ranks_u
  nodeid_t *node_levels_pt; // marks first node from each level
  nodeid_t *node_idx;     // UD nid -> Original nid.
  nodeid_t *node_idx_inv; // Original nid -> UD nid.
  nodeid_t *edge_groups_in_level_u_b; // number of <=1024 edge groups in each level
  nodeid_t *edge_groups_in_level_d_b; // number of <=1024 edge groups in each level
  nodeid_t overlay_size;
  nodeid_t n_levels_alloc; // Size of level- (aka rank)-indexed arrays.
  nodeid_t max_rank;
  /// might modify this for the GPU version, or alternatively use an available code/struct for APSP
  APSP_path_t overlay_APSP_path; // APSP results
}cu_graph_CH_UD_t;

void graph_CH_2_cu_CH(cu_graph_CH_bi_t **cu_graph_CH, graph_CH_bi_t *graph_CH);
void graph_cu_CH_2_CH(graph_CH_bi_t **graph_CH, cu_graph_CH_bi_t *cu_graph_CH);
size_t graph_cu_CH_sizeof(cu_graph_CH_bi_t *graph);
void graph_cu_CH_bi_free_host(cu_graph_CH_bi_t*& graph);
void graph_cu_CH_free_host(cu_graph_CH_t *graph);

void graph_cu_UD_free_host(cu_graph_CH_UD_t*& graph_UD_h);


// note that for this initialization, edge_list_length and max_rank should be (over)estimates since
// unlike the CPU version where UD would be written at the end, in the GPU version, the graph is
// filled as the algorithm progresses, also, here node ranks and node levels are assumed to be the
// same since results from the CPU code show that redoing the levels has only minimal impact, so the
// node_ranks_u/d are redundant.
/// the host init is redundant; to be removed
void graph_cu_CH_UD_init(cu_graph_CH_UD_t **graph_cu_UD, nodeid_t num_nodes, uint32_t edge_list_length, nodeid_t max_rank);
void graph_cu_CH_UD_2_CH_UD(graph_CH_UD_t **graph_UD, cu_graph_CH_UD_t *graph_cu_UD);


// Storage for the shortcuts
struct __builtin_align__(16) shc_t
{
  // It is very important that the compiler emits a vector load rather than
  // four loads when accessing an instance of this structure.
  nodeid_t src_id, dst_id, wht, mid_oid;
};
  
struct cu_shc_CH_t
{
  cu_shc_CH_t():
    n_segs(0),max_len_per_seg(0){ ptrs_len_reset(); ptrs_shc_reset(); }
  void ptrs_len_reset(){ len_sto = len = len_dst = nullptr; }
  void ptrs_shc_reset(){ shc_sto = shc = shc_dst = nullptr; }
  uint32_t n_segs;
  uint32_t max_len_per_seg; // max added shc per segment. depends on tpn and vertex_per_block
  // Number of shc_t elements allocated for each list, shc & shc_dst.
  uint32_t *len_sto; // Base of allocated storage.
  uint32_t *len, *len_dst; // actual number of shortcuts each block has added.
  shc_t *shc_sto; // Base of allocated storage.
  shc_t *shc, *shc_dst; // These will have size n_segs*len_per_block.
};

class cu_shc_CH_set
{
public:
  cu_shc_CH_set()
    :n_segs_allocated(0),n_shc_allocated(0),
     len_lists_bytes(0), shc_lists_bytes(0), d(nullptr){};
  ~cu_shc_CH_set(){free();}
  void alloc(uint32_t n_segs, uint32_t max_len_per_seg);
  void free();
  void dtoh_len();
  void dtoh_shc();
  void alloc_h(uint32_t n_segs, uint32_t max_len_per_seg);
  void htod_all();
  void ptrs_len_set(cu_shc_CH_t& cpy)
    {
      cpy.len_dst = cpy.len_sto;
      cpy.len = n_sets == 1 ? nullptr : cpy.len_sto + cpy.n_segs;
    }
  void ptrs_shc_set(cu_shc_CH_t& cpy)
    {
      cpy.shc_dst = cpy.shc_sto;
      cpy.shc = n_sets == 1 ? nullptr : cpy.shc_sto + n_shc_per_set;
    }

  int n_sets; // 1, just shc_dst; 2, both shc and shc_dst.
  uint32_t n_shc_per_set;  // Number in each (src and dst) list needed.
  uint32_t n_segs_allocated, n_shc_allocated;
  size_t len_lists_bytes, shc_lists_bytes;
  uint32_t seg_stride; // Number of shortcuts per segment maybe rounded up.

  cu_shc_CH_t h;  // Pointers to host storage, eg, h.shc is a host addr.
  cu_shc_CH_t dh; // Pointers to CUDA storage, eg, dh.shc is a CUDA addr.
  cu_shc_CH_t *d; // Pointer to CUDA storage, ie, d is a CUDA address.
};

// Intended for ad-hoc analyses.
//
struct Ege {
  Ege():src(~nodeid_t(0)),dst(~nodeid_t(0)){}
  Ege(const shc_t& ei):src(ei.src_id),dst(ei.dst_id){}
  Ege(nodeid_t s,nodeid_t d):src(s),dst(d){}
  nodeid_t src, dst;
};
inline bool operator<(const Ege& e0, const Ege& e1)
{ return e0.src < e1.src || e0.src == e1.src && e0.dst < e1.dst; }
inline bool operator ==(const Ege& e0, const Ege& e1)
{ return e0.src == e1.src && e0.dst == e1.dst; }
namespace std
{
  template<> struct hash<Ege>
  { size_t operator ()(Ege const& e) const noexcept
    { return e.src ^ e.dst << 1; }};
}


struct cu_CH_query_data
{
  cu_CH_query_data() { memset(this,0,sizeof(*this)); }

  nodeid_t src;
  int src_lvl;
  int up_1_lvl_start, up_1_lvl_stop, n_small_group_up, n_small_group_down;

  // Timing of kernels collected by CUDA events.
  double tk_up_0_s; // First 31 levels.
  double tk_up_1_s; // u1_lvl_start to < u1_lvl_stop
  double tk_up_small_s; // u1_lvl_stop to num levels-1
  double tk_up_final_s; // Only when there are small levels.
  double tk_OL_s;
  double tk_down_small_s;
  double tk_down_normal_s;
  double tk_unpack_OL_s;
  double tk_unpack_renum_s;

  // Gap-less host wall time.
  double tw_prep_sync_s;
  double tw_up_0_s, tw_up_1_s, tw_up_small_s;
  double tw_OL_s;
  double tw_down_small_s;
  double tw_down_normal_s;
  double tw_unpack_OL_s, tw_unpack_renum_s;

  double t_up_s;
  double t_down_s;
  double t_OL_s;
  double t_dist_kernel_s;
  double t_dist_wall_s;
  double t_unpack_kernel_s;
  double t_unpack_wall_s;
};

struct cu_CH_stats_per_iter
{
  double mean_weight;
  double time_select_fl_s, time_select_sort1_s, time_select_lo_s;
  double time_select_select_s, time_select_gather_s, time_select_sort2_s;
  int64_t luby_n_iter;
};

typedef struct cu_CH_stats
{
  std::string stats_file_name;
  bool contracting; // If true, contraction-related data set.
  bool env_run_slowed_by_tuning_and_debug_options;
  double env_time_start_ue;
  std::string env_time_start_local;
  std::string env_gpu_name;
  std::string env_host_name;
  std::string env_gpu_cc;
  int env_gpu_nmps; // Number of streaming multiprocessors.
  int env_cuda_driver_version;
  int env_cuda_runtime_version;
  std::string env_nvcc_version;
  std::string env_compiler_version;
  bool env_assertion_checking, env_cuda_debug, env_host_optimization;
  bool env_contract_eval;
  std::string env_apsp_benchmark_path;
  std::string in_graph_path;
  bool opt_use_shadow_wps;
  int env_verbosity;
  uint32_t num_nodes;
  uint32_t num_nodes_overlay_final;
  uint32_t num_edges_f_original;
  uint32_t num_edges_b_original;
  uint32_t num_iterations;
  int num_iterations_invert; // Number of iterations in which inversion perf.
  uint32_t max_num_iters; // just for control
  // time_total unit: milliseconds.
  double time_total; // includes the APSP times
  double time_total_scoring;
  double time_total_MIS;
  double time_total_contraction;
  double time_total_update;
  double time_total_OL_extraction_0;
  double time_total_OL_extraction_1;
  double time_total_invert_0, time_total_invert_1;
  // Items below are after the main loop.
  double time_ud_invert;
  double time_ud_invert_0_s, time_ud_invert_1_s;
  double time_ud_invert_2_s, time_ud_invert_3_s;
  double time_APSP_prep_s;
  double time_ud_renum_s;
  double time_APSP_s;
  double time_APSP_0_s, time_APSP_1_s, time_APSP_2_s;
  double time_compress_0_s;
  double time_compress_1_s;
  double wall_time_verify_pre_s;
  double wall_time_verify_post_s;
  double wall_time_shadow_wps_s;
  double wall_time_total_ms;
  double wall_time_pre_main_loop_ms;
  double wall_time_main_loop_ms;
  double wall_time_post_main_loop_ms;
  std::string partition_app_text;
  double *time_iter;
  std::vector<double> wall_time_iter_ms;
  float *time_scoring;
  float *time_MIS;
  float *time_contraction;
  float *time_update;
  float *time_OL_extraction_0;
  float *time_OL_extraction_1;
  std::vector<double> time_invert_0, time_invert_1;
  std::vector<cu_CH_stats_per_iter> it;
  uint32_t *num_nodes_in_lvl;
  uint32_t *num_nodes_overlay;
  double *C;
  std::vector<uint32_t> it_num_edges; // At start of iteration.
  std::vector<int> it_tpn;
  uint32_t *max_degree;
  double *mean_degree;
  std::vector<int> it_num_longcuts_cut;    // All culled longcuts.
  std::vector<int> it_num_longcuts_orig;   // Culled original edges.
  std::vector<double> it_avg_longcuts_age;
  std::vector<int> it_num_longcuts_cut_ideal;

  // The node occupancy (nd_occ) is the number of nodes being operated
  // on by the resident warps. For example, if one warp operates on
  // two nodes, and there are 24 wps * 10 mps = 240 active warps then
  // node occupancy is 2*240=480.
  std::vector<double> it_kernel_scoring_nd_occ;
  std::vector<double> it_kernel_contract_nd_occ;
  std::vector<int> it_kernel_scoring_wp_occ;
  std::vector<int> it_kernel_contract_wp_occ;

  double *deg;// mean of deg rounded up x8 (to calc. off-chip BW), needs CONTRACTION_EVAL enabled
  uint32_t *wps_shadow;
  uint32_t *wps_shadow_shc;
  uint32_t *wps_1hs;
  uint32_t *wps_1hs_loop;
  uint32_t *wps_2hs;
  uint32_t *wps_2hs_loop;
  uint32_t *wps_ij;
  uint32_t *wps_2hop;
  uint32_t *wps_fp;
  uint32_t *wps_shc;
  uint32_t max_degree_OL;
  uint32_t max_degree_u_f_DAG;// currently not tracked
  uint32_t max_degree_u_b_DAG;// currently not tracked
  uint32_t max_degree_d_b_DAG;// currently not tracked
  double mean_degree_u_f_DAG;// currently not tracked
  double mean_degree_u_b_DAG;// currently not tracked
  double mean_degree_d_b_DAG;// currently not tracked
  uint32_t num_edges_u_b_DAG;
  uint32_t num_edges_d_b_DAG;

  // Query Stats.
  std::vector<nodeid_t> edge_groups_in_level_d_b, edge_groups_in_level_u_b;
  std::vector<cu_CH_query_data> q;

}cu_CH_stats_t;

struct cu_CH_query_stats
{
  uint64_t qstart, qend;
  union {
    struct { uint64_t dbuf, dcomp1, dcomp2, dfinish; };
  };
  union {
    struct {  // query stats
      int n_ij, n_iter, n_fpath, n_coll;
      int n_1hshc, n_1hop_iter, n_fph1;
      int n_2hop,  n_2hshc, n_fph2;
    };
    struct { int n_edge_replace, n_edge_append, n_edge_realloc; }; // upd stats
  };
};


void
cu_CH_stats_init
(cu_CH_stats_t* stats, graph_CH_bi *graph,
 uint32_t max_num_iters, const std::string fname);
void
cu_CH_stats_init
(cu_CH_stats_t *stats, const cu_graph_CH_UD_t *graph,
 const std::string file_name);

void cu_CH_stats_dump(cu_CH_stats_t *ds);
void cu_CH_stats_free(cu_CH_stats_t *ds);



#endif 
