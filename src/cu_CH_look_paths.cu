/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <functional>
#include "util/pstring.h"
#include "cu_CH.cuh"
#include "cu_main.h"
#include "cu_verify.h"
#include "cu_util_timing.cuh"

using namespace std;

string
ch_edge_cull
(cu_graph_CH_bi_t *g_d, nodeid_t *selected_list,
 cu_CH_stats_t *stats, uint32_t iter)
{
  cu_graph_CH_bi_t *g;
  graph_cu_CH_bi_d2h(&g, g_d);

  pString msg;

  const size_t n_nodes = g->num_nodes;
  int hh_bins = 8; // Number of bins in hops histogram.

  Array_Heap<SSSP_Node> wps_nodes(n_nodes);

  const bool opt_ideal_longcuts_cull = chopts.cull_ideal_hops >= 1;
  const int hop_limit =
    chopts.cull_ideal_hops > 1 ? chopts.cull_ideal_hops : n_nodes;
  assert( hop_limit > 0 );

  const bool culled_remove = false;
  const bool do_fwd = true;

  int64_t n_edges_unneeded = 0;
  int64_t n_edges = 0;

  vector<int> hops_unn_histo(hh_bins);
  map<nodeid_t,map<nodeid_t,int>> fwd_dests_cull;

  cu_graph_CH& gbhh = g->graph_b; // Graph backward host cpy host pointers.
  cu_graph_CH& gfhh = g->graph_f; // Graph backward host cpy host pointers.

  auto nid_is_invalid = [](nodeid_t n){ return n == nodeid_invalid; };
  auto split =
    []( cu_graph_CH& ghh, nodeid_t nid, function<bool(nodeid_t)> f)
    {
      nodeid_t e_lo = ghh.pointer[nid];
      const int deg = ghh.num_neighbors[nid];
      nodeid_t e_hi = e_lo + deg - 1;
      while ( e_lo <= e_hi )
        {
          if ( !f(ghh.neighbors[e_lo]) ) { e_lo++; continue; }
          if (  f(ghh.neighbors[e_hi]) ) { e_hi--; continue; }
          swap( ghh.neighbors[e_lo], ghh.neighbors[e_hi] );
          ghh.weights[e_lo] = ghh.weights[e_hi];
          ghh.midpoint[e_lo] = ghh.midpoint[e_hi];
          e_lo++; e_hi--;
        }
      const int deg_new = e_lo - ghh.pointer[nid];
      assert( !f(ghh.neighbors[e_lo-1]) && f(ghh.neighbors[e_lo]) );
      assert( deg_new > 0 && deg_new < deg );
      ghh.num_neighbors[nid] = deg_new;
    };

  for ( nodeid_t nid = 0; nid < n_nodes; nid++ )
    {
      const int nnbr_b = g->graph_b.num_neighbors[nid];
      const int e_bwd_0 = g->graph_b.pointer[nid];
      const int eidx_stop = e_bwd_0 + nnbr_b;
      weight_t wht_bwd_max = 0;
      for ( int eidx_b = e_bwd_0; eidx_b < eidx_stop; eidx_b++ )
        {
          const nodeid_t bwd_nbr = g->graph_b.neighbors[eidx_b];
          if ( nodeid_is_special(bwd_nbr) ) continue;
          assert( bwd_nbr != nid );
          set_max( wht_bwd_max, g->graph_b.weights[eidx_b] );
        }

      const weight_t dist_lim = wht_bwd_max;
      n_edges += nnbr_b;
      const int ns_serial = wps_nodes.heap_reset();
      wps_nodes.priority_set(nid,0,0);

      while ( SSSP_Node* const wn_h = wps_nodes.heap_pop() )
        {
          const nodeid_t nnbr_h = g->graph_b.num_neighbors[wn_h->idx];
          const nodeid_t e_h_0 = g->graph_b.pointer[wn_h->idx];
          const int hops_next = wn_h->dist_hops + 1;
          if ( hops_next > hop_limit ) continue;

          for ( uint eidx = e_h_0;  eidx < e_h_0 + nnbr_h; eidx++ )
            {
              const nodeid_t nid_hb = g->graph_b.neighbors[eidx];
              if ( nodeid_is_special(nid_hb) ) continue;
              SSSP_Node* const wn_hb = &wps_nodes[nid_hb];
              const weight_t wht_hb = g->graph_b.weights[eidx];
              const weight_t dist_next = wn_h->dist + wht_hb;
              if ( dist_next > dist_lim ) continue;
              if ( wn_hb->ns_serial != ns_serial
                   || dist_next < wn_hb->dist
                   || dist_next == wn_hb->dist && hops_next < wn_hb->dist_hops )
                  wps_nodes.priority_set(wn_hb,dist_next,hops_next);
            }
        }

      int n_edges_unneeded_here = 0;

      for ( int eidx_b = e_bwd_0; eidx_b < eidx_stop; eidx_b++ )
        {
          const nodeid_t nid_hb = g->graph_b.neighbors[eidx_b];
          if ( nodeid_is_special(nid_hb) ) continue;
          SSSP_Node* const wn_hb = &wps_nodes[nid_hb];
          assert( wn_hb->ns_serial == ns_serial );
          if ( wn_hb->dist_hops <= 1 ) continue;
          const int bin = min(wn_hb->dist_hops,hh_bins-1);
          hops_unn_histo[bin]++;
          n_edges_unneeded++; n_edges_unneeded_here++;
          if ( !opt_ideal_longcuts_cull ) continue;
          gbhh.neighbors[eidx_b] = nodeid_invalid;
          if ( do_fwd ) fwd_dests_cull[nid_hb][nid]++;
        }

      if ( !opt_ideal_longcuts_cull ) continue;
      if ( !n_edges_unneeded_here ) continue;

      if ( culled_remove )
        split(gbhh,nid,nid_is_invalid);
    }

  if ( opt_ideal_longcuts_cull )
    {
      if ( culled_remove )
        {
          for ( auto& m: fwd_dests_cull )
            split
              ( gfhh, m.first,
                [&](nodeid_t n){ return m.second.count(n) > 0; } );
        }
      else
        {
          for ( auto& elt: fwd_dests_cull )
            {
              const nodeid_t nid = elt.first;
              for ( nodeid_t eidx = gfhh.pointer[nid];
                    eidx < gfhh.pointer[nid] + gfhh.num_neighbors[nid]; eidx++ )
                if ( elt.second.count(gfhh.neighbors[eidx]) )
                  gfhh.neighbors[eidx] = nodeid_invalid;
            }
        }

      if ( culled_remove )
        {
          map<Ege,nodeid_t> edge_to_eidx;
          for ( nodeid_t nid = 0;  nid < n_nodes;  nid++ )
            for ( nodeid_t eidx = gbhh.pointer[nid];
                  eidx < gbhh.pointer[nid] + gbhh.num_neighbors[nid]; eidx++ )
              edge_to_eidx[Ege(g->graph_b.neighbors[eidx],nid)] = eidx;
          for ( nodeid_t nid = 0;  nid < n_nodes;  nid++ )
            for ( nodeid_t eidx = gfhh.pointer[nid];
                  eidx < gfhh.pointer[nid] + gfhh.num_neighbors[nid]; eidx++ )
              {
                nodeid_t xdie =
                  edge_to_eidx[Ege(nid,g->graph_f.neighbors[eidx])];
                assert( g->graph_b.weights[xdie] == g->graph_f.weights[eidx] );
                g->graph_f.midpoint[eidx] = xdie;
              }
        }

      cu_graph_CH_bi& ghd = graph_cache_lookup(g_d);
      auto el_to_dev = [&](nodeid_t *d, nodeid_t *h, nodeid_t n_elts)
        {
          CE( cudaMemcpyAsync(d,h,n_elts*sizeof(d[0]),cudaMemcpyHostToDevice) );
        };
      auto els_to_dev = [&](cu_graph_CH& ghd, cu_graph_CH& ghh)
        {
          el_to_dev(ghd.neighbors, ghh.neighbors, ghh.empty_pointer);
          el_to_dev(ghd.weights, ghh.weights, ghh.empty_pointer);
          el_to_dev(ghd.midpoint, ghh.midpoint, ghh.empty_pointer);
          CE( cudaMemcpyAsync
              ( ghd.num_neighbors,ghh.num_neighbors,
                n_nodes*sizeof(ghd.num_neighbors[0]), cudaMemcpyHostToDevice) );
        };
      els_to_dev(ghd.graph_b, g->graph_b);
      if ( do_fwd ) els_to_dev(ghd.graph_f, g->graph_f);
      stats->it_num_longcuts_cut_ideal.push_back(n_edges_unneeded);
    }

  msg.sprintf
    ("Unneeded edges: iter %3d  %9ld, %6.3f%%, deg %5.1f -> %5.1f\n",
     iter, n_edges_unneeded, 100*double(n_edges_unneeded)/n_edges,
     double(n_edges)/n_nodes, double(n_edges-n_edges_unneeded)/n_nodes);

  auto phisto =
    [&](const char* name, vector<int>& histo)
    {
      msg.sprintf("%-10s: ",name);
      for ( auto& bin: histo ) msg.sprintf(" %6d",bin);
      msg.sprintf(".\n");
    };

  phisto("Unneeded",hops_unn_histo);

  graph_cu_CH_bi_free_host(g);

  return msg.ss();
}


string
shadow_wps
(cu_graph_CH_bi_t *g_d, nodeid_t *selected_list,
 cu_shc_CH_set *shc_set, cu_CH_stats_t * stats, uint32_t iter)
{
  const bool use_shadow_wps = chopts.wps_oracle_use;
  cu_graph_CH_bi_t *g;
  graph_cu_CH_bi_d2h(&g, g_d);

  cu_graph_CH& gbhh = g->graph_b; // Graph backward host cpy host pointers.
  cu_graph_CH& gfhh = g->graph_f; // Graph backward host cpy host pointers.

  pString msg;
  #define printf msg.sprintf

  // If true, witness paths can path through the contraction candidate.
  const bool opt_cc_paths = false;

  const bool opt_shc_gpu_analyze = false;

  shc_set->dtoh_len();
  int n_shc_cuch = 0;
  for ( uint32_t i=0; i<shc_set->h.n_segs; i++ )
    n_shc_cuch += shc_set->h.len_dst[i];

  map<Ege,int> shcs_gpu, shcs_cpu;
  const int sseg_len = shc_set->h.max_len_per_seg;

  if ( opt_shc_gpu_analyze )
    {
      shc_set->dtoh_shc();
      for ( uint s=0; s<shc_set->h.n_segs; s++ )
        for ( uint i=0; i<shc_set->h.len_dst[s]; i++ )
          shcs_gpu[Ege(shc_set->h.shc_dst[s*sseg_len+i])]++;
    }

  if(use_shadow_wps){
    shc_set->alloc_h(shc_set->n_segs_allocated, shc_set->seg_stride);
    memset(shc_set->h.len_sto,0,shc_set->len_lists_bytes);
  }

  const size_t n_nodes = g->num_nodes;
  int hh_bins = 8; // Number of bins in hops histogram.

  Array_Heap<SSSP_Node> wps_nodes(n_nodes);

  // Make sure that graph does not have self edges.
  for ( nodeid_t nid = 0; nid < n_nodes; nid++ )
    {
      const int nnbr_f = g->graph_f.num_neighbors[nid];
      const int e_fwd_0 = g->graph_f.pointer[nid];
      for ( int eidx_f = e_fwd_0; eidx_f < e_fwd_0 + nnbr_f; eidx_f++ )
        {
          const nodeid_t fwd_nbr = g->graph_f.neighbors[eidx_f];
          if ( nodeid_is_special(fwd_nbr) ) continue;
          assert( fwd_nbr != nid );
        }
      const int nnbr_b = g->graph_b.num_neighbors[nid];
      const int e_bwd_0 = g->graph_b.pointer[nid];
      for ( int eidx_b = e_bwd_0; eidx_b < e_bwd_0 + nnbr_b; eidx_b++ )
        {
          const nodeid_t bwd_nbr = g->graph_b.neighbors[eidx_b];
          if ( nodeid_is_special(bwd_nbr) ) continue;
          assert( bwd_nbr != nid );
        }
    }

  const nodeid_t n_sel = selected_list[0];
  int n_shc = 0, n_tie = 0;
  int n_pairs = 0;
  vector<int> hops_histo(hh_bins);
  vector<int> hops_rad(hh_bins);
  const double t0 = time_wall_fp();
  const int hop_limit = chopts.wps_oracle_hop_limit;

  for ( nodeid_t i = 1; i <= n_sel; i++ )
    {
      const nodeid_t nid = selected_list[i]; // This is the cc.
      assert( nid < n_nodes );
      const int nnbr_b = g->graph_b.num_neighbors[nid];
      const int nnbr_f = g->graph_f.num_neighbors[nid];
      const int e_bwd_0 = g->graph_b.pointer[nid];
      const int e_fwd_0 = g->graph_f.pointer[nid];
      weight_t wht_in_max = 0;
      for ( int eidx_b = e_bwd_0; eidx_b < e_bwd_0 + nnbr_b; eidx_b++ )
        if ( !nodeid_is_special(gbhh.neighbors[eidx_b]) )
          set_max( wht_in_max, gbhh.weights[eidx_b] );
      for ( int eidx_f = e_fwd_0; eidx_f < e_fwd_0 + nnbr_f; eidx_f++ )
        {
          const weight_t wht_fwd = g->graph_f.weights[eidx_f];
          const weight_t dist_lim = wht_fwd + wht_in_max;
          const nodeid_t fwd_nbr = g->graph_f.neighbors[eidx_f];
          if ( nodeid_is_special(fwd_nbr) ) continue;
          const int ns_serial = wps_nodes.heap_reset();
          wps_nodes.priority_set(fwd_nbr,0,0);
          wps_nodes[fwd_nbr].from_cc = false;
          int search_hops_max = 0;

          while ( SSSP_Node* const wn_h = wps_nodes.heap_pop() )
            {
              const nodeid_t nnbr_h = g->graph_b.num_neighbors[wn_h->idx];
              const nodeid_t e_h_0 = g->graph_b.pointer[wn_h->idx];
              const bool from_cc = wn_h->idx == nid;
              const int hops_next = wn_h->dist_hops + 1;
              if ( hops_next > hop_limit ) continue;
              set_max(search_hops_max,hops_next);
              for ( uint eidx = e_h_0;  eidx < e_h_0 + nnbr_h; eidx++ )
                {
                  const nodeid_t nid_hb = g->graph_b.neighbors[eidx];
                  if ( nodeid_is_special(nid_hb) ) continue;
                  if ( !opt_cc_paths && nid_hb == nid ) continue;
                  SSSP_Node* const wn_hb = &wps_nodes[nid_hb];
                  const weight_t wht_hb = g->graph_b.weights[eidx];
                  const weight_t dist_next = wn_h->dist + wht_hb;
                  if ( dist_next > dist_lim ) continue;
                  if ( wn_hb->ns_serial == ns_serial &&
                       ( dist_next > wn_hb->dist ||
                         dist_next == wn_hb->dist
                         && ( from_cc || !opt_cc_paths ) ) )
                    continue;
                  wps_nodes.priority_set(wn_hb,dist_next,hops_next);
                  wn_hb->from_cc = from_cc;
                  wn_hb->relax_search_hops_max = search_hops_max;
                }
            }
          for ( int eidx_b = e_bwd_0; eidx_b < e_bwd_0 + nnbr_b; eidx_b++ )
            {
              nodeid_t bwd_nbr = g->graph_b.neighbors[eidx_b];
              if ( nodeid_is_special(bwd_nbr) ) continue;
              if ( bwd_nbr == fwd_nbr ) continue;
              n_pairs++;
              SSSP_Node* const wn_b = &wps_nodes[bwd_nbr];
              weight_t bwd_wht = g->graph_b.weights[eidx_b];
	      // counting WPs with same length of more than one hop misrepresents WPS performance
	      // in later iterations it was making serial WPS count half as many added shortcuts
              if ( wn_b->ns_serial == ns_serial
                   && wn_b->dist < bwd_wht + wht_fwd )
                {
                  int bin = min(wn_b->dist_hops,hh_bins-1);
                  assert( !wn_b->from_cc || wn_b->dist_hops > 2 );
                  hops_histo[bin]++;
                  hops_rad[bin] += wn_b->relax_search_hops_max;
                  continue;
                }
              if ( wn_b->ns_serial == ns_serial
                   && !wn_b->from_cc
                   && wn_b->dist == bwd_wht + wht_fwd )
                {
                  int bin = min(wn_b->dist_hops,hh_bins-1);
		  if(bin == 1){
		    hops_histo[bin]++;
                    hops_rad[bin] += wn_b->relax_search_hops_max;
		    continue;
		  }
		  n_tie++;
                }
              n_shc++;
	      if(use_shadow_wps){
                const nodeid_t m_or_inv = gfhh.midpoint[ eidx_f ];
                const nodeid_t m_curr = gbhh.midpoint[m_or_inv];
                const nodeid_t midpoint =
                  nodeid_is_special(m_curr) ? g->node_ids[ nid ] : m_curr;

		shc_t ei;
		ei.src_id = bwd_nbr;
		ei.dst_id = fwd_nbr;
		ei.wht = bwd_wht + wht_fwd;
		ei.mid_oid = midpoint;

		const int shc_dbin = ei.dst_id % shc_set->h.n_segs;

		assert(shc_set->h.len_dst[shc_dbin] < shc_set->h.max_len_per_seg);

		shc_set->h.shc_dst
		  [shc_set->h.max_len_per_seg * shc_dbin + shc_set->h.len_dst[shc_dbin]] = ei;

		shc_set->h.len_dst[shc_dbin]++;

	      }
            }
        }
    }

  const double t1 = time_wall_fp();

  const int n_wp = n_pairs - n_shc;

  if(use_shadow_wps)
    {
      if ( opt_shc_gpu_analyze )
        {
          for ( uint s=0; s<shc_set->h.n_segs; s++ )
            for ( uint i=0; i<shc_set->h.len_dst[s]; i++ )
              shcs_cpu[Ege(shc_set->h.shc_dst[s*sseg_len+i])]++;

          vector<Ege> cpu_only, gpu_only, both;
          int n_cpu = 0, n_gpu = 0;

          for ( auto& e: shcs_gpu ) n_gpu += e.second;
          for ( auto& e: shcs_cpu ) n_cpu += e.second;
          for ( auto& e: shcs_gpu )
            if ( shcs_cpu.count(e.first) ) both.push_back(e.first);
            else gpu_only.push_back(e.first);

          for ( auto& e: shcs_cpu )
            if ( !shcs_gpu.count(e.first) ) cpu_only.push_back(e.first);

          printf("Shc Sets at Iter %3d, CPU %5zu,  Both %5zu,  GPU %5zu."
                 " Dups %4.1f %4.1f\n",
                 iter, cpu_only.size(), both.size(), gpu_only.size(),
                 double(n_cpu)/max(size_t(1),shcs_cpu.size()),
                 double(n_gpu)/max(size_t(1),shcs_gpu.size()));
        }

      shc_set->htod_all();
    }

  /// RRRR test shortcut count
  if(!use_shadow_wps){
    if ( stats->wps_ij[iter]
         && n_pairs !=
         int(stats->wps_1hs[iter] + stats->wps_2hs[iter]) + n_shc_cuch )
      {
        printf
          ("\n\n WARNING!!! Incorrect shortcut path count\n"
           "  n_pairs = %u, sum = %u\n\n",
           n_pairs, stats->wps_1hs[iter] + stats->wps_2hs[iter] + n_shc_cuch);
      }
  }

  printf("Shd wps NH iter %3d  time %8.3f ms\n",iter,1e3*(t1-t0));

  printf
    ("Shadow wps found %d bp,  %d wp, %d tie, %d shortcuts, %d shc cuch.\n",
     n_pairs, n_pairs-n_shc, n_tie, n_shc, n_shc_cuch );
  stats->wps_shadow[iter] = n_pairs-n_shc;
  stats->wps_shadow_shc[iter] = n_shc;
  printf("WP / Nd Pair: exact %.5f,  cuch %.5f\n",
         double(n_wp)/max(1,n_pairs),
         double(n_pairs-n_shc_cuch)/max(1,n_pairs));

  auto phisto =
    [&](const char* name, vector<int>& histo)
    {
      printf("%-10s: ",name);
      for ( auto& bin: histo ) printf(" %6d",bin);
      printf(".\n");
    };

  phisto("Hops histo",hops_histo);
  printf("Search rad: ");
  for ( int i=0; i<hh_bins; i++ )
    printf(" %6.2f", double(hops_rad[i])/max(1,hops_histo[i]));
  printf(".\n");
  if ( stats->wps_ij[iter] )
    printf("CUCH histo:  0 %u %u\n",stats->wps_1hs[iter],stats->wps_2hs[iter]);

  graph_cu_CH_bi_free_host(g);
# undef printf
  return msg.ss();
}

