/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#ifndef __APSP_H__
#define __APSP_H__

#include "graphs.h"
#include "main.h"
#include "heap.h"

void APSP_st(graph_st *graph, APSP_path_t *path);

void APSP_dij_st(graph_st *graph, APSP_path_t *path);

void APSP_blocked_st(graph_st_t *graph, APSP_path_t *path);


#endif 
 
