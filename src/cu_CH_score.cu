/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>

#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"

#include <nppdefs.h>

using namespace std;

const bool __attribute__((unused)) debug_sm_idx = false;

static const bool opt_hash_power_2 = true;
static const bool stats_wps_want = false;  // Slows down execution.

static const bool cull_inner_loop = true;


// #define ST
// guarantees that different syncthreads() calls will not affect each other by checking line number
// allows for threads to skip a syncthreads() (only applies to the last syncthread in a kernel)
#ifdef ST

#define __syncthreads()                                                       \
  { sync_threads();                                                           \
    st_line = atomicExch(&sync_count_sh, __LINE__);                           \
    assert(st_line == 0 || st_line == __LINE__);                              \
    sync_threads();                                                           \
    sync_count_sh = 0; }

#endif

__constant__ CH_Options chopts_c;
__constant__ cu_graph_CH_UD graph_UD_c;

__host__ void
cu_CH_score_nodes_pre_launch
(cu_graph_CH_bi_t *graph_bi_d, cu_graph_CH_UD *graph_UD_d)
{
  cuda_sym_host_to_dev(chopts_c,&chopts);

  if ( !opt_static_cull_want_age ) return;
  // Only need to do this when collecting cull statistics.
  cuda_sym_host_to_dev(graph_UD_c, graph_UD_d);
}

__device__ float
cu_CH_score_compute
(const cu_graph_CH_bi_t& graph_c, Score_Components sc, float n_shc)
{
  // the constant factors for the score. tuning parameters
  // current values are based on limited by the hand testing.
  // proper optimization should result in a more optimal value
  const float c0 = chopts_c.score_edge_diff;
  const float c1 = chopts_c.score_max_deg_nbr;
  const float c2 = chopts_c.score_mean_weight;
  const float c3 = chopts_c.score_max_weight;
  const float c4 = chopts_c.score_deg;

  const float mean_weight = sc.deg ? float(sc.wht_sum) / sc.deg : 0;
  const float mean_weight_g_inv = chopts_c.score_g_wht_0
    ? graph_c.mean_weight_0_inv : graph_c.mean_weight_inv;
  const float the_score =
    c0 * ( float(sc.deg) - n_shc )
    + c1 * sc.nbr_deg_max
    + ( c2 * mean_weight + c3 * sc.wht_max ) * mean_weight_g_inv
    + c4 * sc.deg;

  return the_score;
}

__global__ void __launch_bounds__(1024)
elist_populate(cu_graph_CH_bi_t graph)
{
  constexpr int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int tid = threadIdx.x + blockIdx.x * block_dim;
  const int n_thds = block_dim * gridDim.x;
  for ( const auto& g: { graph.graph_f, graph.graph_b } )
    for ( int e_idx = tid; e_idx < g.empty_pointer; e_idx += n_thds )
      g.elist[e_idx] = { g.neighbors[e_idx], g.weights[e_idx] };
}

template <uint32_t tpn, bool static_opt_cull> __global__ void
__launch_bounds__(contract_nodes_tpn_to_block_size(tpn))
cu_CH_score_nodes
(cu_graph_CH_bi_t graph_c, float *node_scores,
 cu_CH_query_stats *qstats_a, Score_Components *score_components)
{
  const cu_graph_CH_bi_t* const graph = &graph_c;

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  constexpr bool opt_longcuts_cull = static_opt_cull;
  assert( static_opt_cull == chopts_c.cull_score ||
          !static_opt_cull && tpn < chopts_c.cull_threshold_tpn );
  const bool opt_loop_after_collision =
    static_wps_hash_loop_after_collision
    && chopts_c.wps_hash_loop_after_collision;

  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  assert( wp_sz == warpSize );
  const int TPN = 1 << tpn;
  const uint32_t tnid_msk = TPN - 1;
  const int block_lg = contract_nodes_tpn_to_block_lg(tpn);
  const int block_dim = contract_nodes_tpn_to_block_size(tpn);
  assert( block_dim == blockDim.x );

  auto sum_nd = [&](nodeid_t v) { return sum_block_grp(block_lg,tpn,v); };
  auto max_nd = [&](nodeid_t v) { return max_block_grp(block_lg,tpn,v); };

  const int grp_lg = 3;
  const int grp_sz = 1 << grp_lg;
  const int grp_mk = grp_sz - 1;

  const int bfb_rnds = 1;

  // Size of backward-forward buffer.
  const int bfb_sz = bfb_rnds * grp_sz;
  // Size of local storage (registers, we hope)
  const int bfb_sz_max = grp_sz;
  assert( bfb_sz <= bfb_sz_max );

#ifdef NDEBUG
  const bool stats_want = false;
#else
  const bool stats_want = true;
#endif
  const bool stats_check = false;
  const bool stats_blk = stats_want && qstats_a;
  const auto qstats = &qstats_a[blockIdx.x];
  const bool stats = stats_blk && threadIdx.x == 0;
  const bool stats_wps = stats_blk && stats_wps_want;
  __shared__ int64_t tbuf, tcomp1, tcomp2, dbuf, dcomp1, dcomp2, *stats_current;
  __shared__ int64_t tfinish, dfinish;

  auto stats_continue =
    [&](int64_t& expect, int64_t& set, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
        assert( &expect == stats_current ); stats_current = &set; }
      set = clock64();
      accum += set - expect;
    };

  auto stats_start =
    [&](int64_t& set)
    {
      if ( !stats ) return;
      if ( stats_check ) { assert( !stats_current ); stats_current = &set; }
      set = clock64();
    };

  auto stats_stop =
    [&](int64_t& expect, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
         assert( &expect == stats_current ); stats_current = NULL; }
      accum += clock64() - expect;
    };

  __shared__ int n_ij, n_iter, n_1hshc, n_1hop_iter, n_coll;

  if ( stats )
    {
      qstats->qstart = clock64();
      dbuf = dcomp1 = dcomp2 = dfinish = 0;
      if ( stats_check ) stats_current = NULL;
      n_ij = 0; n_iter = 0; n_1hshc = 0; n_coll = n_1hop_iter = 0;
    }

  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * block_dim;
  // compute num_threads
  const int num_thd = block_dim * gridDim.x;

  const nodeid_t num_nodes = graph->num_nodes;

  assert(tpn == graph->thread_per_node);

  // increment value in each iteration
  const int vertex_inc = num_thd >> tpn;
  // calculate num_iters to avoid using a while loop to avoid syncthreads imbalance
  const nodeid_t num_iters = div_up(num_nodes, vertex_inc);
  // number of vertices handled at each iteration by each block
  const int vertex_per_iter = block_dim >> tpn;
  

  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)

  const int stride = block_dim + 1;

  const int shared_misc_elts = 8 * 2;
  const int shared_elts_wo_hash =
    4*vertex_per_iter + 4*block_dim + grp_sz * stride;
  const int target_wp_per_mp = 24; // Based on reg use.
  const int target_bl_per_mp = target_wp_per_mp / ( block_dim >> wp_lg );
  const int elts_per_mp = mp_shared_size_bytes / 4;
  const int shared_elts_target = elts_per_mp / target_bl_per_mp;
  const int shm_size_max = shared_elts_target - shared_misc_elts;
  const int shm_size_extra_elts = shm_size_max - shared_elts_wo_hash;
  const int shm_size_extra_bl = shm_size_extra_elts / block_dim;
  assert( shm_size_extra_bl > 0 );
  const int shared_elts = shared_elts_wo_hash + shm_size_extra_bl * block_dim;

  __shared__ uint32_t shm[shared_elts];

  int make_f_pos = 0;
  auto make_f = [&](uint n) {
      const int pos = make_f_pos;
      make_f_pos += n;
      return [=](uint idx) -> uint32_t&
        { assert( !debug_sm_idx || idx < n ); return shm[pos+idx]; };  };

  auto SH_CURRENT_POINTER_F = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_B = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_F = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_B = make_f(vertex_per_iter);

  auto SH_FORWARD_NBR = make_f(block_dim);
  auto SH_FORWARD_WEIGHT = make_f(block_dim);
  auto SH_BACKWARD_POINTER_F = make_f(block_dim);
  auto SH_BACKWARD_NNBR_F = make_f(block_dim);

  auto SH_BACKWARD_FORWARD_DATA = make_f(grp_sz*stride);

  make_f_pos -= 4 * block_dim;

  // Used before cache and bwd_fwd populated.
  auto SH_BACKWARD_NBR = make_f(block_dim);
  auto SH_BACKWARD_WEIGHT = make_f(block_dim);
  // Used after done with cache and bwd_fwd.
  auto SH_FORWARD_NNBR_B = make_f(block_dim);
  auto SH_SCORE = make_f(block_dim);

  const int hash_size_pt_max = (shared_elts - make_f_pos) / block_dim;
  const int hash_size_pt_goal = 100;
  const int hash_size_pt = min(hash_size_pt_max,hash_size_pt_goal);
  assert( hash_size_pt > 0 );
  assert( hash_size_pt == shm_size_extra_bl );

  auto SH_NID_TO_FWD_BWD_TIDX = make_f( block_dim * hash_size_pt );
  assert( shared_elts >= make_f_pos );

  const uint32_t iter_per_node_r = div_up(TPN,bfb_sz);

  nodeid_t current = tid >> tpn;
  // local_ID, thread group's working set id within current block (needed for shared mem accesses)
  const nodeid_t lid = vertex_per_iter > 1 ? threadIdx.x >> tpn : 0;
  // starting current vertex per block (current of thread 0)
  nodeid_t current_0 = (blockIdx.x * block_dim) >> tpn;

  // thread's neighbor id, effectively tid%(1<<tpn)
  const uint32_t tnid = vertex_per_iter > 1 ? threadIdx.x % TPN : threadIdx.x;

  uint32_t* const fwd_hash_base = &SH_NID_TO_FWD_BWD_TIDX(0);
  const int hash_word_per_node_raw = hash_size_pt << tpn;
  const int hash_word_per_node =
    opt_hash_power_2 ? floor2(hash_word_per_node_raw) : hash_word_per_node_raw;
  const int hash_byte_per_node = hash_word_per_node * 4;
  uint32_t* const fwd_hash_lw = fwd_hash_base + lid * hash_word_per_node;
  uint8_t* const fwd_hash_lb = (uint8_t*) fwd_hash_lw;

  auto nid_to_fwd_hash_get =
    [&](nodeid_t nid) -> uint8_t&
    { return fwd_hash_lb[nid%hash_byte_per_node]; };

  const uint8_t hash_empty = 0xff;

  auto hash_clear =
    [&]() { for ( int c = 0;  c < hash_size_pt;  c++ )
              fwd_hash_lw[tnid + (c<<tpn)] = uint32_t(-1); };

  if ( stats_blk ) sync_threads();
  stats_start(tbuf);

  nodeid_t num_longcuts_cut = 0;
  nodeid_t num_longcuts_orig = 0; // Longcuts that are not shortcut edges.
  nodeid_t sum_longcuts_rank = 0;

  for(uint32_t iter = 0; iter < num_iters; iter++){

    // load the pointer, nnbr and node status information for current iteration into shared mem
    /// need an extra check for the last iteration since checking current is not enough (might be avoidable)
    if ( threadIdx.x < vertex_per_iter )
      {
        if ( current_0+threadIdx.x < num_nodes )
          {
            SH_CURRENT_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[current_0+threadIdx.x];
            SH_CURRENT_POINTER_B( threadIdx.x ) = graph->graph_b.pointer[current_0+threadIdx.x];
            SH_CURRENT_NNBR_F( threadIdx.x ) = graph->graph_f.num_neighbors[current_0+threadIdx.x];
            SH_CURRENT_NNBR_B( threadIdx.x ) = graph->graph_b.num_neighbors[current_0+threadIdx.x];
          }
        else
          {
            SH_CURRENT_NNBR_F( threadIdx.x ) = 0;
            SH_CURRENT_NNBR_B( threadIdx.x ) = 0;
          }
      }

    // sync needed here because the first warps are cooperating in loading the entire block's data

    __syncthreads();

    const bool participant = current < num_nodes;

    SH_BACKWARD_NBR( threadIdx.x ) = nodeid_invalid;
    SH_FORWARD_NBR( threadIdx.x ) = nodeid_invalid;

    if ( participant ) {

      // load the current iteration's forward vertices into shared along with their state (rank)
      if(tnid < SH_CURRENT_NNBR_F( lid )){
        const int feidx = SH_CURRENT_POINTER_F( lid ) + tnid;
        const Edge_Nbr_Wht e = opt_elist_use
          ? graph->graph_f.elist[feidx]
          : Edge_Nbr_Wht
            ({graph->graph_f.neighbors[feidx],graph->graph_f.weights[feidx]});
	SH_FORWARD_NBR( threadIdx.x ) = e.neighbor;
	SH_FORWARD_WEIGHT( threadIdx.x ) = e.weight;
      }
      // load the current iteration's backward vertices into shared (check already contracted)
      if(tnid < SH_CURRENT_NNBR_B( lid )){
        const nodeid_t beidx = SH_CURRENT_POINTER_B( lid ) + tnid;
        const Edge_Nbr_Wht e = opt_elist_use
          ? graph->graph_b.elist[beidx]
          : Edge_Nbr_Wht
            ({graph->graph_b.neighbors[beidx],graph->graph_b.weights[beidx]});
	SH_BACKWARD_NBR( threadIdx.x ) = e.neighbor;
	SH_BACKWARD_WEIGHT( threadIdx.x ) = e.weight;
      }
    }

    const nodeid_t lnid = SH_BACKWARD_NBR( threadIdx.x );
    const bool bwd_nbr_here = !nodeid_is_special(lnid);
    const bool fwd_nbr_here = !nodeid_is_special(SH_FORWARD_NBR(threadIdx.x));

    assert( tnid < SH_CURRENT_NNBR_B(lid) || !bwd_nbr_here );

      // no need to sync, threads working on their own data

      // load incoming vertices' forward edges into register space (using shared to speed things up)
      // load pointer/nnbr/status for the incoming neighboring nodes
      /// this might result in horrible branch diversions, threads with different tnid have different loop bounds
      /// might need to change the loop bounds to max_degree and move the degree check to inside the loop

    SH_BACKWARD_POINTER_F( threadIdx.x ) =
      bwd_nbr_here ? graph->graph_f.pointer[lnid] : nodeid_invalid;
    SH_BACKWARD_NNBR_F( threadIdx.x ) =
      bwd_nbr_here ? graph->graph_f.num_neighbors[lnid] : 0;

    // so long as vertices are handled by only one warp no need for this syncthread

    const nodeid_t bwd_wht = SH_BACKWARD_WEIGHT( threadIdx.x );
    const int lnnbr = SH_BACKWARD_NNBR_F( threadIdx.x );

    if(tpn>5){
      __syncthreads();
    }

    stats_continue(tbuf,tcomp1,dbuf);
    const bool use_hash = true;

    int n_shc_needed = 0;

    const int lsh_lg = 5;
    const int lsh_wid = 1 << lsh_lg;
    const int lsh_msk = lsh_wid - 1;

    if ( participant ) {
      const int nnbr_f = SH_CURRENT_NNBR_F( lid );
      const int nnbr_b = SH_CURRENT_NNBR_B( lid );
      const int nfull = !bwd_nbr_here ? -1 : nnbr_f >> lsh_lg;
      const uint32_t b = nnbr_f & lsh_msk;
      n_shc_needed = nfull * 32 + b;

      bool linear_scan = !use_hash && TPN <= 32; // Can be modified below.
      if ( !linear_scan )
        {
          hash_clear();
          if ( tpn > 5 ) __syncthreads();
          if ( tnid < nnbr_f )
            nid_to_fwd_hash_get(SH_FORWARD_NBR(threadIdx.x)) = tnid;
          if ( tpn > 5 ) __syncthreads();
          const uint8_t fwd_nbr_eidx = nid_to_fwd_hash_get(lnid);
          const int fwd_nbr_sidx = ( lid << tpn ) + fwd_nbr_eidx;
          const bool hit =
            fwd_nbr_eidx < nnbr_f && SH_FORWARD_NBR( fwd_nbr_sidx ) == lnid;
          if ( hit ) n_shc_needed--;
          linear_scan = !hit && fwd_nbr_eidx != 0xff;
        }

      // unmark shortcuts to self
      if ( linear_scan )
        for ( int i=0; i<TPN; i++ ) {
          if ( i >= nnbr_f ) break;
          const int e_fwd_sidx = i + ( lid << tpn );
          const nodeid_t fwd_nbr = SH_FORWARD_NBR( e_fwd_sidx );
          if ( lnid != fwd_nbr ) continue;
          n_shc_needed--;
          break;
        }
    }
    stats_stop(tcomp1,dcomp1);
    
    /// Potential shared mem bank conflicts avoided by setting stride to
    /// a power of 2 + 1.
    // we only have bfb_sz backward-forward neighbor per node at any
    // given time (shared size limitations)
    for(uint32_t r=0; r<iter_per_node_r; r++){

      // local storage (hopefully registers)
      nodeid_t lweight[bfb_sz_max];
      nodeid_t lnbr[bfb_sz_max];

      if(tpn>5){
        __syncthreads();
      }

      if ( r )
        {
          const bool thd_has_work =
            participant && grp_sz*r < lnnbr && bwd_nbr_here;
          const uint32_t warp_has_work = __ballot_sync(~0,thd_has_work);
          if ( !warp_has_work ) continue;
        }

      stats_start(tbuf);

      if ( participant )
        {
          const int unid = threadIdx.x & grp_mk;
          const int unt0 = threadIdx.x & ~grp_mk;

          const int bwd_fwd_e_num = grp_sz * r + unid;
          const int nnbr_b = SH_CURRENT_NNBR_B( lid );
          bool edge_here[grp_sz];
          int gidx[grp_sz];

          for ( int j=0; j<grp_sz; j++ )
            {
              const int bwd_sidx = unt0 + j;
              const int bwd_e_num = bwd_sidx & tnid_msk;
              const int bwd_nnbr_f = SH_BACKWARD_NNBR_F( bwd_sidx );
              edge_here[j] = bwd_e_num < nnbr_b && bwd_fwd_e_num < bwd_nnbr_f;
              if ( edge_here[j] )
                gidx[j] = SH_BACKWARD_POINTER_F( bwd_sidx ) + bwd_fwd_e_num;
            }

          if ( opt_elist_use )
            {
              Edge_Nbr_Wht xfer[grp_sz];
              const uint32_t amask = __activemask();

              for ( int j=0; j<grp_sz; j++ )
                if ( edge_here[j] ) xfer[j] = graph->graph_f.elist[ gidx[j] ];

              for ( int j=0; j<grp_sz; j++ )
                SH_BACKWARD_FORWARD_DATA( threadIdx.x + j*stride ) =
                  xfer[j].neighbor;

              if ( grp_sz*r < lnnbr && tnid < nnbr_b )
                for(uint32_t j=0; j<grp_sz; j++)
                  lnbr[j] = SH_BACKWARD_FORWARD_DATA( unid*stride + unt0 + j );
              __syncwarp(amask);

              for ( int j=0; j<grp_sz; j++ )
                SH_BACKWARD_FORWARD_DATA(threadIdx.x+j*stride) = xfer[j].weight;

              if ( grp_sz*r < lnnbr && tnid < nnbr_b )
                for(uint32_t j=0; j<grp_sz; j++)
                  lweight[j] = SH_BACKWARD_FORWARD_DATA(unid*stride + unt0 + j);
              __syncwarp(amask);
            }
          else
            {
              nodeid_t xfer[grp_sz];

#pragma unroll
              for ( int wht = 0; wht < 2; wht++ )
                {
                  nodeid_t* const data_ptr =
                    wht ? graph->graph_f.weights : graph->graph_f.neighbors;

                  for ( int j=0; j<grp_sz; j++ )
                    if ( edge_here[j] ) xfer[j] = data_ptr[ gidx[j] ];

                  for ( int j=0; j<grp_sz; j++ )
                    SH_BACKWARD_FORWARD_DATA(threadIdx.x + j*stride) = xfer[j];

                  if ( grp_sz*r < lnnbr && tnid < nnbr_b )
                    for(uint32_t j=0; j<grp_sz; j++)
                      ( wht ? lweight : lnbr) [j]
                        = SH_BACKWARD_FORWARD_DATA( unid * stride + unt0 + j );
                }
            }
        }

      stats_continue(tbuf,tcomp2,dbuf);

      if ( participant && grp_sz*r < lnnbr && bwd_nbr_here ){

        // Remove edges for which a shorter path is found.
        auto cull = [&](int l_idx)
          {
            const nodeid_t fwd_e_gidx =
              SH_BACKWARD_POINTER_F( threadIdx.x ) + grp_sz*r + l_idx;
            const nodeid_t dst_prev =
              atomicExch(&graph->graph_f.neighbors[fwd_e_gidx],nodeid_invalid);
            if ( nodeid_is_special(dst_prev) ) return;
            num_longcuts_cut++;
            const nodeid_t bwd_e_gidx = graph->graph_f.midpoint[fwd_e_gidx];
            assert( true || graph->graph_b.neighbors[bwd_e_gidx] == lnid );
            graph->graph_b.neighbors[bwd_e_gidx] = nodeid_invalid;
            if ( !opt_static_cull_want_age ) return;
            const nodeid_t mid_nid_or = graph->graph_b.midpoint[bwd_e_gidx];
            if ( mid_nid_or != NODEID_NULL )
              {
                const nodeid_t mid_nid_ud = graph_UD_c.node_idx_inv[mid_nid_or];
                const nodeid_t rank = graph_UD_c.node_ranks[mid_nid_ud];
                sum_longcuts_rank += rank;
              }
            else
              {
                num_longcuts_orig++;
              }
          };

        const int fwd_nnbr = SH_CURRENT_NNBR_F( lid );

        bool collision = false;

        // each thread calculate its potential shortcuts and do the
        // 1-hop search

	/// for the following code to be correct, there can be no duplicate/alternate edges between
	/// the same two nodes
        if ( use_hash )
          {
            int n_shc_avoided = 0;
            int longcut_idx = -1;
            for ( int j=0; j<grp_sz; j++ )
              if ( grp_sz*r + j < lnnbr )
                {
                  if ( stats_wps ) atomicAdd(&n_ij, 1);
                  const nodeid_t bwd_fwd_nbr = lnbr[j];
                  if ( nodeid_is_special( bwd_fwd_nbr ) ) continue;
                  const uint8_t fwd_eidx = nid_to_fwd_hash_get(bwd_fwd_nbr);
                  if ( fwd_eidx == hash_empty ) continue;
                  const int fwd_nbr_sidx = ( lid << tpn ) + fwd_eidx;
                  const bool hit =
                    SH_FORWARD_NBR( fwd_nbr_sidx ) == bwd_fwd_nbr;
                  if ( !hit )
                    {
                      if ( stats_wps ) atomicAdd(&n_coll,1);
                      collision = true;
                      break;
                    }
                  if ( stats_wps ) atomicAdd(&n_1hshc,1);
                  const nodeid_t path_wht =
                    bwd_wht + SH_FORWARD_WEIGHT(fwd_nbr_sidx);
                  if ( lweight[j] <= path_wht ){ n_shc_avoided++; continue; }
                  if ( !opt_longcuts_cull ) continue;
                  if ( cull_inner_loop ) cull(j); else longcut_idx = j;
                }
            if ( !collision ) n_shc_needed -= n_shc_avoided;
            if ( opt_longcuts_cull && longcut_idx >= 0 ) cull(longcut_idx);
          }
        const bool need_iterative_loop = !use_hash || collision;
        if ( opt_loop_after_collision && need_iterative_loop )
          {
            if ( stats_wps ) atomicAdd(&n_iter,1);
            for (uint32_t i=0; i<TPN ; i++){
              if ( i >= fwd_nnbr ) break;

              const int e_fwd_sidx = i + ( lid << tpn );
              const nodeid_t fwd_nbr = SH_FORWARD_NBR( e_fwd_sidx );
              const nodeid_t path_wht = bwd_wht + SH_FORWARD_WEIGHT(e_fwd_sidx);

              if ( lnid == fwd_nbr ) continue;

              for ( int j=0; j<grp_sz; j++ ){
                if ( grp_sz*r + j < lnnbr
                     && lnbr[j] == fwd_nbr && lweight[j] <= path_wht ){
                  if ( stats_wps ) atomicAdd(&n_1hop_iter,1);
                  n_shc_needed--;
                  break;
                }
              }
            }
          }
      }

      stats_stop(tcomp2,dcomp2);

    } // End of r loop.

    if ( tpn > 5 ) __syncthreads();

    stats_start(tfinish);

    /// this is only used to calculate max_nbr_degree which is used
    /// as a score criterion
    //
    /// if graph is symmetrical this is not necessary, even for
    /// non-symmetrical graphs, it might be a good compromise to
    /// assume symmetry, not loading the forward-backward nnbr and
    /// only use the backward-forward NNBR
    //
    SH_FORWARD_NNBR_B( threadIdx.x ) = participant && fwd_nbr_here ?
      graph->graph_b.num_neighbors[SH_FORWARD_NBR( threadIdx.x )] : 0;

    // current thread's confirmed shortcuts
    const uint32_t shc_count = bwd_nbr_here ? n_shc_needed : 0;

    // so long as vertices are handled by only one warp no need for this syncthread
    __syncthreads();

    Score_Components sc;
    const int shc_count_nd = sum_nd(shc_count);
    const nodeid_t my_bwd_wht = bwd_nbr_here ? bwd_wht : 0;
    const nodeid_t my_fwd_wht =
      fwd_nbr_here ? SH_FORWARD_WEIGHT(threadIdx.x) : 0;
    const nodeid_t my_wht = my_fwd_wht + my_bwd_wht;
    sc.wht_sum = sum_nd(my_wht);
    sc.wht_max = max_nd( max( my_fwd_wht, my_bwd_wht ) );
    sc.nbr_deg_max =
      max_nd( max( SH_BACKWARD_NNBR_F( threadIdx.x ),
                   SH_FORWARD_NNBR_B( threadIdx.x )) );
    sc.deg = sum_nd( int(bwd_nbr_here) + int(fwd_nbr_here) );

    if ( tnid == 0 )
      *(float *)&SH_SCORE( lid ) =
        cu_CH_score_compute(graph_c,sc,shc_count_nd);

    __syncthreads();

    // write back the scores to global mem
    /// need an extra check for the last iteration since checking current is not enough (might be avoidable)
    if(threadIdx.x < vertex_per_iter && (current_0 + threadIdx.x < num_nodes)){
      node_scores[current_0 + threadIdx.x] = *(float *)&SH_SCORE( threadIdx.x );
    }

    // setup next iteration
    current += vertex_inc;
    current_0 += vertex_inc;
    __syncthreads();

    stats_continue(tfinish,tbuf,dfinish);
  }

  if ( opt_longcuts_cull )
    {
      atomicAdd( &graph_c.self_d->num_longcuts_cut, num_longcuts_cut );
      if ( opt_static_cull_want_age )
        {
          atomicAdd( &graph_c.self_d->num_longcuts_orig, num_longcuts_orig );
          atomicAdd( &graph_c.self_d->sum_longcuts_rank, sum_longcuts_rank );
        }
    }

  if ( !stats || threadIdx.x >= 32 ) return;

  if ( stats )
    {
      qstats->qend = clock64();
      qstats->dbuf = dbuf;
      qstats->dcomp1 = dcomp1;
      qstats->dcomp2 = dcomp2;
      qstats->dfinish = dfinish;
      qstats->n_ij = n_ij;
      qstats->n_iter = n_iter;
      qstats->n_1hshc = n_1hshc;
      qstats->n_1hop_iter = n_1hop_iter;
      qstats->n_coll = n_coll;
    }
}


template <uint32_t tpn, bool static_opt_cull> __global__ void
__launch_bounds__(1 << score_nodes_tpn_to_block_size_blocked_lg)
cu_CH_score_nodes_blocked
(cu_graph_CH_bi_t graph_c, float *node_scores,
 cu_CH_query_stats *qstats_a, Score_Components *score_components)
{
  const cu_graph_CH_bi_t* const graph = &graph_c;

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  constexpr bool opt_longcuts_cull = static_opt_cull;
  const bool opt_loop_after_collision =
    static_wps_hash_loop_after_collision
    && chopts_c.wps_hash_loop_after_collision;

  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  assert( wp_sz == warpSize );
  const int TPN = 1 << tpn;
  const int block_lg = score_nodes_tpn_to_block_size_blocked_lg;
  const int block_dim = 1 << block_lg;
  assert( block_dim == blockDim.x );
  const int sq_block_per_node = TPN >> block_lg;
  const int block_per_node = sq_block_per_node*sq_block_per_node;

  auto sum_nd = [&](nodeid_t v) { return sum_block_grp(block_lg,block_lg,v); };
  auto max_nd = [&](nodeid_t v) { return max_block_grp(block_lg,block_lg,v); };

  const int grp_lg = 3;
  const int grp_sz = 1 << grp_lg;
  const int grp_mk = grp_sz - 1;

  const int bfb_rnds = 1;

  // Size of backward-forward buffer.
  const int bfb_sz = bfb_rnds * grp_sz;
  // Size of local storage (registers, we hope)
  const int bfb_sz_max = grp_sz;
  assert( bfb_sz <= bfb_sz_max );

#ifdef NDEBUG
  const bool stats_want = false;
#else
  const bool stats_want = true;
#endif
  const bool stats_check = false;
  const bool stats_blk = stats_want && qstats_a;
  const auto qstats = &qstats_a[blockIdx.x];
  const bool stats = stats_blk && threadIdx.x == 0;
  const bool stats_wps = stats_blk && stats_wps_want;
  __shared__ int64_t tbuf, tcomp1, tcomp2, dbuf, dcomp1, dcomp2, *stats_current;
  __shared__ int64_t tfinish, dfinish;

  auto stats_continue =
    [&](int64_t& expect, int64_t& set, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
        assert( &expect == stats_current ); stats_current = &set; }
      set = clock64();
      accum += set - expect;
    };

  auto stats_start =
    [&](int64_t& set)
    {
      if ( !stats ) return;
      if ( stats_check ) { assert( !stats_current ); stats_current = &set; }
      set = clock64();
    };

  auto stats_stop =
    [&](int64_t& expect, int64_t& accum)
    {
      if ( !stats ) return;
      if ( stats_check ) {
         assert( &expect == stats_current ); stats_current = NULL; }
      accum += clock64() - expect;
    };

  __shared__ int n_ij, n_iter, n_1hshc, n_1hop_iter, n_coll;

  if ( stats )
    {
      qstats->qstart = clock64();
      dbuf = dcomp1 = dcomp2 = dfinish = 0;
      if ( stats_check ) stats_current = NULL;
      n_ij = 0; n_iter = 0; n_1hshc = 0; n_coll = n_1hop_iter = 0;
    }

  const nodeid_t num_nodes = graph->num_nodes;

  assert(tpn == graph->thread_per_node);

  // number of vertices handled at each iteration by each block (technically multiple blocks here!)
  const int vertex_per_iter = 1; // To be honest, 1/block_per_node.

  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)

  const int stride = block_dim + 1;

  const int shared_misc_elts = 8 * 2;
  const int shared_elts_wo_hash =
    4*vertex_per_iter + 4*block_dim + grp_sz * stride;
  const int target_wp_per_mp = 24; // Based on reg use.
  const int target_bl_per_mp = target_wp_per_mp / ( block_dim >> wp_lg );
  const int elts_per_mp = mp_shared_size_bytes / 4;
  const int shared_elts_target = elts_per_mp / target_bl_per_mp;
  const int shm_size_max = shared_elts_target - shared_misc_elts;
  const int shm_size_extra_elts = shm_size_max - shared_elts_wo_hash;
  const int shm_size_extra_bl = shm_size_extra_elts / block_dim;
  assert( shm_size_extra_bl > 0 );
  const int shared_elts = shared_elts_wo_hash + shm_size_extra_bl * block_dim;

  __shared__ uint32_t shm[shared_elts];

  int make_f_pos = 0;
  auto make_f = [&](uint n) {
      const int pos = make_f_pos;
      make_f_pos += n;
      return [=](uint idx) -> uint32_t&
        { assert( !debug_sm_idx || idx < n ); return shm[pos+idx]; };  };

  auto SH_CURRENT_POINTER_F = make_f(vertex_per_iter);
  auto SH_CURRENT_POINTER_B = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_F = make_f(vertex_per_iter);
  auto SH_CURRENT_NNBR_B = make_f(vertex_per_iter);

  auto SH_FORWARD_NBR = make_f(block_dim);
  auto SH_FORWARD_WEIGHT = make_f(block_dim);
  auto SH_BACKWARD_POINTER_F = make_f(block_dim);
  auto SH_BACKWARD_NNBR_F = make_f(block_dim);

  auto SH_BACKWARD_FORWARD_DATA = make_f(grp_sz*stride);

  make_f_pos -= 3 * block_dim;

  // Used before cache and bwd_fwd populated.
  auto SH_BACKWARD_NBR = make_f(block_dim);
  auto SH_BACKWARD_WEIGHT = make_f(block_dim);
  // Used after done with cache and bwd_fwd.
  auto SH_FORWARD_NNBR_B = make_f(block_dim);

  const int hash_size_pt_max = (shared_elts - make_f_pos) / block_dim;
  const int hash_size_pt_goal = 100;
  const int hash_size_pt = min(hash_size_pt_max,hash_size_pt_goal);
  assert( hash_size_pt > 0 );
  assert( hash_size_pt == shm_size_extra_bl );

  auto SH_NID_TO_FWD_BWD_TIDX = make_f( block_dim * hash_size_pt );
  assert( shared_elts >= make_f_pos );

  const uint32_t iter_per_node_r = div_up(TPN,bfb_sz);

  uint32_t* const fwd_hash_base = &SH_NID_TO_FWD_BWD_TIDX(0);
  const int hash_word_per_node_raw = hash_size_pt << block_lg;
  const int hash_word_per_node =
    opt_hash_power_2 ? floor2(hash_word_per_node_raw) : hash_word_per_node_raw;
  const int hash_byte_per_node = 4 * hash_word_per_node;

  uint32_t* const fwd_hash_lw = fwd_hash_base;
  uint8_t* const fwd_hash_lb = (uint8_t*) fwd_hash_lw;

  auto nid_to_fwd_hash_get =
    [&](nodeid_t nid) -> uint8_t&
    { return fwd_hash_lb[nid%hash_byte_per_node]; };

  const uint8_t hash_empty = 0xff;
  assert( block_dim <= 256 ); // Hash won't work if threadIdx.x > 255.

  auto hash_clear =
    [&]() { for ( int c = 0;  c < hash_size_pt;  c++ )
              fwd_hash_lw[threadIdx.x + ( c << block_lg ) ] = uint32_t(-1); };

  if ( stats_blk ) sync_threads();
  stats_start(tbuf);

  nodeid_t num_longcuts_cut = 0;
  const nodeid_t work_limit = num_nodes * block_per_node;

  while ( true ) {
    __shared__ nodeid_t work_item;
    if ( !threadIdx.x )
      work_item = atomicAdd(&graph_c.self_d->work_item_next,1);
    __syncthreads();
    if ( work_item >= work_limit ) break;

    const nodeid_t current = work_item / block_per_node;
    const int lbid = work_item % block_per_node;
    const int lbid_b = lbid / sq_block_per_node;
    const int lbid_f = lbid % sq_block_per_node;

    // load the pointer, nnbr and node status information for current iteration into shared mem
    if ( threadIdx.x == 0 ){
      SH_CURRENT_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[current];
      SH_CURRENT_POINTER_B( threadIdx.x ) = graph->graph_b.pointer[current];
      SH_CURRENT_NNBR_F( threadIdx.x ) = graph->graph_f.num_neighbors[current];
      SH_CURRENT_NNBR_B( threadIdx.x ) = graph->graph_b.num_neighbors[current];
    }

    // sync needed here because the first warps are cooperating in loading the entire block's data
    __syncthreads();

    const int nnbr_f = SH_CURRENT_NNBR_F( 0 );
    const int nnbr_b = SH_CURRENT_NNBR_B( 0 );
    const int fwd_sidx_stop = nnbr_f - lbid_f * block_dim;
    const int bwd_sidx_stop = nnbr_b - lbid_b * block_dim;
    const bool fwd_elt_here = threadIdx.x < fwd_sidx_stop;
    const bool bwd_elt_here = threadIdx.x < bwd_sidx_stop;

    if ( lbid_f*block_dim < nnbr_f  &&  lbid_b*block_dim < nnbr_b ) {

      // load the current iteration's forward vertices into shared along with their state (rank)
      if ( fwd_elt_here ) {
        const int feidx = SH_CURRENT_POINTER_F( 0 ) + lbid_f*block_dim + threadIdx.x;
        const nodeid_t fid = graph->graph_f.neighbors[feidx];
        SH_FORWARD_NBR( threadIdx.x ) = fid;
        SH_FORWARD_WEIGHT( threadIdx.x ) = graph->graph_f.weights[feidx];
      } else {
        SH_FORWARD_NBR( threadIdx.x ) = nodeid_invalid;
      }

      // load the current iteration's backward vertices into shared (check already contracted)

      const nodeid_t beidx =
        SH_CURRENT_POINTER_B( 0 ) + lbid_b*block_dim + threadIdx.x;
      const nodeid_t lnid =
        bwd_elt_here ? graph->graph_b.neighbors[beidx] : nodeid_invalid;
      SH_BACKWARD_NBR( threadIdx.x ) = lnid;
      const bool bwd_here = !nodeid_is_special(lnid);

      if ( bwd_here ) {
        SH_BACKWARD_WEIGHT( threadIdx.x ) = graph->graph_b.weights[beidx];

	// no need to sync, threads working on their own data
	
	// load incoming vertices' forward edges into register space (using shared to speed things up)
	// load pointer/nnbr for the incoming neighboring nodes

        SH_BACKWARD_POINTER_F( threadIdx.x ) = graph->graph_f.pointer[lnid];
        SH_BACKWARD_NNBR_F( threadIdx.x ) = graph->graph_f.num_neighbors[lnid];
      } else {
        SH_BACKWARD_NNBR_F( threadIdx.x ) = 0;
      }

      const bool fwd_here = !nodeid_is_special(SH_FORWARD_NBR(threadIdx.x));

      const nodeid_t bwd_wht = SH_BACKWARD_WEIGHT( threadIdx.x );
      const int lnnbr = SH_BACKWARD_NNBR_F( threadIdx.x );
      
      __syncthreads();
      
      stats_continue(tbuf,tcomp1,dbuf);

      // Initialize n_shc_needed to the number of paths through
      // current starting at the backward neighbor assigned to this thread ..
      // .. and ending at forward neighbors in the current block (lbid_f) ..
      // .. except for any forward neighbor equal to this thread's
      // backward neighbor.


      // If true, use hash table to find whether lnid matches any fwd edge.
      const bool use_hash = true;
      
      int n_shc_needed = 0;

      {
        n_shc_needed = fwd_sidx_stop < block_dim ? fwd_sidx_stop : block_dim;
	
	bool linear_scan = !use_hash; // Can be modified below.
	if ( !linear_scan ){

          // Clear hash table entries to hash_empty (0xff).
	  hash_clear();

	  __syncthreads();

          // Write forward edges, except for forward edge assigned
          // to thread number hash_empty. 
          //
	  if ( ( block_dim < hash_empty || threadIdx.x < hash_empty )
               && fwd_here )
	    nid_to_fwd_hash_get(SH_FORWARD_NBR(threadIdx.x)) = threadIdx.x;
          //
          // Note that it would not be possible to detect a collision
          // if thread number hash_empty wrote the table after another
          // colliding thread. That's because a collision is concluded
          // when the node id's don't match and the element value is
          // other than hash_empty. If the element value is hash_empty
          // then the entry is concluded to be empty.
          //
          // For example, tid 250 carries node 0x9 and tid 255
          // (assuming hash_empty = 255) carries node 0x100009. Both
          // map to hash table entry number 9. The code above would
          // write a 250. A lookup for 0x9 would hit and a lookup for
          // 0x100009 would be a detected collision. If instead tid
          // 255 wrote the table after 250, a lookup for 0x100009
          // would hit, and a lookup for 0x9 would conclude that the
          // entry was empty.

	  __syncthreads();

          if ( bwd_here )
            {
              const uint8_t fwd_nbr_eidx = nid_to_fwd_hash_get(lnid);
              const int fwd_nbr_sidx = fwd_nbr_eidx;
              const bool hit =
                ( block_dim >= 256 || fwd_nbr_eidx != hash_empty )
                && SH_FORWARD_NBR( fwd_nbr_sidx ) == lnid;
              if ( hit ) n_shc_needed--;
              linear_scan = !hit && fwd_nbr_eidx != 0xff;
            }
	}
	
	// Use a linear scan if a hash table collides or is not used.
	if ( linear_scan ){
	  for ( int i=0; i<block_dim; i++ ) {
	    if ( i + lbid_f*block_dim >= nnbr_f ) break;
	    const int e_fwd_sidx = i;
	    const nodeid_t fwd_nbr = SH_FORWARD_NBR( e_fwd_sidx );
	    if ( lnid != fwd_nbr ) continue;
	    n_shc_needed--;
	    break;
	  }
	}
      }
      stats_stop(tcomp1,dcomp1);
    
      /// Potential shared mem bank conflicts avoided by setting stride to
      /// a power of 2 + 1.
      // we only have bfb_sz backward-forward neighbor per node at any
      // given time (shared size limitations)
      for(uint32_t r=0; r<iter_per_node_r; r++){

	// local storage (hopefully registers)
	nodeid_t lweight[bfb_sz_max];
	nodeid_t lnbr[bfb_sz_max];

	__syncthreads();

        if ( r )
          {
            const bool thd_has_work = grp_sz*r < lnnbr && bwd_here;
            const uint32_t warp_has_work = __ballot_sync(~0,thd_has_work);
            if ( !warp_has_work ) continue;
          }

	stats_start(tbuf);

        {
	  const int unid = threadIdx.x & grp_mk;
	  const int unt0 = threadIdx.x & ~grp_mk;
	
	  const int bwd_fwd_e_num = grp_sz * r + unid;
	  const int nnbr_b = SH_CURRENT_NNBR_B( 0 );
	  bool edge_here[grp_sz];
	  int gidx[grp_sz];
	  nodeid_t xfer[grp_sz];

	  for ( int j=0; j<grp_sz; j++ ){
	    const int bwd_sidx = unt0 + j;
	    const int bwd_e_num = bwd_sidx;
	    const int bwd_nnbr_f = SH_BACKWARD_NNBR_F( bwd_sidx );
	    edge_here[j] = bwd_e_num + lbid_b*block_dim < nnbr_b && bwd_fwd_e_num < bwd_nnbr_f;
	    if ( edge_here[j] ){
	      gidx[j] = SH_BACKWARD_POINTER_F( bwd_sidx ) + bwd_fwd_e_num;
	    }
	  }

#pragma unroll
	  for ( int wht = 0; wht < 2; wht++ ){
	    nodeid_t* const data_ptr =
	      wht ? graph->graph_f.weights : graph->graph_f.neighbors;

	    for ( int j=0; j<grp_sz; j++ )
	      if ( edge_here[j] ) xfer[j] = data_ptr[ gidx[j] ];

	    for ( int j=0; j<grp_sz; j++ )
	      SH_BACKWARD_FORWARD_DATA( threadIdx.x + j*stride ) = xfer[j];

	    if ( grp_sz*r < lnnbr && threadIdx.x + lbid_b*block_dim < nnbr_b ){
	      for(uint32_t j=0; j<grp_sz; j++){
		( wht ? lweight : lnbr) [j]
		  = SH_BACKWARD_FORWARD_DATA( unid * stride + unt0 + j );
	      }
	    }
	  }
	}

        stats_continue(tbuf,tcomp2,dbuf);

	if ( grp_sz*r < lnnbr && bwd_here ) {

          // Remove edges for which a shorter path is found.
          auto cull = [&](int l_idx)
            {
              const nodeid_t fwd_e_gidx =
                SH_BACKWARD_POINTER_F( threadIdx.x ) + grp_sz*r + l_idx;
              const nodeid_t dst_prev =
                atomicExch
                ( &graph->graph_f.neighbors[fwd_e_gidx], nodeid_invalid );
              if ( nodeid_is_special(dst_prev) ) return;
              num_longcuts_cut++;
              const nodeid_t bwd_e_gidx=graph->graph_f.midpoint[fwd_e_gidx];
              assert( true || graph->graph_b.neighbors[bwd_e_gidx]==lnid );
              graph->graph_b.neighbors[bwd_e_gidx] = nodeid_invalid;
            };

	  // each thread calculate its potential shortcuts and do the
	  // 1-hop search

	  const int fwd_nnbr = SH_CURRENT_NNBR_F( 0 );

	  bool collision = false;

	  /// for the following code to be correct, there can be no duplicate/alternate edges between
	  /// the same two nodes
	  if ( use_hash ){
	    int n_shc_avoided = 0;
            int longcut_idx = -1;
	    for ( int j=0; j<grp_sz; j++ ){
	      if ( grp_sz*r + j < lnnbr ){
                if ( stats_wps ) atomicAdd(&n_ij, 1);
		const nodeid_t bwd_fwd_nbr = lnbr[j];
                if ( nodeid_is_special( bwd_fwd_nbr ) ) continue;
		const uint8_t fwd_eidx = nid_to_fwd_hash_get(bwd_fwd_nbr);
                if ( block_dim < 256 && fwd_eidx == hash_empty ) continue;
		const int fwd_nbr_sidx = fwd_eidx;
		const bool hit = SH_FORWARD_NBR( fwd_nbr_sidx ) == bwd_fwd_nbr;
		if ( !hit )
                  {
                    if ( block_dim >= 256 && fwd_eidx == hash_empty ) continue;
                    if ( stats_wps ) atomicAdd(&n_coll,1);
                    collision = true;
                    break;
                  }
                if ( stats_wps ) atomicAdd(&n_1hshc,1);
		const nodeid_t path_wht =
		  bwd_wht + SH_FORWARD_WEIGHT(fwd_nbr_sidx);
                if ( lweight[j] <= path_wht ){ n_shc_avoided++; continue; }
                if ( !opt_longcuts_cull ) continue;
                if ( cull_inner_loop ) cull(j); else longcut_idx = j;
	      }
	    }
	    if ( !collision ) n_shc_needed -= n_shc_avoided;
            if ( opt_longcuts_cull && longcut_idx >= 0 ) cull(longcut_idx);
	  }
	  const bool need_iterative_loop = !use_hash || collision;
	  if ( opt_loop_after_collision && need_iterative_loop ){
            if ( stats_wps ) atomicAdd(&n_iter,1);
	    for (uint32_t i=0; i<block_dim ; i++){
	      if ( i + lbid_f*block_dim >= fwd_nnbr ) break;
	    
	      const int e_fwd_sidx = i;
	      const nodeid_t fwd_nbr = SH_FORWARD_NBR( e_fwd_sidx );
	      const nodeid_t path_wht = bwd_wht + SH_FORWARD_WEIGHT(e_fwd_sidx);

	      if ( lnid == fwd_nbr ) continue;

	      for ( int j=0; j<grp_sz; j++ ){
		if ( grp_sz*r + j < lnnbr
		     && lnbr[j] == fwd_nbr && lweight[j] <= path_wht ){
                  if ( stats_wps ) atomicAdd(&n_1hop_iter,1);
		  n_shc_needed--;
		  break;
		}
	      }
	    }
	  }
	}
        stats_stop(tcomp2,dcomp2);

      } // End of r loop.

      __syncthreads();

      stats_start(tfinish);

      /// this is only used to calculate max_nbr_degree which is used
      /// as a score criterion
      //
      /// if graph is symmetrical this is not necessary, even for
      /// non-symmetrical graphs, it might be a good compromise to
      /// assume symmetry, not loading the forward-backward nnbr and
      /// only use the backward-forward NNBR
      //
      if ( lbid_b == 0){
	if ( fwd_here ){
	  SH_FORWARD_NNBR_B( threadIdx.x )
	    = graph->graph_b.num_neighbors[SH_FORWARD_NBR( threadIdx.x )];
	}
      }
      if(lbid_f == 0){
	SH_BACKWARD_WEIGHT( threadIdx.x ) = bwd_wht;
      }

      //
      // Write information needed to compute score.
      //

      // current thread's confirmed shortcuts
      const int shc_count_nd = sum_block( bwd_here ? n_shc_needed : 0 );
      if ( threadIdx.x == 0 )
        atomicAdd( &node_scores[current], float(shc_count_nd) );

      if ( !lbid_f || !lbid_b )
        {
          const bool bwd_use = !lbid_f && bwd_here;
          const bool fwd_use = !lbid_b && fwd_here;
          const nodeid_t my_bwd_wht = bwd_use ? bwd_wht : 0;
          const nodeid_t my_fwd_wht =
            fwd_use ? SH_FORWARD_WEIGHT(threadIdx.x) : 0;
          const nodeid_t my_wht = my_fwd_wht + my_bwd_wht;
          const nodeid_t wt_sum = sum_nd(my_wht);
          const nodeid_t wht_max = max_nd( max( my_fwd_wht, my_bwd_wht ) );
          const nodeid_t nbr_deg_max =
            max_nd( max( bwd_use ? SH_BACKWARD_NNBR_F( threadIdx.x ) : 0,
                         fwd_use ? SH_FORWARD_NNBR_B( threadIdx.x )  : 0 ) );
          const int true_deg = sum_nd( int(bwd_use) + int(fwd_use) );

          if ( threadIdx.x == 0 )
            {
              Score_Components* const sc = &score_components[current];

              atomicAdd( &sc->deg, true_deg );
              atomicAdd( &sc->wht_sum, wt_sum );
              atomicMax( &sc->nbr_deg_max, nbr_deg_max );
              atomicMax( &sc->wht_max, wht_max );
            }
        }

    } else { // else for out of range blocks condition
      stats_continue(tbuf,tfinish,dbuf);
    }

    __syncthreads();

    stats_continue(tfinish,tbuf,dfinish);
  }

  if ( opt_longcuts_cull )
    {
      atomicAdd( &graph_c.self_d->num_longcuts_cut, num_longcuts_cut );
    }

  if ( !stats || threadIdx.x >= 32 ) return;

  if ( stats )
    {
      qstats->qend = clock64();
      qstats->dbuf = dbuf;
      qstats->dcomp1 = dcomp1;
      qstats->dcomp2 = dcomp2;
      qstats->dfinish = dfinish;
      qstats->n_ij = n_ij;
      qstats->n_iter = n_iter;
      qstats->n_1hshc = n_1hshc;
      qstats->n_1hop_iter = n_1hop_iter;
      qstats->n_coll = n_coll;
    }

}

__global__ void __launch_bounds__(32)
cu_CH_score_nodes_blocked_2
(cu_graph_CH_bi_t graph_c,
 const Score_Components* __restrict__ sc, float* __restrict__ scores )
{
  constexpr int block_dim = 32;
  assert( block_dim == blockDim.x );

  const uint n_threads = gridDim.x * block_dim;
  const uint tid = blockIdx.x * block_dim + threadIdx.x;
  const nodeid_t n_nodes = graph_c.num_nodes;
  for ( nodeid_t nid = tid;  nid < n_nodes;  nid += n_threads )
    scores[nid] = cu_CH_score_compute( graph_c, sc[nid], scores[nid] );
}


// instantiate the templates

map<int,array<LConfig,2>>
configs_score_get(GPU_Info& gpu_info)
{
  map<int,array<LConfig,2>> configs;
  const int nmps = gpu_info.cuda_prop.multiProcessorCount;

  auto occupancy_sc =
    [&](int tpn, bool cull, int block_lg, Func_K_Score k_ptr)
    {
      LConfig& config = configs[tpn][cull];
      config.inited = true;
      config.tie_breaker = 0;
      config.shared_dim = 0;
      config.block_dim = 1 << block_lg;
      config.k_sc = k_ptr;
      CE( cudaFuncGetAttributes(&config.cfa, k_ptr) );
      CE( cudaOccupancyMaxActiveBlocksPerMultiprocessor
          ( &config.blks_per_mp, k_ptr,
            config.block_dim, config.shared_dim) );
      config.grid_dim = config.blks_per_mp * nmps;
      const int TPN = 1 << tpn;
      const bool blocked = block_lg < tpn;
      const double thd_per_tpn = config.block_dim / double( TPN );
      config.nd_per_blk_iter =
        blocked ? thd_per_tpn * thd_per_tpn : thd_per_tpn;
      config.nd_occ = config.nd_per_blk_iter * config.grid_dim;
    };

#define INST_C(tpn,cull) \
  occupancy_sc \
    (tpn,cull,contract_nodes_tpn_to_block_lg(tpn), \
  cu_CH_score_nodes<tpn,cull>);

#define INST(tpn) INST_C(tpn,false) INST_C(tpn,true)
  INST(3); INST(4); INST(5); INST(6); INST(7); INST(8);
  assert( score_nodes_tpn_to_block_size_blocked_lg == 8 );

#undef INST_C
#define INST_C(tpn,cull) \
  occupancy_sc \
    (tpn,cull,score_nodes_tpn_to_block_size_blocked_lg, \
     cu_CH_score_nodes_blocked<tpn,cull>);

  // RRRR technically this can be defined infinitely, realistically it
  // should not be needed past 10 (degree of 1024 which corresponds to
  // a complete graph for current K) even if blocked version is used
  // starting from TPN==5, currently it is defined only till 4096
  //
  INST(9); INST(10); INST(11); INST(12);

  return configs;
}
