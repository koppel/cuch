/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#undef NDEBUG
#include <assert.h>
#include <set>
#include "cu_main.h"
#include "cu_verify.h"
#include "cu_CH.h"
#include "cu_util_timing.h"
#include "util/pstring.h"

using namespace std;


// Lightweight self consistency check.
void
Contract_Dist_Verify::verify_self_consistency
(cu_graph_CH_bi_t *g_d, bool check_fwd)
{
  cu_graph_CH_bi_t *g;
  graph_cu_CH_bi_d2h(&g, g_d);
  const nodeid_t nnodes = g->num_nodes;
  const bool zero_wht_okay = n_zero_wht;
  assert( nnodes == g->graph_f.num_nodes );
  assert( nnodes == g->graph_b.num_nodes );
  vector<nodeid_t> bwd_nbr_seen(nnodes,nnodes);
  unordered_map<Ege,nodeid_t> bwd_weights;
  for ( nodeid_t nid = 0; nid < nnodes; nid++ )
    {
      int nnbr_b = g->graph_b.num_neighbors[nid];
      nodeid_t e_bwd_0 = g->graph_b.pointer[nid];
      for ( int nidx = 0; nidx < nnbr_b; nidx++ )
        {
          const nodeid_t nbr_b = g->graph_b.neighbors[e_bwd_0 + nidx];
          if ( nodeid_is_special(nbr_b) ) continue;
          const nodeid_t wht_b = g->graph_b.weights[e_bwd_0 + nidx];
          assert( nbr_b != nid );
          assert( nbr_b < nnodes );
          assert( bwd_nbr_seen[nbr_b] != nid );
          assert( zero_wht_okay || wht_b > 0 );
          bwd_nbr_seen[nbr_b] = nid;
          if ( check_fwd )
            {
              Ege edge(nid,nbr_b);
              assert( bwd_weights.count(edge) == 0 );
              bwd_weights[edge] = wht_b;
            }
        }
    }

  if ( check_fwd )
    {
      vector<nodeid_t> fwd_nbr_seen(nnodes,nnodes);
      for ( nodeid_t nid = 0; nid < nnodes; nid++ )
        {
          int nnbr_f = g->graph_f.num_neighbors[nid];
          nodeid_t e_fwd_0 = g->graph_f.pointer[nid];
          for ( int nidx = 0; nidx < nnbr_f; nidx++ )
            {
              const nodeid_t nbr_f = g->graph_f.neighbors[e_fwd_0 + nidx];
              if ( nodeid_is_special(nbr_f) ) continue;
              const nodeid_t wht_f = g->graph_f.weights[e_fwd_0 + nidx];
              assert( nbr_f != nid );
              assert( nbr_f < nnodes );
              assert( fwd_nbr_seen[nbr_f] != nid );
              assert( zero_wht_okay || wht_f > 0 );
              fwd_nbr_seen[nbr_f] = nid;
              Ege edge(nbr_f,nid);
              assert( bwd_weights.count(edge) == 1 );
              assert( bwd_weights[edge] == wht_f );
            }
        }
    }

  graph_cu_CH_bi_free_host(g);
};

void
Contract_Dist_Verify::init(cu_graph_CH_bi_t *graph_d)
{
  if ( !graph_d ) return;
  cu_graph_CH_bi_t* graph_h;
  graph_cu_CH_bi_d2h(&graph_h, graph_d);
  g_orig_h = graph_h;
  init();
  host_graphs_ours = true;
}

void
Contract_Dist_Verify::init_h
(cu_graph_CH_bi_t *graph_h, cu_graph_CH_UD_t *graph_UD_h)
{
  init_h(graph_h); init_h(graph_UD_h);
}

void
Contract_Dist_Verify::init_h
(cu_graph_CH_bi_t *graph_h)
{
  g_orig_h = graph_h;
  host_graphs_ours = false;
  init();
}

void
Contract_Dist_Verify::init_h
(cu_graph_CH_UD_t *graph_UD_h)
{
  g_UD_h = graph_UD_h;
  assert( g_orig_h );
  assert( !host_graphs_ours );
}

void
Contract_Dist_Verify::init()
{
  nnodes_or = g_orig_h->num_nodes;
  n_queries_per_iter = 2;

  const double t_start = time_wall_fp();

  cu_graph_CH_bi_t& g = *g_orig_h;
  size_t n_two_way_diff_w = 0, n_two_way = 0, n_edges = 0, n_self = 0;
  n_zero_wht = 0;
  unordered_map<Ege,nodeid_t> edge_weights( nnodes_or * 2 );
  or_edge_weights.reserve( nnodes_or * 2 );
  for ( nodeid_t nid=0; nid<g.num_nodes; nid++ )
    {
      const nodeid_t e0 = g.graph_f.pointer[nid];
      for ( uint e=0; e<g.graph_f.num_neighbors[nid]; e++ )
        {
          nodeid_t wht = g.graph_f.weights[e0+e];
          nodeid_t nbr = g.graph_f.neighbors[e0+e];
          if ( nid == nbr ) n_self++;
          Ege edge(nid,nbr), egde(nbr,nid);
          n_edges++;
          if ( !wht ) n_zero_wht++;
          or_edge_weights[edge] = wht;
          if ( edge_weights.count(egde) )
            {
              n_two_way++;
              nodeid_t wht_nbr = edge_weights[egde];
              if ( wht_nbr != wht ) n_two_way_diff_w++;
            }
          else
            {
              assert( !edge_weights.count(edge) );
              edge_weights[edge] = wht;
            }
        }
    }

  // There is at least one (src,dst) edge without a (dst,src).
  is_topol_dir = n_edges > 2 * n_two_way;

  // For all (s,d) in E there exists (d,s) in E and W(s,d) = W(d,s).
  // That is, for every edge there is an edge in the opposite direction
  // of the same weight. 
  is_not_dir = !n_two_way_diff_w && !is_topol_dir;

  // For all (s,d) in E there exists (d,s) in E weights can be different.
  // Edge in both directions, distinct weights.
  is_wht_dir = n_two_way_diff_w && !is_topol_dir;
  pr.tune("Num edges, %zu; 2-way=wht, %zu; 2-way!=wht, %zu. 0=%zd s%zd %d%d%d) "
         "atime %.3f s\n",
         n_edges, n_two_way - n_two_way_diff_w, n_two_way_diff_w, n_zero_wht,
         n_self,  is_not_dir, is_wht_dir, is_topol_dir,
         time_wall_fp() - t_start);
}

Contract_Dist_Verify::~Contract_Dist_Verify()
{
  if ( !host_graphs_ours ) return;
  if ( g_orig_h ) graph_cu_CH_bi_free_host( g_orig_h );
  if ( g_curr_h ) graph_cu_CH_bi_free_host( g_curr_h );
  if ( g_UD_h ) graph_cu_UD_free_host( g_UD_h );
}

int
Contract_Dist_Verify::n_queries_per_iter_set(int n)
{
  const int rv = n_queries_per_iter;
  n_queries_per_iter = n;
  return rv;
}

class Comb_Node
{
public:
  Comb_Node():serial(0){};
  void init(nodeid_t idxp, bool cp)
    {
      assert( ++serial == 1 );
      idx = idxp;
      contracted = cp;
    }
  void set(int serial_expect, SSSP_Node sn)
    {
      assert( ++serial == serial_expect );
      dist = sn.dist;  dist_hops = sn.dist_hops;
    }
  void set_dist(int serial_expect, Weight_Sum distp)
    {
      assert( ++serial == serial_expect );
      dist = distp;
    }
  Weight_Sum get_dist(int serial_expect)
    {
      assert( serial == serial_expect );
      return dist;
    }
  int serial;
  nodeid_t idx; // Overlay or UD;
  bool contracted;
  Weight_Sum dist;
  int dist_hops;
};

class Verify_Edge
{
public:
  Verify_Edge(nodeid_t d, weight_t w, bool s):dst(d),wht(w),is_shc(s){};
  Verify_Edge(const Verify_Edge& e):dst(e.dst),wht(e.wht),is_shc(e.is_shc){};
  Verify_Edge():dst(nodeid_invalid),wht(WEIGHT_INF),is_shc(false){};
  nodeid_t dst;
  weight_t wht;
  bool is_shc;
};

void
Contract_Dist_Verify::verify
(cu_Contract_Query_State *cq_state,
 cu_graph_CH_bi_t *curr_d, cu_graph_CH_UD_t *UD_d)
{
  if ( !g_orig_h ) return;
  if ( !n_queries_per_iter ) return;

  // Retrieve current overlay.
  if ( g_curr_h ) graph_cu_CH_bi_free_host( g_curr_h );
  graph_cu_CH_bi_d2h(&g_curr_h, curr_d);

  // Retrieve current up/down graph.
  if ( g_UD_h ) graph_cu_UD_free_host( g_UD_h );
  graph_cu_CH_UD_d2h(&g_UD_h, UD_d);

  const nodeid_t nnodes_ov = g_curr_h->num_nodes;
  const nodeid_t nnodes_db = g_UD_h->graph_d_b.num_nodes;
  const nodeid_t nnodes_ub = g_UD_h->graph_u_b.num_nodes;
  const nodeid_t nnodes_uf = g_UD_h->graph_u_f.num_nodes;
  const bool zero_wht_okay = n_zero_wht;

  // Overlay node id to original node id.
  auto v_to_r = [&](nodeid_t nid_ov){ return g_curr_h->node_ids[nid_ov]; };
  nodeid_t* const u_to_r = g_UD_h->node_idx;
  vector<nodeid_t> r_to_u(nnodes_or,NODEID_NULL);
  for ( nodeid_t i=0; i<nnodes_or; i++ )
    {
      const nodeid_t nid_r = u_to_r[i];
      if ( nid_r == NODEID_NULL ) break;
      assert( nid_r < nnodes_or );
      assert( r_to_u[nid_r] == NODEID_NULL );
      r_to_u[nid_r] = i;
    }

  vector<nodeid_t> or_to_ov(nnodes_or,NODEID_NULL);
  for ( nodeid_t nid_ov = 0;  nid_ov < nnodes_ov;  nid_ov++ )
    or_to_ov[ v_to_r(nid_ov) ] = nid_ov;

  // Prepare sssp_comb: Node information indexed by original node ID.
  // Used to verify down/backward graph.
  vector<Comb_Node> sssp_comb(nnodes_or);
  for ( nodeid_t nid = 0;  nid < nnodes_ov;  nid++ )
    sssp_comb[ v_to_r(nid) ].init(nid,false);
  for ( nodeid_t nid = 0;  nid < nnodes_db;  nid++ )
    sssp_comb[ g_UD_h->node_idx[nid] ].init(nid,true);

  const cu_graph_CH_t& gdb = g_UD_h->graph_d_b;
  const cu_graph_CH_t& gub = g_UD_h->graph_u_b;
  const cu_graph_CH_t& guf = g_UD_h->graph_u_f;

  vector< vector<Verify_Edge> > elist_uf(nnodes_or);
  for ( nodeid_t nid_u=0; nid_u<nnodes_ub; nid_u++ )
    {
      const nodeid_t e0 = gub.pointer[nid_u];
      const nodeid_t nid_r = u_to_r[nid_u];
      for ( nodeid_t e=0; e<gub.num_neighbors[nid_u]; e++ )
        {
          const nodeid_t nbr_u = gub.neighbors[e0+e];
          const nodeid_t mid_r = gub.midpoint[e0+e];
          const nodeid_t nbr_r = u_to_r[nbr_u];
          const bool is_shortcut = mid_r != NODEID_NULL && mid_r != nbr_r;
          assert( nbr_u < nid_u );
          elist_uf[nbr_r].emplace_back( nid_r, gub.weights[e0+e], is_shortcut );
        }
    }

  if ( !nnodes_ub )
    for ( nodeid_t nid_u=0;  nid_u<nnodes_uf; nid_u++ )
      {
        const nodeid_t e0 = guf.pointer[nid_u];
        const nodeid_t nid_r = u_to_r[nid_u];
        for ( nodeid_t e=0; e<guf.num_neighbors[nid_u]; e++ )
          {
            const nodeid_t nbr_r = guf.neighbors[e0+e];
            const nodeid_t nbr_u = r_to_u[nbr_r];
            const nodeid_t mid_r = guf.midpoint[e0+e];
            const bool is_shortcut = mid_r != NODEID_NULL && mid_r != nbr_r;
            elist_uf[nid_r].emplace_back(nbr_r,guf.weights[e0+e],is_shortcut);
            if ( nbr_u == NODEID_NULL ) continue;
            assert( nbr_u > nid_u );
          }
      }

  vector<nodeid_t> ub_check(nnodes_or,nnodes_or);
  for ( nodeid_t nid_u = 0; nid_u < guf.num_nodes; nid_u++ )
    {
      const nodeid_t nid_r = u_to_r[nid_u];
      const nodeid_t e0 = guf.pointer[nid_u];
      for ( nodeid_t e=0; e<guf.num_neighbors[nid_u]; e++ )
        {
          assert( ub_check[guf.neighbors[e0+e]] != nid_r );
          const nodeid_t wht = guf.weights[e0+e];
          assert( zero_wht_okay || wht );
          ub_check[guf.neighbors[e0+e]] = nid_r;
        }
      auto& nbrs = elist_uf[nid_r];
      assert( nbrs.size() <= guf.num_neighbors[nid_u] );
      for ( auto e: nbrs ) assert( ub_check[ e.dst ] == nid_r );
    }

  vector<Verify_Edge> df_echeck(nnodes_or);
  for ( nodeid_t nid_u = 0;  nid_u < nnodes_db;  nid_u++ )
    {
      const nodeid_t nid_r = u_to_r[nid_u];
      const uint deg_db = gdb.num_neighbors[nid_u];

      for ( nodeid_t e=0; e<deg_db; e++ )
        {
          const nodeid_t eidx = gdb.pointer[nid_u] + e;
          const nodeid_t nbr_r = gdb.neighbors[eidx];
          const nodeid_t mid_r = gdb.midpoint[eidx];
          const bool is_shortcut = mid_r != NODEID_NULL && mid_r != nbr_r;
          const nodeid_t wht = gdb.weights[eidx];
          assert( df_echeck[nbr_r].dst != nid_r );
          assert( zero_wht_okay || wht );
          df_echeck[nbr_r] = Verify_Edge(nid_r,wht,is_shortcut);
        }

      if ( is_topol_dir || chopts.wps_oracle_use
           || !chopts.wps_hash_loop_after_collision
           || chopts.longcuts_cull || chopts.cull_ideal_hops >= 1 )
        continue;

      for ( auto e: elist_uf[nid_r] )
        {
          const nodeid_t nbr_r = e.dst;
          assert( df_echeck[nbr_r].dst == nid_r || is_wht_dir && e.is_shc );
          assert( is_wht_dir || df_echeck[nbr_r].wht == e.wht );
        }
      assert( is_wht_dir || deg_db == elist_uf[nid_r].size() );
    }

  double dur_pq_heap = 0;

  h_sssp_orig.init(g_orig_h->num_nodes);
  Array_Heap<SSSP_Node> h_sssp_curr(nnodes_ov);

  pr.user("Testing overlay and down graph with %d testcases..\n",
         n_queries_per_iter);
  long n_err_all = 0, n_err_msg = 0, n_err_mid = 0;
  for ( int tn=0; tn<n_queries_per_iter; tn++ )
    {
      const int se = tn+2; // Expected serial # when read, or after written.

      // Use command-line provided source if within overlay graph ..
      // .. otherwise choose SSSP source among nodes in overlay graph.

      const nodeid_t src_ov_maybe =
        tn < int(cq_state->srcs) ? or_to_ov[cq_state->srcs[tn]] : NODEID_NULL;
      const nodeid_t src_ov =
        src_ov_maybe != NODEID_NULL ? src_ov_maybe : random() % nnodes_ov;
      const nodeid_t src_or = v_to_r(src_ov);

      const double t0 = time_wall_fp();

      // Perform SSSP queries on original and overlay graphs.
      vector<SSSP_Node>& sssp_orig = sssp(h_sssp_orig, g_orig_h, src_or);
      vector<SSSP_Node>& sssp_curr = sssp(h_sssp_curr, g_curr_h, src_ov);
      dur_pq_heap += time_wall_fp() - t0;

      // Copy distances of nodes in overlay graph to sssp_comb ...
      for ( SSSP_Node n: sssp_curr ) sssp_comb[ v_to_r(n.idx) ].set(se,n);

      // ... and insert distances of nodes in UD graph, computed with
      // a PHAST downward pass.
      for ( nodeid_t dn = nnodes_db; dn-- != 0; )
        {
          weight_t dist = WEIGHT_INF;
          for ( uint e=0; e<gdb.num_neighbors[dn]; e++ )
            {
              const int e_idx = gdb.pointer[dn] + e;
              const nodeid_t nbr_or = gdb.neighbors[e_idx];
              weight_t dd = sssp_comb[nbr_or].get_dist(se) + gdb.weights[e_idx];
              set_min(dist,dd);
            }
          sssp_comb[ g_UD_h->node_idx[dn] ].set_dist(se,dist);
        }

      // Compare distances from original and overly/UD graphs.
      int n_err = 0;
      for ( nodeid_t nid_or = 0; nid_or < nnodes_or; nid_or++ )
        {
          if ( ! h_sssp_orig.visited(nid_or) ) continue; // No path from src.
          Comb_Node& cn = sssp_comb[nid_or];
          Weight_Sum dist_now = cn.get_dist(se);
          if ( sssp_orig[nid_or].dist == dist_now ) continue;
          n_err++; n_err_all++; n_err_msg++;
          if ( n_err >= 5 ) continue;
          if ( n_err_msg == 1 )
            pr.tune("For source orig nid %u, OV nid %u, test #%d:\n",
                    src_or, src_ov, tn);
          pr.tune("Orig nid %8u  %s nid %8u  dist (o,c)  %5u  %5u\n",
                 nid_or, cn.contracted ? "Down/Bwd" : "Overlay",
                 cn.idx, sssp_orig[nid_or].dist, dist_now );
        }

      // Verify Midpoints
      auto mid_verify = [&]
        (const cu_graph_CH_t& g, nodeid_t dn, bool fwd, bool is_ov)
        {
          const nodeid_t nid_or = is_ov ? v_to_r(dn) : u_to_r[dn];
          const weight_t d_nd = sssp_comb[nid_or].get_dist(se);
          for ( uint e=0; e<g.num_neighbors[dn]; e++ )
            {
              const int e_idx = g.pointer[dn] + e;
              const weight_t edb_wht = g.weights[e_idx];
              const nodeid_t nbr_x = g.neighbors[e_idx];
              const nodeid_t nbr_or = is_ov ? v_to_r(nbr_x) : nbr_x;
              const nodeid_t mid_or = g.midpoint[e_idx];
              const nodeid_t src_or =  fwd ? nid_or : nbr_or;
              const nodeid_t dst_or = !fwd ? nid_or : nbr_or;
              const weight_t d_nbr = sssp_comb[nbr_or].get_dist(se);
              const weight_t d_src =  fwd ? d_nd   : d_nbr;
              const weight_t d_dst = !fwd ? d_nd   : d_nbr;
              const bool is_shortcut = mid_or != NODEID_NULL && mid_or!=nbr_or;
              const nodeid_t tnbr_or = is_shortcut ? mid_or : src_or;
              const bool on_sp = d_src + edb_wht == d_dst;
              Ege edge(tnbr_or,dst_or);
              const weight_t e_wht = or_edge_weights[edge];
              assert( zero_wht_okay || e_wht );
              if ( !is_shortcut ) continue;
              const weight_t d_mid = sssp_comb[mid_or].get_dist(se);

              if ( on_sp && d_dst != d_mid + e_wht )
                {
                  n_err_mid++; n_err++; n_err_msg++;
                  if ( n_err_mid >= 5 ) continue;
                  if ( n_err_msg == 1 )
                   pr.user("For source orig nid %u, OV nid %u, test #%d:\n",
                           src_or, src_ov, tn);
                  pr.user("Shc on sp but not mid. %u---%u-->%u\n",
                          src_or, mid_or, dst_or );
                  pr.user
                    ("  Dists: %u---%u-->%u; Whts: %u (shc) %u (mid,dst)\n",
                     d_src, d_mid, d_dst, edb_wht, e_wht );
                }
            }
        };

      for ( nodeid_t nid_ov = 0;  nid_ov < nnodes_ov; nid_ov++ )
        mid_verify(g_curr_h->graph_b,nid_ov,false,true);

      for ( nodeid_t nid_ud = 0;  nid_ud < nnodes_db; nid_ud++ )
        mid_verify(gdb,nid_ud,false,false);

      for ( nodeid_t nid_ud = 0;  nid_ud < nnodes_uf; nid_ud++ )
        mid_verify(guf,nid_ud,true,false);

      assert( n_err == 0 );

    }
  pr.user("... done, found %ld node distance and %ld midpoint errors.\n",
          n_err_all, n_err_mid);
  pr.tune("Priority queue host time %8.3f ms\n", 1e3*dur_pq_heap);
}

string
Contract_Dist_Verify::verify
(cu_Contract_Query_State *cq_state, cu_graph_CH_UD_t *UD_final_d)
{
  if ( !g_orig_h ) return string("");

  pString msg;

  // Number of SSSP queries to perform.
  const size_t n_tests_min = 2;
  const size_t n_tests = max( cq_state->srcs.num_args(), n_tests_min );

  if ( !n_tests ) return "";

  // Retrieve up/down graph.
  if ( host_graphs_ours )
    {
      if ( g_UD_h ) graph_cu_UD_free_host( g_UD_h );
      graph_cu_CH_UD_d2h(&g_UD_h, UD_final_d);
    }

  const nodeid_t nnodes_db = g_UD_h->graph_d_b.num_nodes;
  const nodeid_t nnodes_ub = g_UD_h->graph_u_b.num_nodes;
  const nodeid_t n_levels = g_UD_h->max_rank;
  const nodeid_t nnodes_contracted = g_UD_h->node_levels_pt[n_levels-1];
  const int nnodes_k = nnodes_or - nnodes_contracted;
  nodeid_t* const r_to_u = g_UD_h->node_idx_inv;
  auto k_to_u = [&](nodeid_t nid_k) { return nid_k + nnodes_contracted; };

  assert( nnodes_db == nnodes_ub );
  assert( nnodes_db == nnodes_or );
  assert( nnodes_or == nnodes_contracted + nnodes_k );

  const cu_graph_CH_t& gdb = g_UD_h->graph_d_b;
  const cu_graph_CH_t& gub = g_UD_h->graph_u_b;

  // Prepare an up-forward list that includes weights.
  //
  vector< vector<Verify_Edge> > elist_uf(nnodes_or);
  for ( nodeid_t nid_u=0; nid_u<nnodes_ub; nid_u++ )
    {
      const nodeid_t e0 = gub.pointer[nid_u];
      for ( nodeid_t e=0; e<gub.num_neighbors[nid_u]; e++ )
        {
          const nodeid_t nbr_u = gub.neighbors[e0+e];
          assert( nbr_u < nid_u );
          elist_uf[nbr_u].emplace_back( nid_u, gub.weights[e0+e], false );
        }
    }

  long n_err_all = 0;
  msg.sprintf
    ("Verifying final contracted graph on %zd testcases...\n",n_tests);

  h_sssp_orig.init(nnodes_or);

  for ( size_t tn=0; tn<n_tests; tn++ )
    {
      const nodeid_t src_or =
        tn < cq_state->srcs ? cq_state->srcs[tn] : random() % nnodes_or;
      const nodeid_t src_ud = r_to_u[src_or];

      // Perform SSSP query on original graph.
      vector<SSSP_Node>& sssp_orig = sssp(h_sssp_orig,g_orig_h, src_or);

      vector<Weight_Sum> dist_ud(nnodes_or,WEIGHT_INF); // nid_u-indexed.
      dist_ud[src_ud] = 0;

      // Upward Pass
      set<nodeid_t> work_queue({src_ud});
      for ( nodeid_t nid: work_queue )
        for ( Verify_Edge e: elist_uf[nid] )
          if ( set_min( dist_ud[e.dst], dist_ud[nid] + e.wht ) )
            work_queue.insert(e.dst);

      // Complete Graph
      APSP_path_t& gk = g_UD_h->overlay_APSP_path;
      const int apsp_sz = gk.num_nodes;
      vector<weight_t> dist_k(apsp_sz,WEIGHT_INF);
      auto gk_dist = [&](int i, int j) { return gk.dist[i*apsp_sz+j]; };
      vector<int> nz_nid_k;
      for ( int nid_k = 0; nid_k < nnodes_k; nid_k++ )
        {
          dist_k[nid_k] = dist_ud[k_to_u(nid_k)];
          if ( dist_k[nid_k] != WEIGHT_INF ) nz_nid_k.push_back(nid_k);
        }
      for ( auto nid_k: nz_nid_k )
        for ( int nbr_k = 0; nbr_k < nnodes_k; nbr_k++ )
          set_min(dist_k[nbr_k], gk_dist(nid_k,nbr_k) + dist_k[nid_k] );

      for ( int nid_k = 0; nid_k < nnodes_k; nid_k++ )
        dist_ud[k_to_u(nid_k)] = dist_k[nid_k];

      // ... and insert distances of nodes in UD graph, computed with
      // a PHAST downward pass.
      for ( nodeid_t nid_u = nnodes_db; nid_u-- != 0; )
        for ( uint e=0; e<gdb.num_neighbors[nid_u]; e++ )
          {
            const int e_idx = gdb.pointer[nid_u] + e;
            const weight_t nbr_dist = dist_ud[gdb.neighbors[e_idx]];
            if ( nbr_dist == WEIGHT_INF && dist_ud[nid_u] == WEIGHT_INF )
              continue;
            if ( nbr_dist == WEIGHT_INF )
              {
                msg.sprintf("nid u %u, nbr u %u\n",nid_u,gdb.neighbors[e_idx]);
                printf("%s\n",msg.s);
                fflush(stdout);
              }
            assert( nbr_dist != WEIGHT_INF );
            set_min( dist_ud[nid_u], nbr_dist + gdb.weights[e_idx] );
          }

      // Compare distances from original and overly/UD graphs.
      int n_err = 0;
      for ( nodeid_t nid_r = 0; nid_r < nnodes_or; nid_r++ )
        {
          if ( ! h_sssp_orig.visited(nid_r) ) continue; // No path from src.
          const nodeid_t nid_u = r_to_u[nid_r];
          Weight_Sum dist_now = dist_ud[nid_u];
          if ( sssp_orig[nid_r].dist == dist_now ) continue;
          n_err++; n_err_all++;
          if ( n_err >= 5 ) continue;
          if ( n_err == 1 )
            msg.sprintf("For source orig nid %u, UD nid %u, test #%d:\n",
                        src_or, src_ud, tn);
          msg.sprintf("  Orig nid %8u  UD nid %8u  dist (o,c)  %5u  %5u\n",
                      nid_r, nid_u, sssp_orig[nid_r].dist, dist_now );
        }
      if ( n_err )
        {
          msg.sprintf("Total number of errors, %u\n", n_err);
          printf("%s",msg.s);
          fflush(stdout);
          assert( n_err == 0 );
        }
    }
  msg.sprintf("... done with tests, %ld incorrect nodes found.\n", n_err_all);
  return msg.ss();
}


void
Contract_Dist_Verify::verify
(cu_Contract_Query_State *cq_state, graph_CH_bi_t *graph)
{
  if ( !g_orig_h ) return;

  // Number of SSSP queries to perform.
  const size_t n_tests_min = 2;
  const size_t n_tests = max( cq_state->srcs.num_args(), n_tests_min );

  if ( !n_tests ) return;

  long n_err_all = 0;
  pr.user
    ("Verifying graph on %zd testcases...\n",n_tests);

  assert( nnodes_or == graph->num_nodes );

  h_sssp_orig.init(nnodes_or);
  Array_Heap<SSSP_Node> sssp_nodes(nnodes_or);
  graph_CH_t& g = graph->graph_f;
  graph_CH_t& gb = graph->graph_b;

  // Make sure that backward and forward edges are identical.
  //
  unordered_map<Ege,int> edges;
  int n_fwd = 0, n_bwd_match = 0;
  for ( nodeid_t nid=0; nid<nnodes_or; nid++ )
    {
      node_CH_t& nd = g.nodes[nid];
      const nodeid_t deg = nd.num_neighbors;
      for ( uint eidx = 0; eidx < deg; eidx++ )
        if ( !edges[Ege(nid,nd.neighbors[eidx])]++ ) n_fwd++;
    }
  for ( nodeid_t nid=0; nid<nnodes_or; nid++ )
    {
      node_CH_t& nd = gb.nodes[nid];
      const nodeid_t deg = nd.num_neighbors;
      for ( uint eidx = 0; eidx < deg; eidx++ )
        {
          auto& ec = edges[Ege(nd.neighbors[eidx],nid)];
          assert( ec );
          if ( ec < 0 ) continue;
          n_bwd_match++;
          ec = -1;
        }
    }
  assert( n_fwd == n_bwd_match );

  nodeid_t* const nid_to_rank = graph->node_ranks;

  vector<nodeid_t> rank_to_nid(nnodes_or);
  for ( nodeid_t nid = 0;  nid < nnodes_or;  nid++ )
    rank_to_nid[ nid_to_rank[nid] ] = nid;

  int n_err_db = 0;

  for ( size_t tn=0; tn<n_tests; tn++ )
    {
      const nodeid_t src = random() % nnodes_or;

      // Perform SSSP query on original graph.
      vector<SSSP_Node>& sssp_orig = sssp(h_sssp_orig,g_orig_h, src);

      vector<Weight_Sum> dist_ud(nnodes_or,WEIGHT_INF);
      dist_ud[src] = 0;

      // Upward Pass
      //
      set<nodeid_t> work_queue({nid_to_rank[src]});
      for ( nodeid_t rank: work_queue )
        {
          const nodeid_t nid = rank_to_nid[rank];
          node_CH_t& nd = g.nodes[nid];
          assert( dist_ud[nid] != WEIGHT_INF );
          const nodeid_t deg = nd.num_neighbors;
          for ( uint eidx = 0; eidx < deg; eidx++ )
            {
              const nodeid_t nbr_f = nd.neighbors[eidx];
              const nodeid_t rank_f = nid_to_rank[nbr_f];
              if ( rank_f < rank ) continue;
              const weight_t wht_hf = nd.weights[eidx];
              if ( set_min( dist_ud[nbr_f], dist_ud[nid] + wht_hf ) )
                work_queue.insert(rank_f);
            }
        }

      assert( dist_ud[rank_to_nid[nnodes_or-1]] != WEIGHT_INF );

      // Downward Pass
      //
      for ( nodeid_t i=0; i<nnodes_or; i++ )
        {
          const nodeid_t rank = nnodes_or - 1 - i;
          const nodeid_t nid = rank_to_nid[rank];
          node_CH_t& nd = gb.nodes[nid];
          const nodeid_t deg = nd.num_neighbors;
          uint n_visited = 0, n_db = 0;

          for ( uint eidx = 0; eidx < deg; eidx++ )
            {
              const nodeid_t nid_b = nd.neighbors[eidx];
              if ( graph->node_ranks[nid_b] < rank ) continue;
              n_db++;
              const weight_t nbr_dist = dist_ud[nid_b];
              if ( nbr_dist == WEIGHT_INF ) continue;
              n_visited++;
              const weight_t wht = nd.weights[eidx];
              set_min( dist_ud[nid], nbr_dist + wht );
            }
          if ( !( n_visited == n_db || n_visited == 0 ) ) n_err_db++;
        }

      // Compare distances from original and overly/UD graphs.
      int n_err = 0;
      for ( nodeid_t nid_r = 0; nid_r < nnodes_or; nid_r++ )
        {
          if ( ! h_sssp_orig.visited(nid_r) ) continue; // No path from src.
          Weight_Sum dist_now = dist_ud[nid_r];
          if ( sssp_orig[nid_r].dist == dist_now ) continue;
          n_err++; n_err_all++;
          if ( n_err >= 5 ) continue;
          if ( n_err == 1 )
            pr.user("For source orig nid %u, test #%d:\n",
                    src, tn);
          pr.user("  Orig nid %8u   dist (o,c)  %5u  %5u\n",
                  nid_r, sssp_orig[nid_r].dist, dist_now );
        }
      if ( n_err || n_err_db )
        {
          pr.user("Total number of errors, %d, %d down-bwd\n", n_err, n_err_db);
          fflush(stdout);
          assert( n_err == 0 && n_err_db == 0 );
        }
    }
  pr.user("... done with tests, %ld incorrect nodes found.\n", n_err_all);
}

void
Contract_Dist_Verify::verify(path_t *result)
{
  if ( !g_orig_h ) return;

  pString msg;

  const nodeid_t src = result->src;
  const nodeid_t nnodes = result->num_nodes;
  const weight_t wht_unvisited = ~weight_t(0); // And what if it's float?

  pr.user("About to verify query of %u ...",src);

  assert( nnodes == nnodes_or );

  h_sssp_orig.init(nnodes_or);

  // Perform SSSP query on original graph.
  vector<SSSP_Node>& sssp_orig = sssp(h_sssp_orig, g_orig_h, src);

  nodeid_t n_err_dist = 0, n_err_prev = 0, n_err_msg = 0;

  for ( nodeid_t nid = 0; nid < nnodes;  nid++ )
    {
      const weight_t rdist = result->weight[nid];
      const weight_t odist =
        h_sssp_orig.visited(nid) ? sssp_orig[nid].dist : wht_unvisited;
      if ( odist != rdist )
        {
          n_err_dist++;
          if ( n_err_msg++ < 5 )
            {
              if ( n_err_msg == 1 )
                pr.user("\nFor source nid %u:\n", src);
              pr.user
                ("  Nid %8u  dist (o,c)  %5u  %5u\n", nid, odist, rdist);
            }
        }
      if ( rdist == 0 || rdist == wht_unvisited ) continue;
      if ( chopts.query_distance_only ) continue;
      const nodeid_t rprev = result->prev[nid];
      if ( rprev >= nnodes )
        {
          n_err_prev++;
          if ( n_err_msg++ >= 5 ) continue;
          if ( n_err_msg == 1 ) pr.user("For source nid %u:\n", src);
          pr.user
            ("  Nid %8u dist %5u  pred %u = %#x (out of range)\n",
             nid, odist, rprev, rprev);
          continue;
        }
      const int deg = g_orig_h->graph_b.num_neighbors[nid];
      const nodeid_t eidx_0 = g_orig_h->graph_b.pointer[nid];
      for ( int ei = 0; ei < deg; ei++ )
        {
          const nodeid_t eidx = eidx_0 + ei;
          const nodeid_t pnid = g_orig_h->graph_b.neighbors[eidx];
          if ( pnid != rprev )
            {
              if ( ei + 1 < deg ) continue;
              n_err_prev++;
              if ( n_err_msg++ >= 5 ) break;
              if ( n_err_msg == 1 ) pr.user("For source nid %u:\n", src);
              pr.user
                ("  Nid %8u dist %5u  pred %5u not found, deg %d\n",
                 nid, odist,  rprev, deg );
              break;
            }

          const weight_t pwht = g_orig_h->graph_b.weights[eidx];
          const weight_t pdist_plus = result->weight[pnid] + pwht;
          if ( pdist_plus == rdist ) break;

          n_err_prev++;
          if ( n_err_msg++ >= 5 ) break;
          if ( n_err_msg == 1 ) pr.user("For source nid %u:\n", src);
          pr.user
            ("  Nid %8u dist %5u  pred %5u dist %5u  wht %5u\n",
             nid, odist,  rprev, result->weight[pnid], pwht );
          break;
        }
    }
  const size_t n_err_all = n_err_dist + n_err_prev;
  if ( n_err_all )
    {
      pr.user("Total number of errors, %u dist  %d predecessor\n",
              n_err_dist, n_err_prev);
      assert( n_err_all == 0 );
    }

  pr.user("done, %ld incorrect nodes found.\n", n_err_all);
}

vector<SSSP_Node>&
Contract_Dist_Verify::sssp
(Array_Heap<SSSP_Node>& sssp_nodes, cu_graph_CH_bi_t *g, nodeid_t src)
{
  const nodeid_t n_nodes = g->num_nodes;
  assert( src < n_nodes );
  const int ns_serial = sssp_nodes.heap_reset();
  sssp_nodes.priority_set(src,0,0);

  while ( SSSP_Node* const wn_h = sssp_nodes.heap_pop() )
    {
      const nodeid_t nnbr_h = g->graph_f.num_neighbors[wn_h->idx];
      const nodeid_t e_h_0 = g->graph_f.pointer[wn_h->idx];
      for ( uint eidx = e_h_0;  eidx < e_h_0 + nnbr_h; eidx++ )
        {
          const nodeid_t nid_hf = g->graph_f.neighbors[eidx];
          SSSP_Node* const wn_hf = &sssp_nodes[nid_hf];
          const weight_t wht_hf = g->graph_f.weights[eidx];
          const weight_t dist_next = wn_h->dist + wht_hf;
          const int hops_next = wn_h->dist_hops + 1;
          if ( wn_hf->ns_serial == ns_serial
               && wn_hf->dist <= dist_next ) continue;
          sssp_nodes.priority_set(wn_hf,dist_next,hops_next);
        }
    }
  return sssp_nodes;
}
