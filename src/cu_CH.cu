/// CUCH - CUDA Graph Contraction and Query
/// Copyright (c) 2020 Louisiana State University
//

#include <assert.h>
#include <map>

#include "cu_CH.cuh"
#include "cu_CH_prefix.cuh"

#include <nppdefs.h>

const bool __attribute__((unused)) debug_sm_idx = false;

// #define ST
// guarantees that different syncthreads() calls will not affect each other by checking line number
// allows for threads to skip a syncthreads() (only applies to the last syncthread in a kernel)
#ifdef ST

#define __syncthreads()                                                       \
  { sync_threads();                                                           \
    st_line = atomicExch(&sync_count_sh, __LINE__);                           \
    assert(st_line == 0 || st_line == __LINE__);                              \
    sync_threads();                                                           \
    sync_count_sh = 0; }

#endif

using namespace std;

static int edge_cache_elt_serial = 0;

class Edge_Cache_Elt {
public:
  Edge_Cache_Elt(){ zero(); serial = ++edge_cache_elt_serial; }
  Edge_Cache_Elt(const Edge_Cache_Elt& e){ cpy(e); }
  Edge_Cache_Elt(Edge_Cache_Elt&& e){ cpy(e); e.zero(); }
  Edge_Cache_Elt& operator= (Edge_Cache_Elt&& e)
  { assert( !edges_d ); cpy(e); e.zero(); return *this; }
  Edge_Cache_Elt& operator= (const Edge_Cache_Elt& e)
  { assert( !edges_d && !e.edges_d ); return *this; }
  ~Edge_Cache_Elt(){ if ( edges_d ) CE( cudaFree( edges_d ) ); }
  nodeid_t *edges_d;
  size_t n_edges;
  int serial;
private:
  void cpy(const Edge_Cache_Elt& e)
  { edges_d=e.edges_d; n_edges=e.n_edges; serial=e.serial; }
  void zero(){ edges_d=NULL; n_edges=0; serial=0;}
};

enum { GC_bwd, GC_fwd };

class Graph_Cache_Elt {
public:
  Graph_Cache_Elt()
    :in_use(false), graph_d(NULL), nodes_d(NULL), n_nodes(0){};
  ~Graph_Cache_Elt()
    {
      if ( nodes_d ) CE( cudaFree( nodes_d ) );
      nodes_d = (nodeid_t*)1; // Detect use-after-free errors.
    }
  cu_graph_CH_t* graph_get(int dir)
    { return dir == GC_bwd ? &graph_h.graph_b : &graph_h.graph_f; }
  void struct_host_to_device()
    {
      assert( graph_h.self_d == graph_d );
      CE( cudaMemcpy
          ( graph_d, &graph_h, sizeof(graph_h), cudaMemcpyHostToDevice) );
      struct_latest_on_device = true;
      struct_latest_on_host = false; // Conservative.
    }
  void struct_device_to_host()
    {
      if ( struct_latest_on_host ) return;
      CE( cudaMemcpy
          ( &graph_h, graph_d, sizeof(graph_h), cudaMemcpyDeviceToHost) );
      assert( graph_h.self_d == graph_d );
      struct_latest_on_host = true;
      struct_latest_on_device = false;
    }
  void assert_dev_clean()
    {
      assert( in_use );
      assert( struct_latest_on_device );
    }
  void set_dev_dirty()
  {
    assert( in_use );
    assert( struct_latest_on_device );
    struct_latest_on_host = false;
  }
  void assume_dev_dirty()
  {
    assert( in_use );
    struct_latest_on_device = true;
    struct_latest_on_host = false;
  }
  void set_host_dirty()
  {
    assert( in_use );
    assert( struct_latest_on_host );
    struct_latest_on_device = false;
  }
  void assume_all_clean()
  {
    assert( in_use );
    struct_latest_on_host = true;
    struct_latest_on_device = true;
  }

  bool in_use;
  Edge_Cache_Elt e_elts[2];
  cu_graph_CH_bi_t *graph_d, graph_h;
  nodeid_t *nodes_d;
  nodeid_t n_nodes; // Allocated.
  bool struct_latest_on_device, struct_latest_on_host;
};

map<void*,Graph_Cache_Elt> graph_cache;

cu_graph_CH_bi_t&
graph_cache_lookup(void* dev_addr)
{
  Graph_Cache_Elt& elt = graph_cache[dev_addr];
  assert( elt.in_use );
  return elt.graph_h;
}

void
graph_cache_assert_dev_clean(void* dev_addr)
{ graph_cache[dev_addr].assert_dev_clean(); }
void
graph_cache_set_dev_dirty(void* dev_addr)
{ graph_cache[dev_addr].set_dev_dirty(); }
void
graph_cache_set_host_dirty(void* dev_addr)
{ graph_cache[dev_addr].set_host_dirty(); }
void
graph_cache_assume_dev_dirty(void* dev_addr)
{ graph_cache[dev_addr].assume_dev_dirty(); }
void
graph_cache_assume_all_clean(void* dev_addr)
{ graph_cache[dev_addr].assume_all_clean(); }

cu_graph_CH_bi_t&
graph_cache_lookup_struct_to_host(void* dev_addr)
{
  Graph_Cache_Elt& elt = graph_cache[dev_addr];
  assert( elt.in_use );
  elt.assume_dev_dirty();
  elt.struct_device_to_host();
  return elt.graph_h;
}

Graph_Cache_Elt*
graph_cache_elt_lookup(cu_graph_CH_bi_t* graph_d)
{
  Graph_Cache_Elt& elt = graph_cache[graph_d];
  assert( elt.in_use );
  return &elt;
}

void
edge_cache_alloc(Edge_Cache_Elt& elt, nodeid_t n_edges)
{
  const ptrdiff_t underuse_limit_bytes = ptrdiff_t(1) << 27;
  // If true print allocation messages.
  const bool trace_allocs = trace_mem_showing() || chopts.print_tuning_in_loops;

  const int nea = edge_cache_n_edge_arrays;

  const ptrdiff_t over_alloc_e =
    sizeof(elt.edges_d[0]) * nea * ( ptrdiff_t(elt.n_edges) - n_edges );

  if ( over_alloc_e > underuse_limit_bytes || elt.n_edges < n_edges )
    {
      if ( elt.edges_d ) CE( cudaFree( elt.edges_d ) );
      const size_t edge_stride_elts = rnd_up(n_edges,32);
      const size_t alloc_amt = edge_stride_elts * nea * sizeof(elt.edges_d[0]);
      if ( trace_allocs )
        printf("Edge Cache edges: #%d have %8zu, "
               "allocating %2d * %8zu eds,  %4zu MiB.\n",
               elt.serial, elt.n_edges, nea, edge_stride_elts, alloc_amt >> 20);
      CE( cudaMalloc( &elt.edges_d, alloc_amt ) );
      elt.n_edges = edge_stride_elts;

      TRACE_MEM_USAGE();
    }
}


Graph_Cache_Elt* graph_cu_CH_bi_alloc(nodeid_t num_nodes, nodeid_t n_edges);
void graph_cu_CH_bi_edges_alloc
(Graph_Cache_Elt *ge, nodeid_t n_edges, int dirp = 3);
void graph_cu_CH_bi_edges_init(Graph_Cache_Elt *ge, nodeid_t n_edges);

void graph_cu_CH_bi_h2d(cu_graph_CH_bi_t **cu_graph_CH_d, cu_graph_CH_bi_t *cu_graph_CH_h)
{
  const nodeid_t num_nodes = cu_graph_CH_h->num_nodes;
  const nodeid_t num_edges =
    max( cu_graph_CH_h->graph_f.max_num_edges,
         cu_graph_CH_h->graph_b.max_num_edges );

  Graph_Cache_Elt* const ge = graph_cu_CH_bi_alloc(num_nodes,num_edges);
  cu_graph_CH_bi_t* const graph_h = &ge->graph_h;

# define cm(m) graph_h->m = cu_graph_CH_h->m;
# define cmfb(m) cm(graph_f.m) cm(graph_b.m)

  graph_h->num_nodes = num_nodes;
  graph_h->max_degree = cu_graph_CH_h->max_degree;
  graph_h->max_degree_overlay = cu_graph_CH_h->max_degree_overlay;
  graph_h->thread_per_node = cu_graph_CH_h->thread_per_node;
  graph_h->mean_degree = cu_graph_CH_h->mean_degree;
  graph_h->mean_degree_overlay = cu_graph_CH_h->mean_degree_overlay;
  graph_h->max_weight = cu_graph_CH_h->max_weight;
  graph_h->mean_weight = cu_graph_CH_h->mean_weight;
  cm(mean_weight_inv);
  cm(mean_weight_0_inv);
  graph_h->overlay_size = cu_graph_CH_h->overlay_size;
  graph_h->max_rank = cu_graph_CH_h->max_rank;
  graph_h->graph_f.num_nodes = cu_graph_CH_h->graph_f.num_nodes;
  graph_h->graph_f.max_weight = cu_graph_CH_h->graph_f.max_weight;
  graph_h->graph_f.max_num_edges = cu_graph_CH_h->graph_f.max_num_edges;
  graph_h->graph_f.empty_pointer = cu_graph_CH_h->graph_f.empty_pointer;
  graph_h->graph_b.num_nodes = cu_graph_CH_h->graph_b.num_nodes;
  graph_h->graph_b.max_weight = cu_graph_CH_h->graph_b.max_weight;
  graph_h->graph_b.max_num_edges = cu_graph_CH_h->graph_b.max_num_edges;
  graph_h->graph_b.empty_pointer = cu_graph_CH_h->graph_b.empty_pointer;
  cmfb(num_edges_exact);
# undef cm
# undef cmfb

  const size_t nd_array_sz_bytes = num_nodes * sizeof( graph_h->node_ids[0] );
  const size_t ed_array_sz_bytes = num_edges * sizeof( graph_h->node_ids[0] );

# define cg(m)                                                                \
  CE( cudaMemcpy                                                              \
      ( graph_h->m, cu_graph_CH_h->m,                                         \
        array_sz_bytes, cudaMemcpyHostToDevice ) );

  size_t array_sz_bytes = nd_array_sz_bytes;
  cg(node_ranks); cg(node_ids);

# define cfb(m) cg(graph_f.m); cg(graph_b.m);

  cfb(pointer);
  cfb(num_neighbors); 
  cfb(max_num_neighbors_allowed);

  array_sz_bytes = ed_array_sz_bytes;
  cfb(neighbors); cfb(weights); cfb(midpoint);

# undef cg
# undef cfb

  CE( cudaMemcpy(ge->graph_d,graph_h,sizeof(*graph_h),cudaMemcpyHostToDevice) );

  *cu_graph_CH_d = ge->graph_d;
}

void graph_cu_CH_bi_d2h(cu_graph_CH_bi_t **cu_graph_CH_h, cu_graph_CH_bi_t *cu_graph_CH_d)
{

  cu_graph_CH_bi_t *graph_out = (cu_graph_CH_bi_t *) malloc(sizeof(cu_graph_CH_bi_t));
  CE( cudaMemcpy(graph_out, cu_graph_CH_d, sizeof(cu_graph_CH_bi_t), cudaMemcpyDeviceToHost) );

  nodeid_t *node_ranks = (nodeid_t *)malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(node_ranks, graph_out->node_ranks, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_ranks = node_ranks;
  nodeid_t *node_ids = (nodeid_t *)malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(node_ids, graph_out->node_ids, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_ids = node_ids;

  nodeid_t *pointer = (nodeid_t *)  malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(pointer, graph_out->graph_f.pointer, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_f.pointer = pointer;
  nodeid_t *num_neighbors = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(num_neighbors, graph_out->graph_f.num_neighbors, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_f.num_neighbors = num_neighbors;
  nodeid_t *max_num_neighbors_allowed = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(max_num_neighbors_allowed, graph_out->graph_f.max_num_neighbors_allowed, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_f.max_num_neighbors_allowed = max_num_neighbors_allowed;
  nodeid_t *neighbors = (nodeid_t *) malloc(graph_out->graph_f.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(neighbors, graph_out->graph_f.neighbors, graph_out->graph_f.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_f.neighbors = neighbors;
  weight_t *weights = (weight_t *) malloc(graph_out->graph_f.max_num_edges * sizeof(weight_t));
  CE( cudaMemcpy(weights, graph_out->graph_f.weights, graph_out->graph_f.max_num_edges * sizeof(weight_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_f.weights = weights;
  nodeid_t *midpoint = (nodeid_t *) malloc(graph_out->graph_f.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(midpoint, graph_out->graph_f.midpoint, graph_out->graph_f.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_f.midpoint = midpoint;

  pointer = (nodeid_t *)  malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(pointer, graph_out->graph_b.pointer, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_b.pointer = pointer;
  num_neighbors = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(num_neighbors, graph_out->graph_b.num_neighbors, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_b.num_neighbors = num_neighbors;
  max_num_neighbors_allowed = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(max_num_neighbors_allowed, graph_out->graph_b.max_num_neighbors_allowed, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_b.max_num_neighbors_allowed = max_num_neighbors_allowed;
  neighbors = (nodeid_t *) malloc(graph_out->graph_b.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(neighbors, graph_out->graph_b.neighbors, graph_out->graph_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_b.neighbors = neighbors;
  weights = (weight_t *) malloc(graph_out->graph_b.max_num_edges * sizeof(weight_t));
  CE( cudaMemcpy(weights, graph_out->graph_b.weights, graph_out->graph_b.max_num_edges * sizeof(weight_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_b.weights = weights;
  midpoint = (nodeid_t *) malloc(graph_out->graph_b.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(midpoint, graph_out->graph_b.midpoint, graph_out->graph_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_b.midpoint = midpoint;

  *cu_graph_CH_h = graph_out;
  
}


void graph_cu_CH_bi_free_device(cu_graph_CH_bi_t *graph)
{
  if ( !graph ) { graph_cache.clear(); return; }
  Graph_Cache_Elt& elt = graph_cache[graph];
  assert( elt.in_use );
  elt.in_use = false;
}

void graph_cu_CH_d2h(cu_graph_CH_t **cu_graph_CH_h, cu_graph_CH_t *cu_graph_CH_d)
{

  cu_graph_CH_t *graph_out = (cu_graph_CH_t *) malloc(sizeof(cu_graph_CH_t));
  CE( cudaMemcpy(graph_out, cu_graph_CH_d, sizeof(cu_graph_CH_t), cudaMemcpyDeviceToHost) );

  nodeid_t *pointer = (nodeid_t *)  malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(pointer, graph_out->pointer, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->pointer = pointer;
  nodeid_t *num_neighbors = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(num_neighbors, graph_out->num_neighbors, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->num_neighbors = num_neighbors;
  nodeid_t *max_num_neighbors_allowed = (nodeid_t *) malloc(graph_out->num_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(max_num_neighbors_allowed, graph_out->max_num_neighbors_allowed, graph_out->num_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->max_num_neighbors_allowed = max_num_neighbors_allowed;
  nodeid_t *neighbors = (nodeid_t *) malloc(graph_out->max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(neighbors, graph_out->neighbors, graph_out->max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->neighbors = neighbors;
  weight_t *weights = (weight_t *) malloc(graph_out->max_num_edges * sizeof(weight_t));
  CE( cudaMemcpy(weights, graph_out->weights, graph_out->max_num_edges * sizeof(weight_t), cudaMemcpyDeviceToHost) );
  graph_out->weights = weights;
  nodeid_t *midpoint = (nodeid_t *) malloc(graph_out->max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(midpoint, graph_out->midpoint, graph_out->max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->midpoint = midpoint;

  *cu_graph_CH_h = graph_out;


}

void
cu_shc_CH_set::alloc(uint32_t n_segs_need, uint32_t max_len_per_seg_need)
{
  const double overalloc = 1.1; // To avoid reallocating a little more.
  const bool trace_allocs = trace_mem_showing();
  dh.n_segs = h.n_segs = n_segs_need;
  seg_stride = rnd_up(max_len_per_seg_need,8);
  const uint32_t seg_stride_alloc = rnd_up(max_len_per_seg_need*overalloc,8);
  dh.max_len_per_seg = h.max_len_per_seg = seg_stride;

  n_sets = 1;
  n_shc_per_set = seg_stride * h.n_segs;
  const uint32_t n_shc_needed = n_sets * n_shc_per_set;
  const uint32_t n_shc_to_alloc = n_sets * seg_stride_alloc * h.n_segs;
  const size_t len_lists_needed_bytes =
    n_sets * h.n_segs * sizeof(h.len_sto[0]);

  if ( len_lists_needed_bytes > len_lists_bytes )
    {
      if ( dh.len_sto ) CE( cudaFree(dh.len_sto) );
      len_lists_bytes = len_lists_needed_bytes;
      CE( cudaMalloc(&dh.len_sto, len_lists_bytes) );
      if ( trace_allocs )
        printf("Shortcut Segments: have %8d, allocating %8d, free %zu MiB.\n",
               n_segs_allocated, h.n_segs, cuda_mem_free_get()>>20 );
      n_segs_allocated = h.n_segs;
      if ( h.len_sto ){ ::free(h.len_sto); h.ptrs_len_reset(); }
      TRACE_MEM_USAGE();
    }
  ptrs_len_set(dh);

  if ( n_shc_needed > n_shc_allocated )
    {
      if ( dh.shc_sto ) CE( cudaFree(dh.shc_sto) );
      shc_lists_bytes = n_shc_to_alloc * sizeof(h.shc_sto[0]);
      CE( cudaMalloc(&dh.shc_sto, shc_lists_bytes) );
      if ( trace_allocs )
        printf("Shortcut Storage: have %8d, allocating %8u, free %5zu MiB.\n",
               n_shc_allocated, n_shc_to_alloc, cuda_mem_free_get()>>20 );
      n_shc_allocated = n_shc_to_alloc;
      TRACE_MEM_USAGE();
    }
  ptrs_shc_set(dh);

  if ( !d ) CE( cudaMalloc( &d, sizeof(*d) ) );

  CE( cudaMemsetAsync( dh.len_sto, 0, len_lists_needed_bytes ) );
  CE( cudaMemcpyAsync( d, &dh, sizeof(dh), cudaMemcpyHostToDevice ) );
}

void
cu_shc_CH_set::dtoh_len()
{
  if ( !h.len_sto )
    {
      h.len_sto = (typeof &h.len_sto[0]) malloc( len_lists_bytes );
      ptrs_len_set(h);
    }
  CE( cudaMemcpy
      ( h.len_sto, dh.len_sto, len_lists_bytes, cudaMemcpyDeviceToHost) );
}
void
cu_shc_CH_set::dtoh_shc()
{
  if ( !h.shc_sto )
    {
      h.shc_sto = (typeof &h.shc_sto[0]) malloc( shc_lists_bytes );
      ptrs_shc_set(h);
    }
  CE( cudaMemcpy
      ( h.shc_sto, dh.shc_sto, shc_lists_bytes, cudaMemcpyDeviceToHost) );
}

void
cu_shc_CH_set::alloc_h(uint32_t n_segs_need, uint32_t max_len_per_seg_need)
{

  // since this is only used for shadow search, it must follow an already allocated shc_set
  assert(h.n_segs == n_segs_need);
  assert(dh.n_segs == n_segs_need);
  assert(n_segs_allocated <= n_segs_need);
  assert(seg_stride == (uint32_t)rnd_up(max_len_per_seg_need,8));
  assert(h.max_len_per_seg == seg_stride);
  assert(dh.max_len_per_seg == seg_stride);

  if ( h.shc_sto ) { ::free(h.shc_sto); h.shc_sto = nullptr; }

  h.shc_sto = (typeof &h.shc_sto[0]) malloc(shc_lists_bytes);
  memset(h.shc_sto,0,shc_lists_bytes);
  ptrs_shc_set(h);

}

void
cu_shc_CH_set::htod_all()
{

  // only used for shadow search, assumes shc_set already allocated
  CE( cudaMemcpyAsync
      ( dh.len_sto, h.len_sto, len_lists_bytes, cudaMemcpyHostToDevice) );
  ptrs_len_set(dh);

  CE( cudaMemcpyAsync
      ( dh.shc_sto, h.shc_sto, shc_lists_bytes, cudaMemcpyHostToDevice) );

  ptrs_shc_set(dh);

  CE( cudaMemcpyAsync( d, &dh, sizeof(dh), cudaMemcpyHostToDevice ) );

}

void
cu_shc_CH_set::free()
{
  #define FREE_ME(d) if ( d ){ CE( cudaFree( d ) ); d = NULL; }
  FREE_ME(d); FREE_ME(dh.len_sto); FREE_ME(dh.shc_sto);
  #undef FREE_ME
  dh.ptrs_len_reset(); dh.ptrs_shc_reset();
  n_segs_allocated = n_shc_allocated = 0;
}


// we won't bother having an init function on the host side since it would serve no purpose!
void
graph_cu_CH_UD_init_d
( cu_graph_CH_UD_t **graph_cu_UD_d,
  nodeid_t num_nodes, size_t edge_list_length, nodeid_t max_rank,
  nodeid_t K, nodeid_t overlay_edge_list_length,
  UD_Purpose purpose)
{
  const bool need_vid = purpose == UDP_Compress;
  const bool need_post = need_vid;
  
  cu_graph_CH_UD_t *graph_h = (cu_graph_CH_UD_t *) malloc(sizeof(cu_graph_CH_UD_t));
  
  memset(graph_h,0,sizeof(*graph_h));
  
  auto alloc_eset = [&](nodeid_t*& ptr)
    {
      CE( cudaMalloc(&ptr,           edge_list_length * sizeof(ptr[0])) );
      CE( cudaMemsetAsync(ptr, 0xFF, edge_list_length * sizeof(ptr[0])) );
    };
  auto alloc_nset = [&](nodeid_t*& ptr)
    {
      CE( cudaMalloc(&ptr,           num_nodes * sizeof(ptr[0])) );
      CE( cudaMemsetAsync(ptr, 0xff, num_nodes * sizeof(ptr[0])) );
    };

  graph_h->num_nodes = num_nodes;
  graph_h->max_degree = 0;
  graph_h->n_levels_alloc = max_rank;

  alloc_nset(graph_h->node_idx);
  alloc_nset(graph_h->node_idx_inv);

  nodeid_t *node_ranks;
  CE( cudaMalloc(&node_ranks, num_nodes * sizeof(nodeid_t)) );
  graph_h->node_ranks = node_ranks;
  // note that unlike the CPU version, levels are not initialized to zero (no need since levels=ranks)
  nodeid_t *node_levels;
  CE( cudaMalloc(&node_levels, num_nodes * sizeof(nodeid_t)) );
  graph_h->node_levels = node_levels;
  nodeid_t *node_levels_pt;
  CE( cudaMalloc(&node_levels_pt, max_rank * sizeof(nodeid_t)) );
  CE( cudaMemset(node_levels_pt, 0, max_rank * sizeof(nodeid_t)) );
  graph_h->node_levels_pt = node_levels_pt;

  // we'll update max_rank to its actual value at the last iteration when finishing the UD graph
  graph_h->max_rank = max_rank;
  // the actual value for overlay size will be updated later on (here we initialize it to zero)
  graph_h->overlay_size = 0;

  graph_h->overlay_APSP_path.num_nodes = 0;

  // we'll increase num_nodes as we put contracted nodes in
  graph_h->graph_u_f.num_nodes = 0;
  graph_h->graph_u_f.max_num_edges = edge_list_length;
  // empty_pointer == pointer[num_nodes] must hold, this variable is redundant, assertion perhaps?
  graph_h->graph_u_f.empty_pointer = 0;
  nodeid_t *pointer;
  CE( cudaMalloc(&pointer, num_nodes * sizeof(nodeid_t)) );
  graph_h->graph_u_f.pointer = pointer;
  nodeid_t *num_neighbors;
  CE( cudaMalloc(&num_neighbors, num_nodes * sizeof(nodeid_t)) );
  graph_h->graph_u_f.num_neighbors = num_neighbors;
  nodeid_t *nieg_count;
  CE( cudaMalloc(&nieg_count, num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
  graph_h->graph_u_f.NNIEG = nieg_count;

  alloc_eset(graph_h->graph_u_f.neighbors);
  alloc_eset(graph_h->graph_u_f.weights);
  alloc_eset(graph_h->graph_u_f.midpoint);

  if ( need_vid )
    {
      alloc_eset(graph_h->graph_u_f.vertex_ID);
      alloc_eset(graph_h->graph_u_b.vertex_ID);
      alloc_eset(graph_h->graph_d_b.vertex_ID);
    }

  graph_h->graph_u_b.max_num_edges = edge_list_length;

  // we'll increase num_nodes as we put contracted nodes in
  graph_h->graph_d_b.num_nodes = 0;
  graph_h->graph_d_b.max_num_edges = edge_list_length;
  // empty_pointer == pointer[num_nodes] must hold, this variable is redundant, assertion perhaps?
  graph_h->graph_d_b.empty_pointer = 0;
  CE( cudaMalloc(&pointer, num_nodes * sizeof(nodeid_t)) );
  graph_h->graph_d_b.pointer = pointer;
  CE( cudaMalloc(&num_neighbors, num_nodes * sizeof(nodeid_t)) );
  graph_h->graph_d_b.num_neighbors = num_neighbors;
  CE( cudaMalloc(&nieg_count, num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
  graph_h->graph_d_b.NNIEG = nieg_count;
  nodeid_t *neighbors = nullptr;
  CE( cudaMalloc(&neighbors, edge_list_length * sizeof(nodeid_t)) );
  CE( cudaMemset(neighbors, 0xFF, edge_list_length * sizeof(nodeid_t)) );
  graph_h->graph_d_b.neighbors = neighbors;
  
  alloc_eset(graph_h->graph_d_b.weights);
  alloc_eset(graph_h->graph_d_b.midpoint);
  
  // we'll increase num_nodes as we put contracted nodes in
  graph_h->overlay_CH.num_nodes = 0;
  
  graph_h->overlay_CH.max_num_edges = overlay_edge_list_length;

  // empty_pointer == pointer[num_nodes] must hold, this variable is redundant, assertion perhaps?
  graph_h->overlay_CH.empty_pointer = 0;

  graph_h->graph_u_f.num_edges_exact = 0;
  graph_h->graph_u_b.num_edges_exact = 0;
  graph_h->graph_d_b.num_edges_exact = 0;

  if ( need_post ) graph_cu_CH_UD_alloc_more( *graph_h, K );

  cu_graph_CH_UD_t *graph_out;
  CE( cudaMalloc(&graph_out, sizeof(cu_graph_CH_UD_t)) );
  CE( cudaMemcpy(graph_out, graph_h, sizeof(cu_graph_CH_UD_t), cudaMemcpyHostToDevice) );

  *graph_cu_UD_d = graph_out;

  free(graph_h);

}

__global__ void
graph_cu_CH_apsp_init(int nnodes_k, nodeid_t *prev, nodeid_t *dist)
{
  const int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int iter_per_col = div_up( nnodes_k, block_dim );
  const int row = blockIdx.x;

  // use the overlay_size goal value and allocate path structure for
  // it (filled with NULL/INF)
  //
  // note that despite using the existing APSP path structure, we
  // won't use the 2D array on GPU

  for ( int i=0; i<iter_per_col; i++ )
    {
      const int col = i * block_dim + threadIdx.x;
      if ( col >= nnodes_k ) break;
      const int idx = row * nnodes_k + col;
      prev[idx] = row == col ? row : NPP_MAX_32U;
      dist[idx] = row == col ?   0 : NPP_MAX_32U;
    }
}

void
graph_cu_CH_UD_alloc_more(cu_graph_CH_UD& graph, uint32_t K)
{
  // At this point we could allocate less, but it's not that much memory
  // and it's better to not have two sizes.
  const size_t num_nodes = graph.num_nodes;

  auto alloc_set = [&](nodeid_t*& ptr, size_t n_elts, uint16_t zval=256)
    {
      if ( ptr ) return;
      CE( cudaMalloc(&ptr,           n_elts * sizeof(ptr[0])) );
      if ( zval == 256 ) return;
      assert( zval < 256 );
      CE( cudaMemsetAsync(ptr, zval, n_elts * sizeof(ptr[0])) );
    };
  auto alloc_nset = [&](nodeid_t*& ptr){ alloc_set(ptr,num_nodes,0xff); };
  auto alloc_nniegset = [&](nodeid_t*& ptr){ alloc_set(ptr,num_nodes*NNIEG_OVER_ALLOC,0xff); };
  auto alloc_esets = [&](cu_graph_CH_t& g)
    {
      alloc_set(g.neighbors,g.max_num_edges);
      alloc_set(g.weights,g.max_num_edges,0xff);
      alloc_set(g.midpoint,g.max_num_edges);
    };

  const nodeid_t nnodes_k = K;
  const size_t nnodes_ap = nnodes_k * nnodes_k;

  graph.overlay_APSP_path.num_nodes = nnodes_k;

  alloc_set( graph.overlay_APSP_path.prev, nnodes_ap);
  alloc_set( graph.overlay_APSP_path.dist, nnodes_ap);
  graph_cu_CH_apsp_init<<<nnodes_k,1024>>>
    (nnodes_k, graph.overlay_APSP_path.prev, graph.overlay_APSP_path.dist );
  alloc_set( graph.overlay_CH.pointer, nnodes_k );
  alloc_set( graph.overlay_CH.num_neighbors, nnodes_k );
  alloc_esets( graph.overlay_CH );

  graph.graph_u_b.max_num_edges = graph.graph_u_f.max_num_edges;
  alloc_set( graph.graph_u_b.pointer, num_nodes, 0 );
  alloc_nset( graph.graph_u_b.num_neighbors );
  alloc_nniegset( graph.graph_u_b.NNIEG ); // Used by ud_invert.
  alloc_esets( graph.graph_u_b );
}

void graph_cu_CH_UD_d2h(cu_graph_CH_UD_t **cu_graph_UD_h, cu_graph_CH_UD_t *cu_graph_UD_d)
{

  cu_graph_CH_UD_t *graph_out = (cu_graph_CH_UD_t *) malloc(sizeof(cu_graph_CH_UD_t));
  CE( cudaMemcpy(graph_out, cu_graph_UD_d, sizeof(cu_graph_CH_UD_t), cudaMemcpyDeviceToHost) );

  // Note: UD graph has fewer nodes, but node lists sometimes indexed
  // using original node index rather than rank (UD) node indices.
  const nodeid_t n_nodes = graph_out->num_nodes;

  auto list_cpy = [](nodeid_t*& ptr, size_t n_elts)
    {
      if ( !ptr ) return;
      nodeid_t* const devptr = ptr;
      size_t n_bytes = n_elts * sizeof(*ptr);
      ptr = (nodeid_t *) malloc( n_bytes );
      CE( cudaMemcpyAsync(ptr, devptr, n_bytes, cudaMemcpyDeviceToHost) );
    };

  auto nlist_cpy = [&](nodeid_t*& ptr){ return list_cpy(ptr, n_nodes); };
  auto nnieglist_cpy = [&](nodeid_t*& ptr){ return list_cpy(ptr, n_nodes * NNIEG_OVER_ALLOC); };
  auto elist_cpy = [&](cu_graph_CH& gr, nodeid_t*& ptr)
                   { return list_cpy(ptr, gr.max_num_edges); };

  nodeid_t *node_idx = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(node_idx, graph_out->node_idx, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_idx = node_idx;
  nodeid_t *node_idx_inv = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(node_idx_inv, graph_out->node_idx_inv, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_idx_inv = node_idx_inv;
  nodeid_t *node_ranks = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(node_ranks, graph_out->node_ranks, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_ranks = node_ranks;
  nodeid_t *node_levels = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(node_levels, graph_out->node_levels, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_levels = node_levels;

  nodeid_t *node_levels_pt = (nodeid_t *)malloc(graph_out->max_rank * sizeof(nodeid_t));
  CE( cudaMemcpy(node_levels_pt, graph_out->node_levels_pt, graph_out->max_rank * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->node_levels_pt = node_levels_pt;

  nodeid_t *pointer = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(pointer, graph_out->graph_u_f.pointer, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_u_f.pointer = pointer;
  nodeid_t *num_neighbors = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(num_neighbors, graph_out->graph_u_f.num_neighbors, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_u_f.num_neighbors = num_neighbors;
  nodeid_t *nieg_count = (nodeid_t *)malloc(n_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t));
  CE( cudaMemcpy(nieg_count, graph_out->graph_u_f.NNIEG, n_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_u_f.NNIEG = nieg_count;

  elist_cpy(graph_out->graph_u_f, graph_out->graph_u_f.neighbors);
  elist_cpy(graph_out->graph_u_f, graph_out->graph_u_f.weights);
  elist_cpy(graph_out->graph_u_f, graph_out->graph_u_f.midpoint);
  elist_cpy(graph_out->graph_u_f, graph_out->graph_u_f.vertex_ID);


  nodeid_t *neighbors = nullptr;

  nlist_cpy(graph_out->graph_u_b.pointer);
  nlist_cpy(graph_out->graph_u_b.num_neighbors);
  nnieglist_cpy(graph_out->graph_u_b.NNIEG);
  elist_cpy(graph_out->graph_u_b, graph_out->graph_u_b.neighbors);
  elist_cpy(graph_out->graph_u_b, graph_out->graph_u_b.weights);
  elist_cpy(graph_out->graph_u_b, graph_out->graph_u_b.midpoint);

  elist_cpy(graph_out->graph_u_f /* [sic] */, graph_out->graph_u_b.vertex_ID);

  pointer = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(pointer, graph_out->graph_d_b.pointer, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_d_b.pointer = pointer;
  num_neighbors = (nodeid_t *)malloc(n_nodes * sizeof(nodeid_t));
  CE( cudaMemcpy(num_neighbors, graph_out->graph_d_b.num_neighbors, n_nodes * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_d_b.num_neighbors = num_neighbors;
  nieg_count = (nodeid_t *)malloc(n_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t));
  CE( cudaMemcpy(nieg_count, graph_out->graph_d_b.NNIEG, n_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_d_b.NNIEG = nieg_count;
  neighbors = (nodeid_t *)malloc(graph_out->graph_d_b.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(neighbors, graph_out->graph_d_b.neighbors, graph_out->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_d_b.neighbors = neighbors;
  elist_cpy(graph_out->graph_u_f /* [sic] */, graph_out->graph_d_b.vertex_ID);

  nodeid_t* weights = (nodeid_t *)malloc(graph_out->graph_d_b.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(weights, graph_out->graph_d_b.weights, graph_out->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_d_b.weights = weights;
  nodeid_t* midpoint = (nodeid_t *)malloc(graph_out->graph_d_b.max_num_edges * sizeof(nodeid_t));
  CE( cudaMemcpy(midpoint, graph_out->graph_d_b.midpoint, graph_out->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyDeviceToHost) );
  graph_out->graph_d_b.midpoint = midpoint;

  const size_t nnodes_k = graph_out->overlay_APSP_path.num_nodes;
  const size_t nnodes_ap = nnodes_k * nnodes_k;

  list_cpy(graph_out->overlay_CH.num_neighbors, nnodes_k);
  list_cpy(graph_out->overlay_CH.pointer, nnodes_k);
  elist_cpy(graph_out->overlay_CH,graph_out->overlay_CH.neighbors);
  elist_cpy(graph_out->overlay_CH,graph_out->overlay_CH.weights);
  elist_cpy(graph_out->overlay_CH,graph_out->overlay_CH.midpoint);

  assert( nnodes_ap || !graph_out->overlay_APSP_path.prev );
  list_cpy(graph_out->overlay_APSP_path.prev,nnodes_ap);
  list_cpy(graph_out->overlay_APSP_path.dist,nnodes_ap);

  *cu_graph_UD_h = graph_out;
  
}

void graph_cu_CH_UD_h2d(cu_graph_CH_UD_t **cu_graph_UD_d, cu_graph_CH_UD_t *cu_graph_UD_h)
{
  cu_graph_CH_UD_t *graph_h =
    (cu_graph_CH_UD_t *) calloc(1,sizeof(cu_graph_CH_UD_t));
  graph_h->max_degree = cu_graph_UD_h->max_degree;
  graph_h->thread_per_node = cu_graph_UD_h->thread_per_node;
  graph_h->overlay_size = cu_graph_UD_h->overlay_size;
  graph_h->max_rank = cu_graph_UD_h->max_rank;

  graph_h->graph_u_f.num_nodes = cu_graph_UD_h->graph_u_f.num_nodes;
  graph_h->graph_u_f.max_weight = cu_graph_UD_h->graph_u_f.max_weight;
  graph_h->graph_u_f.max_num_edges = cu_graph_UD_h->graph_u_f.max_num_edges;
  graph_h->graph_u_f.empty_pointer = cu_graph_UD_h->graph_u_f.empty_pointer;

  graph_h->graph_u_b.num_nodes = cu_graph_UD_h->graph_u_b.num_nodes;
  graph_h->graph_u_b.max_weight = cu_graph_UD_h->graph_u_b.max_weight;
  graph_h->graph_u_b.max_num_edges = cu_graph_UD_h->graph_u_b.max_num_edges;
  graph_h->graph_u_b.empty_pointer = cu_graph_UD_h->graph_u_b.empty_pointer;

  graph_h->graph_d_b.num_nodes = cu_graph_UD_h->graph_d_b.num_nodes;
  graph_h->graph_d_b.max_weight = cu_graph_UD_h->graph_d_b.max_weight;
  graph_h->graph_d_b.max_num_edges = cu_graph_UD_h->graph_d_b.max_num_edges;
  graph_h->graph_d_b.empty_pointer = cu_graph_UD_h->graph_d_b.empty_pointer;

  graph_h->overlay_CH.num_nodes = cu_graph_UD_h->overlay_CH.num_nodes;
  graph_h->overlay_CH.max_weight = cu_graph_UD_h->overlay_CH.max_weight;
  graph_h->overlay_CH.max_num_edges = cu_graph_UD_h->overlay_CH.max_num_edges;
  graph_h->overlay_CH.empty_pointer = cu_graph_UD_h->overlay_CH.empty_pointer;

  const nodeid_t nnodes_K = cu_graph_UD_h->overlay_APSP_path.num_nodes;
  graph_h->overlay_APSP_path.num_nodes = nnodes_K;

  nodeid_t *val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->node_ranks, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->node_ranks = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->node_levels, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->node_levels = val;
  CE( cudaMalloc(&val, (cu_graph_UD_h->node_levels[cu_graph_UD_h->graph_u_f.num_nodes-1]+2) * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->node_levels_pt, (cu_graph_UD_h->node_levels[cu_graph_UD_h->graph_u_f.num_nodes-1]+2) * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->node_levels_pt = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->node_idx, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->node_idx = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->node_idx_inv, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->node_idx_inv = val;

  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_f.pointer, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_f.pointer = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_f.num_neighbors, graph_h->graph_u_f.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_f.num_neighbors = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_f.NNIEG, graph_h->graph_u_f.num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_f.NNIEG = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_f.neighbors, graph_h->graph_u_f.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_f.neighbors = val;
  CE( cudaMalloc(&val, graph_h->graph_u_f.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_f.vertex_ID, graph_h->graph_u_f.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_f.vertex_ID = val;
  
  CE( cudaMalloc(&val, graph_h->graph_u_b.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.pointer, graph_h->graph_u_b.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.pointer = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.num_neighbors, graph_h->graph_u_b.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.num_neighbors = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.NNIEG, graph_h->graph_u_b.num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.NNIEG = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.neighbors, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.neighbors = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.vertex_ID, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.vertex_ID = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.weights, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.weights = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_u_b.midpoint, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_u_b.midpoint = val;
  CE( cudaMalloc(&val, graph_h->graph_u_b.max_num_edges * sizeof(nodeid_t)) );
  
  CE( cudaMalloc(&val, graph_h->graph_d_b.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.pointer, graph_h->graph_d_b.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.pointer = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.num_neighbors, graph_h->graph_d_b.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.num_neighbors = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.neighbors, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.neighbors = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.NNIEG, graph_h->graph_d_b.num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.NNIEG = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.vertex_ID, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.vertex_ID = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.weights, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.weights = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->graph_d_b.midpoint, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->graph_d_b.midpoint = val;
  CE( cudaMalloc(&val, graph_h->graph_d_b.max_num_edges * sizeof(nodeid_t)) );

  CE( cudaMalloc(&val, graph_h->overlay_CH.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_CH.pointer, graph_h->overlay_CH.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_CH.pointer = val;
  CE( cudaMalloc(&val, graph_h->overlay_CH.num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_CH.num_neighbors, graph_h->overlay_CH.num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_CH.num_neighbors = val;
  CE( cudaMalloc(&val, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_CH.neighbors, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_CH.neighbors = val;
  CE( cudaMalloc(&val, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_CH.weights, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_CH.weights = val;
  CE( cudaMalloc(&val, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_CH.midpoint, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_CH.midpoint = val;
  CE( cudaMalloc(&val, graph_h->overlay_CH.max_num_edges * sizeof(nodeid_t)) );
  
  CE( cudaMalloc(&val, nnodes_K * nnodes_K * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_APSP_path.prev, nnodes_K * nnodes_K * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_APSP_path.prev = val;
  CE( cudaMalloc(&val, nnodes_K * nnodes_K * sizeof(nodeid_t)) );
  CE( cudaMemcpy(val, cu_graph_UD_h->overlay_APSP_path.dist, nnodes_K * nnodes_K * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->overlay_APSP_path.dist = val;

  cu_graph_CH_UD_t *graph_d;
  CE( cudaMalloc(&graph_d, sizeof(cu_graph_CH_UD_t)) );
  CE( cudaMemcpy(graph_d, graph_h, sizeof(cu_graph_CH_UD_t), cudaMemcpyHostToDevice) );

  free(graph_h);

  *cu_graph_UD_d = graph_d;

}


// reassign the node scores so that each node has a unique score, this helps remove any symmetry
// when using luby's parallel MIS algorithm using the node scores
__global__ void
cu_CH_linear_order
(nodeid_t *node_scores, nodeid_t *nid, cu_graph_CH_bi_t *graph)
{
  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  // compute num_threads
  const int num_thd = blockDim.x * gridDim.x;

  const nodeid_t num_nodes = graph->graph_f.num_nodes;
  const nodeid_t max_num_selected = graph->max_num_selected;

  for ( nodeid_t i=tid; i<max_num_selected; i+=num_thd ){
    const nodeid_t nid_l = nid[i];
    node_scores[ nid_l ] = num_nodes - i;
  }
}


// gathers combined nnbr (both forwards and backwards) for each of the nodes in selected_list
__global__ void cu_CH_gather_s_key(nodeid_t *nnbr, nodeid_t *selected_list, cu_graph_CH_bi_t *graph)
{
  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  // compute num_threads
  const int num_thd = blockDim.x * gridDim.x;

  const uint32_t num_nodes = graph->graph_f.num_nodes;
  const uint32_t list_length = selected_list[0];

  const bool sort_by_orig_id = true;

  for(uint32_t i=tid; i<list_length; i+=num_thd){
    uint32_t current = selected_list[i + 1];
    if ( sort_by_orig_id )
      {
        nnbr[i] = graph->node_ids[current];
        continue;
      }
    uint32_t combined_degree;
    assert(current < num_nodes);
    combined_degree = graph->graph_f.num_neighbors[current];
    combined_degree += graph->graph_b.num_neighbors[current];
    nnbr[i] = combined_degree;
  }

}

__constant__ cu_graph_CH_bi_t graph_bi_c;
__host__ void
cu_CH_select_nodes_pre_launch(cu_graph_CH_bi_t *graph_bi_d)
{
  CE( cudaMemcpyToSymbolAsync
      (graph_bi_c,graph_bi_d,sizeof(graph_bi_c), 0, cudaMemcpyDeviceToDevice) );
}

// selected list allocated by caller with a fixed length (cut_off+1 * num_nodes)
// active_length - 1 (number of selected nodes) will be put into element zero by this function.
// note, this version runs a single iteration of Luby's algorithm and uses multiple blocks.
template <uint32_t tpn> __global__ void
__launch_bounds__(1 << select_nodes_tpn_to_block_lg(tpn))
cu_CH_select_nodes_gl
(nodeid_t *selected_list,
 nodeid_t *node_scores_in, nodeid_t *node_scores_out, nodeid_t *nid)
{
  
#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const cu_graph_CH_bi_t* const graph = &graph_bi_c;

  const int block_lg = select_nodes_tpn_to_block_lg(tpn);
  const int block_dim = 1 << block_lg;
  assert(block_dim == blockDim.x);

  const int e_tpn = tpn > block_lg ? block_lg : tpn;

  // compute num_threads
  const int num_thd = block_dim * gridDim.x;

  // local_ID, thread group's working set id within current block (needed for shared mem accesses)
  const nodeid_t lid = threadIdx.x >> e_tpn;

  const uint32_t tnid = threadIdx.x - (lid<<e_tpn); // thread's neighbor id, tid%(1<<tpn)
  
  const nodeid_t num_nodes = graph->graph_f.num_nodes;
  const nodeid_t max_num_selected = graph->max_num_selected;
  
  // increment value in each iteration
  const int vertex_inc = num_thd >> e_tpn;
  // number of vertices handled at each iteration by each block
  const int vertex_per_iter = block_dim >> e_tpn;
  
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)

  __shared__ uint32_t sh_current_id[vertex_per_iter];
  __shared__ volatile uint32_t sh_current_status[vertex_per_iter];
  __shared__ uint32_t sh_current_score[vertex_per_iter];
  __shared__ uint32_t sh_current_pointer_f[vertex_per_iter];
  __shared__ uint32_t sh_current_pointer_b[vertex_per_iter];
  __shared__ uint32_t sh_current_nnbr_f[vertex_per_iter];
  __shared__ uint32_t sh_current_nnbr_b[vertex_per_iter];

  auto make_f = [&](uint32_t* p) {
    return [=](uint idx) -> uint32_t&
    { assert( !debug_sm_idx || idx < vertex_per_iter ); return p[idx]; };  };

  auto SH_CURRENT_ID = make_f(sh_current_id);
  auto SH_CURRENT_SCORE = make_f(sh_current_score);
  auto SH_CURRENT_POINTER_F = make_f(sh_current_pointer_f);
  auto SH_CURRENT_POINTER_B = make_f(sh_current_pointer_b);
  auto SH_CURRENT_NNBR_F = make_f(sh_current_nnbr_f);
  auto SH_CURRENT_NNBR_B = make_f(sh_current_nnbr_b);

  constexpr int sl_buffer_size = block_dim;
  constexpr int sl_buffer_limit = sl_buffer_size - vertex_per_iter;
  __shared__ nodeid_t sh_sl_buffer[sl_buffer_size];
  __shared__ int sl_buffer_tail;

  if ( !threadIdx.x ) sl_buffer_tail = 0;

  /// RRRR careful of syncthreads imbalance, in the single block/multiple block version you had to make sure it
  /// does not happen, it may or may not be necessary here

  // first iteration of Luby's algorithm for MIS; for any of the nodes contesting to be selected for
  // contraction (contesting to be in MIS), if they have the highest score in their neighborhood of
  // other contesting nodes, add them to the selected list

  nodeid_t current_pos = blockIdx.x * vertex_per_iter + lid; // current thread group's position within node_list
  nodeid_t current_pos_0 = blockIdx.x * vertex_per_iter;
  while( current_pos_0 < max_num_selected ){
    // load current iteration's node IDs and the corresponding pointer/nnbr/node status into shared
    if ( threadIdx.x < vertex_per_iter )
      {
        const nodeid_t current_pos_s = current_pos_0 + threadIdx.x;
        if ( current_pos_s < max_num_selected )
          {
            const nodeid_t cid = nid[ current_pos_s ];
            const uint32_t current_score = node_scores_in[ cid ];
            const bool contesting =
              current_pos_s < max_num_selected
              && current_score != 0 && current_score != num_nodes+1;
            sh_current_status[ threadIdx.x ] = contesting;
            if ( contesting )
              {
                SH_CURRENT_ID( threadIdx.x ) = cid;
                SH_CURRENT_SCORE( threadIdx.x ) = current_score;
                SH_CURRENT_POINTER_F(threadIdx.x) = graph->graph_f.pointer[cid];
                SH_CURRENT_POINTER_B(threadIdx.x) = graph->graph_b.pointer[cid];
                SH_CURRENT_NNBR_F(threadIdx.x)
                  = graph->graph_f.num_neighbors[cid];
                SH_CURRENT_NNBR_B(threadIdx.x)
                  = graph->graph_b.num_neighbors[cid];
              }
          }
        else
          {
            sh_current_status[ threadIdx.x ] = 0;
          }
    }

    // sync needed here because the first warps are cooperating in loading the entire block's data
    __syncthreads();

    // since we're re-issueing the scores before entering this kernel, all non-contesting nodes
    // have score 0.

    const uint32_t current_score = SH_CURRENT_SCORE( lid );

    constexpr int n_blk = 1 << tpn - e_tpn;
    nodeid_t fwd_nbrs[n_blk], bwd_nbrs[n_blk];

    if ( sh_current_status[ lid ] ){
      // for cases where tpn > block_lg
      for(uint32_t i=0; i < n_blk; i++){

        const uint32_t amask = __activemask();

	const uint32_t e_tnid = tnid + i*block_dim;
        const nodeid_t fwd_nbr = e_tnid < SH_CURRENT_NNBR_F( lid )
          ? graph->graph_f.neighbors[SH_CURRENT_POINTER_F( lid ) + e_tnid]
          : nodeid_invalid;
        fwd_nbrs[i] = fwd_nbr;

	// if a neighbor with higher score is contesting, we are eliminated

        if ( !nodeid_is_special(fwd_nbr)
             && current_score < node_scores_in[ fwd_nbr ] )
          sh_current_status[ lid ] = 0;

        __syncwarp(amask);

        if ( sh_current_status[ lid ] == 0 ) break;

        const nodeid_t bwd_nbr = e_tnid < SH_CURRENT_NNBR_B( lid )
          ? graph->graph_b.neighbors[SH_CURRENT_POINTER_B( lid ) + e_tnid]
          : nodeid_invalid;

        bwd_nbrs[i] = bwd_nbr;

        if ( !nodeid_is_special( bwd_nbr )
             && current_score < node_scores_in[ bwd_nbr ] )
          sh_current_status[ lid ] = 0;

        if ( sh_current_status[ lid ] == 0 ) break;
      }
    }
    
    __syncthreads();

    // set the scores for neighbors to winning nodes to 0, essentially removing them from contesting
    // this is to allow for multi-iter Luby's
    if ( sh_current_status[ lid ] ){
      // for cases where tpn > block_lg
      for(uint32_t i=0; i < n_blk; i++){
	
        const nodeid_t fwd_nbr = fwd_nbrs[i];
        const nodeid_t bwd_nbr = bwd_nbrs[i];

        if ( !nodeid_is_special( fwd_nbr ) ) node_scores_out[ fwd_nbr ] = 0;
        if ( !nodeid_is_special( bwd_nbr ) ) node_scores_out[ bwd_nbr ] = 0;

      }
      if(tnid == 0){
	// to avoid inserting the same node into list multiple times
	node_scores_out[ SH_CURRENT_ID( lid ) ] = num_nodes + 1;
      }
    }

    if ( vertex_per_iter == 1 )
      {
        if ( !threadIdx.x && sh_current_status[0] )
          sh_sl_buffer[ sl_buffer_tail++ ] = sh_current_id[ 0 ];
      }
    else if ( vertex_per_iter == 2 )
      {
        if ( threadIdx.x < 2 )
          {
            const int status_0 = sh_current_status[0];
            const int status_1 = sh_current_status[1];
            const int bidx = sl_buffer_tail + ( threadIdx.x ? status_0 : 0 );
            if ( sh_current_status[threadIdx.x] )
              sh_sl_buffer[ bidx ] = sh_current_id[ threadIdx.x ];
            __syncwarp(0x3);
            if ( threadIdx.x == 0 ) sl_buffer_tail += status_0 + status_1;
          }
      }
    else if ( true )
      {
        if ( !tnid && sh_current_status[lid] )
          {
            const int bidx = atomicAdd( &sl_buffer_tail, 1 );
            assert( bidx < sl_buffer_size );
            sh_sl_buffer[ bidx ] = sh_current_id[ lid ];
          }
      }
    else if ( threadIdx.x < vertex_per_iter )
      {
        const uint32_t smap = ( 1 << vertex_per_iter ) - 1;
        assert( vertex_per_iter <= 32 );
        __syncwarp(smap);
        const bool contest_s = sh_current_status[threadIdx.x];
        const uint32_t cvec = __ballot_sync(smap, contest_s);
        const uint32_t ovec = cvec >> threadIdx.x;
        const int pfx = __popc(ovec);
        const int bidx = sl_buffer_tail + pfx - contest_s;
        if ( contest_s )
          sh_sl_buffer[ bidx ] = sh_current_id[ threadIdx.x ];
        __syncwarp(smap);
        if ( !threadIdx.x ) sl_buffer_tail += pfx;
      }

    __syncthreads();

    // setup next iteration
    current_pos += vertex_inc;
    current_pos_0 += vertex_inc;

    const bool last_iter = current_pos_0 >= max_num_selected;

    if ( last_iter || sl_buffer_tail >= sl_buffer_limit )
      {
        __shared__ nodeid_t sl_start;
        const bool have_work = threadIdx.x < sl_buffer_tail;
        if ( !threadIdx.x )
          sl_start = 1 + atomicAdd( &selected_list[0], sl_buffer_tail );
        __syncthreads();
        if ( have_work )
          selected_list[ sl_start + threadIdx.x ] = sh_sl_buffer[threadIdx.x];
        if ( !threadIdx.x ) sl_buffer_tail = 0;
      }
  }

  __syncthreads();
  assert( sl_buffer_tail == 0 );
}


// fill array with ID (array[0]=0, array[1]=1 ...) used to prepare data for sort
__global__ void array_fill_linear(uint32_t *array, uint32_t len)
{
  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  // compute num_threads
  const int num_thd = blockDim.x * gridDim.x;

  for(uint32_t i=tid; i<len; i+=num_thd){
    array[i] = i;
  }
}

// initialize an empty cu_graph_CH on the device (once used for graph_b_removed)
void graph_cu_CH_init_d(cu_graph_CH_t **cu_graph_CH, uint32_t num_nodes, uint32_t len, uint32_t max_nnbr_init, bool UD)
{

  ASSERTA( num_nodes * max_nnbr_init < len );

  cu_graph_CH_t *graph_h = (cu_graph_CH_t *) malloc(sizeof(cu_graph_CH_t));
  nodeid_t *pointer_h = (nodeid_t *) malloc(num_nodes * sizeof(nodeid_t));
  for(uint32_t i=0; i<num_nodes; i++){
    pointer_h[i] = i*max_nnbr_init;
  }
  nodeid_t *pointer;
  CE( cudaMalloc(&pointer, num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(pointer, pointer_h, num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->pointer = pointer;
  nodeid_t *num_neighbors;
  CE( cudaMalloc(&num_neighbors, num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemset(num_neighbors, 0, num_nodes * sizeof(nodeid_t)) );
  graph_h->num_neighbors = num_neighbors;
  if(UD){
    nodeid_t *NNIEG;
    CE( cudaMalloc(&NNIEG, num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
    CE( cudaMemset(NNIEG, 0, num_nodes * NNIEG_OVER_ALLOC * sizeof(nodeid_t)) );
    graph_h->NNIEG = NNIEG;
  } else {
    graph_h->NNIEG = NULL;
  }
  nodeid_t *max_num_neighbors_allowed_h = (nodeid_t *) malloc(num_nodes * sizeof(nodeid_t));
  for(uint32_t i=0; i<num_nodes; i++){
    max_num_neighbors_allowed_h[i] = max_nnbr_init;
  }
  nodeid_t *max_num_neighbors_allowed;
  CE( cudaMalloc(&max_num_neighbors_allowed, num_nodes * sizeof(nodeid_t)) );
  CE( cudaMemcpy(max_num_neighbors_allowed, max_num_neighbors_allowed_h, num_nodes * sizeof(nodeid_t), cudaMemcpyHostToDevice) );
  graph_h->max_num_neighbors_allowed = max_num_neighbors_allowed;
  if(UD){
    nodeid_t *vertex_ID;
    CE( cudaMalloc(&vertex_ID, num_nodes * sizeof(nodeid_t)) );
    CE( cudaMemset(vertex_ID, 0, num_nodes * sizeof(nodeid_t)) );
    graph_h->vertex_ID = vertex_ID;
  } else {
    graph_h->vertex_ID = NULL;
  }
  nodeid_t *neighbors;
  CE( cudaMalloc(&neighbors, len * sizeof(nodeid_t)) );
  graph_h->neighbors = neighbors;
  nodeid_t *weights;
  CE( cudaMalloc(&weights, len * sizeof(nodeid_t)) );
  graph_h->weights = weights;
  nodeid_t *midpoint;
  CE( cudaMalloc(&midpoint, len * sizeof(nodeid_t)) );
  CE( cudaMemset(midpoint, 0xFF, len * sizeof(nodeid_t)) );
  graph_h->midpoint = midpoint;
  graph_h->num_nodes = num_nodes;
  graph_h->max_weight = 0;
  graph_h->max_num_edges = len;
  graph_h->empty_pointer = num_nodes * max_nnbr_init;


  

  cu_graph_CH_t *graph_out;
  CE( cudaMalloc(&graph_out, sizeof(cu_graph_CH_t)) );
  CE( cudaMemcpy(graph_out, graph_h, sizeof(cu_graph_CH_t), cudaMemcpyHostToDevice) );

  *cu_graph_CH = graph_out;

  free(graph_h);

  free(pointer_h);
  free(max_num_neighbors_allowed_h);

}



Graph_Cache_Elt*
graph_cu_CH_bi_alloc(nodeid_t num_nodes, nodeid_t num_edges)
{
  Graph_Cache_Elt *ge = NULL;

  const int nna = 8;  // Number of node arrays.
  const ptrdiff_t underuse_limit_bytes = size_t(1) << 33;
  const bool trace_allocs = verbosity;  // If true print allocation messages.

  for ( auto& elt: graph_cache )
    if ( !elt.second.in_use ) { ge = &elt.second; break; }

  if ( !ge )
    {
      cu_graph_CH_bi_t* graph_d = NULL;
      CE( cudaMalloc(&graph_d, sizeof(*graph_d)) );
      ge = &graph_cache[graph_d];
      ge->graph_d = graph_d;
      ge->graph_h.self_d = graph_d;
    }

  assert( !ge->in_use );
  ge->struct_latest_on_device = false;
  ge->struct_latest_on_host = false;
  ge->in_use = true;

  const ptrdiff_t over_alloc_n =
    sizeof(ge->nodes_d[0]) * nna * ( ptrdiff_t(ge->n_nodes) - num_nodes );

# define afb(m) ag(graph_f.m) ag(graph_b.m)
  if ( over_alloc_n > underuse_limit_bytes || ge->n_nodes < num_nodes )
    {
      if ( ge->nodes_d ) CE( cudaFree( ge->nodes_d ) );
      const size_t node_stride_elts = rnd_up(num_nodes,32);
      const size_t alloc_amt = node_stride_elts * nna * sizeof(ge->nodes_d[0]);
      if ( trace_allocs )
        printf("Graph Cache nodes: have %8d, allocating %2d * %8zu nds,  %4zu MiB.\n",
               ge->n_nodes, nna, node_stride_elts, alloc_amt >> 20);
      int na = 0;         // Node array number.
      CE( cudaMalloc( &ge->nodes_d, alloc_amt ) );
#     define ag(m) ge->graph_h.m = &ge->nodes_d[node_stride_elts*na++];
      afb(pointer);
      afb(num_neighbors);
      afb(max_num_neighbors_allowed);
      ag(node_ranks); ag(node_ids);
      assert( nna == na );
#     undef ag
      ge->n_nodes = node_stride_elts;
    }
# undef afb

  graph_cu_CH_bi_edges_alloc(ge,num_edges);

  return ge;
}

void
edge_elt_assign(Graph_Cache_Elt *ge, int dir)
{
  cu_graph_CH_t* const graph = ge->graph_get(dir);
  Edge_Cache_Elt& elt = ge->e_elts[dir];
  int ea = 0;
# define ag(m) graph->m = (decltype(graph->m)) &elt.edges_d[elt.n_edges*ea++];
  ag(neighbors); ag(weights); ag(midpoint);
  if ( opt_elist_use ) { ag(elist); ea++; }
  assert( edge_cache_n_edge_arrays == ea );
# undef ag
}

nodeid_t
graph_cache_n_edges_allocated_get(cu_graph_CH_bi_t *graph)
{
  Graph_Cache_Elt* const ge = graph_cache_elt_lookup(graph);
  assert( bool(ge->e_elts[GC_bwd].n_edges)==bool(ge->e_elts[GC_fwd].n_edges) );
  if ( !ge->e_elts[GC_bwd].n_edges ) return 0;
  return min( ge->e_elts[GC_bwd].n_edges, ge->e_elts[GC_fwd].n_edges );
}


void
graph_cu_CH_bi_edges_alloc(Graph_Cache_Elt *ge, nodeid_t n_edges, int dirp)
{
  if ( !n_edges ) return;
  for ( int dir: {GC_bwd, GC_fwd } )
    if ( dirp == 3 || dirp == dir )
      {
        edge_cache_alloc(ge->e_elts[dir],n_edges);
        edge_elt_assign(ge,dir);
      }
}

void
graph_cu_CH_bi_edges_realloc
(cu_graph_CH_bi_t *cu_graph_CH_bi_d, nodeid_t n_edges, int dir)
{
  Graph_Cache_Elt* const ge = graph_cache_elt_lookup(cu_graph_CH_bi_d);
  ge->struct_device_to_host();
  graph_cu_CH_bi_edges_alloc(ge, n_edges, dir);
  if ( n_edges ) graph_cu_CH_bi_edges_init(ge, n_edges);
  ge->struct_host_to_device();
}

void
graph_cu_CH_bi_edges_reuse_fwd_for_bwd
(cu_graph_CH_bi_t *graph_new, cu_graph_CH_bi_t *graph_old, nodeid_t n_edges)
{
  Graph_Cache_Elt* const ge_new = graph_cache_elt_lookup(graph_new);
  Graph_Cache_Elt* const ge_old = graph_cache_elt_lookup(graph_old);
  assert( !ge_new->e_elts[GC_bwd].n_edges );
  swap( ge_new->e_elts[GC_bwd], ge_old->e_elts[GC_fwd] );
  graph_cu_CH_bi_edges_realloc(graph_old, 0, GC_fwd );
  graph_cu_CH_bi_edges_realloc(graph_new, n_edges, GC_bwd );
}
void
graph_cu_CH_bi_edges_reuse_bwd_for_fwd
(cu_graph_CH_bi_t *graph_new, cu_graph_CH_bi_t *graph_old)
{
  Graph_Cache_Elt* const ge_new = graph_cache_elt_lookup(graph_new);
  Graph_Cache_Elt* const ge_old = graph_cache_elt_lookup(graph_old);
  assert( !ge_new->e_elts[GC_fwd].n_edges );
  const nodeid_t n_edges = ge_new->e_elts[GC_bwd].n_edges;
  swap( ge_old->e_elts[GC_bwd], ge_new->e_elts[GC_fwd] );
  graph_cu_CH_bi_edges_realloc(graph_new, n_edges, GC_fwd );
}

void
graph_cu_CH_bi_init_d
(cu_graph_CH_bi_t **cu_graph_CH_bi, nodeid_t num_nodes, nodeid_t n_edges_alloc)
{
  Graph_Cache_Elt *ge = graph_cu_CH_bi_alloc(num_nodes,n_edges_alloc);

  const size_t node_array_sz_bytes = num_nodes*sizeof(ge->graph_h.node_ids[0]);
  CE( cudaMemsetAsync(ge->graph_h.node_ranks, 0, node_array_sz_bytes) );

  ge->graph_h.graph_f.empty_pointer = 0;
  ge->graph_h.graph_b.empty_pointer = 0;
  ge->graph_h.work_item_next = 0;
  ge->graph_h.num_nodes = num_nodes;
  ge->graph_h.graph_f.num_nodes = num_nodes;
  ge->graph_h.graph_b.num_nodes = num_nodes;
  ge->struct_latest_on_device = false;
  ge->struct_latest_on_host = true;

  graph_cu_CH_bi_edges_init(ge, n_edges_alloc);

  ge->struct_host_to_device();
  *cu_graph_CH_bi = ge->graph_d;
}

void
graph_cu_CH_bi_edges_init
(Graph_Cache_Elt *ge, nodeid_t n_edges)
{
  const size_t edge_array_sz_bytes = n_edges*sizeof(ge->graph_h.node_ids[0]);
  if ( n_edges && ge->e_elts[GC_fwd].n_edges )
    for ( auto d: {ge->graph_h.graph_f.midpoint,ge->graph_h.graph_f.midpoint} )
      CE( cudaMemsetAsync(d, 0xFF, edge_array_sz_bytes) );

  assert( ge->struct_latest_on_host == true );
  assert( ge->struct_latest_on_device == false );
  ge->graph_h.graph_f.max_num_edges = n_edges;
  ge->graph_h.graph_b.max_num_edges = n_edges;
}

template <bool verify>
__global__ void __launch_bounds__(1024)
cu_APSP_blocked(weight_t *dist, nodeid_t *prev, uint32_t num_nodes, uint32_t iter, uint32_t phase)
{

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  // If true, perform overflow check.
  const bool ver = verify;

  const int ndiag = ( blockIdx.x == iter ) + ( blockIdx.y == iter );
  if ( phase == 0 && ndiag != 2 ) return;
  if ( phase == 1 && ndiag != 1 ) return;
  if ( phase == 2 && ndiag != 0 ) return;

  assert(gridDim.x == gridDim.y);
  assert(gridDim.x * APSP_BLSZ == num_nodes);
  assert(iter < gridDim.x);
  assert(phase < 3);
  assert(blockDim.x == 1024);

  weight_t *column;
  weight_t *row;
  weight_t *dist_out;
  nodeid_t *prev_out;

  column = &dist[APSP_BLSZ*blockIdx.x*num_nodes + APSP_BLSZ*iter];
  row = &dist[APSP_BLSZ*iter*num_nodes + APSP_BLSZ*blockIdx.y];
  dist_out = &dist[APSP_BLSZ*blockIdx.x*num_nodes + APSP_BLSZ*blockIdx.y];
  prev_out = &prev[APSP_BLSZ*blockIdx.x*num_nodes + APSP_BLSZ*blockIdx.y];

  __shared__ weight_t sh_column[APSP_BLSZ*APSP_BLSZ];
  __shared__ weight_t sh_row[APSP_BLSZ*APSP_BLSZ];

  weight_t dist_out_l[4];
  nodeid_t prev_out_l[4];

  const uint32_t lid = threadIdx.x>>6;
  const uint32_t tnid = threadIdx.x - (lid<<6);

  // load column and row into shared and dist into registers (dist_out)
  /// to keep the code clean and homogenous we'll ignore some possible optimizations for phase 0 and 1
  for(uint32_t i=0; i<4; i++){
    // note that i*1024 + threadIdx.x == (16*i + lid)*64 + tnid
    sh_column[i*1024 + threadIdx.x] = column[ (16*i + lid) * num_nodes + tnid ];
    sh_row[i*1024 + threadIdx.x] = row[ (16*i + lid) * num_nodes + tnid ];
    dist_out_l[i] = dist_out[ (16*i + lid) * num_nodes + tnid ];
    prev_out_l[i] = prev_out[ (16*i + lid) * num_nodes + tnid ];
  }
  __syncthreads();

  // calculate shortest paths

  auto calc_BL = [&](bool upd_row, bool upd_col)
    {
      for(uint32_t k=0; k<64; k++){

        const weight_t row_dist = sh_row[ k*64 + tnid ];

        if ( row_dist != NPP_MAX_32U )
          for(uint32_t i=0; i<4; i++){

            /// to avoid having to load prev for column and row, we can't
            /// use prev_...[...]==NODEID_NULL so we must make sure that the
            /// unconnected nodes are initialized to max_uint32

            const weight_t col_dist = sh_column[ (16*i + lid) * 64 + k ];
            if ( col_dist == NPP_MAX_32U ) continue;

            const weight_t dsum = col_dist + row_dist;

            if ( dist_out_l[i] <= dsum ) continue;

            // Check for overflow if verification is on.
            assert( !ver || dsum > col_dist && dsum > row_dist );
            dist_out_l[i] = dsum;
            prev_out_l[i] = iter*64 + k;
          }

        if ( !upd_row && !upd_col ) continue;

        // In phase 0 and 1 of the blocked APSP, there is a direct
        // dependence between the k loops. Synchronize and update the
        // shared data after each k iteration to ensure correctness

        if ( upd_row )
          for ( int i=0; i<4; i++ ) sh_row[i*1024+threadIdx.x]=dist_out_l[i];
        if ( upd_col )
          for ( int i=0; i<4; i++ ) sh_column[i*1024+threadIdx.x]=dist_out_l[i];
        __syncthreads();
      }
  };

  const bool upd_r = phase == 0 || phase == 1 && blockIdx.x == iter;
  const bool upd_c = phase == 0 || phase == 1 && blockIdx.y == iter;

  if ( upd_r || upd_c ) calc_BL(upd_r,upd_c); else calc_BL(false,false);

  // write back the updated dist_out and prev_out to global memory
  for(uint32_t i=0; i<4; i++){
    dist_out[ (16*i + lid) * num_nodes + tnid ] = dist_out_l[i];
    prev_out[ (16*i + lid) * num_nodes + tnid ] = prev_out_l[i];
  }

}

template __global__ void
cu_APSP_blocked<true>
(weight_t *dist, nodeid_t *prev, uint32_t num_nodes,
 uint32_t iter, uint32_t phase);
template __global__ void
cu_APSP_blocked<false>
(weight_t *dist, nodeid_t *prev, uint32_t num_nodes,
 uint32_t iter, uint32_t phase);


template <uint32_t tpn> __global__ void __launch_bounds__(block_dim_APSP_prep)
cu_CH_APSP_prep(cu_graph_CH_UD_t *graph_UD, cu_graph_CH_bi_t *graph)
{

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const int block_lg = block_lg_APSP_prep;
  const int block_size = 1 << block_lg;
  assert( block_size == blockDim.x );
  
  const int e_tpn = tpn > block_lg_APSP_prep ? block_lg_APSP_prep : tpn; // tpn capped
  const int E_TPN = 1 << e_tpn;

  const int iter_per_node = 1 << (tpn - e_tpn);

  // the shared mem for block (used to store all shared data)
  /// we assume (at least for now) nodeid_t and weight_t are the same type (both of type uint32_t)
  extern __shared__ uint32_t shm[];
  
  int make_f_pos = 0;
  auto make_f = [&](uint n) {
    const int pos = make_f_pos;
    make_f_pos += n;
    return [=](uint idx) -> uint32_t&
    { assert( !debug_sm_idx || idx < n ); return shm[pos+idx]; };  };

  // technically this allocation is wasting some share space since not all blocks will access all
  // these arrays, but since only 3 active blocks doesn't matter (unless only 1 SM available!)
  auto SH_CURRENT_NNBR = make_f(block_size);
  auto SH_CURRENT_POINTER = make_f(block_size);
  auto SH_OVERLAY_NNBR = make_f(block_size);
  auto SH_OVERLAY_POINTER = make_f(block_size);
  auto SH_FORWARD_NBR = make_f(block_size);
  auto SH_FORWARD_WEIGHT = make_f(block_size);
  
  const nodeid_t nnodes_ov = graph->num_nodes;

  // block 0 will handle updating the DAGs in graph_UD as roots for U and leafs for D
  if(blockIdx.x==0){
    nodeid_t rank;
    const nodeid_t nnodes_or = graph_UD->num_nodes;
    const nodeid_t nnodes_uf = graph_UD->graph_u_f.num_nodes;
    assert( nnodes_uf + nnodes_ov == nnodes_or );
    nodeid_t UD_max_deg = graph_UD->max_degree;

    __syncthreads();

    if ( nnodes_uf == 0 ){
      // first iteration (note that ranks start from 1, rank==0 means not contracted)
      rank = 1;
      if(threadIdx.x == 0){
	graph_UD->node_levels_pt[0] = 0;
      }
    } else {
      rank = graph_UD->node_ranks[nnodes_uf-1] + 1;
    }

    const nodeid_t empty_pointer_uf = graph_UD->graph_u_f.empty_pointer;
    const nodeid_t empty_pointer_db = graph_UD->graph_d_b.empty_pointer;

    for(uint32_t i=threadIdx.x; i<nnodes_ov; i+=block_size){
      // load the current node into shared (all 96 threads)

      const nodeid_t nid_ud = nnodes_uf + i;

      graph_UD->graph_u_f.pointer[nid_ud] = empty_pointer_uf;
      graph_UD->graph_d_b.pointer[nid_ud] = empty_pointer_db;
      graph_UD->graph_u_f.num_neighbors[nid_ud] = 0;
      graph_UD->graph_d_b.num_neighbors[nid_ud] = 0;

      graph_UD->node_ranks[nid_ud] = rank;
      graph_UD->node_levels[nid_ud] = rank-1;

    }

    if ( threadIdx.x == 0 )
      {
        graph_UD->graph_u_f.num_nodes = nnodes_or;
        graph_UD->graph_d_b.num_nodes = nnodes_or;
        graph_UD->overlay_CH.num_nodes = nnodes_ov;


        // determine tpn for the UD graph

      UD_max_deg = UD_max_deg >> 3; // divide by 8 to make sure 2^n*8 format
      uint32_t nn = 0;
      while(UD_max_deg != 0){
	nn++;
	UD_max_deg = UD_max_deg>>1;
      }
      graph_UD->thread_per_node = nn + 3;
    }

  }
  
  // block 1 will write overlay_CH in graph_UD (needed for unpacking path)
  /// note, we'll write the backwards graph (the CPU code currently uses forward graph which is slower!)
  if(blockIdx.x==1){
    /// it is possible to directly use the working graph, but it can be wasteful in mem usage

    // local_ID, thread group's working set id within current block (needed for shared mem accesses)
    const nodeid_t lid = threadIdx.x >> e_tpn;
    const uint32_t tnid = threadIdx.x - (lid<<e_tpn); // thread's neighbor id

    // load nnbr for all overlay nodes and set the pointers and nnbr to each node accordingly
    uint32_t num_iter = (nnodes_ov / block_size) + 1;
    uint32_t vertex_per_iter = block_size>>e_tpn;
    uint32_t current = threadIdx.x;
    uint32_t next_pt = 0;
    for(uint32_t iter=0; iter<num_iter; iter++){
      if(current < nnodes_ov){
	SH_OVERLAY_NNBR( threadIdx.x ) = graph->graph_b.num_neighbors[current];
	SH_CURRENT_POINTER( threadIdx.x ) = graph->graph_b.pointer[current];
      }
      __syncthreads();
      // thread 0 accumulates nnbr and sets the pointers accordingly
      if(threadIdx.x==0){
	for(uint32_t i=0; i<block_size; i++){
	  if(current + i < nnodes_ov){
	    SH_OVERLAY_POINTER( i ) = next_pt;
	    // align to multiples of 8
	    next_pt += (((SH_OVERLAY_NNBR( i ) + 7)>>3)<<3);
	    // make sure enough space has been allocated for overlay_CH
	    assert(next_pt < graph_UD->overlay_CH.max_num_edges);
	  }
	}
      }
      __syncthreads();
      if(current < nnodes_ov){
	graph_UD->overlay_CH.pointer[current] = SH_OVERLAY_POINTER( threadIdx.x );
	graph_UD->overlay_CH.num_neighbors[current] = SH_OVERLAY_NNBR( threadIdx.x );	
      }
      // copy graph_b into overlay_CH (tpn based)
      /// we don't actually need overlay_CH to hold weight information (only need midpoint)
      for(uint32_t i=0; i < E_TPN; i++){
	const int e_lid = i*vertex_per_iter + lid;
	// we WILL NOT translate node_ids into input domain, instead we'll keep the current node_ids
	// this can be easily translated into UD numbers by adding a constant value:
	// node_levels_u_pt[ max_level ]
	/// since we're not translating node_ids and directly copying everything, using shared does
	/// not make things faster so we'll copy directly from global to global
	if((iter*block_size + e_lid) < nnodes_ov){
	  for(uint32_t j=0; j<iter_per_node; j++){
	    const int e_tnid = j*E_TPN + tnid;
	    if(e_tnid < SH_OVERLAY_NNBR( e_lid )){
	      graph_UD->overlay_CH.neighbors[SH_OVERLAY_POINTER( e_lid ) + e_tnid] = graph->graph_b.neighbors[SH_CURRENT_POINTER( e_lid ) + e_tnid];
	      graph_UD->overlay_CH.weights[SH_OVERLAY_POINTER( e_lid ) + e_tnid] = graph->graph_b.weights[SH_CURRENT_POINTER( e_lid ) + e_tnid];
	      graph_UD->overlay_CH.midpoint[SH_OVERLAY_POINTER( e_lid ) + e_tnid] = graph->graph_b.midpoint[SH_CURRENT_POINTER( e_lid ) + e_tnid];
	    }
	  }
	}
      }
      current += block_size;
      __syncthreads();
    }
    // thread 0 marks the end of the overlay_CH edge_list (needed for when renumbering midpoints)
    if(threadIdx.x==0){
      graph_UD->overlay_CH.empty_pointer = next_pt;
    }
  }

  // block 2 will write the overlay into the APSP_path
  /// possible to modify this so more than one block can work on it together
  if(blockIdx.x==2){

    // local_ID, thread group's working set id within current block (needed for shared mem accesses)
    const nodeid_t lid = threadIdx.x >> e_tpn;
    const uint32_t tnid = threadIdx.x - (lid<<e_tpn); // thread's neighbor id

    // copy weights into the corresponding location in APSP_path (tpn based)
    uint32_t stride = graph_UD->overlay_APSP_path.num_nodes;
    uint32_t *dist = graph_UD->overlay_APSP_path.dist;
    uint32_t *prev = graph_UD->overlay_APSP_path.prev;
    
    // load nnbr for all overlay nodes and set the pointers and nnbr to each node accordingly
    uint32_t num_iter = (nnodes_ov / block_size) + 1;
    uint32_t vertex_per_iter = block_size>>e_tpn;
    uint32_t current = threadIdx.x;
    for(uint32_t iter=0; iter<num_iter; iter++){
      if(current < nnodes_ov){
	SH_CURRENT_NNBR( threadIdx.x ) = graph->graph_f.num_neighbors[current];
	SH_CURRENT_POINTER( threadIdx.x ) = graph->graph_f.pointer[current];
      }
      __syncthreads();
      for(uint32_t i=0; i<E_TPN; i++){
	const int e_lid = i*vertex_per_iter + lid;
	// load everything into shared
	/// using shared here does not necessarily make things faster (if enough space, we can unroll
	/// this loop and get some speedup)
	if((iter*block_size + e_lid) < nnodes_ov){
	  for(uint32_t j=0; j<iter_per_node; j++){
	    const int e_tnid = j*E_TPN + tnid;
	    if(e_tnid < SH_CURRENT_NNBR( e_lid )){
	      SH_FORWARD_NBR( threadIdx.x ) = graph->graph_f.neighbors[SH_CURRENT_POINTER( e_lid ) + e_tnid];
	      SH_FORWARD_WEIGHT( threadIdx.x ) = graph->graph_f.weights[SH_CURRENT_POINTER( e_lid ) + e_tnid];
	    }
	    if(e_tnid < SH_CURRENT_NNBR( e_lid )){
	      dist[ (iter*block_size + e_lid) * stride + SH_FORWARD_NBR( threadIdx.x ) ] = SH_FORWARD_WEIGHT( threadIdx.x );
	      prev[ (iter*block_size + e_lid) * stride + SH_FORWARD_NBR( threadIdx.x ) ] = (iter*block_size + e_lid);
	    }
	  }
	}
      }
      current += block_size;
      __syncthreads();
    }
  }

}


__constant__ cu_graph_CH_UD_t graph_UD_c;

__host__ void
cu_graph_CH_UD_c_update(cu_graph_CH_UD_t *graph_UD_d)
{
  CE( cudaMemcpyToSymbolAsync
      ( graph_UD_c, graph_UD_d, sizeof(graph_UD_c), 0,
        cudaMemcpyDeviceToDevice ) );
}

__global__ void __launch_bounds__(1024,1)
cu_CH_UD_edge_count(cu_graph_CH_UD_t *graph_UD_d)
{
  const int block_dim = 1024;
  assert( block_dim == blockDim.x );
  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int nwps = block_dim >> wp_lg;
  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  const int num_threads = block_dim * gridDim.x;
  const int lane = threadIdx.x % wp_sz;
  const int wp_num = threadIdx.x >> wp_lg;

  nodeid_t thd_n_edges_uf = 0;
  nodeid_t thd_n_edges_ub = 0;
  nodeid_t thd_n_edges_db = 0;

  for ( nodeid_t nid = tid; nid < graph_UD_c.graph_u_f.num_nodes;
        nid += num_threads )
    {
      thd_n_edges_uf += graph_UD_c.graph_u_f.num_neighbors[nid];
      thd_n_edges_ub += graph_UD_c.graph_u_b.num_neighbors[nid];
      thd_n_edges_db += graph_UD_c.graph_d_b.num_neighbors[nid];
    }

  const nodeid_t wp_n_edges_uf = sum_wp(thd_n_edges_uf);
  const nodeid_t wp_n_edges_ub = sum_wp(thd_n_edges_ub);
  const nodeid_t wp_n_edges_db = sum_wp(thd_n_edges_db);

  enum UD_Type { uf, ub, db };
  __shared__ nodeid_t wps_n_edges[3][nwps];
  if ( lane == 0 )
    {
      wps_n_edges[uf][wp_num] = wp_n_edges_uf;
      wps_n_edges[ub][wp_num] = wp_n_edges_ub;
      wps_n_edges[db][wp_num] = wp_n_edges_db;
    }
  __syncthreads();
  if ( wp_num >= 3 ) return;
  const nodeid_t blk_n_edges = sum_wp(((nodeid_t*)wps_n_edges)[threadIdx.x]);
  if ( lane ) return;
  if ( wp_num == 0 )
    atomicAdd(&graph_UD_d->graph_u_f.num_edges_exact, blk_n_edges);
  else if ( wp_num == 1 )
    atomicAdd(&graph_UD_d->graph_u_b.num_edges_exact, blk_n_edges);
  else
    atomicAdd(&graph_UD_d->graph_d_b.num_edges_exact, blk_n_edges);
}


// this is the final kernel of CH which goes over the UD graphs and renumbers all nbrs to their idx
// in the UD space (not doing this will result in an extra level of pointer chasing during queries)
// call this with 1024 blockDim (it's just a sweep/rename of all edges)
// will also renumber the midpoints to allow path unpacking in UD space without pointer chasing
// once everything is finished, the paths and prevs will be translated to input domain, O(N)
__global__ void cu_CH_UD_renum(cu_graph_CH_UD_t *graph_UD)
{

  // Compute thread id
  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  // compute num_threads
  const int num_thd = blockDim.x * gridDim.x;

  const nodeid_t num_nodes = graph_UD->graph_u_f.num_nodes;

  const nodeid_t num_edges_u_f = graph_UD->graph_u_f.empty_pointer;
  const nodeid_t num_edges_d_b = graph_UD->graph_d_b.empty_pointer;

  // calculate num_iters to avoid using a while loop to avoid syncthreads imbalance
  const nodeid_t num_iters_u_f = (num_edges_u_f / num_thd) + 1;
  const nodeid_t num_iters_d_b = (num_edges_d_b / num_thd) + 1;

  uint32_t current = tid;
  uint32_t current_id;
  
  for(uint32_t i=0; i<num_iters_u_f; i++){
    if(current < num_edges_u_f){
      current_id = graph_UD->graph_u_f.neighbors[current];
      // since the UD graphs are much more compressed, during the sweep we go over edges list instead
      // of going over nodes and based on nnbr of each node
      // the condition is to avoid going out of bound when renaming edge endpoints
      if(current_id < num_nodes){
	graph_UD->graph_u_f.neighbors[current] = graph_UD->node_idx_inv[current_id];
      }
    }
    current += num_thd;
  }

  current = tid;
  for(uint32_t i=0; i<num_iters_d_b; i++){
    if(current < num_edges_d_b){
      current_id = graph_UD->graph_d_b.neighbors[current];
      // since the UD graphs are much more compressed, during the sweep we go over edges list instead
      // of going over nodes and based on nnbr of each node
      // the condition is to avoid going out of bound when renaming edge endpoints
      if(current_id < num_nodes){
	graph_UD->graph_d_b.neighbors[current] = graph_UD->node_idx_inv[current_id];
      }
    }
    current += num_thd;
  }
}

void
path_init_d
(path_t& path_hd, path_t*& path_d, nodeid_t num_nodes, nodeid_t src,
 Path_Init_Arrays a )
{
  const size_t nlist1_sz_bytes = num_nodes * sizeof( path_hd.prev[0] );
  const size_t nlist_sz_bytes =
    a == PI_Dist_Array ? nlist1_sz_bytes : 2 * nlist1_sz_bytes;

  if ( !path_d )
    {
      path_hd.num_nodes = num_nodes;
      CE( cudaMalloc( &path_hd.storage, nlist_sz_bytes ) );
      CE( cudaMalloc( &path_d, sizeof(*path_d) ) );
      nodeid_t* const nid_sto = (decltype(path_hd.weight))path_hd.storage;
      switch ( a ) {
      case PI_Dist_Array_Prev_Array:
        path_hd.dist_prev = nullptr;
        path_hd.weight = nid_sto;
        path_hd.prev = nid_sto + num_nodes;
        break;
      case PI_Dist_Array:
        path_hd.dist_prev = nullptr;
        path_hd.weight = nid_sto;
        path_hd.prev = nullptr;
        break;
      case PI_Dist_Prev_Array:
        path_hd.dist_prev = (decltype(path_hd.dist_prev))path_hd.storage;
        path_hd.weight = nullptr;
        path_hd.prev = nullptr;
        break;
      default:
        assert( false );
      }
    }

  assert( path_hd.num_nodes == num_nodes );

  path_hd.src = src;
  path_hd.dest = NPP_MAX_32U;

  CE( cudaMemsetAsync( path_hd.storage, 0xff, nlist_sz_bytes ) );
  cuda_host_to_dev( path_d, path_hd );
}

void
path_free_d(path_t& path_hd, path_t*& path_d)
{
  if ( !path_d ) return;
  CE( cudaFree( path_hd.storage ) );
  CE( cudaFree( path_d ) );
  path_d = nullptr;
  bzero(&path_hd,sizeof(path_hd));
}

void
path_free_h(path_t& path_h)
{
  if ( !path_h.num_nodes ) return;
  free( path_h.storage );
  bzero(&path_h,sizeof(path_h));
}

void
path_d2h(path_t& path_h, const path_t& path_dh)
{
  const nodeid_t num_nodes = path_dh.num_nodes;
  assert( num_nodes );
  const size_t nlist1_sz_bytes = num_nodes * sizeof( path_dh.weight[0] );
  const size_t nlist_sz_bytes =
    path_dh.dist_prev || path_dh.prev ? 2 * nlist1_sz_bytes : nlist1_sz_bytes;
  const bool dist_prev = path_dh.dist_prev;
  if ( path_h.num_nodes == 0 )
    {
      const size_t alloc_amt = dist_prev ? 2 * nlist_sz_bytes : nlist_sz_bytes;
      path_h.storage = (char*)malloc( alloc_amt );
      path_h.num_nodes = num_nodes;
      nodeid_t* const nid_sto = (decltype(path_h.weight))path_h.storage;
      if ( dist_prev )
        {
          path_h.dist_prev = (decltype(path_h.dist_prev))path_h.storage;
          path_h.weight = &nid_sto[2*num_nodes];
          path_h.prev = &nid_sto[3*num_nodes];
        }
      else
        {
          path_h.dist_prev = nullptr;
          path_h.weight = nid_sto;
          path_h.prev = path_dh.prev ? nid_sto + num_nodes : nullptr;
        }
    }

  path_h.src = path_dh.src;
  path_h.dest = path_dh.dest;
  cuda_dev_to_host(path_h.storage,path_dh.storage,nlist_sz_bytes);
  if ( !dist_prev ) return;
  for ( nodeid_t i=0; i<num_nodes; i++ )
    {
      path_h.weight[i] = path_h.dist_prev[i].dist;
      path_h.prev[i] = path_h.dist_prev[i].prev;
    }
}


void graph_cu_UD_free_device(cu_graph_CH_UD_t*& graph_d)
{
  if ( !graph_d ) return;
  cu_graph_CH_UD_t Graph_h;
  cu_graph_CH_UD_t *graph_h = &Graph_h;
  CE( cudaMemcpy(graph_h, graph_d, sizeof(cu_graph_CH_UD_t), cudaMemcpyDeviceToHost) );
  
  CE( cudaFree(graph_h->graph_u_f.pointer) );
  CE( cudaFree(graph_h->graph_u_f.num_neighbors) );
  CE( cudaFree(graph_h->graph_u_f.NNIEG) );
  CE( cudaFree(graph_h->graph_u_f.neighbors) );
  CE( cudaFree(graph_h->graph_u_f.vertex_ID) );

  CE( cudaFree(graph_h->graph_u_b.pointer) );
  CE( cudaFree(graph_h->graph_u_b.num_neighbors) );
  CE( cudaFree(graph_h->graph_u_b.NNIEG) );
  CE( cudaFree(graph_h->graph_u_b.neighbors) );
  CE( cudaFree(graph_h->graph_u_b.vertex_ID) );
  CE( cudaFree(graph_h->graph_u_b.weights) );
  CE( cudaFree(graph_h->graph_u_b.midpoint) );

  CE( cudaFree(graph_h->graph_d_b.pointer) );
  CE( cudaFree(graph_h->graph_d_b.num_neighbors) );
  CE( cudaFree(graph_h->graph_d_b.NNIEG) );
  CE( cudaFree(graph_h->graph_d_b.neighbors) );
  CE( cudaFree(graph_h->graph_d_b.vertex_ID) );
  CE( cudaFree(graph_h->graph_d_b.weights) );
  CE( cudaFree(graph_h->graph_d_b.midpoint) );

  CE( cudaFree(graph_h->overlay_CH.pointer) );
  CE( cudaFree(graph_h->overlay_CH.num_neighbors) );
  CE( cudaFree(graph_h->overlay_CH.neighbors) );
  CE( cudaFree(graph_h->overlay_CH.weights) );
  CE( cudaFree(graph_h->overlay_CH.midpoint) );
  
  CE( cudaFree(graph_h->node_ranks) );
  CE( cudaFree(graph_h->node_levels) );
  CE( cudaFree(graph_h->node_levels_pt) );
  CE( cudaFree(graph_h->node_idx) );
  CE( cudaFree(graph_h->node_idx_inv) );

  CE( cudaFree(graph_h->overlay_APSP_path.prev) );
  CE( cudaFree(graph_h->overlay_APSP_path.dist) );
  
  CE( cudaFree(graph_d) );

  graph_d = nullptr;
}


void graph_UD_dump(FILE *f, cu_graph_CH_UD_t *graph)
{
  uint32_t i, j;

  // header
  fprintf(f, "CHFF0\n");

  fprintf(f, "%u\n", graph->graph_u_f.num_nodes);
  fprintf(f, "%u\n", graph->max_degree);
  fprintf(f, "%u\n", graph->thread_per_node);
  fprintf(f, "%u\n", graph->overlay_size);
  fprintf(f, "%u\n", graph->max_rank);

  fprintf(f, "%u\n", graph->graph_u_b.max_weight);
  fprintf(f, "%u\n", graph->graph_d_b.max_weight);
  fprintf(f, "%u\n", graph->graph_u_f.empty_pointer);
  fprintf(f, "%u\n", graph->graph_u_b.empty_pointer);
  fprintf(f, "%u\n", graph->graph_d_b.empty_pointer);

  for ( i = 0; i < graph->graph_u_f.num_nodes; i++ ){
    if(graph->node_levels[i] < NNIEG_OVER_ALLOC_CUT_OFF){
      fprintf(f, "%"FNODEID",%"FNODEID",%"FNODEID",%"
	      FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"
	      FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"FNODEID"\n",(nodeid_t)i,
	      graph->node_ranks[i],
	      graph->node_levels[i],
	      graph->node_idx[i],
	      graph->node_idx_inv[i],
	      graph->graph_u_f.num_neighbors[i],
	      graph->graph_u_f.NNIEG[i],
	      graph->graph_u_b.num_neighbors[i],
	      graph->graph_u_b.NNIEG[i],
	      graph->graph_d_b.num_neighbors[i],
	      graph->graph_d_b.NNIEG[i],
	      graph->graph_u_f.pointer[i],
	      graph->graph_u_b.pointer[i],
	      graph->graph_d_b.pointer[i]);
    } else {
      const uint32_t lvl_cut_off_pt = graph->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
      const uint32_t nnieg_pt = lvl_cut_off_pt + (i - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
      fprintf(f, "%"FNODEID",%"FNODEID",%"FNODEID",%"
	      FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"
	      FNODEID",%"FNODEID",%"FNODEID",%"FNODEID",%"FNODEID"\n",(nodeid_t)i,
	      graph->node_ranks[i],
	      graph->node_levels[i],
	      graph->node_idx[i],
	      graph->node_idx_inv[i],
	      graph->graph_u_f.num_neighbors[i],
	      graph->graph_u_f.NNIEG[nnieg_pt],
	      graph->graph_u_b.num_neighbors[i],
	      graph->graph_u_b.NNIEG[nnieg_pt],
	      graph->graph_d_b.num_neighbors[i],
	      graph->graph_d_b.NNIEG[nnieg_pt],
	      graph->graph_u_f.pointer[i],
	      graph->graph_u_b.pointer[i],
	      graph->graph_d_b.pointer[i]);
      for(uint32_t j=1; j < NNIEG_OVER_ALLOC_RATIO; j++){
	fprintf(f, "%"FNODEID",%"FNODEID",%"FNODEID"\n",
		graph->graph_u_f.NNIEG[nnieg_pt + j],
		graph->graph_u_b.NNIEG[nnieg_pt + j],
		graph->graph_d_b.NNIEG[nnieg_pt + j]);
      }
    }

    for(j=0; j<graph->graph_u_f.num_neighbors[i]; j++){
      fprintf(f, "%u:%"FNODEID",%"FNODEID"\n", j,
	      graph->graph_u_f.neighbors[graph->graph_u_f.pointer[i] + j],
	      graph->graph_u_f.vertex_ID[graph->graph_u_f.pointer[i] + j]);
    }
    for(j=0; j<graph->graph_u_b.num_neighbors[i]; j++){
      fprintf(f, "%u:%"FNODEID",%"FNODEID",%"FWEIGHT",%"FNODEID"\n", j, 
	      graph->graph_u_b.neighbors[graph->graph_u_b.pointer[i] + j],
	      graph->graph_u_b.vertex_ID[graph->graph_u_b.pointer[i] + j],
	      graph->graph_u_b.weights[graph->graph_u_b.pointer[i] + j],
	      graph->graph_u_b.midpoint[graph->graph_u_b.pointer[i] + j]);
    }
    for(j=0; j<graph->graph_d_b.num_neighbors[i]; j++){
      fprintf(f, "%u:%"FNODEID",%"FNODEID",%"FWEIGHT",%"FNODEID"\n", j, 
	      graph->graph_d_b.neighbors[graph->graph_d_b.pointer[i] + j],
	      graph->graph_d_b.vertex_ID[graph->graph_d_b.pointer[i] + j],
	      graph->graph_d_b.weights[graph->graph_d_b.pointer[i] + j],
	      graph->graph_d_b.midpoint[graph->graph_d_b.pointer[i] + j]);
    }
  }

  // now the overlay data
  fprintf(f, "OLG%"FNODEID",%"FNODEID",%"FNODEID"\n", graph->overlay_CH.num_nodes, graph->overlay_CH.max_weight, graph->overlay_CH.empty_pointer);
  for ( i = 0; i < graph->overlay_CH.num_nodes; i++ ){
    fprintf(f, "%"FNODEID",%"FNODEID",%"FNODEID"\n",(nodeid_t)i,
	    graph->overlay_CH.num_neighbors[i], graph->overlay_CH.pointer[i]);
    for(j=0; j<graph->overlay_CH.num_neighbors[i]; j++){
      fprintf(f, "%u:%"FNODEID",%"FWEIGHT",%"FNODEID"\n", j, 
	      graph->overlay_CH.neighbors[graph->overlay_CH.pointer[i] + j],
	      graph->overlay_CH.weights[graph->overlay_CH.pointer[i] + j],
	      graph->overlay_CH.midpoint[graph->overlay_CH.pointer[i] + j]);
    }
  }
  const nodeid_t nnodes_K = graph->overlay_APSP_path.num_nodes;
  fprintf(f, "OLP%"FNODEID"\n", nnodes_K);
  for (size_t i=0; i<nnodes_K; i++){
    for(size_t j=0; j<nnodes_K; j++){
      if ( graph->overlay_APSP_path.prev[i*nnodes_K + j] == NODEID_NULL ){
	fprintf(f, "-:%"FNODEID"->%"FNODEID",%"FWEIGHT"\n", (nodeid_t)i,
		(nodeid_t)j, graph->overlay_APSP_path.dist[i*nnodes_K + j]);
      } else {
	fprintf(f, "%"FNODEID":%"FNODEID"->%"FNODEID",%"FWEIGHT"\n",
		graph->overlay_APSP_path.prev[i*nnodes_K + j],
		(nodeid_t)i, (nodeid_t)j,
		graph->overlay_APSP_path.dist[i*nnodes_K + j]);
      }
    }
  }

}

void
graph_UD_finish(cu_graph_CH_UD_t *graph_h)
{
  if ( graph_h->edge_groups_in_level_u_b ) return;

  const uint n_levels = graph_h->node_levels[graph_h->graph_u_f.num_nodes-1];
  const size_t larray_sz_elts = n_levels + 2;
  const size_t larray_sz_bytes =
    larray_sz_elts * sizeof(graph_h->edge_groups_in_level_d_b[0]);

  graph_h->edge_groups_in_level_u_b = (nodeid_t *) malloc( larray_sz_bytes );
  graph_h->edge_groups_in_level_d_b = (nodeid_t *) malloc( larray_sz_bytes );

  auto nlpt = graph_h->node_levels_pt;

  for ( uint32_t i=0; i<n_levels; i++ )
    {
      graph_h->edge_groups_in_level_u_b[i] =
        ( graph_h->graph_u_b.pointer[nlpt[i+1]]
          - graph_h->graph_u_b.pointer[nlpt[i]] ) >> 10;
      graph_h->edge_groups_in_level_d_b[i] =
        ( graph_h->graph_d_b.pointer[nlpt[i+1]]
          - graph_h->graph_d_b.pointer[nlpt[i]] ) >> 10;
    }

  graph_h->edge_groups_in_level_u_b[n_levels] =
    ( graph_h->graph_u_b.empty_pointer
      - graph_h->graph_u_b.pointer[ nlpt[n_levels] ] ) >> 10;
  graph_h->edge_groups_in_level_d_b[n_levels] =
    ( graph_h->graph_d_b.empty_pointer
      - graph_h->graph_d_b.pointer[ nlpt[n_levels] ] ) >> 10;

  graph_h->edge_groups_in_level_u_b[n_levels+1] = 0;
  graph_h->edge_groups_in_level_d_b[n_levels+1] = 0;
}

int graph_UD_get_from_file(cu_graph_CH_UD_t **graph, FILE *f)
{

  uint32_t i, j;

  cu_graph_CH_UD_t *graph_out = // Make sure unused pointers set to null.
    (cu_graph_CH_UD_t *) calloc(1,sizeof(cu_graph_CH_UD_t));
  ASSERTA(fscanf(f, "%u\n", &graph_out->graph_u_f.num_nodes)==1);
  graph_out->graph_u_b.num_nodes = graph_out->graph_u_f.num_nodes;
  graph_out->graph_d_b.num_nodes = graph_out->graph_u_f.num_nodes;
  ASSERTA(fscanf(f, "%u\n", &graph_out->max_degree)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->thread_per_node)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->overlay_size)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->max_rank)==1);

  ASSERTA(fscanf(f, "%u\n", &graph_out->graph_u_b.max_weight)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->graph_d_b.max_weight)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->graph_u_f.empty_pointer)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->graph_u_b.empty_pointer)==1);
  ASSERTA(fscanf(f, "%u\n", &graph_out->graph_d_b.empty_pointer)==1);

  // no need to have a scratch pad anymore
  graph_out->graph_u_f.max_num_edges = graph_out->graph_u_f.empty_pointer;
  graph_out->graph_u_b.max_num_edges = graph_out->graph_u_b.empty_pointer;
  graph_out->graph_d_b.max_num_edges = graph_out->graph_d_b.empty_pointer;

  graph_out->node_ranks = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->node_levels = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->node_idx = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->node_idx_inv = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  
  graph_out->graph_u_f.num_neighbors = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  // RRRR: it is possible to know the exact amount of nnieg over_alloc ... maybe some day!
  graph_out->graph_u_f.NNIEG = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*NNIEG_OVER_ALLOC*sizeof(nodeid_t));
  graph_out->graph_u_f.pointer = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_f.neighbors = (nodeid_t *) malloc(graph_out->graph_u_f.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_u_f.vertex_ID = (nodeid_t *) malloc(graph_out->graph_u_f.max_num_edges*sizeof(nodeid_t));

  graph_out->graph_u_b.num_neighbors = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.NNIEG = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*NNIEG_OVER_ALLOC*sizeof(nodeid_t));
  graph_out->graph_u_b.pointer = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->graph_u_b.neighbors = (nodeid_t *) malloc(graph_out->graph_u_b.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_u_b.vertex_ID = (nodeid_t *) malloc(graph_out->graph_u_b.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_u_b.weights = (nodeid_t *) malloc(graph_out->graph_u_b.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_u_b.midpoint = (nodeid_t *) malloc(graph_out->graph_u_b.max_num_edges*sizeof(nodeid_t));
  for(uint32_t i=0; i<graph_out->graph_u_b.max_num_edges; i++){
    graph_out->graph_u_b.weights[i] = NPP_MAX_32U;
  }
 
  graph_out->graph_d_b.num_neighbors = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.NNIEG = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*NNIEG_OVER_ALLOC*sizeof(nodeid_t));
  graph_out->graph_d_b.pointer = (nodeid_t *) malloc(graph_out->graph_u_f.num_nodes*sizeof(nodeid_t));
  graph_out->graph_d_b.neighbors = (nodeid_t *) malloc(graph_out->graph_d_b.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_d_b.vertex_ID = (nodeid_t *) malloc(graph_out->graph_d_b.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_d_b.weights = (nodeid_t *) malloc(graph_out->graph_d_b.max_num_edges*sizeof(nodeid_t));
  graph_out->graph_d_b.midpoint = (nodeid_t *) malloc(graph_out->graph_d_b.max_num_edges*sizeof(nodeid_t));
  for(uint32_t i=0; i<graph_out->graph_d_b.max_num_edges; i++){
    graph_out->graph_d_b.weights[i] = NPP_MAX_32U;
  }

  graph_out->node_levels_pt = (nodeid_t *) calloc(graph_out->max_rank + 1, sizeof(nodeid_t));
  graph_out->node_levels_pt[graph_out->max_rank] = graph_out->graph_u_f.num_nodes;

  uint32_t lvl_iter = 0;
  graph_out->node_levels_pt[0] = 0;

  for ( i = 0; i < graph_out->graph_u_f.num_nodes; i++ ){
    nodeid_t nodeid, rank, lvl, idx, idx_i, pt_u_f, pt_u_b, pt_d_b, vtx_id;
    nodeid_t nn_u_f, nn_u_b, nn_d_b;
    nodeid_t nieg_u_f[NNIEG_OVER_ALLOC_RATIO];
    nodeid_t nieg_u_b[NNIEG_OVER_ALLOC_RATIO];
    nodeid_t nieg_d_b[NNIEG_OVER_ALLOC_RATIO];
 
    ASSERTA(!feof(f));

    ASSERTA(fscanf(f, "%"SNODEID",%"SNODEID",%"SNODEID",%"
		   SNODEID",%"SNODEID",%"SNODEID",%"SNODEID",%"SNODEID",%"SNODEID",%"
		   SNODEID",%"SNODEID",%"SNODEID",%"SNODEID",%"SNODEID"\n", &nodeid,
		   &rank, &lvl, &idx, &idx_i, &nn_u_f, &nieg_u_f[0],
		   &nn_u_b, &nieg_u_b[0], &nn_d_b, &nieg_d_b[0], &pt_u_f, &pt_u_b, &pt_d_b) == 14);

    if(lvl >= NNIEG_OVER_ALLOC_CUT_OFF){
      for( j=1; j < NNIEG_OVER_ALLOC_RATIO; j++ ){
	ASSERTA(fscanf(f, "%"SNODEID",%"SNODEID",%"SNODEID"\n",
		       &nieg_u_f[j], &nieg_u_b[j], &nieg_d_b[j]) == 3);
      }
    }

    ASSERTA(i == nodeid);

    if(lvl > lvl_iter){
      lvl_iter++;
      ASSERTA(lvl_iter == lvl);
      graph_out->node_levels_pt[lvl] = nodeid;
    }

    graph_out->node_ranks[nodeid] = rank;
    graph_out->node_levels[nodeid] = lvl;
    graph_out->node_idx[nodeid] = idx;
    graph_out->node_idx_inv[nodeid] = idx_i;
    graph_out->graph_u_f.num_neighbors[nodeid] = nn_u_f;
    graph_out->graph_u_b.num_neighbors[nodeid] = nn_u_b;
    graph_out->graph_d_b.num_neighbors[nodeid] = nn_d_b;
    graph_out->graph_u_f.pointer[nodeid] = pt_u_f;
    graph_out->graph_u_b.pointer[nodeid] = pt_u_b;
    graph_out->graph_d_b.pointer[nodeid] = pt_d_b;

    if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
      graph_out->graph_u_f.NNIEG[nodeid] = nieg_u_f[0];
      graph_out->graph_u_b.NNIEG[nodeid] = nieg_u_b[0];
      graph_out->graph_d_b.NNIEG[nodeid] = nieg_d_b[0];
    } else {
      const uint32_t lvl_cut_off_pt = graph_out->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
      const uint32_t nnieg_pt = lvl_cut_off_pt + (nodeid - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
      for(j=0; j < NNIEG_OVER_ALLOC_RATIO; j++){
	graph_out->graph_u_f.NNIEG[nnieg_pt + j] = nieg_u_f[j];
	graph_out->graph_u_b.NNIEG[nnieg_pt + j] = nieg_u_b[j];
	graph_out->graph_d_b.NNIEG[nnieg_pt + j] = nieg_d_b[j];
      }
    }

    for(j=0; j<nn_u_f; j++){
      nodeid_t nborid, nbor;
      ASSERTA(fscanf(f, "%"SNODEID":%"SNODEID",%"SNODEID"\n", &nborid, &nbor, &vtx_id) == 3);
      graph_out->graph_u_f.neighbors[pt_u_f + nborid] = nbor;
      graph_out->graph_u_f.vertex_ID[pt_u_f + nborid] = vtx_id;
    }

    for(j=0; j<nn_u_b; j++){
      nodeid_t nborid, nbor, midpnt;
      weight_t weight;
      ASSERTA(fscanf(f, "%"SNODEID":%"SNODEID",%"SNODEID",%"SWEIGHT",%"SNODEID"\n", &nborid, &nbor, &vtx_id, &weight, &midpnt) == 5);
      graph_out->graph_u_b.neighbors[pt_u_b + nborid] = nbor;
      graph_out->graph_u_b.vertex_ID[pt_u_b + nborid] = vtx_id;
      graph_out->graph_u_b.weights[pt_u_b + nborid] = weight;
      graph_out->graph_u_b.midpoint[pt_u_b + nborid] = midpnt;
    }

    for(j=0; j<nn_d_b; j++){
      nodeid_t nborid, nbor, midpnt;
      weight_t weight;
      ASSERTA(fscanf(f, "%"SNODEID":%"SNODEID",%"SNODEID",%"SWEIGHT",%"SNODEID"\n", &nborid, &nbor, &vtx_id, &weight, &midpnt) == 5);
      graph_out->graph_d_b.neighbors[pt_d_b + nborid] = nbor;
      graph_out->graph_d_b.vertex_ID[pt_d_b + nborid] = vtx_id;
      graph_out->graph_d_b.weights[pt_d_b + nborid] = weight;
      graph_out->graph_d_b.midpoint[pt_d_b + nborid] = midpnt;
    }
  }

  ASSERTA(graph_out->max_rank == graph_out->node_levels[graph_out->graph_u_f.num_nodes-1] + 1);
  
  // load OLG
  ASSERTA(fscanf(f, "OLG%"SNODEID",%"SNODEID",%"SNODEID"\n", &graph_out->overlay_CH.num_nodes, &graph_out->overlay_CH.max_weight, &graph_out->overlay_CH.empty_pointer)==3);

  // no need to have a scratch pad anymore
  graph_out->overlay_CH.max_num_edges = graph_out->overlay_CH.empty_pointer;

  graph_out->overlay_CH.num_neighbors = (nodeid_t *) malloc(graph_out->overlay_CH.num_nodes*sizeof(nodeid_t));
  graph_out->overlay_CH.pointer = (nodeid_t *) malloc(graph_out->overlay_CH.num_nodes*sizeof(nodeid_t));
  graph_out->overlay_CH.neighbors = (nodeid_t *) malloc(graph_out->overlay_CH.max_num_edges*sizeof(nodeid_t));
  graph_out->overlay_CH.weights = (nodeid_t *) malloc(graph_out->overlay_CH.max_num_edges*sizeof(nodeid_t));
  graph_out->overlay_CH.midpoint = (nodeid_t *) malloc(graph_out->overlay_CH.max_num_edges*sizeof(nodeid_t));

  for (uint32_t i = 0; i < graph_out->overlay_CH.num_nodes; i++ ){
    nodeid_t nodeid, pt_ol;
    nodeid_t nn_ol;

    ASSERTA(!feof(f));

    ASSERTA(fscanf(f, "%"SNODEID",%"SNODEID",%"SNODEID"\n", &nodeid, &nn_ol, &pt_ol) == 3);
    graph_out->overlay_CH.num_neighbors[nodeid] = nn_ol;
    graph_out->overlay_CH.pointer[nodeid] = pt_ol;
    for(j=0; j<nn_ol; j++){
      nodeid_t nborid, nbor, midpnt;
      weight_t weight;
      ASSERTA(fscanf(f, "%"SNODEID":%"SNODEID",%"SWEIGHT",%"SNODEID"\n", &nborid, &nbor, &weight, &midpnt) == 4);
      graph_out->overlay_CH.neighbors[pt_ol + nborid] = nbor;
      graph_out->overlay_CH.weights[pt_ol + nborid] = weight;
      graph_out->overlay_CH.midpoint[pt_ol + nborid] = midpnt;
    }
  }
    
  // load OLP
  nodeid_t nnodes_K;
  ASSERTA(fscanf(f, "OLP%"SNODEID"\n", &nnodes_K)==1);
  graph_out->overlay_APSP_path.prev = (nodeid_t *)malloc(nnodes_K * nnodes_K * sizeof(nodeid_t));
  graph_out->overlay_APSP_path.dist = (nodeid_t *)malloc(nnodes_K * nnodes_K * sizeof(nodeid_t));
  graph_out->overlay_APSP_path.num_nodes = nnodes_K;

  char instr[256];
  for (size_t i = 0; i < nnodes_K * nnodes_K; i++){
    nodeid_t ref, node_i, node_j;
    weight_t weight;

    ASSERTA(!feof(f));

    ASSERTA(fgets(instr, sizeof(instr), f) != NULL);
    if (instr[0] == '-'){
      sscanf(instr, "-:%"SNODEID"->%"SNODEID",%"SWEIGHT, &node_i, &node_j, &weight);
      graph_out->overlay_APSP_path.prev[node_i*nnodes_K + node_j] = NODEID_NULL;
      graph_out->overlay_APSP_path.dist[node_i*nnodes_K + node_j] = weight;
    }else {
      sscanf(instr, "%"SNODEID":%"SNODEID"->%"SNODEID",%"SWEIGHT, &ref, &node_i, &node_j, &weight);
      graph_out->overlay_APSP_path.prev[node_i*nnodes_K + node_j] = ref;
      graph_out->overlay_APSP_path.dist[node_i*nnodes_K + node_j] = weight;
    }
  }

  *graph = graph_out;

  return 0;

}


void graph_UD_bin_dump(FILE *f, cu_graph_CH_UD_t *graph)
{

  // unlike text dump, this maintains array layout to avoid having to rebuild when reading in

  const nodeid_t nnodes = graph->num_nodes;
  const nodeid_t nnodes_OL = graph->overlay_CH.num_nodes;
  const nodeid_t nnodes_K2 = (graph->overlay_APSP_path.num_nodes)*(graph->overlay_APSP_path.num_nodes);
  const nodeid_t mlvl = graph->max_rank;
  // no need to have a scratch pad anymore
  const nodeid_t nedge_uf = graph->graph_u_f.empty_pointer;
  const nodeid_t nedge_ub = graph->graph_u_b.empty_pointer;
  const nodeid_t nedge_db = graph->graph_d_b.empty_pointer;
  const nodeid_t nedge_OL = graph->overlay_CH.empty_pointer;

  // header
  fprintf(f, "CHFF1\n");

  // write main UD struct
  fwrite(graph, sizeof(*graph), 1, f);

  // write arrays from UD struct
  fwrite(graph->node_ranks, sizeof(graph->node_ranks[0]), nnodes, f);
  fwrite(graph->node_levels, sizeof(graph->node_levels[0]), nnodes, f);
  fwrite(graph->node_idx, sizeof(graph->node_idx[0]), nnodes, f);
  fwrite(graph->node_idx_inv, sizeof(graph->node_idx_inv[0]), nnodes, f);

  fwrite(graph->node_levels_pt, sizeof(graph->node_levels_pt[0]), mlvl+1, f);

  // write arrays from graph_u_f
  fwrite(graph->graph_u_f.pointer, sizeof(graph->graph_u_f.pointer[0]), nnodes, f);
  fwrite(graph->graph_u_f.num_neighbors, sizeof(graph->graph_u_f.num_neighbors[0]), nnodes, f);
  // RRRR NNIEG might not be necessary for graph_u_f, will need to check. for now keeping it.
  fwrite(graph->graph_u_f.NNIEG, sizeof(graph->graph_u_f.NNIEG[0]), nnodes*NNIEG_OVER_ALLOC, f);

  fwrite(graph->graph_u_f.neighbors, sizeof(graph->graph_u_f.neighbors[0]), nedge_uf, f);
  fwrite(graph->graph_u_f.vertex_ID, sizeof(graph->graph_u_f.vertex_ID[0]), nedge_uf, f);

  // write arrays from graph_u_b
  fwrite(graph->graph_u_b.pointer, sizeof(graph->graph_u_b.pointer[0]), nnodes, f);
  fwrite(graph->graph_u_b.num_neighbors, sizeof(graph->graph_u_b.num_neighbors[0]), nnodes, f);
  fwrite(graph->graph_u_b.NNIEG, sizeof(graph->graph_u_b.NNIEG[0]), nnodes*NNIEG_OVER_ALLOC, f);

  fwrite(graph->graph_u_b.neighbors, sizeof(graph->graph_u_b.neighbors[0]), nedge_ub, f);
  fwrite(graph->graph_u_b.vertex_ID, sizeof(graph->graph_u_b.vertex_ID[0]), nedge_ub, f);
  fwrite(graph->graph_u_b.weights, sizeof(graph->graph_u_b.weights[0]), nedge_ub, f);
  fwrite(graph->graph_u_b.midpoint, sizeof(graph->graph_u_b.midpoint[0]), nedge_ub, f);

  // write arrays from graph_d_b
  fwrite(graph->graph_d_b.pointer, sizeof(graph->graph_d_b.pointer[0]), nnodes, f);
  fwrite(graph->graph_d_b.num_neighbors, sizeof(graph->graph_d_b.num_neighbors[0]), nnodes, f);
  fwrite(graph->graph_d_b.NNIEG, sizeof(graph->graph_d_b.NNIEG[0]), nnodes*NNIEG_OVER_ALLOC, f);

  fwrite(graph->graph_d_b.neighbors, sizeof(graph->graph_d_b.neighbors[0]), nedge_db, f);
  fwrite(graph->graph_d_b.vertex_ID, sizeof(graph->graph_d_b.vertex_ID[0]), nedge_db, f);
  fwrite(graph->graph_d_b.weights, sizeof(graph->graph_d_b.weights[0]), nedge_db, f);
  fwrite(graph->graph_d_b.midpoint, sizeof(graph->graph_d_b.midpoint[0]), nedge_db, f);

  // write arrays from overlay_CH
  fwrite(graph->overlay_CH.pointer, sizeof(graph->overlay_CH.pointer[0]), nnodes_OL, f);
  fwrite(graph->overlay_CH.num_neighbors, sizeof(graph->overlay_CH.num_neighbors[0]), nnodes_OL, f);

  fwrite(graph->overlay_CH.neighbors, sizeof(graph->overlay_CH.neighbors[0]), nedge_OL, f);
  fwrite(graph->overlay_CH.weights, sizeof(graph->overlay_CH.weights[0]), nedge_OL, f);
  fwrite(graph->overlay_CH.midpoint, sizeof(graph->overlay_CH.midpoint[0]), nedge_OL, f);

  // write arrays from overlay_APSP_path
  fwrite(graph->overlay_APSP_path.prev, sizeof(graph->overlay_APSP_path.prev[0]), nnodes_K2, f);
  fwrite(graph->overlay_APSP_path.dist, sizeof(graph->overlay_APSP_path.dist[0]), nnodes_K2, f);

}


int graph_UD_bin_get_from_file(cu_graph_CH_UD_t **graph, FILE *f)
{

  // read main UD struct
  cu_graph_CH_UD_t *graph_out = (cu_graph_CH_UD_t *) malloc(sizeof(cu_graph_CH_UD_t));
  fread(graph_out, sizeof(*graph_out), 1, f);

  const nodeid_t nnodes = graph_out->num_nodes;
  const nodeid_t nnodes_OL = graph_out->overlay_CH.num_nodes;
  const nodeid_t nnodes_K2 = (graph_out->overlay_APSP_path.num_nodes)*(graph_out->overlay_APSP_path.num_nodes);
  const nodeid_t mlvl = graph_out->max_rank;
  // no need to have a scratch pad anymore
  const nodeid_t nedge_uf = graph_out->graph_u_f.max_num_edges = graph_out->graph_u_f.empty_pointer;
  const nodeid_t nedge_ub = graph_out->graph_u_b.max_num_edges = graph_out->graph_u_b.empty_pointer;
  const nodeid_t nedge_db = graph_out->graph_d_b.max_num_edges = graph_out->graph_d_b.empty_pointer;
  const nodeid_t nedge_OL = graph_out->overlay_CH.max_num_edges = graph_out->overlay_CH.empty_pointer;

  // read arrays from UD struct
  graph_out->node_ranks = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->node_levels = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->node_idx = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->node_idx_inv = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  fread(graph_out->node_ranks, sizeof(graph_out->node_ranks[0]), nnodes, f);
  fread(graph_out->node_levels, sizeof(graph_out->node_levels[0]), nnodes, f);
  fread(graph_out->node_idx, sizeof(graph_out->node_idx[0]), nnodes, f);
  fread(graph_out->node_idx_inv, sizeof(graph_out->node_idx_inv[0]), nnodes, f);

  graph_out->node_levels_pt = (nodeid_t *) malloc((mlvl+1)*sizeof(nodeid_t));
  graph_out->edge_groups_in_level_u_b = nullptr;
  graph_out->edge_groups_in_level_d_b = nullptr;
  fread(graph_out->node_levels_pt, sizeof(graph_out->node_levels_pt[0]), mlvl+1, f);

  // write arrays from graph_u_f
  graph_out->graph_u_f.pointer = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->graph_u_f.num_neighbors = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->graph_u_f.NNIEG = (nodeid_t *) malloc(nnodes*NNIEG_OVER_ALLOC*sizeof(nodeid_t));
  fread(graph_out->graph_u_f.pointer, sizeof(graph_out->graph_u_f.pointer[0]), nnodes, f);
  fread(graph_out->graph_u_f.num_neighbors, sizeof(graph_out->graph_u_f.num_neighbors[0]), nnodes, f);
  // RRRR NNIEG might not be necessary for graph_u_f, will need to check. for now keeping it.
  fread(graph_out->graph_u_f.NNIEG, sizeof(graph_out->graph_u_f.NNIEG[0]), nnodes*NNIEG_OVER_ALLOC, f);

  graph_out->graph_u_f.neighbors = (nodeid_t *) malloc(nedge_uf*sizeof(nodeid_t));
  graph_out->graph_u_f.vertex_ID = (nodeid_t *) malloc(nedge_uf*sizeof(nodeid_t));
  fread(graph_out->graph_u_f.neighbors, sizeof(graph_out->graph_u_f.neighbors[0]), nedge_uf, f);
  fread(graph_out->graph_u_f.vertex_ID, sizeof(graph_out->graph_u_f.vertex_ID[0]), nedge_uf, f);

  // write arrays from graph_u_b
  graph_out->graph_u_b.pointer = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->graph_u_b.num_neighbors = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->graph_u_b.NNIEG = (nodeid_t *) malloc(nnodes*NNIEG_OVER_ALLOC*sizeof(nodeid_t));
  fread(graph_out->graph_u_b.pointer, sizeof(graph_out->graph_u_b.pointer[0]), nnodes, f);
  fread(graph_out->graph_u_b.num_neighbors, sizeof(graph_out->graph_u_b.num_neighbors[0]), nnodes, f);
  fread(graph_out->graph_u_b.NNIEG, sizeof(graph_out->graph_u_b.NNIEG[0]), nnodes*NNIEG_OVER_ALLOC, f);

  graph_out->graph_u_b.neighbors = (nodeid_t *) malloc(nedge_ub*sizeof(nodeid_t));
  graph_out->graph_u_b.vertex_ID = (nodeid_t *) malloc(nedge_ub*sizeof(nodeid_t));
  graph_out->graph_u_b.weights = (nodeid_t *) malloc(nedge_ub*sizeof(nodeid_t));
  graph_out->graph_u_b.midpoint = (nodeid_t *) malloc(nedge_ub*sizeof(nodeid_t));
  fread(graph_out->graph_u_b.neighbors, sizeof(graph_out->graph_u_b.neighbors[0]), nedge_ub, f);
  fread(graph_out->graph_u_b.vertex_ID, sizeof(graph_out->graph_u_b.vertex_ID[0]), nedge_ub, f);
  fread(graph_out->graph_u_b.weights, sizeof(graph_out->graph_u_b.weights[0]), nedge_ub, f);
  fread(graph_out->graph_u_b.midpoint, sizeof(graph_out->graph_u_b.midpoint[0]), nedge_ub, f);

  // write arrays from graph_d_b
  graph_out->graph_d_b.pointer = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->graph_d_b.num_neighbors = (nodeid_t *) malloc(nnodes*sizeof(nodeid_t));
  graph_out->graph_d_b.NNIEG = (nodeid_t *) malloc(nnodes*NNIEG_OVER_ALLOC*sizeof(nodeid_t));
  fread(graph_out->graph_d_b.pointer, sizeof(graph_out->graph_d_b.pointer[0]), nnodes, f);
  fread(graph_out->graph_d_b.num_neighbors, sizeof(graph_out->graph_d_b.num_neighbors[0]), nnodes, f);
  fread(graph_out->graph_d_b.NNIEG, sizeof(graph_out->graph_d_b.NNIEG[0]), nnodes*NNIEG_OVER_ALLOC, f);

  graph_out->graph_d_b.neighbors = (nodeid_t *) malloc(nedge_db*sizeof(nodeid_t));
  graph_out->graph_d_b.vertex_ID = (nodeid_t *) malloc(nedge_db*sizeof(nodeid_t));
  graph_out->graph_d_b.weights = (nodeid_t *) malloc(nedge_db*sizeof(nodeid_t));
  graph_out->graph_d_b.midpoint = (nodeid_t *) malloc(nedge_db*sizeof(nodeid_t));
  fread(graph_out->graph_d_b.neighbors, sizeof(graph_out->graph_d_b.neighbors[0]), nedge_db, f);
  fread(graph_out->graph_d_b.vertex_ID, sizeof(graph_out->graph_d_b.vertex_ID[0]), nedge_db, f);
  fread(graph_out->graph_d_b.weights, sizeof(graph_out->graph_d_b.weights[0]), nedge_db, f);
  fread(graph_out->graph_d_b.midpoint, sizeof(graph_out->graph_d_b.midpoint[0]), nedge_db, f);

  // write arrays from overlay_CH
  graph_out->overlay_CH.pointer = (nodeid_t *) malloc(nnodes_OL*sizeof(nodeid_t));
  graph_out->overlay_CH.num_neighbors = (nodeid_t *) malloc(nnodes_OL*sizeof(nodeid_t));
  fread(graph_out->overlay_CH.pointer, sizeof(graph_out->overlay_CH.pointer[0]), nnodes_OL, f);
  fread(graph_out->overlay_CH.num_neighbors, sizeof(graph_out->overlay_CH.num_neighbors[0]), nnodes_OL, f);

  graph_out->overlay_CH.neighbors = (nodeid_t *) malloc(nedge_OL*sizeof(nodeid_t));
  graph_out->overlay_CH.weights = (nodeid_t *) malloc(nedge_OL*sizeof(nodeid_t));
  graph_out->overlay_CH.midpoint = (nodeid_t *) malloc(nedge_OL*sizeof(nodeid_t));
  fread(graph_out->overlay_CH.neighbors, sizeof(graph_out->overlay_CH.neighbors[0]), nedge_OL, f);
  fread(graph_out->overlay_CH.weights, sizeof(graph_out->overlay_CH.weights[0]), nedge_OL, f);
  fread(graph_out->overlay_CH.midpoint, sizeof(graph_out->overlay_CH.midpoint[0]), nedge_OL, f);

  // write arrays from overlay_APSP_path
  graph_out->overlay_APSP_path.prev = (nodeid_t *)malloc(nnodes_K2 * sizeof(nodeid_t));
  graph_out->overlay_APSP_path.dist = (nodeid_t *)malloc(nnodes_K2 * sizeof(nodeid_t));
  fread(graph_out->overlay_APSP_path.prev, sizeof(graph_out->overlay_APSP_path.prev[0]), nnodes_K2, f);
  fread(graph_out->overlay_APSP_path.dist, sizeof(graph_out->overlay_APSP_path.dist[0]), nnodes_K2, f);

  *graph = graph_out;

  return 0;
}


# define idxf(i) threadIdx.x + (i) * szbl

__constant__ cu_graph_CH_UD_t graph_UD_out_c, graph_UD_in_c;

__host__ void
cu_CH_UD_compress_pre_launch
(cu_graph_CH_UD_t *graph_UD_out_d, cu_graph_CH_UD_t *graph_UD_in_d)
{
  cuda_sym_dev_to_dev(graph_UD_out_c,graph_UD_out_d);
  cuda_sym_dev_to_dev(graph_UD_in_c,graph_UD_in_d);
}

// determines pointers for the edge list of each node in the compressed output DAGs
__global__ void __launch_bounds__(512,1)
cu_CH_UD_compress_0
(cu_graph_CH_UD_t* graph_UD_out_d, cu_graph_CH_UD_t* graph_UD_in_d)
{

#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif

  const cu_graph_CH_UD_t* const graph_UD_in = &graph_UD_in_c;

  constexpr uint32_t block_dim = 512;

  assert(blockDim.x==block_dim);

  const int lgN = 12;
  const int szbl = 512;
  const int elt_p_thd = 8;
  
  __shared__ uint32_t sh_pref_0[8*512 + 1];
  
  __shared__ uint32_t sh_nieg_count[512 + 32];// number of nodes in edge group
  __shared__ uint32_t sh_nieg_prev;

  __shared__ uint32_t pointer_base;
  __shared__ uint32_t sh_alloc_iter;
  __shared__ int sh_last;
  
  __shared__ uint32_t finished;
  __shared__ uint32_t pad_amount;
  __shared__ uint32_t pad_amount_total;
  __shared__ uint32_t boundary_nodes[512 + 32];
  __shared__ uint32_t edge_group_count_total;
  __shared__ uint32_t edge_group_count_iter;
  __shared__ uint32_t edge_group_count_lvl;

  __shared__ uint32_t lvl_pt[2];
  __shared__ uint32_t nnieg_pt;

  const int bidx = blockIdx.x;

  uint32_t num_nodes = graph_UD_in->graph_u_f.num_nodes;
  uint32_t num_levels = graph_UD_in->node_levels[num_nodes-1] + 1; // levels start from 0

  uint32_t nieg_max = 5*1024; // limitation imposed by share memory during query
  uint32_t nieg_count;

  if ( blockIdx.x == 0 && threadIdx.x == 0 ){
      graph_UD_out_d->graph_u_f.num_nodes = 0; // we'll use this as counter for copying the edges
      graph_UD_out_d->graph_u_b.num_nodes = num_nodes;
      graph_UD_out_d->graph_d_b.num_nodes = num_nodes;
    }

  //
  // Choose sub-graph to operate on.
  //
# define pick_tilt(ud) \
  ( bidx == 0 ? ud graph_u_f : bidx == 1 ? ud graph_u_b : ud graph_d_b )
  const cu_graph_CH_t& gi_tilt = pick_tilt(graph_UD_in_c.);
  const cu_graph_CH_t& go_tilt = pick_tilt(graph_UD_out_c.);
  cu_graph_CH_t& go_tilt_d = pick_tilt(graph_UD_out_d->);

  if ( !threadIdx.x ) {
    sh_pref_0[0] = 0;
    sh_alloc_iter = 0;
    pointer_base = 0;
  }

  for(uint32_t lvl=0; lvl<num_levels; lvl++){
    __syncthreads();

    if(threadIdx.x == 0){
      pointer_base += sh_alloc_iter;
      sh_alloc_iter = 0;
    }
      
    // load current lvl_pt as well as the next lvl_pt (to know where it finishes)
    if(lvl == num_levels-1){
      // last lvl (overlay)
      if(threadIdx.x==0){
        lvl_pt[0] = lvl_pt[1];
        lvl_pt[1] = num_nodes;
      }
    } else {
      if(threadIdx.x < 2){
        lvl_pt[threadIdx.x] = graph_UD_in->node_levels_pt[lvl + threadIdx.x];
      }
    }

    __syncthreads();
    uint32_t num_iters = ((lvl_pt[1] - lvl_pt[0])>>lgN) + 1;
        
    uint32_t my_prefix_0[elt_p_thd];
      
    for(uint32_t iter=0; iter<num_iters; iter++){
      for ( int i=0; i<elt_p_thd; i++ ){
        if(lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) < lvl_pt[1]){
          sh_pref_0[ idxf(i) + 1 ] =  my_prefix_0[i] = go_tilt.num_neighbors[lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i)] = gi_tilt.num_neighbors[lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i)];
        }else {
          // for last iteration
          sh_pref_0[ idxf(i) + 1 ] = my_prefix_0[i] = 0;
        }
      }
      __syncthreads();
      // prepare the new bases (on first iter does nothing)
      if(threadIdx.x == 0){
        pointer_base += sh_alloc_iter;
      }
	
      // prefix_sum
      for ( int i=0; i<lgN; i++ ){
        const int dist = (1 << i) - 1;
        __syncthreads();

        for ( int j = 0; j<elt_p_thd; j++ )
          if(idxf(j) >= dist){
            my_prefix_0[j] += sh_pref_0[ idxf(j) - dist ];
          }
      
        __syncthreads();

        for ( int j=0; j<elt_p_thd; j++ ){
          sh_pref_0[ idxf(j) + 1] = my_prefix_0[j];
        }
      }
      if(threadIdx.x==511){
        sh_alloc_iter = my_prefix_0[elt_p_thd-1];
      }
      __syncthreads();

      // write the result of the first prefix_sum (pointers) into global
      for ( int i=0; i<elt_p_thd; i++ ){
        if(lvl != num_levels-1){
          if(lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) < lvl_pt[1]){
            go_tilt.pointer[lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i)] = sh_pref_0[ idxf(i) ] + pointer_base;
          }
        } else {
          if(lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) < lvl_pt[1]+1){
            go_tilt.pointer[lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i)] = sh_pref_0[ idxf(i) ] + pointer_base;
          }
        }
      }
      __syncthreads();
    }
  }
  // group edge list into edge groups <=1024
  if(threadIdx.x == 0){
    pad_amount_total = 0;
    edge_group_count_total = 0;
  }
  for(uint32_t lvl=0; lvl<num_levels; lvl++){
    __syncthreads();
  
    if(threadIdx.x == 0){
      sh_last = -1;
      sh_nieg_prev = 0;
      edge_group_count_lvl = 0;
    }
  
    // load current lvl_pt as well as the next lvl_pt (to know where it finishes)
    if(lvl == num_levels-1){
      // last lvl (overlay)
      if(threadIdx.x==0){
        lvl_pt[0] = lvl_pt[1];
        lvl_pt[1] = num_nodes;
        if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
          nnieg_pt = lvl_pt[0];
        } else {
          const uint32_t lvl_cut_off_pt = graph_UD_in->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
          nnieg_pt =  lvl_cut_off_pt + (lvl_pt[0] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
        }
      }
    } else {
      if(threadIdx.x < 2){
        lvl_pt[threadIdx.x] = graph_UD_in->node_levels_pt[lvl + threadIdx.x];
      }
      if(threadIdx.x==0){
        if(lvl < NNIEG_OVER_ALLOC_CUT_OFF){
          nnieg_pt = lvl_pt[0];
        } else {
          const uint32_t lvl_cut_off_pt = graph_UD_in->node_levels_pt[NNIEG_OVER_ALLOC_CUT_OFF];
          nnieg_pt = lvl_cut_off_pt + (lvl_pt[0] - lvl_cut_off_pt) * NNIEG_OVER_ALLOC_RATIO;
        }
      }
    }

    __syncthreads();
    uint32_t num_iters = ((lvl_pt[1] - lvl_pt[0])>>lgN) + 1;
        
    for(uint32_t iter=0; iter<num_iters; iter++){
      for ( int i=0; i<elt_p_thd; i++ ){
        if(lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) < lvl_pt[1]){
          sh_pref_0[ idxf(i) + 1 ] = go_tilt.pointer[lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) + 1] + pad_amount_total;
        }else {
          // for last iteration
          if(lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) == lvl_pt[1]){
            sh_last = idxf(i);
          }
          sh_pref_0[ idxf(i) + 1 ] = 0;
        }
      }

      __syncthreads();
      for ( int i=0; i<elt_p_thd; i++ ){
        if(sh_last != -1 && sh_pref_0[ idxf(i) + 1 ] == 0){
          sh_pref_0[ idxf(i) + 1 ] = sh_pref_0[sh_last];
        }
      }
        
      __syncthreads();
      if(threadIdx.x==0){
        edge_group_count_iter = 0;
        finished = 0;
        uint32_t egc_n = edge_group_count_total + edge_group_count_iter + 1;
        if(sh_last == -1){
          nieg_count = elt_p_thd*block_dim + sh_nieg_prev;
          if( (sh_pref_0[elt_p_thd*block_dim] <= egc_n*1024) && (nieg_count <= nieg_max) ){
            finished = 1;
            // NOTE: nodes might have no neighbors, hence nieg can be >1024, CARE DURING QUERY
            sh_nieg_prev = nieg_count;
          }
        } else {
          nieg_count = sh_last + sh_nieg_prev;
          if( (sh_pref_0[elt_p_thd*block_dim] <= egc_n*1024) && (nieg_count <= nieg_max) ){
            finished = 1;
            // NOTE: nodes might have no neighbors, hence nieg can be >1024, CARE DURING QUERY
            sh_nieg_count[edge_group_count_iter] = nieg_count;
            edge_group_count_iter++;
          }
        }
      }

      __syncthreads();
      while(!finished){
        // thread corresponding to the edge_group_count'th last vertex marks itself and determines
        // pad amount needed for 4kiB alignment
        for(uint32_t i=0; i<elt_p_thd; i++){
          if(edge_group_count_iter == 0){
            nieg_count = idxf(i) + sh_nieg_prev;
          } else {
            nieg_count = idxf(i) - boundary_nodes[edge_group_count_iter - 1];
          }
          uint32_t egc_n = edge_group_count_total + edge_group_count_iter + 1;
          if( (nieg_count <= nieg_max) && (sh_pref_0[idxf(i)] <= (egc_n*1024)) && ( (sh_pref_0[idxf(i) + 1] > (egc_n*1024)) || ((nieg_count + 1) > nieg_max) ) ){
            if(nieg_count == 0){
              // case where a node has degree higher than 1024
              // determine how many edge groups are needed for the high degree node
              uint32_t nnbr = sh_pref_0[idxf(i) + 1] - sh_pref_0[idxf(i)];
              uint32_t egc_inc = (nnbr - 1) >> 10;
              // bits 30 and 31 of NIEG reserved for nodes with NNBR>1024
              // NIEG = 0x80XX0001 marks the starting edge group for the large node
              // XX is the number of edge groups for the high degree node
              // NIEG = 0x40000001 marks all the following edge groups
              // top 16 bits are 0 since max_nieg = 5*1024 < 2^16
              sh_nieg_count[edge_group_count_iter] = 0x80000001 + ((egc_inc+1)<<16);
              for(uint32_t j=0; j<egc_inc; j++){ 
                edge_group_count_iter++;
                sh_nieg_count[edge_group_count_iter] = 0x40000001;
              }
              pad_amount = ((egc_inc + 1) << 10) - nnbr;
              pad_amount_total += pad_amount;
              // this is technically not the boundary node, it's acting on behalf of it
              boundary_nodes[edge_group_count_iter] = idxf(i) + 1;
            } else {
              pad_amount = egc_n*1024 - sh_pref_0[idxf(i)];
              pad_amount_total += pad_amount;
              boundary_nodes[edge_group_count_iter] = idxf(i);
              sh_nieg_count[edge_group_count_iter] = nieg_count;
            }
          }
        }
        __syncthreads();
        // in the rare case where 4097 values need to be incremented by pad_amount
        if(edge_group_count_iter==0){
          if(threadIdx.x==0){
            if(boundary_nodes[0]==0){
              sh_pref_0[0] += pad_amount;
            }
          }
        }
        // corresponding threads apply the padding
        for(uint32_t i=0; i<elt_p_thd; i++){
          if(idxf(i) + 1 >= boundary_nodes[edge_group_count_iter]){
            sh_pref_0[idxf(i) + 1] += pad_amount;
          }
        }
        __syncthreads();
        // increment edge_group_count and determines if finished and set total allocated amount
        if(threadIdx.x == block_dim-1){
          edge_group_count_iter++;
          uint32_t egc_n = edge_group_count_total + edge_group_count_iter + 1;
          if(sh_last == -1){
            nieg_count = elt_p_thd*block_dim - boundary_nodes[edge_group_count_iter - 1];
            if( (sh_pref_0[elt_p_thd*block_dim] <= egc_n*1024) && (nieg_count <= nieg_max) ){
              finished = 1;
              // NOTE: nodes might have no neighbors, hence nieg can be >1024, CARE DURING QUERY
              sh_nieg_prev = nieg_count;
            }
          } else {
            nieg_count = sh_last - boundary_nodes[edge_group_count_iter - 1];
            if( (sh_pref_0[elt_p_thd*block_dim] <= egc_n*1024) && (nieg_count <= nieg_max) ){
              finished = 1;
              // if last node of level is multi-EG, next level already aligned, don't add another EG
              if( nieg_count != 0 ){
                // NOTE: nodes might have no neighbors, hence nieg can be >1024, CARE DURING QUERY
                sh_nieg_count[edge_group_count_iter] = nieg_count;
                edge_group_count_iter++;
              }
            }
          }
        }
        __syncthreads();

        // if NIEGs are very small, shared arrays will overflow and need to be reset
        if(edge_group_count_iter > szbl){
          go_tilt.NNIEG[nnieg_pt + edge_group_count_lvl + threadIdx.x] = sh_nieg_count[threadIdx.x];

          __syncthreads();
          if(threadIdx.x == block_dim-1){
            edge_group_count_total += szbl;
            edge_group_count_lvl += szbl;
            edge_group_count_iter -= szbl;
          }
          if(threadIdx.x < 32){
            sh_nieg_count[threadIdx.x] = sh_nieg_count[szbl + threadIdx.x];
            boundary_nodes[threadIdx.x] = boundary_nodes[szbl + threadIdx.x];
          }
        }
        __syncthreads();
      }
        
      for ( int i=0; i<elt_p_thd; i++ ){
        if(lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i) < lvl_pt[1]){
          go_tilt.pointer[lvl_pt[0] + iter*elt_p_thd*block_dim + idxf(i)] = sh_pref_0[ idxf(i) ];
        }
      }
      if(threadIdx.x < edge_group_count_iter){
        go_tilt.NNIEG[nnieg_pt + edge_group_count_lvl + threadIdx.x] = sh_nieg_count[threadIdx.x];
      }
      __syncthreads();
        
      if(threadIdx.x == block_dim-1){
        if(sh_last == -1){
          edge_group_count_total += edge_group_count_iter;
          edge_group_count_lvl += edge_group_count_iter;
          sh_pref_0[0] = sh_pref_0[elt_p_thd * block_dim];
        } else {
          edge_group_count_total += edge_group_count_iter;
          sh_pref_0[0] = edge_group_count_total * 1024;
          pad_amount_total += sh_pref_0[0] - sh_pref_0[elt_p_thd * block_dim];
        }
        if(lvl == num_levels-1){
          go_tilt_d.empty_pointer = sh_pref_0[ 0 ];
        }
      }
      __syncthreads();
    }
  }
}

__global__ void __launch_bounds__(1024)
cu_CH_UD_compress_1
(cu_graph_CH_UD_t *graph_UD_out_d, cu_graph_CH_UD_t *graph_UD_in_d)
{
#ifdef ST
  __shared__ uint32_t sync_count_sh;
  sync_count_sh = 0;
  uint32_t st_line;
#endif
  
  assert(blockDim.x == 1024);

  const cu_graph_CH_UD_t* const graph_UD_out = &graph_UD_out_c;
  const cu_graph_CH_UD_t* const graph_UD_in = &graph_UD_in_c;

  uint32_t num_thd = gridDim.x * blockDim.x;
  uint32_t tid = blockIdx.x * blockDim.x + threadIdx.x;
  
  const nodeid_t num_nodes = graph_UD_in->graph_u_f.num_nodes;
  const uint32_t num_levels = graph_UD_in->node_levels[num_nodes-1] + 1; // levels start from 0

  nodeid_t lid = threadIdx.x >> 5; // warp ID

  uint32_t tnid = threadIdx.x - (lid<<5); // thread's id within corresponding warp

  __shared__ volatile uint32_t current[32];
  
  if(tnid==0){
    current[lid] = atomicAdd(&(graph_UD_out_d->graph_u_f.num_nodes), 1);
  }
  __syncwarp();
  uint32_t current_l = current[lid];
  
  while(current_l < num_nodes){
    uint32_t nnbr = graph_UD_in->graph_u_f.num_neighbors[current_l];
    uint32_t pointer_in = graph_UD_in->graph_u_f.pointer[current_l];
    uint32_t pointer_out = graph_UD_out->graph_u_f.pointer[current_l];
    uint32_t nbr = tnid;
    while(nbr<nnbr){
      graph_UD_out->graph_u_f.neighbors[pointer_out + nbr] = graph_UD_in->graph_u_f.neighbors[pointer_in + nbr];
      graph_UD_out->graph_u_f.vertex_ID[pointer_out + nbr] = current_l;
      nbr += 32;
    }
    
    nnbr = graph_UD_in->graph_u_b.num_neighbors[current_l];
    pointer_in = graph_UD_in->graph_u_b.pointer[current_l];
    pointer_out = graph_UD_out->graph_u_b.pointer[current_l];
    nbr = tnid;
    while(nbr<nnbr){
      graph_UD_out->graph_u_b.neighbors[pointer_out + nbr] = graph_UD_in->graph_u_b.neighbors[pointer_in + nbr];
      graph_UD_out->graph_u_b.vertex_ID[pointer_out + nbr] = current_l;
      graph_UD_out->graph_u_b.weights[pointer_out + nbr] = graph_UD_in->graph_u_b.weights[pointer_in + nbr];
      graph_UD_out->graph_u_b.midpoint[pointer_out + nbr] = graph_UD_in->graph_u_b.midpoint[pointer_in + nbr];
      nbr += 32;
    }

    nnbr = graph_UD_in->graph_d_b.num_neighbors[current_l];
    pointer_in = graph_UD_in->graph_d_b.pointer[current_l];
    pointer_out = graph_UD_out->graph_d_b.pointer[current_l];
    nbr = tnid;
    while(nbr<nnbr){
      graph_UD_out->graph_d_b.neighbors[pointer_out + nbr] = graph_UD_in->graph_d_b.neighbors[pointer_in + nbr];
      graph_UD_out->graph_d_b.vertex_ID[pointer_out + nbr] = current_l;
      graph_UD_out->graph_d_b.weights[pointer_out + nbr] = graph_UD_in->graph_d_b.weights[pointer_in + nbr];
      graph_UD_out->graph_d_b.midpoint[pointer_out + nbr] = graph_UD_in->graph_d_b.midpoint[pointer_in + nbr];
      nbr += 32;
    }
    __syncwarp();
    if(tnid==0){
      current[lid] = atomicAdd(&(graph_UD_out_d->graph_u_f.num_nodes), 1);
    }
    __syncwarp();
    current_l = current[lid];
    
  }

  if(tnid==0){
    graph_UD_out_d->graph_u_f.num_nodes = num_nodes;// make sure num_nodes is correct at the end
  }

  // copy everything else
  uint32_t OL_num_edges = graph_UD_in->overlay_CH.empty_pointer;
  uint32_t OL_num_nodes = graph_UD_in->overlay_CH.num_nodes;
  uint32_t APSP_nnodes = graph_UD_in->overlay_APSP_path.num_nodes;
  uint32_t APSP_dim = APSP_nnodes * APSP_nnodes;
  if(tid == 0){
    graph_UD_out_d->overlay_CH.num_nodes = OL_num_nodes;
    graph_UD_out_d->overlay_CH.empty_pointer = OL_num_edges;
    graph_UD_out_d->overlay_size = OL_num_nodes;
  }
  
  uint32_t num_el = max(max(OL_num_edges, num_nodes), APSP_dim);
  current_l = tid;

  while(current_l < num_el){
    if(current_l < num_levels){
      graph_UD_out->node_levels_pt[current_l] = graph_UD_in->node_levels_pt[current_l];
    }

    if(current_l < APSP_dim){
      // make sure that the prev in APSP results point to real edges
      // the algorithm does not guarantee this so this extra step is needed
      /// RRRR with variable K, can't use masks, must use remainder
      uint32_t olid_col = current_l % APSP_nnodes;
      uint32_t prev_OL = graph_UD_in->overlay_APSP_path.prev[current_l];
      if(prev_OL != NPP_MAX_32U){
	uint32_t prev_new = graph_UD_in->overlay_APSP_path.prev[prev_OL*APSP_nnodes + olid_col];
	while(prev_OL != prev_new){
	  prev_OL = prev_new;
	  assert(prev_new != NPP_MAX_32U);
	  prev_new = graph_UD_in->overlay_APSP_path.prev[prev_new*APSP_nnodes + olid_col];
	}
      }
      
      graph_UD_out->overlay_APSP_path.prev[current_l] = prev_OL;
      graph_UD_out->overlay_APSP_path.dist[current_l] = graph_UD_in->overlay_APSP_path.dist[current_l];
    }
    
    if(current_l < num_nodes){
      graph_UD_out->node_ranks[current_l] = graph_UD_in->node_ranks[current_l];
      graph_UD_out->node_levels[current_l] = graph_UD_in->node_levels[current_l];
      graph_UD_out->node_idx[current_l] = graph_UD_in->node_idx[current_l];
      graph_UD_out->node_idx_inv[current_l] = graph_UD_in->node_idx_inv[current_l];
    }

    if(current_l < OL_num_edges){
      graph_UD_out->overlay_CH.neighbors[current_l] = graph_UD_in->overlay_CH.neighbors[current_l];
      graph_UD_out->overlay_CH.weights[current_l] = graph_UD_in->overlay_CH.weights[current_l];
      graph_UD_out->overlay_CH.midpoint[current_l] = graph_UD_in->overlay_CH.midpoint[current_l];
    }
    
    if(current_l < OL_num_nodes){
      graph_UD_out->overlay_CH.pointer[current_l] = graph_UD_in->overlay_CH.pointer[current_l];
      graph_UD_out->overlay_CH.num_neighbors[current_l] = graph_UD_in->overlay_CH.num_neighbors[current_l];
    }
    
    current_l += num_thd;
  }
  

}




// instantiate the templates

#define INST(tpn)                                                       \
  template __global__ void cu_CH_select_nodes_gl<tpn>                   \
  (nodeid_t *selected_list,                                             \
   nodeid_t *iscore_in, nodeid_t *iscore_out, nodeid_t *nid);
INST(3); INST(4); INST(5); INST(6); INST(7); INST(8); INST(9); INST(10); INST(11); INST(12);
#undef INST

#define INST(tpn)							\
  template __global__ void cu_CH_APSP_prep<tpn>				\
  (cu_graph_CH_UD_t *graph_UD, cu_graph_CH_bi_t *graph);
INST(3); INST(4); INST(5); INST(6); INST(7); INST(8); INST(9); INST(10); INST(11); INST(12);
#undef INST
